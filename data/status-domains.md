# État de la traduction (domains)

 * **libre**: 33
 * **changé**: 25


Dernière mise à jour: 2022-07-05 07:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1aoUqGYDrdpnPWio.htm](domains/1aoUqGYDrdpnPWio.htm)|Family Domain|Famille|changé|
|[1BU8deh48XZFclWl.htm](domains/1BU8deh48XZFclWl.htm)|Healing Domain|Guérison|changé|
|[8pPvbTMZLIsvCwQk.htm](domains/8pPvbTMZLIsvCwQk.htm)|Darkness Domain|Ténèbres|changé|
|[a0fe0kFowMMwUFZa.htm](domains/a0fe0kFowMMwUFZa.htm)|Nightmares Domain|Cauchemars|changé|
|[c9odhpRoKId5dXmn.htm](domains/c9odhpRoKId5dXmn.htm)|Perfection Domain|Perfection|changé|
|[giUsAWI9NbpdeUzl.htm](domains/giUsAWI9NbpdeUzl.htm)|Knowledge Domain|Savoir|changé|
|[HYe7Yv1fYUANVVI3.htm](domains/HYe7Yv1fYUANVVI3.htm)|Death Domain|Mort|changé|
|[jWmGQxJvKh5y5zfB.htm](domains/jWmGQxJvKh5y5zfB.htm)|Protection Domain|Protection|changé|
|[KzuJAIWdjwoPjHkc.htm](domains/KzuJAIWdjwoPjHkc.htm)|Secrecy Domain|Secret|changé|
|[M7koZH0zimcMgRDb.htm](domains/M7koZH0zimcMgRDb.htm)|Destruction Domain|Destruction|changé|
|[MktBsoHR9HsKrbbr.htm](domains/MktBsoHR9HsKrbbr.htm)|Zeal Domain|Zèle|changé|
|[MRHDhBQvgJhDZ1zq.htm](domains/MRHDhBQvgJhDZ1zq.htm)|Cities Domain|Villes|changé|
|[NA4v0iwIPgkde8DP.htm](domains/NA4v0iwIPgkde8DP.htm)|Ambition Domain|Ambition|changé|
|[OsM8NfP408uB6yTi.htm](domains/OsM8NfP408uB6yTi.htm)|Wyrmkin Domain|Parenté dracosire|changé|
|[unN0otycQZanf3va.htm](domains/unN0otycQZanf3va.htm)|Duty Domain|Devoir|changé|
|[v4SDXgCuPdZqhMeL.htm](domains/v4SDXgCuPdZqhMeL.htm)|Fire Domain|Feu|changé|
|[wCPGej4ZwdKCNtym.htm](domains/wCPGej4ZwdKCNtym.htm)|Change Domain|Changement|changé|
|[wPtGuF1bh4wvKE6Q.htm](domains/wPtGuF1bh4wvKE6Q.htm)|Confidence Domain|Confiance en soi|changé|
|[xYx8UD0JnFyBHGhJ.htm](domains/xYx8UD0JnFyBHGhJ.htm)|Nature Domain|Nature|changé|
|[y3TTKFLPbP09HZUW.htm](domains/y3TTKFLPbP09HZUW.htm)|Cold Domain|Froid|changé|
|[YQ6IT8DgEpqvOREx.htm](domains/YQ6IT8DgEpqvOREx.htm)|Might Domain|Puissance|changé|
|[ywn4ODaUt382Z3Nz.htm](domains/ywn4ODaUt382Z3Nz.htm)|Lightning Domain|Foudre|changé|
|[ZAx1RUB376BjNdlF.htm](domains/ZAx1RUB376BjNdlF.htm)|Repose Domain|Quiétude|changé|
|[Ze2hoTyOQHbaQ6jD.htm](domains/Ze2hoTyOQHbaQ6jD.htm)|Air Domain|Air|changé|
|[ZyFTUCbA0zYrzynD.htm](domains/ZyFTUCbA0zYrzynD.htm)|Creation Domain|Création|changé|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0cbxczrql4MwAHwV.htm](domains/0cbxczrql4MwAHwV.htm)|Glyph Domain|Glyphes|libre|
|[1NHV4ujqoR2JVWpY.htm](domains/1NHV4ujqoR2JVWpY.htm)|Travel Domain|Voyage|libre|
|[41qaGiMit8nDP4xv.htm](domains/41qaGiMit8nDP4xv.htm)|Abomination Domain|Abomination|libre|
|[6klWznsb0f2bNg3T.htm](domains/6klWznsb0f2bNg3T.htm)|Void Domain|Vide|libre|
|[8ITGLquhimrr9CNv.htm](domains/8ITGLquhimrr9CNv.htm)|Indulgence Domain|Petits plaisirs|libre|
|[9blxcDLIRPWenK5f.htm](domains/9blxcDLIRPWenK5f.htm)|Plague Domain|Fléau|libre|
|[9tsJg13xeJLGzzGV.htm](domains/9tsJg13xeJLGzzGV.htm)|Undeath Domain|Non-Mort|libre|
|[AaY3BmDItGry4oac.htm](domains/AaY3BmDItGry4oac.htm)|Decay Domain|Décomposition|libre|
|[B40VxP6oZ0mIR4PS.htm](domains/B40VxP6oZ0mIR4PS.htm)|Fate Domain|Destin|libre|
|[BlovCvjhk4Ag07w2.htm](domains/BlovCvjhk4Ag07w2.htm)|Dreams Domain|Rêves|libre|
|[dnljU1twPjH4KFgO.htm](domains/dnljU1twPjH4KFgO.htm)|Swarm Domain|Nuées|libre|
|[FJ9D4qpeRhJvjHai.htm](domains/FJ9D4qpeRhJvjHai.htm)|Star Domain|Étoiles|libre|
|[fqr2OnTww3bAq0ae.htm](domains/fqr2OnTww3bAq0ae.htm)|Sun Domain|Soleil|libre|
|[fVfFKKvGocG2JM5q.htm](domains/fVfFKKvGocG2JM5q.htm)|Toil Domain|Labeur|libre|
|[HpZ4NQIqBRcFihyE.htm](domains/HpZ4NQIqBRcFihyE.htm)|Wealth Domain|Richesse|libre|
|[i4UU3qCjIMwejIQF.htm](domains/i4UU3qCjIMwejIQF.htm)|Delirium Domain|Delirium|libre|
|[J7K7kHoIE69558Su.htm](domains/J7K7kHoIE69558Su.htm)|Freedom Domain|Liberté|libre|
|[KXFxeEyD6MmJ3a6V.htm](domains/KXFxeEyD6MmJ3a6V.htm)|Naga Domain|Naga|libre|
|[l2EFJssJKu7rG77m.htm](domains/l2EFJssJKu7rG77m.htm)|Luck Domain|Chance|libre|
|[mBvjWSvg7UYdS9TL.htm](domains/mBvjWSvg7UYdS9TL.htm)|Moon Domain|Lune|libre|
|[O1qeC0mIufSf3wv5.htm](domains/O1qeC0mIufSf3wv5.htm)|Passion Domain|Passion|libre|
|[p5Q5RGl1lKgs5DZZ.htm](domains/p5Q5RGl1lKgs5DZZ.htm)|Soul Domain|Âme|libre|
|[PrFvU65ewfst69Mp.htm](domains/PrFvU65ewfst69Mp.htm)|Water Domain|Eau|libre|
|[rIDXRIdb9m2E3qC6.htm](domains/rIDXRIdb9m2E3qC6.htm)|Earth Domain|Terre|libre|
|[rIZ7OoG8c4Cct42M.htm](domains/rIZ7OoG8c4Cct42M.htm)|Vigil Domain|Veille|libre|
|[TpFgfwcWrfT8zVMP.htm](domains/TpFgfwcWrfT8zVMP.htm)|Pain Domain|Souffrance|libre|
|[udASTZy5jJWFCt5w.htm](domains/udASTZy5jJWFCt5w.htm)|Time Domain|Temps|libre|
|[uy8GUGIOmEUNqIhH.htm](domains/uy8GUGIOmEUNqIhH.htm)|Trickery Domain|Tromperie|libre|
|[WrmaTmOHojfhiENF.htm](domains/WrmaTmOHojfhiENF.htm)|Truth Domain|Vérité|libre|
|[X7MkBRJGUIp91k6f.htm](domains/X7MkBRJGUIp91k6f.htm)|Sorrow Domain|Chagrin|libre|
|[Xs6XznYHOZyQ0hJl.htm](domains/Xs6XznYHOZyQ0hJl.htm)|Tyranny Domain|Tyrannie|libre|
|[Y2kOBQydrsSqGCyn.htm](domains/Y2kOBQydrsSqGCyn.htm)|Magic Domain|Magie|libre|
|[zec5N7EnDJANGHmy.htm](domains/zec5N7EnDJANGHmy.htm)|Dust Domain|Poussière|libre|
