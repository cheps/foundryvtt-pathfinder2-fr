# État de la traduction (ac-advanced-maneuvers)

 * **officielle**: 1
 * **libre**: 38


Dernière mise à jour: 2022-07-05 07:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0WtqP7Wei3AxHh5M.htm](ac-advanced-maneuvers/0WtqP7Wei3AxHh5M.htm)|Frightening Display|Posture effrayante|officielle|
|[6LvtdtVVwtbzqUXG.htm](ac-advanced-maneuvers/6LvtdtVVwtbzqUXG.htm)|Screaming Skull|Crâne hurlant|libre|
|[7C9iFWBpEOJs4Uo2.htm](ac-advanced-maneuvers/7C9iFWBpEOJs4Uo2.htm)|Pterosaur Swoop|Piqué du ptérosaure|libre|
|[7H8wqKtsAaSgyLyp.htm](ac-advanced-maneuvers/7H8wqKtsAaSgyLyp.htm)|Grab and Sting|Saisir et piquer|libre|
|[8JdETVTpbZAgtBA3.htm](ac-advanced-maneuvers/8JdETVTpbZAgtBA3.htm)|Breath Weapon|Arme de souffle|libre|
|[ClUiCoBn3lHBBK5c.htm](ac-advanced-maneuvers/ClUiCoBn3lHBBK5c.htm)|Darting Attack|Attaque éclair|libre|
|[cpMkN79PdNci3nGp.htm](ac-advanced-maneuvers/cpMkN79PdNci3nGp.htm)|Distracting Spray|Aspersion distrayante|libre|
|[cVrW2GGLVpydj8h5.htm](ac-advanced-maneuvers/cVrW2GGLVpydj8h5.htm)|Blood Feast|Festin sanglant|libre|
|[EvuMVR9ut9wIHOtq.htm](ac-advanced-maneuvers/EvuMVR9ut9wIHOtq.htm)|Bay|Plainte|libre|
|[EwCqdvU8WOw2SSxm.htm](ac-advanced-maneuvers/EwCqdvU8WOw2SSxm.htm)|Boar Charge|Charge du sanglier|libre|
|[F98ajoIakyOPEuwj.htm](ac-advanced-maneuvers/F98ajoIakyOPEuwj.htm)|Shred|Déchiqueter|libre|
|[FKh7ZMNyKxgnV7kI.htm](ac-advanced-maneuvers/FKh7ZMNyKxgnV7kI.htm)|Throw Rock|Lancer de rocher|libre|
|[g4JEtCnKXyY4LJpm.htm](ac-advanced-maneuvers/g4JEtCnKXyY4LJpm.htm)|Feast on the Fallen|Festin sur le déchu|libre|
|[H1ElYt6KovGYGzLD.htm](ac-advanced-maneuvers/H1ElYt6KovGYGzLD.htm)|Lumbering Knockdown|Renversement boutoir|libre|
|[HLD4JW6SUn5Oy8C9.htm](ac-advanced-maneuvers/HLD4JW6SUn5Oy8C9.htm)|Badger Rage|rage du blaireau|libre|
|[JMHoCFb886K6dT1n.htm](ac-advanced-maneuvers/JMHoCFb886K6dT1n.htm)|Rhinoceros Charge|Charge du rhinocéros|libre|
|[kKW68gi1FNJlNSOp.htm](ac-advanced-maneuvers/kKW68gi1FNJlNSOp.htm)|Constrict|Constriction|libre|
|[LaBfTYUsvoI3nscv.htm](ac-advanced-maneuvers/LaBfTYUsvoI3nscv.htm)|Take a Taste|Goûte un morceau|libre|
|[lrchn6ROZcuKCg3C.htm](ac-advanced-maneuvers/lrchn6ROZcuKCg3C.htm)|Careful Withdraw|Retrait précautionneux|libre|
|[NMaMTiDB40L5O41S.htm](ac-advanced-maneuvers/NMaMTiDB40L5O41S.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[oCgdnQP5bMDD02YC.htm](ac-advanced-maneuvers/oCgdnQP5bMDD02YC.htm)|Bear Hug|Étreinte de l'ours|libre|
|[ODGf6brAHHKrSAPo.htm](ac-advanced-maneuvers/ODGf6brAHHKrSAPo.htm)|Wing Thrash|Frappe violente d'aile|libre|
|[oisXYTTYE0TABboA.htm](ac-advanced-maneuvers/oisXYTTYE0TABboA.htm)|Gnaw|Ronger|libre|
|[oYEXImMSzg0eDqzR.htm](ac-advanced-maneuvers/oYEXImMSzg0eDqzR.htm)|Grabbing Trunk|Trompe agrippante|libre|
|[PAXJgysWqv3T0tTD.htm](ac-advanced-maneuvers/PAXJgysWqv3T0tTD.htm)|Sand Stride|Marche sur le sable|libre|
|[pMPtswKNokDNyYR2.htm](ac-advanced-maneuvers/pMPtswKNokDNyYR2.htm)|Defensive Curl|Posture défensive|libre|
|[q0Vh2V6KlzdMi1Pv.htm](ac-advanced-maneuvers/q0Vh2V6KlzdMi1Pv.htm)|Tongue Pull|Traction de la langue|libre|
|[TCEQtO5DawzVAIyT.htm](ac-advanced-maneuvers/TCEQtO5DawzVAIyT.htm)|Tearing Clutch|Serrage déchirant|libre|
|[tsOaXTRWQvsKTyaL.htm](ac-advanced-maneuvers/tsOaXTRWQvsKTyaL.htm)|Gallop|Galop|libre|
|[ttJ1CanrAuehoSV8.htm](ac-advanced-maneuvers/ttJ1CanrAuehoSV8.htm)|Death Roll|Roulade mortelle|libre|
|[U0J8J5ukmqnOskE8.htm](ac-advanced-maneuvers/U0J8J5ukmqnOskE8.htm)|Cat Pounce|Bond du félin|libre|
|[Upr7u0KhPAIMOqXv.htm](ac-advanced-maneuvers/Upr7u0KhPAIMOqXv.htm)|Snatch and Zap|Arracher et électrocuter|libre|
|[uR3j6TAJFeOVUHpY.htm](ac-advanced-maneuvers/uR3j6TAJFeOVUHpY.htm)|Lurching Rush|Embardée ruante|libre|
|[Uv49tQCwUUjaMCj5.htm](ac-advanced-maneuvers/Uv49tQCwUUjaMCj5.htm)|Bouncing Slam|Coup rebondissant|libre|
|[WdTzlCO2i4loeYkQ.htm](ac-advanced-maneuvers/WdTzlCO2i4loeYkQ.htm)|Ultrasonic Scream|Cri ultrasonique|libre|
|[X4VjMfVVAjZjwcrT.htm](ac-advanced-maneuvers/X4VjMfVVAjZjwcrT.htm)|Flyby Attack|Attaque en vol|libre|
|[xssFjTXqGlsaddvl.htm](ac-advanced-maneuvers/xssFjTXqGlsaddvl.htm)|Knockdown|Renversement|libre|
|[yXw82Zd5nkcCUfRQ.htm](ac-advanced-maneuvers/yXw82Zd5nkcCUfRQ.htm)|Overwhelm|Mâchoires refermées|libre|
|[zSlsnbvsGKdCTzL1.htm](ac-advanced-maneuvers/zSlsnbvsGKdCTzL1.htm)|Hustle|S'empresser|libre|
