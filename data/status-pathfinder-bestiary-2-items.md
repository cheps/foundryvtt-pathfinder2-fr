# État de la traduction (pathfinder-bestiary-2-items)

 * **libre**: 1639
 * **changé**: 129
 * **officielle**: 807
 * **aucune**: 344
 * **vide**: 48


Dernière mise à jour: 2022-07-05 07:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0D7DkzCzV4iv9Q0r.htm](pathfinder-bestiary-2-items/0D7DkzCzV4iv9Q0r.htm)|Grab|
|[0MEzU0e7MJtwV7WA.htm](pathfinder-bestiary-2-items/0MEzU0e7MJtwV7WA.htm)|Truespeech|
|[0t8zkWyTQVqJBcMG.htm](pathfinder-bestiary-2-items/0t8zkWyTQVqJBcMG.htm)|Impaling Push|
|[0VNX7MbrCqRSKYkE.htm](pathfinder-bestiary-2-items/0VNX7MbrCqRSKYkE.htm)|Shadow Evade|
|[1dUOZTn32vZg4Reo.htm](pathfinder-bestiary-2-items/1dUOZTn32vZg4Reo.htm)|Jaws|
|[1gwUpyZY3w0LloLq.htm](pathfinder-bestiary-2-items/1gwUpyZY3w0LloLq.htm)|Devil's Howl|
|[1jqzGUINJJfVCKfu.htm](pathfinder-bestiary-2-items/1jqzGUINJJfVCKfu.htm)|Smoke Vision|
|[1MWjpvCpwBy0BFwq.htm](pathfinder-bestiary-2-items/1MWjpvCpwBy0BFwq.htm)|Emperor Cobra Venom|
|[1vC7HEUnMXCc5wme.htm](pathfinder-bestiary-2-items/1vC7HEUnMXCc5wme.htm)|Positive Energy Affinity|
|[1zSROTRkSluSvmzR.htm](pathfinder-bestiary-2-items/1zSROTRkSluSvmzR.htm)|Swallow Whole|
|[28xyMG8bDPJrcRsP.htm](pathfinder-bestiary-2-items/28xyMG8bDPJrcRsP.htm)|Death-Stealing Gaze|
|[2a2eONlkTBanM6W0.htm](pathfinder-bestiary-2-items/2a2eONlkTBanM6W0.htm)|Drain Life|
|[2PLVvoyanKPQQdYK.htm](pathfinder-bestiary-2-items/2PLVvoyanKPQQdYK.htm)|Mist Cloud|
|[2tplJgSAMF7CmPNN.htm](pathfinder-bestiary-2-items/2tplJgSAMF7CmPNN.htm)|Attack of Opportunity|
|[2VxyCqJZcASXlsyq.htm](pathfinder-bestiary-2-items/2VxyCqJZcASXlsyq.htm)|Calcification|
|[2waLtCSTbFL4Q6ta.htm](pathfinder-bestiary-2-items/2waLtCSTbFL4Q6ta.htm)|Regeneration 15 (deactivated by acid or fire)|
|[2wgeHmf6T4ScZpAk.htm](pathfinder-bestiary-2-items/2wgeHmf6T4ScZpAk.htm)|Gray Ooze Acid|
|[3IQSdTZPneapXwVf.htm](pathfinder-bestiary-2-items/3IQSdTZPneapXwVf.htm)|Change Shape|
|[3LE8g6DKV4fZYrQq.htm](pathfinder-bestiary-2-items/3LE8g6DKV4fZYrQq.htm)|Regeneration 20 (deactivated by cold iron)|
|[3mOfCebY1QT6qHAG.htm](pathfinder-bestiary-2-items/3mOfCebY1QT6qHAG.htm)|Diligent Assault|
|[3YdQIsd5sKYMGTIc.htm](pathfinder-bestiary-2-items/3YdQIsd5sKYMGTIc.htm)|Regurgitation|
|[43BphUu4vcCAeieN.htm](pathfinder-bestiary-2-items/43BphUu4vcCAeieN.htm)|Carbuncle Empathy|
|[43gvQaRJhhjmfG3Z.htm](pathfinder-bestiary-2-items/43gvQaRJhhjmfG3Z.htm)|Blood Drain|
|[44tsCgB5uAVisAQ3.htm](pathfinder-bestiary-2-items/44tsCgB5uAVisAQ3.htm)|Darkvision|
|[49xv61tdfV6J6P29.htm](pathfinder-bestiary-2-items/49xv61tdfV6J6P29.htm)|Discorporate|
|[4OphdUKxBpxPyBKN.htm](pathfinder-bestiary-2-items/4OphdUKxBpxPyBKN.htm)|Tail Drag|
|[4SqlNn1QJhVGrfJb.htm](pathfinder-bestiary-2-items/4SqlNn1QJhVGrfJb.htm)|Darkvision|
|[5fPhzfjVfDH0Ssgz.htm](pathfinder-bestiary-2-items/5fPhzfjVfDH0Ssgz.htm)|Terrifying Gaze|
|[5g1Keo0L3GpJZyQp.htm](pathfinder-bestiary-2-items/5g1Keo0L3GpJZyQp.htm)|Curse of the Wereboar|
|[5hqdjh4Jn5wtzqGs.htm](pathfinder-bestiary-2-items/5hqdjh4Jn5wtzqGs.htm)|Tendril|
|[5IixHzakyftZgS0g.htm](pathfinder-bestiary-2-items/5IixHzakyftZgS0g.htm)|Speed Surge|
|[6B5ZXZm5DwAILPJ2.htm](pathfinder-bestiary-2-items/6B5ZXZm5DwAILPJ2.htm)|Dance of Death|
|[6eEBzPI5f1Lb1ZPb.htm](pathfinder-bestiary-2-items/6eEBzPI5f1Lb1ZPb.htm)|Coven|
|[6gE7wfS2bLW0PuDW.htm](pathfinder-bestiary-2-items/6gE7wfS2bLW0PuDW.htm)|Kiss of Death|
|[6N8zQ0TKFxKlkOTG.htm](pathfinder-bestiary-2-items/6N8zQ0TKFxKlkOTG.htm)|Claim Wealth|
|[6Psn5RSYm8UGDpwM.htm](pathfinder-bestiary-2-items/6Psn5RSYm8UGDpwM.htm)|Pod Prison|
|[7HuZlfRX4ZQI1GwE.htm](pathfinder-bestiary-2-items/7HuZlfRX4ZQI1GwE.htm)|Blade|
|[7pxsxbazh1PpK8A4.htm](pathfinder-bestiary-2-items/7pxsxbazh1PpK8A4.htm)|Fast Healing 10|
|[8nLUnVkKYxkXvAZs.htm](pathfinder-bestiary-2-items/8nLUnVkKYxkXvAZs.htm)|Constrict|
|[8prRtWLrblxto9pO.htm](pathfinder-bestiary-2-items/8prRtWLrblxto9pO.htm)|Poison Gasp|
|[8r06lqp1MccJLeUV.htm](pathfinder-bestiary-2-items/8r06lqp1MccJLeUV.htm)|Drain Blood|
|[8Xjni4fPUr5P0Dl9.htm](pathfinder-bestiary-2-items/8Xjni4fPUr5P0Dl9.htm)|Jellyfish Venom|
|[9cHX93V0XpVb7b7b.htm](pathfinder-bestiary-2-items/9cHX93V0XpVb7b7b.htm)|Lashing Tongues|
|[9UqGqGPYi6N8yk6X.htm](pathfinder-bestiary-2-items/9UqGqGPYi6N8yk6X.htm)|Storm of Vines|
|[AbCU1QPQN43QSP6d.htm](pathfinder-bestiary-2-items/AbCU1QPQN43QSP6d.htm)|Speed Surge|
|[AFv5FWlpT2zyryiG.htm](pathfinder-bestiary-2-items/AFv5FWlpT2zyryiG.htm)|Swarm Mind|
|[aHamTcS3vkrZLHGN.htm](pathfinder-bestiary-2-items/aHamTcS3vkrZLHGN.htm)|Instant Suggestion|
|[AhfsEZ9fBUS6e02P.htm](pathfinder-bestiary-2-items/AhfsEZ9fBUS6e02P.htm)|Poisonous Warts|
|[aJhxWGibV6WOF98e.htm](pathfinder-bestiary-2-items/aJhxWGibV6WOF98e.htm)|Beckoning Call|
|[aKGDMqsWvw2cOIdE.htm](pathfinder-bestiary-2-items/aKGDMqsWvw2cOIdE.htm)|Power Attack|
|[aQDe7We9BV7qqiIm.htm](pathfinder-bestiary-2-items/aQDe7We9BV7qqiIm.htm)|Envelop|
|[arLLxzC2PfddWyB8.htm](pathfinder-bestiary-2-items/arLLxzC2PfddWyB8.htm)|Whirlwind Blast|
|[aWo3aVa2XXkGsrLY.htm](pathfinder-bestiary-2-items/aWo3aVa2XXkGsrLY.htm)|Final Spite|
|[AytjsrleVTlWADIi.htm](pathfinder-bestiary-2-items/AytjsrleVTlWADIi.htm)|Draining Presence|
|[B6PM6Ak7BCncLxYT.htm](pathfinder-bestiary-2-items/B6PM6Ak7BCncLxYT.htm)|Trample|
|[BAJs3gJ34CxRZX5W.htm](pathfinder-bestiary-2-items/BAJs3gJ34CxRZX5W.htm)|Fair Competition|
|[bC1xvgHpQUnix9Bt.htm](pathfinder-bestiary-2-items/bC1xvgHpQUnix9Bt.htm)|Deep Breath|
|[BdJy1HqrIlkJp8rF.htm](pathfinder-bestiary-2-items/BdJy1HqrIlkJp8rF.htm)|Animated Hair|
|[bEnEcBaXcq0xrckT.htm](pathfinder-bestiary-2-items/bEnEcBaXcq0xrckT.htm)|Flawless Hearing|
|[BJelgf4pdEHXMd9u.htm](pathfinder-bestiary-2-items/BJelgf4pdEHXMd9u.htm)|Change Shape|
|[BmhdDSnmoRYLlefv.htm](pathfinder-bestiary-2-items/BmhdDSnmoRYLlefv.htm)|Consumptive Aura|
|[bMX8ZvBxeu0Ty5cl.htm](pathfinder-bestiary-2-items/bMX8ZvBxeu0Ty5cl.htm)|Devour Soul|
|[bQyms0FzVu1O5MrR.htm](pathfinder-bestiary-2-items/bQyms0FzVu1O5MrR.htm)|Gnaw Metal|
|[bR7DFykvg3BJRRmV.htm](pathfinder-bestiary-2-items/bR7DFykvg3BJRRmV.htm)|Dream Spider Venom|
|[BVXX982lJiCTVido.htm](pathfinder-bestiary-2-items/BVXX982lJiCTVido.htm)|Bloodletting|
|[chnUQOapjPm4tXAS.htm](pathfinder-bestiary-2-items/chnUQOapjPm4tXAS.htm)|Calming Presence|
|[CpDQG0xz9auLn9gr.htm](pathfinder-bestiary-2-items/CpDQG0xz9auLn9gr.htm)|Double Punch|
|[CsrgxIJmjkumjsBJ.htm](pathfinder-bestiary-2-items/CsrgxIJmjkumjsBJ.htm)|Trample|
|[CswqTh16aa0KDlDF.htm](pathfinder-bestiary-2-items/CswqTh16aa0KDlDF.htm)|Necrophidic Paralysis|
|[CwSvR6GqNWvMMxwR.htm](pathfinder-bestiary-2-items/CwSvR6GqNWvMMxwR.htm)|Water Stride|
|[CwxYoKcoC0dSFuhX.htm](pathfinder-bestiary-2-items/CwxYoKcoC0dSFuhX.htm)|Mind-Numbing Touch|
|[CxhKccyCVc1NDZFs.htm](pathfinder-bestiary-2-items/CxhKccyCVc1NDZFs.htm)|Greater Darkvision|
|[D0L9Y3ZIku07uMOk.htm](pathfinder-bestiary-2-items/D0L9Y3ZIku07uMOk.htm)|Fated|
|[DA9qmvZn2NX8sUkj.htm](pathfinder-bestiary-2-items/DA9qmvZn2NX8sUkj.htm)|Jet|
|[dbDXIpmXMwGGSF0O.htm](pathfinder-bestiary-2-items/dbDXIpmXMwGGSF0O.htm)|Breath Weapon|
|[de7GnRXapzBCdDoL.htm](pathfinder-bestiary-2-items/de7GnRXapzBCdDoL.htm)|Infuse Weapon|
|[DfyikbxEMRuzW77m.htm](pathfinder-bestiary-2-items/DfyikbxEMRuzW77m.htm)|Swarm Shape|
|[dn95vUYkWbZHUAEc.htm](pathfinder-bestiary-2-items/dn95vUYkWbZHUAEc.htm)|Swallow Whole|
|[dNIO5C35HeSGtxDD.htm](pathfinder-bestiary-2-items/dNIO5C35HeSGtxDD.htm)|See Invisibility|
|[DteZBAInF0ur5LIY.htm](pathfinder-bestiary-2-items/DteZBAInF0ur5LIY.htm)|Shape Flesh|
|[DVQrLVPCYEJyvk5s.htm](pathfinder-bestiary-2-items/DVQrLVPCYEJyvk5s.htm)|Destructive Harmonics|
|[DxgiMoyx7wyltYqp.htm](pathfinder-bestiary-2-items/DxgiMoyx7wyltYqp.htm)|Drink Blood|
|[dynvtBdr79KGnqBo.htm](pathfinder-bestiary-2-items/dynvtBdr79KGnqBo.htm)|Can't Catch Me|
|[dYvQOwzgoNuS9cG8.htm](pathfinder-bestiary-2-items/dYvQOwzgoNuS9cG8.htm)|Body Thief|
|[E4bINR3oniilC0ba.htm](pathfinder-bestiary-2-items/E4bINR3oniilC0ba.htm)|Shell Defense|
|[e7rgKN11dqX1fsSq.htm](pathfinder-bestiary-2-items/e7rgKN11dqX1fsSq.htm)|Focus Gaze|
|[earmbFXXQH89tgAG.htm](pathfinder-bestiary-2-items/earmbFXXQH89tgAG.htm)|Warhammer|
|[eaXk793zko55RRmG.htm](pathfinder-bestiary-2-items/eaXk793zko55RRmG.htm)|Icy Deflection|
|[eFiZ1wgGbxx9AVu1.htm](pathfinder-bestiary-2-items/eFiZ1wgGbxx9AVu1.htm)|Septic Malaria|
|[eG5iE2SpiXdvquWh.htm](pathfinder-bestiary-2-items/eG5iE2SpiXdvquWh.htm)|Funereal Dirge|
|[EguLo1LUzA4CQ1Qi.htm](pathfinder-bestiary-2-items/EguLo1LUzA4CQ1Qi.htm)|Deep Breath|
|[EhmJLz0h6EUzbywV.htm](pathfinder-bestiary-2-items/EhmJLz0h6EUzbywV.htm)|Haul Away|
|[ej0lyLnZuPKMNMnI.htm](pathfinder-bestiary-2-items/ej0lyLnZuPKMNMnI.htm)|Focused Flames|
|[eN9AmHu7Wb5ZVY5F.htm](pathfinder-bestiary-2-items/eN9AmHu7Wb5ZVY5F.htm)|Whiffling|
|[eoLK3mAvvXiMTLnH.htm](pathfinder-bestiary-2-items/eoLK3mAvvXiMTLnH.htm)|Twist the Hook|
|[EPXehWRhcO5caYov.htm](pathfinder-bestiary-2-items/EPXehWRhcO5caYov.htm)|Shift Fate|
|[eQAeRO9fMIqYp05Q.htm](pathfinder-bestiary-2-items/eQAeRO9fMIqYp05Q.htm)|Sense Fate|
|[eQGUATVlHv8iZ97x.htm](pathfinder-bestiary-2-items/eQGUATVlHv8iZ97x.htm)|Weak Acid|
|[ERMHc3Xc7e0s0qGD.htm](pathfinder-bestiary-2-items/ERMHc3Xc7e0s0qGD.htm)|Withering Touch|
|[ezme3D83xTKiLkhR.htm](pathfinder-bestiary-2-items/ezme3D83xTKiLkhR.htm)|Final End|
|[F1orhEfNkIuGNlEh.htm](pathfinder-bestiary-2-items/F1orhEfNkIuGNlEh.htm)|Buck|
|[F2cUCIHpa06Qkqy2.htm](pathfinder-bestiary-2-items/F2cUCIHpa06Qkqy2.htm)|Squeeze|
|[FH5MdxzW5lzQE9Hf.htm](pathfinder-bestiary-2-items/FH5MdxzW5lzQE9Hf.htm)|Regeneration 7 (deactivated by acid or fire)|
|[fiKhFiynAYN8X5if.htm](pathfinder-bestiary-2-items/fiKhFiynAYN8X5if.htm)|Gemsight|
|[fJR0vMezUrIu4nlW.htm](pathfinder-bestiary-2-items/fJR0vMezUrIu4nlW.htm)|Tail Sting|
|[fJwNiBKQ1mE1NNbz.htm](pathfinder-bestiary-2-items/fJwNiBKQ1mE1NNbz.htm)|Dual Tusks|
|[flCrHBNNesdvv5af.htm](pathfinder-bestiary-2-items/flCrHBNNesdvv5af.htm)|Attack of Opportunity|
|[fon9lS38svdiFHZJ.htm](pathfinder-bestiary-2-items/fon9lS38svdiFHZJ.htm)|Greedy Grab|
|[FpcxdJ4oqnyX8sBs.htm](pathfinder-bestiary-2-items/FpcxdJ4oqnyX8sBs.htm)|Long Neck|
|[fPGmCfWuF8oqK8hX.htm](pathfinder-bestiary-2-items/fPGmCfWuF8oqK8hX.htm)|Otherworldly Laugh|
|[FQZGypLzxjxM8v3a.htm](pathfinder-bestiary-2-items/FQZGypLzxjxM8v3a.htm)|Black Flame Knives|
|[Fu1B7hcvnk9S79DA.htm](pathfinder-bestiary-2-items/Fu1B7hcvnk9S79DA.htm)|Commander's Aura|
|[FVDPg3HIsGffnOjd.htm](pathfinder-bestiary-2-items/FVDPg3HIsGffnOjd.htm)|Shameful Touch|
|[fZOolgYWKm1a27op.htm](pathfinder-bestiary-2-items/fZOolgYWKm1a27op.htm)|Purity Vulnerability|
|[Gbcpa6lIhj2KUyH4.htm](pathfinder-bestiary-2-items/Gbcpa6lIhj2KUyH4.htm)|Rampant Growth|
|[gcdD70PvUw3sfPCK.htm](pathfinder-bestiary-2-items/gcdD70PvUw3sfPCK.htm)|Painful Harmonics|
|[GCGNJYiHP5GMdDAQ.htm](pathfinder-bestiary-2-items/GCGNJYiHP5GMdDAQ.htm)|Exit Body|
|[gd4pTa9mCV6qrbcN.htm](pathfinder-bestiary-2-items/gd4pTa9mCV6qrbcN.htm)|Undulate|
|[gEADE2ZlQ0vRnps7.htm](pathfinder-bestiary-2-items/gEADE2ZlQ0vRnps7.htm)|Stench|
|[gHUajYINDeCp7360.htm](pathfinder-bestiary-2-items/gHUajYINDeCp7360.htm)|Echolocation 120 feet|
|[gixaFzYqsyV9bsLi.htm](pathfinder-bestiary-2-items/gixaFzYqsyV9bsLi.htm)|Cairn Wight Spawn|
|[gYBpkJDYJYKODQRK.htm](pathfinder-bestiary-2-items/gYBpkJDYJYKODQRK.htm)|Gem Gaze|
|[GYQMiGEbQopeX3bK.htm](pathfinder-bestiary-2-items/GYQMiGEbQopeX3bK.htm)|Flowing Hair|
|[gyVj6MqUbxY54dB9.htm](pathfinder-bestiary-2-items/gyVj6MqUbxY54dB9.htm)|Light of Avarice|
|[gZfWtX8zkOoe49o7.htm](pathfinder-bestiary-2-items/gZfWtX8zkOoe49o7.htm)|Regeneration 25 (deactivated by vorpal weapons)|
|[H5H2qTd7RH9FnyiA.htm](pathfinder-bestiary-2-items/H5H2qTd7RH9FnyiA.htm)|Enliven Foliage|
|[H7lzbseyLDhs5Tp3.htm](pathfinder-bestiary-2-items/H7lzbseyLDhs5Tp3.htm)|Sound Mimicry|
|[H97yITJBuctoMctx.htm](pathfinder-bestiary-2-items/H97yITJBuctoMctx.htm)|Change Shape|
|[hkgYTbnzCzYBfP7k.htm](pathfinder-bestiary-2-items/hkgYTbnzCzYBfP7k.htm)|Voice Imitation|
|[HLOEvF4GMvQEPHct.htm](pathfinder-bestiary-2-items/HLOEvF4GMvQEPHct.htm)|Camouflaged Step|
|[Hsh9yw0IW6kN2n8t.htm](pathfinder-bestiary-2-items/Hsh9yw0IW6kN2n8t.htm)|Electrified Blood|
|[hTuKXeGWvUSUobx7.htm](pathfinder-bestiary-2-items/hTuKXeGWvUSUobx7.htm)|Ball Lightning Breath|
|[Hu1SdyNVG4WJmNcx.htm](pathfinder-bestiary-2-items/Hu1SdyNVG4WJmNcx.htm)|Soul Spells|
|[hwnMtKzru4aBoOu3.htm](pathfinder-bestiary-2-items/hwnMtKzru4aBoOu3.htm)|Change Shape|
|[hy3Lxy9hFGa73bQP.htm](pathfinder-bestiary-2-items/hy3Lxy9hFGa73bQP.htm)|Sunlight Powerlessness|
|[i2d0NrqSfbRm2l1Y.htm](pathfinder-bestiary-2-items/i2d0NrqSfbRm2l1Y.htm)|Shocking Douse|
|[IHn483eD21ShK5j4.htm](pathfinder-bestiary-2-items/IHn483eD21ShK5j4.htm)|Infuse Weapons|
|[Ij85zXU5FdthHI5o.htm](pathfinder-bestiary-2-items/Ij85zXU5FdthHI5o.htm)|Stolen Death|
|[iJjnT8l7EIEaChZu.htm](pathfinder-bestiary-2-items/iJjnT8l7EIEaChZu.htm)|Camouflage|
|[InHy3ZKl3MHOyTX4.htm](pathfinder-bestiary-2-items/InHy3ZKl3MHOyTX4.htm)|At-Will Spells|
|[Ir6SQ9XsXTFTSPKi.htm](pathfinder-bestiary-2-items/Ir6SQ9XsXTFTSPKi.htm)|Protected by the Ancestors|
|[irQzM9Rv3zR8bh6X.htm](pathfinder-bestiary-2-items/irQzM9Rv3zR8bh6X.htm)|Whirling Slice|
|[iTVvlmvMKiOcRmTQ.htm](pathfinder-bestiary-2-items/iTVvlmvMKiOcRmTQ.htm)|Ferocity|
|[IWyA3McLAMxHc0jq.htm](pathfinder-bestiary-2-items/IWyA3McLAMxHc0jq.htm)|Capsize|
|[iWYCpNJjdadjk8JX.htm](pathfinder-bestiary-2-items/iWYCpNJjdadjk8JX.htm)|Briny Wound|
|[IZ5Q9oQE8jylYahf.htm](pathfinder-bestiary-2-items/IZ5Q9oQE8jylYahf.htm)|Ultrasonic Blast|
|[izA7GPk45mKcDG02.htm](pathfinder-bestiary-2-items/izA7GPk45mKcDG02.htm)|Giant Ant Venom|
|[J0ri0JZTtI3g3kxN.htm](pathfinder-bestiary-2-items/J0ri0JZTtI3g3kxN.htm)|Change Shape|
|[J4SHx8p5wCH9xlMo.htm](pathfinder-bestiary-2-items/J4SHx8p5wCH9xlMo.htm)|Clench Jaws|
|[jBybuNj39jNUTdkz.htm](pathfinder-bestiary-2-items/jBybuNj39jNUTdkz.htm)|Scent Demons 60 feet|
|[JcDvmYqI4o86358N.htm](pathfinder-bestiary-2-items/JcDvmYqI4o86358N.htm)|Grievous Strike|
|[jCQO2RfqPLKqDeuF.htm](pathfinder-bestiary-2-items/jCQO2RfqPLKqDeuF.htm)|Slow Susceptibility|
|[JGXYh66auv2dCHDv.htm](pathfinder-bestiary-2-items/JGXYh66auv2dCHDv.htm)|Volcanic Eruption|
|[JHlhwEGUybavtbTq.htm](pathfinder-bestiary-2-items/JHlhwEGUybavtbTq.htm)|Living Form|
|[jJ8meK8Q70uy8CHT.htm](pathfinder-bestiary-2-items/jJ8meK8Q70uy8CHT.htm)|Scent (Imprecise) 30 feet|
|[JJTobEFnvJPV524S.htm](pathfinder-bestiary-2-items/JJTobEFnvJPV524S.htm)|Vorpal Fear|
|[JK7loVXQ7UJJt3F9.htm](pathfinder-bestiary-2-items/JK7loVXQ7UJJt3F9.htm)|Fists of Thunder and Lightning|
|[JlQUb1sbsYfmz5Vi.htm](pathfinder-bestiary-2-items/JlQUb1sbsYfmz5Vi.htm)|Cloak in Embers|
|[JS74Gdm0nVhPpG4k.htm](pathfinder-bestiary-2-items/JS74Gdm0nVhPpG4k.htm)|Mindwarping|
|[Jspfa7SNI6gNhPK4.htm](pathfinder-bestiary-2-items/Jspfa7SNI6gNhPK4.htm)|Coven|
|[JTKyH2bv7vq73eF0.htm](pathfinder-bestiary-2-items/JTKyH2bv7vq73eF0.htm)|Whiptail Centipede Venom|
|[JVxGmdUEcMuZmGlg.htm](pathfinder-bestiary-2-items/JVxGmdUEcMuZmGlg.htm)|Light Pulse|
|[jzI4ObWhN1kXyFjY.htm](pathfinder-bestiary-2-items/jzI4ObWhN1kXyFjY.htm)|Attack of Opportunity|
|[JzkjkxNi7EFlzsmZ.htm](pathfinder-bestiary-2-items/JzkjkxNi7EFlzsmZ.htm)|Stealth|
|[k6pOje8wFsRd6e1G.htm](pathfinder-bestiary-2-items/k6pOje8wFsRd6e1G.htm)|Pincer|
|[kBxRAEuGBWNPM636.htm](pathfinder-bestiary-2-items/kBxRAEuGBWNPM636.htm)|Language Adaptation|
|[KDUeQyj1J71Vb3Q1.htm](pathfinder-bestiary-2-items/KDUeQyj1J71Vb3Q1.htm)|Absorb Wraith|
|[kjaeBRLTBCSOl9cS.htm](pathfinder-bestiary-2-items/kjaeBRLTBCSOl9cS.htm)|Swallow Whole|
|[KmdxRqIdBe3pc6XL.htm](pathfinder-bestiary-2-items/KmdxRqIdBe3pc6XL.htm)|Trample|
|[KQBI90QlI48BEPP3.htm](pathfinder-bestiary-2-items/KQBI90QlI48BEPP3.htm)|Giant Toad Poison|
|[KswFDiMoMctr0zFY.htm](pathfinder-bestiary-2-items/KswFDiMoMctr0zFY.htm)|Draining Glance|
|[kSySgkElvNOAzAXn.htm](pathfinder-bestiary-2-items/kSySgkElvNOAzAXn.htm)|Enveloping Kimono|
|[KtaZ7mxQIQwm19ZM.htm](pathfinder-bestiary-2-items/KtaZ7mxQIQwm19ZM.htm)|Dimensional Tether|
|[KTxjm0shb9zyrh0z.htm](pathfinder-bestiary-2-items/KTxjm0shb9zyrh0z.htm)|Drain Life|
|[kuW4IJohw5LunECQ.htm](pathfinder-bestiary-2-items/kuW4IJohw5LunECQ.htm)|Inhale Vitality|
|[lA1KEaw1II1i4twA.htm](pathfinder-bestiary-2-items/lA1KEaw1II1i4twA.htm)|Boar Charge|
|[lD6n6NYRsC4x82eE.htm](pathfinder-bestiary-2-items/lD6n6NYRsC4x82eE.htm)|Fire Missile|
|[LDLwyqZVioLuytc9.htm](pathfinder-bestiary-2-items/LDLwyqZVioLuytc9.htm)|Low-Light Vision|
|[lJ7R0wPVIxspGA72.htm](pathfinder-bestiary-2-items/lJ7R0wPVIxspGA72.htm)|Smoke Form|
|[LmNbsiKPocWzw1cu.htm](pathfinder-bestiary-2-items/LmNbsiKPocWzw1cu.htm)|Blood Berries|
|[LNbjPm5a6CTl4lko.htm](pathfinder-bestiary-2-items/LNbjPm5a6CTl4lko.htm)|Infernal Eye|
|[lOVrcn3AJGm4KWma.htm](pathfinder-bestiary-2-items/lOVrcn3AJGm4KWma.htm)|Eyes Of Flame|
|[lqG3xn6Di5qPS55E.htm](pathfinder-bestiary-2-items/lqG3xn6Di5qPS55E.htm)|Drain Soul|
|[LqnyBEmxdZr7oIfJ.htm](pathfinder-bestiary-2-items/LqnyBEmxdZr7oIfJ.htm)|Light Sickness|
|[LqV3ag2bSeXmlk7D.htm](pathfinder-bestiary-2-items/LqV3ag2bSeXmlk7D.htm)|Boar Empathy|
|[Lt5U1QzZ5Wx4rMiJ.htm](pathfinder-bestiary-2-items/Lt5U1QzZ5Wx4rMiJ.htm)|Caster Link|
|[LvD2kKe3HyLRK3Gr.htm](pathfinder-bestiary-2-items/LvD2kKe3HyLRK3Gr.htm)|Flare Hood|
|[lWwRydUGrcy5WmZf.htm](pathfinder-bestiary-2-items/lWwRydUGrcy5WmZf.htm)|Change Shape|
|[lXe9Yv9DRWtrP5OH.htm](pathfinder-bestiary-2-items/lXe9Yv9DRWtrP5OH.htm)|Shattering Harmonics|
|[LyhMcP0ruqAc5kCP.htm](pathfinder-bestiary-2-items/LyhMcP0ruqAc5kCP.htm)|Fist|
|[LZTLNHZnYh3Eg4aU.htm](pathfinder-bestiary-2-items/LZTLNHZnYh3Eg4aU.htm)|Indispensable Savvy|
|[m1YzMXBeebVN4Cp1.htm](pathfinder-bestiary-2-items/m1YzMXBeebVN4Cp1.htm)|Telepathy 100 feet|
|[M3YBIgGHTHFIcGRF.htm](pathfinder-bestiary-2-items/M3YBIgGHTHFIcGRF.htm)|Icicle|
|[MhPq4EOlBLppwQeu.htm](pathfinder-bestiary-2-items/MhPq4EOlBLppwQeu.htm)|Cocytan Filth|
|[Mj4CtMHXmN85o3tJ.htm](pathfinder-bestiary-2-items/Mj4CtMHXmN85o3tJ.htm)|Focus Gaze|
|[MJIrkULpmp4soA0F.htm](pathfinder-bestiary-2-items/MJIrkULpmp4soA0F.htm)|Rolling Charge|
|[mKh47XLxTvXG71TF.htm](pathfinder-bestiary-2-items/mKh47XLxTvXG71TF.htm)|Soul Crush|
|[MLD9Fi3IkozFjlXZ.htm](pathfinder-bestiary-2-items/MLD9Fi3IkozFjlXZ.htm)|Penetrating Strike|
|[mMCpgVRLUjGayWTB.htm](pathfinder-bestiary-2-items/mMCpgVRLUjGayWTB.htm)|Darkvision|
|[mqsn67bFzywwGQ10.htm](pathfinder-bestiary-2-items/mqsn67bFzywwGQ10.htm)|Toxic Bite|
|[mS3DC54HONPgp9Pg.htm](pathfinder-bestiary-2-items/mS3DC54HONPgp9Pg.htm)|Sunlight Vulnerability|
|[MSbm9DhCPOKdhf7m.htm](pathfinder-bestiary-2-items/MSbm9DhCPOKdhf7m.htm)|Drink Flesh|
|[mTE12ORozb0LZ81C.htm](pathfinder-bestiary-2-items/mTE12ORozb0LZ81C.htm)|Rock|
|[MtM4AVZZ5pbVijPC.htm](pathfinder-bestiary-2-items/MtM4AVZZ5pbVijPC.htm)|+2 Status to All Saves vs Magic|
|[MuzXu4bDzodvaZ7w.htm](pathfinder-bestiary-2-items/MuzXu4bDzodvaZ7w.htm)|Watery Transparency|
|[mY1TZIEvZh0l0OBD.htm](pathfinder-bestiary-2-items/mY1TZIEvZh0l0OBD.htm)|Throw Rock|
|[n4WaiTMOMHY2C2Rc.htm](pathfinder-bestiary-2-items/n4WaiTMOMHY2C2Rc.htm)|Darkvision|
|[N4XGBKf3QWAL89KZ.htm](pathfinder-bestiary-2-items/N4XGBKf3QWAL89KZ.htm)|Change Shape|
|[n5D36yTEt03Ye99c.htm](pathfinder-bestiary-2-items/n5D36yTEt03Ye99c.htm)|Swallow Whole|
|[N9in8b8yuAVDLoyD.htm](pathfinder-bestiary-2-items/N9in8b8yuAVDLoyD.htm)|Motion Sense|
|[N9qNd1NMgEeyNwcF.htm](pathfinder-bestiary-2-items/N9qNd1NMgEeyNwcF.htm)|Spell Deflection|
|[NCoI7vXSD3LDfPTM.htm](pathfinder-bestiary-2-items/NCoI7vXSD3LDfPTM.htm)|Rapid Rake|
|[nHTCIVLK6QSJqHd3.htm](pathfinder-bestiary-2-items/nHTCIVLK6QSJqHd3.htm)|Poisonous Pustules|
|[Nl0sqXtr54YzMawN.htm](pathfinder-bestiary-2-items/Nl0sqXtr54YzMawN.htm)|Consume Death|
|[nLocPM8hKYMstnGr.htm](pathfinder-bestiary-2-items/nLocPM8hKYMstnGr.htm)|Change Shape|
|[nnl33KWF4lCqql97.htm](pathfinder-bestiary-2-items/nnl33KWF4lCqql97.htm)|Easy to Influence|
|[nQuUyJS1AvKyFrA0.htm](pathfinder-bestiary-2-items/nQuUyJS1AvKyFrA0.htm)|Attack of Opportunity|
|[nrXh0T6f3LTaRSGo.htm](pathfinder-bestiary-2-items/nrXh0T6f3LTaRSGo.htm)|At-Will Spells|
|[nszzMLENLPQ4zPoa.htm](pathfinder-bestiary-2-items/nszzMLENLPQ4zPoa.htm)|Tenacious Flames|
|[NWD8KA0NNOrEyrvq.htm](pathfinder-bestiary-2-items/NWD8KA0NNOrEyrvq.htm)|Flying Strafe|
|[nyh1lXyTGoUctOzo.htm](pathfinder-bestiary-2-items/nyh1lXyTGoUctOzo.htm)|Double Chomp|
|[NZhUsB0mdsYLr6UX.htm](pathfinder-bestiary-2-items/NZhUsB0mdsYLr6UX.htm)|Telepathy 100 feet|
|[O10CpdkrcpxqCHHH.htm](pathfinder-bestiary-2-items/O10CpdkrcpxqCHHH.htm)|Circle of Protection|
|[o38MzuC03KOCw8wS.htm](pathfinder-bestiary-2-items/o38MzuC03KOCw8wS.htm)|Jaws That Bite|
|[o4uNCohL2RA2ShrG.htm](pathfinder-bestiary-2-items/o4uNCohL2RA2ShrG.htm)|Negative Healing|
|[o6dcBgCrq9s6bVuO.htm](pathfinder-bestiary-2-items/o6dcBgCrq9s6bVuO.htm)|Swallow Whole|
|[obCe64sZedFgSWGQ.htm](pathfinder-bestiary-2-items/obCe64sZedFgSWGQ.htm)|Luring Cry|
|[oc2KzcT2MuHsRZ1k.htm](pathfinder-bestiary-2-items/oc2KzcT2MuHsRZ1k.htm)|Frozen Strike|
|[oIucCyy3YvC5FoCA.htm](pathfinder-bestiary-2-items/oIucCyy3YvC5FoCA.htm)|Stone Stride|
|[okXnipjpQrYq6vGT.htm](pathfinder-bestiary-2-items/okXnipjpQrYq6vGT.htm)|Constrict|
|[oqYW7IkKIsygycAY.htm](pathfinder-bestiary-2-items/oqYW7IkKIsygycAY.htm)|Reflexive Grab|
|[oUmLwF6HrGb0Y9yj.htm](pathfinder-bestiary-2-items/oUmLwF6HrGb0Y9yj.htm)|Regeneration 20 (deactivated by evil)|
|[Ox2rTRtNAQouxryx.htm](pathfinder-bestiary-2-items/Ox2rTRtNAQouxryx.htm)|Motion Sense 60 feet|
|[OYFgzf5Q5j6UeJhf.htm](pathfinder-bestiary-2-items/OYFgzf5Q5j6UeJhf.htm)|Jaws|
|[P0EyFCnT4og9udgU.htm](pathfinder-bestiary-2-items/P0EyFCnT4og9udgU.htm)|Deep Breath|
|[P92CxO5qbGv9LPbu.htm](pathfinder-bestiary-2-items/P92CxO5qbGv9LPbu.htm)|Snip Thread|
|[p9eUNDT8mPVTSS1D.htm](pathfinder-bestiary-2-items/p9eUNDT8mPVTSS1D.htm)|Dimensional Wormhole|
|[PDPYyeFFpfksUoXK.htm](pathfinder-bestiary-2-items/PDPYyeFFpfksUoXK.htm)|Shadowcloak|
|[pepIdjNqJtUTl9Wn.htm](pathfinder-bestiary-2-items/pepIdjNqJtUTl9Wn.htm)|Endless Nightmare|
|[PF6tfwJNh5sVOavs.htm](pathfinder-bestiary-2-items/PF6tfwJNh5sVOavs.htm)|Thrashing Retreat|
|[pHckdhGspQesS1nl.htm](pathfinder-bestiary-2-items/pHckdhGspQesS1nl.htm)|Ravage|
|[PLo6RE4lIKmw9DQL.htm](pathfinder-bestiary-2-items/PLo6RE4lIKmw9DQL.htm)|Blurred Form|
|[pMGUO0z7GhIrgGlF.htm](pathfinder-bestiary-2-items/pMGUO0z7GhIrgGlF.htm)|Reactive Slime|
|[PrFbn0CsipqWtp4z.htm](pathfinder-bestiary-2-items/PrFbn0CsipqWtp4z.htm)|Flytrap Toxin|
|[pSl7SmKRkTYkQI1N.htm](pathfinder-bestiary-2-items/pSl7SmKRkTYkQI1N.htm)|Stone Curse|
|[Q1JyYPsUlKes2ELl.htm](pathfinder-bestiary-2-items/Q1JyYPsUlKes2ELl.htm)|Split|
|[Q7cHvTaIWyRwhMYl.htm](pathfinder-bestiary-2-items/Q7cHvTaIWyRwhMYl.htm)|Web Trap|
|[qgyml1ToPqFdJIKa.htm](pathfinder-bestiary-2-items/qgyml1ToPqFdJIKa.htm)|Drag Below|
|[QK7BnQXUv5A7jm84.htm](pathfinder-bestiary-2-items/QK7BnQXUv5A7jm84.htm)|Warden of Erebus|
|[QleXAKxC6oYMhWIK.htm](pathfinder-bestiary-2-items/QleXAKxC6oYMhWIK.htm)|Roll|
|[QNy9Hrx2ib5tkQKt.htm](pathfinder-bestiary-2-items/QNy9Hrx2ib5tkQKt.htm)|Shadow Blend|
|[QTnEqxIt2MaL0WlZ.htm](pathfinder-bestiary-2-items/QTnEqxIt2MaL0WlZ.htm)|Spirit Naga Venom|
|[QuXauTCFwPyxjFZ6.htm](pathfinder-bestiary-2-items/QuXauTCFwPyxjFZ6.htm)|Accursed Breath|
|[qXrvqRXGNuAyfORa.htm](pathfinder-bestiary-2-items/qXrvqRXGNuAyfORa.htm)|Wrench Spirit|
|[qxsT5YmSCH2dqJAT.htm](pathfinder-bestiary-2-items/qxsT5YmSCH2dqJAT.htm)|Amalgam|
|[QY55zAJAyvaoflJY.htm](pathfinder-bestiary-2-items/QY55zAJAyvaoflJY.htm)|Nature Empathy|
|[R1voQq09UIwdTVP5.htm](pathfinder-bestiary-2-items/R1voQq09UIwdTVP5.htm)|Mercy Vulnerability|
|[R3HSbrKclMuhH014.htm](pathfinder-bestiary-2-items/R3HSbrKclMuhH014.htm)|Planar Acclimation|
|[r62684lc4BkUMn7p.htm](pathfinder-bestiary-2-items/r62684lc4BkUMn7p.htm)|Solidify Mist|
|[RCjZk0ilv6kHRuT5.htm](pathfinder-bestiary-2-items/RCjZk0ilv6kHRuT5.htm)|Curse of the Weretiger|
|[rkxRbGfC97VpayNy.htm](pathfinder-bestiary-2-items/rkxRbGfC97VpayNy.htm)|Triumvirate|
|[RniBjxUV8FfeP05P.htm](pathfinder-bestiary-2-items/RniBjxUV8FfeP05P.htm)|Blue-Ringed Octopus Venom|
|[rQn75QJUJH2hQKpE.htm](pathfinder-bestiary-2-items/rQn75QJUJH2hQKpE.htm)|Chain of Malebolge|
|[RTUQPDiCd9wKvFow.htm](pathfinder-bestiary-2-items/RTUQPDiCd9wKvFow.htm)|Tentacle Encage|
|[rx9aGcoGrLLVPzhV.htm](pathfinder-bestiary-2-items/rx9aGcoGrLLVPzhV.htm)|Withering Opportunity|
|[RZEi7CewQZtkaHnE.htm](pathfinder-bestiary-2-items/RZEi7CewQZtkaHnE.htm)|Bleeding Critical|
|[S8XYYEnqxZeY5C7K.htm](pathfinder-bestiary-2-items/S8XYYEnqxZeY5C7K.htm)|Daemonic Famine|
|[sabgo6OVGbFhpOOA.htm](pathfinder-bestiary-2-items/sabgo6OVGbFhpOOA.htm)|Compel Condemned|
|[sc9sTDDxNEMsTklK.htm](pathfinder-bestiary-2-items/sc9sTDDxNEMsTklK.htm)|Mist Vision|
|[scNTrSPuw9K5tRtq.htm](pathfinder-bestiary-2-items/scNTrSPuw9K5tRtq.htm)|Wind Form|
|[Sep8lU0AriLntZmy.htm](pathfinder-bestiary-2-items/Sep8lU0AriLntZmy.htm)|Vanth's Curse|
|[sfq8i9rLngOZhIeU.htm](pathfinder-bestiary-2-items/sfq8i9rLngOZhIeU.htm)|Low-Light Vision|
|[Sg3CFomZepZdIWcH.htm](pathfinder-bestiary-2-items/Sg3CFomZepZdIWcH.htm)|Death Gaze|
|[sN3JKaIDG5kFRAvC.htm](pathfinder-bestiary-2-items/sN3JKaIDG5kFRAvC.htm)|Natural Camouflage|
|[sNUoqbThXZbfj5S7.htm](pathfinder-bestiary-2-items/sNUoqbThXZbfj5S7.htm)|Fade from View|
|[ssitSO6O1esw1YHS.htm](pathfinder-bestiary-2-items/ssitSO6O1esw1YHS.htm)|Frightful Presence|
|[swAWlgrj8JoJq6xB.htm](pathfinder-bestiary-2-items/swAWlgrj8JoJq6xB.htm)|Darkvision|
|[t9eUo7KSFGGelyru.htm](pathfinder-bestiary-2-items/t9eUo7KSFGGelyru.htm)|Trample|
|[TcRZ95uAXroBAwRv.htm](pathfinder-bestiary-2-items/TcRZ95uAXroBAwRv.htm)|Verdurous Ooze Acid|
|[tCt2b8QlMXslGLNM.htm](pathfinder-bestiary-2-items/tCt2b8QlMXslGLNM.htm)|Feel the Blades|
|[TFsDNtIHhwteQUz9.htm](pathfinder-bestiary-2-items/TFsDNtIHhwteQUz9.htm)|Web Sense|
|[tmi63gM84pc6s5qK.htm](pathfinder-bestiary-2-items/tmi63gM84pc6s5qK.htm)|Tongue Grab|
|[TNAO4qk6h9SrlrOP.htm](pathfinder-bestiary-2-items/TNAO4qk6h9SrlrOP.htm)|Sever Fate|
|[TQzV6HYAwG8ru5Jc.htm](pathfinder-bestiary-2-items/TQzV6HYAwG8ru5Jc.htm)|Serpentfolk Venom|
|[TW4FUgjb7YgD89jt.htm](pathfinder-bestiary-2-items/TW4FUgjb7YgD89jt.htm)|Ancestral Guardian|
|[u9uoDYlFsBGEdg0u.htm](pathfinder-bestiary-2-items/u9uoDYlFsBGEdg0u.htm)|Sleep Gas|
|[U9ZtZBoz8oQqwrWa.htm](pathfinder-bestiary-2-items/U9ZtZBoz8oQqwrWa.htm)|Regeneration 15 (deactivated by acid or cold)|
|[UEAEQewstiRA9wbc.htm](pathfinder-bestiary-2-items/UEAEQewstiRA9wbc.htm)|Paralytic Perfection|
|[uh97rOTxqm9aQTwC.htm](pathfinder-bestiary-2-items/uh97rOTxqm9aQTwC.htm)|Consume Berries|
|[uhW4OwaKUkyMz6W3.htm](pathfinder-bestiary-2-items/uhW4OwaKUkyMz6W3.htm)|Capsize|
|[UjEJBgE9VWnbTMRQ.htm](pathfinder-bestiary-2-items/UjEJBgE9VWnbTMRQ.htm)|Negative Healing|
|[ujQKL0Ru853GDYUK.htm](pathfinder-bestiary-2-items/ujQKL0Ru853GDYUK.htm)|Grab|
|[Uoxf8sSuS6pG7QQF.htm](pathfinder-bestiary-2-items/Uoxf8sSuS6pG7QQF.htm)|Pod Spawn|
|[URm0Bj51PHUjgnXS.htm](pathfinder-bestiary-2-items/URm0Bj51PHUjgnXS.htm)|Breath Weapon|
|[usIKpszaYrCWfN8r.htm](pathfinder-bestiary-2-items/usIKpszaYrCWfN8r.htm)|Shadow Breath|
|[UuUlz5A5IwoGhe6n.htm](pathfinder-bestiary-2-items/UuUlz5A5IwoGhe6n.htm)|Frightful Strike|
|[uzaPYD2SNbrlxuM9.htm](pathfinder-bestiary-2-items/uzaPYD2SNbrlxuM9.htm)|Jaws|
|[V4WvWjukxtroI8Xs.htm](pathfinder-bestiary-2-items/V4WvWjukxtroI8Xs.htm)|Swallow Whole|
|[V5c6DTpIF11FCsWw.htm](pathfinder-bestiary-2-items/V5c6DTpIF11FCsWw.htm)|Drain Life|
|[V9cC85qOc8a3vbBc.htm](pathfinder-bestiary-2-items/V9cC85qOc8a3vbBc.htm)|Abyssal Rot|
|[vEMwAfv76TagQTy9.htm](pathfinder-bestiary-2-items/vEMwAfv76TagQTy9.htm)|Hurl Net|
|[vMUHP7DevP54tmz4.htm](pathfinder-bestiary-2-items/vMUHP7DevP54tmz4.htm)|Chameleon Skin|
|[vMYnNlSJnYJepYe8.htm](pathfinder-bestiary-2-items/vMYnNlSJnYJepYe8.htm)|Impaling Barb|
|[VPVWe9JZVLpTixZC.htm](pathfinder-bestiary-2-items/VPVWe9JZVLpTixZC.htm)|Change Shape|
|[VpzTcikdwnBsqtN8.htm](pathfinder-bestiary-2-items/VpzTcikdwnBsqtN8.htm)|Regeneration 2 (deactivated by good or silver)|
|[vVucSfThIT1iEEjO.htm](pathfinder-bestiary-2-items/vVucSfThIT1iEEjO.htm)|Hook Shake|
|[VWC3NNUYpN6qbf09.htm](pathfinder-bestiary-2-items/VWC3NNUYpN6qbf09.htm)|Breath Weapon|
|[W8NWW39lZmTUYk0e.htm](pathfinder-bestiary-2-items/W8NWW39lZmTUYk0e.htm)|Water Sprint|
|[whb2OZIXgagZrUoX.htm](pathfinder-bestiary-2-items/whb2OZIXgagZrUoX.htm)|Corrosive Surface|
|[WHISh35la4f2ULAj.htm](pathfinder-bestiary-2-items/WHISh35la4f2ULAj.htm)|Stunning Chain|
|[wHsOiFn1P7QcaaJ2.htm](pathfinder-bestiary-2-items/wHsOiFn1P7QcaaJ2.htm)|Stolen Identity|
|[WIPFxMzyJ87R3t7k.htm](pathfinder-bestiary-2-items/WIPFxMzyJ87R3t7k.htm)|Supernatural Speed|
|[WjdPeEIa3jf7j8U2.htm](pathfinder-bestiary-2-items/WjdPeEIa3jf7j8U2.htm)|Constrict|
|[WsGuoYeAA0yWHOXE.htm](pathfinder-bestiary-2-items/WsGuoYeAA0yWHOXE.htm)|Pall of Shadow|
|[wTAXkMHuxjJpD8OH.htm](pathfinder-bestiary-2-items/wTAXkMHuxjJpD8OH.htm)|Darkvision|
|[x0HpOyXvVG8ecvWu.htm](pathfinder-bestiary-2-items/x0HpOyXvVG8ecvWu.htm)|Constrict|
|[X8BOiH4KPnC3n94o.htm](pathfinder-bestiary-2-items/X8BOiH4KPnC3n94o.htm)|Regeneration 15 (acid or fire)|
|[XaaNqqz2ZFyt0c4o.htm](pathfinder-bestiary-2-items/XaaNqqz2ZFyt0c4o.htm)|Specious Suggestion|
|[xcs2mYc8h9YshZle.htm](pathfinder-bestiary-2-items/xcs2mYc8h9YshZle.htm)|Attack of Opportunity|
|[xdv7JO5TXVfEGxJr.htm](pathfinder-bestiary-2-items/xdv7JO5TXVfEGxJr.htm)|Tenacious Stance|
|[xgpvgBd8BjAbDY4N.htm](pathfinder-bestiary-2-items/xgpvgBd8BjAbDY4N.htm)|Puddled Ambush|
|[XhhOGnxq5cUHkcEd.htm](pathfinder-bestiary-2-items/XhhOGnxq5cUHkcEd.htm)|Burble|
|[XHjjW9wEW3pVVm5K.htm](pathfinder-bestiary-2-items/XHjjW9wEW3pVVm5K.htm)|Tendriculos Venom|
|[XrJk8sW9W9FE8rtL.htm](pathfinder-bestiary-2-items/XrJk8sW9W9FE8rtL.htm)|Guardian Spirit|
|[XZ1UeBvCwRe6I510.htm](pathfinder-bestiary-2-items/XZ1UeBvCwRe6I510.htm)|Magma Spit|
|[y5b7lQEAyBanNTYQ.htm](pathfinder-bestiary-2-items/y5b7lQEAyBanNTYQ.htm)|Constrict|
|[Y7LW92BfHBvUX6RP.htm](pathfinder-bestiary-2-items/Y7LW92BfHBvUX6RP.htm)|Forfeiture Aversion|
|[yFWK77O4rMtMcEmp.htm](pathfinder-bestiary-2-items/yFWK77O4rMtMcEmp.htm)|Swallow Whole|
|[yHiEUWvnlHVJLlCA.htm](pathfinder-bestiary-2-items/yHiEUWvnlHVJLlCA.htm)|Draining Strike|
|[YJzGe5kr1qCZKV34.htm](pathfinder-bestiary-2-items/YJzGe5kr1qCZKV34.htm)|Magma Scorpion Venom|
|[YkuWnKDR7iQkfOh8.htm](pathfinder-bestiary-2-items/YkuWnKDR7iQkfOh8.htm)|Megalania Venom|
|[yn2c3QvZMZCfu55r.htm](pathfinder-bestiary-2-items/yn2c3QvZMZCfu55r.htm)|Darkvision|
|[yooFGlf4aPfCD1vV.htm](pathfinder-bestiary-2-items/yooFGlf4aPfCD1vV.htm)|Verdant Burst|
|[YS1pxmdE8Ebmyy23.htm](pathfinder-bestiary-2-items/YS1pxmdE8Ebmyy23.htm)|Find Target|
|[ysLsaBnQ2SlAerXO.htm](pathfinder-bestiary-2-items/ysLsaBnQ2SlAerXO.htm)|Fatal Faker|
|[yZCKHSiJwqO7N1sy.htm](pathfinder-bestiary-2-items/yZCKHSiJwqO7N1sy.htm)|Claws That Catch|
|[z3NGGrVV1V5tnFUX.htm](pathfinder-bestiary-2-items/z3NGGrVV1V5tnFUX.htm)|Bodak Spawn|
|[z8CeXCEQdHlFMc0u.htm](pathfinder-bestiary-2-items/z8CeXCEQdHlFMc0u.htm)|Attack of Opportunity|
|[zglT7mSqeYfcswev.htm](pathfinder-bestiary-2-items/zglT7mSqeYfcswev.htm)|Hair Snare|
|[zhobc1PADu4qQ2LB.htm](pathfinder-bestiary-2-items/zhobc1PADu4qQ2LB.htm)|Deep Breath|
|[zKvBvpelIhRXPoSW.htm](pathfinder-bestiary-2-items/zKvBvpelIhRXPoSW.htm)|Thoughtsense|
|[zNeDe02hwHzcyE1s.htm](pathfinder-bestiary-2-items/zNeDe02hwHzcyE1s.htm)|Squirming Embrace|
|[Zttbe7aDN1s4zibB.htm](pathfinder-bestiary-2-items/Zttbe7aDN1s4zibB.htm)|Regeneration 15 (deactivated by chaotic)|
|[zvArmvrursLxbgGq.htm](pathfinder-bestiary-2-items/zvArmvrursLxbgGq.htm)|Gnaw Flesh|
|[ZvUTgfQlDSMlOtEP.htm](pathfinder-bestiary-2-items/ZvUTgfQlDSMlOtEP.htm)|Entangling Tresses|
|[zYVeJXwYqCDo3GsP.htm](pathfinder-bestiary-2-items/zYVeJXwYqCDo3GsP.htm)|Telepathy 100 feet|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[03MMQCPuK2UJR1Z6.htm](pathfinder-bestiary-2-items/03MMQCPuK2UJR1Z6.htm)|Graft Flesh|Greffe de chair|changé|
|[0FwbXedAmkMCm8OR.htm](pathfinder-bestiary-2-items/0FwbXedAmkMCm8OR.htm)|Tremorsense 30 feet|Perception des vibrations|changé|
|[0G6ZKuqCpYAAIPki.htm](pathfinder-bestiary-2-items/0G6ZKuqCpYAAIPki.htm)|Constant Spells|Sorts constants|changé|
|[0uYLHfv1lvXE4qG4.htm](pathfinder-bestiary-2-items/0uYLHfv1lvXE4qG4.htm)|Constant Spells|Sorts constants|changé|
|[1BBbgSS3coktFqQA.htm](pathfinder-bestiary-2-items/1BBbgSS3coktFqQA.htm)|Constant Spells|Sorts constants|changé|
|[2hIYkkGgRCsVdWhY.htm](pathfinder-bestiary-2-items/2hIYkkGgRCsVdWhY.htm)|Constant Spells|Sorts constants|changé|
|[3BSxL95mz6QjoWi1.htm](pathfinder-bestiary-2-items/3BSxL95mz6QjoWi1.htm)|Sneak Attack|Attaque sournoise|changé|
|[49zcmiZjqimM6a7b.htm](pathfinder-bestiary-2-items/49zcmiZjqimM6a7b.htm)|Wrap in Coils|Enrouler les anneaux|changé|
|[4eg3EowtqBauiDtw.htm](pathfinder-bestiary-2-items/4eg3EowtqBauiDtw.htm)|Constant Spells|Sorts constants|changé|
|[4f8Xn8AVpubItk3h.htm](pathfinder-bestiary-2-items/4f8Xn8AVpubItk3h.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[4zBBMGUTIvxbdVBf.htm](pathfinder-bestiary-2-items/4zBBMGUTIvxbdVBf.htm)|Fist|Poing|changé|
|[6A6lheQTnhAryrxG.htm](pathfinder-bestiary-2-items/6A6lheQTnhAryrxG.htm)|Constant Spells|Sorts constants|changé|
|[6mmZWINTcnrrNQcU.htm](pathfinder-bestiary-2-items/6mmZWINTcnrrNQcU.htm)|Constant Spells|Sorts constants|changé|
|[6xKgrVRYAjGEO0p7.htm](pathfinder-bestiary-2-items/6xKgrVRYAjGEO0p7.htm)|Flame Jet|Jet de flammes|changé|
|[7EYorPSwB5MGbm8A.htm](pathfinder-bestiary-2-items/7EYorPSwB5MGbm8A.htm)|Constant Spells|Sorts constants|changé|
|[7wi6tn6G6vLyc1tN.htm](pathfinder-bestiary-2-items/7wi6tn6G6vLyc1tN.htm)|Wrap in Coils|Enrouler les anneaux|changé|
|[8GCV9NHdglsydwmB.htm](pathfinder-bestiary-2-items/8GCV9NHdglsydwmB.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[8og5NF6ejbQcG1bF.htm](pathfinder-bestiary-2-items/8og5NF6ejbQcG1bF.htm)|Rusting Grasp|Poigne de rouille|changé|
|[8RH2spsc79xmzAXn.htm](pathfinder-bestiary-2-items/8RH2spsc79xmzAXn.htm)|Allergen Aura|Aura allergène|changé|
|[A46dQA87Lnn71IzJ.htm](pathfinder-bestiary-2-items/A46dQA87Lnn71IzJ.htm)|Gory Rend|Mutilation sanglante|changé|
|[agTNdZe3k9VHuHav.htm](pathfinder-bestiary-2-items/agTNdZe3k9VHuHav.htm)|Constant Spells|Sorts constants|changé|
|[B3HRE3VYFmesNO4i.htm](pathfinder-bestiary-2-items/B3HRE3VYFmesNO4i.htm)|Scent (Imprecise) 60 feet|Odorat|changé|
|[b9sqjjGGkWII8xxu.htm](pathfinder-bestiary-2-items/b9sqjjGGkWII8xxu.htm)|Mimic Form|Imitation de forme|changé|
|[BfXMGYIBKEZIaKVV.htm](pathfinder-bestiary-2-items/BfXMGYIBKEZIaKVV.htm)|Evisceration|Éviscération|changé|
|[bMypsTuAesTEDTll.htm](pathfinder-bestiary-2-items/bMypsTuAesTEDTll.htm)|Constant Spells|Sorts constants|changé|
|[bR35AECKKuKfjL3N.htm](pathfinder-bestiary-2-items/bR35AECKKuKfjL3N.htm)|Constant Spells|Sorts constants|changé|
|[c1COFBWgk0baYBNq.htm](pathfinder-bestiary-2-items/c1COFBWgk0baYBNq.htm)|Constant Spells|Sorts constants|changé|
|[cE9GHCJxhOHcgxgi.htm](pathfinder-bestiary-2-items/cE9GHCJxhOHcgxgi.htm)|Shattering Ice|Projection de glace|changé|
|[cHiKqfceiHjpXqNg.htm](pathfinder-bestiary-2-items/cHiKqfceiHjpXqNg.htm)|Constant Spells|Sorts constants|changé|
|[CmBRVSLQzMzJ4WUR.htm](pathfinder-bestiary-2-items/CmBRVSLQzMzJ4WUR.htm)|Lifesense 120 feet|Perception de la vie|changé|
|[D89GcjloR9CLNbq8.htm](pathfinder-bestiary-2-items/D89GcjloR9CLNbq8.htm)|Elemental Bulwark|Rempart élémentaire|changé|
|[dfO2vkmoVzinZBdb.htm](pathfinder-bestiary-2-items/dfO2vkmoVzinZBdb.htm)|Constant Spells|Sorts constants|changé|
|[dl10m47AKrgF3eVj.htm](pathfinder-bestiary-2-items/dl10m47AKrgF3eVj.htm)|Pour Down Throat|Avaler de force|changé|
|[dp75b5be8kghUzzi.htm](pathfinder-bestiary-2-items/dp75b5be8kghUzzi.htm)|Ventral Tube|Tube ventral|changé|
|[Dye4kplXn2HWokpZ.htm](pathfinder-bestiary-2-items/Dye4kplXn2HWokpZ.htm)|Constant Spells|Sorts constants|changé|
|[EAsAGjtMDBLWo0ZO.htm](pathfinder-bestiary-2-items/EAsAGjtMDBLWo0ZO.htm)|+2 Status to All Saves vs Mental|Bonus de statut de +1 contre Mental|changé|
|[eLovXAlbDKmZARNK.htm](pathfinder-bestiary-2-items/eLovXAlbDKmZARNK.htm)|Constant Spells|Sorts constants|changé|
|[eOpB0SxDMEU65Oyy.htm](pathfinder-bestiary-2-items/eOpB0SxDMEU65Oyy.htm)|Blood Drain|Absorption de sang|changé|
|[ESiP5kSLjg2exhAh.htm](pathfinder-bestiary-2-items/ESiP5kSLjg2exhAh.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[fBpvD0xb43MGDESG.htm](pathfinder-bestiary-2-items/fBpvD0xb43MGDESG.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[fDOQq8mfa2PhlHcF.htm](pathfinder-bestiary-2-items/fDOQq8mfa2PhlHcF.htm)|Bore into Brain|Forage de cerveau|changé|
|[FhwufRLfw0dtrKFI.htm](pathfinder-bestiary-2-items/FhwufRLfw0dtrKFI.htm)|Kneecapper|Briseur de genoux|changé|
|[gbsLwd2FBQal3xdZ.htm](pathfinder-bestiary-2-items/gbsLwd2FBQal3xdZ.htm)|Sneak Attack|Attaque sournoise|changé|
|[ghbtjQahExs1AvpE.htm](pathfinder-bestiary-2-items/ghbtjQahExs1AvpE.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[GHcUVfhfs6mcm7mS.htm](pathfinder-bestiary-2-items/GHcUVfhfs6mcm7mS.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[GimuDrJ7GoLAne8i.htm](pathfinder-bestiary-2-items/GimuDrJ7GoLAne8i.htm)|Avoid the Swat|Éviter les coups|changé|
|[GL5oMe9C6pcXxqcU.htm](pathfinder-bestiary-2-items/GL5oMe9C6pcXxqcU.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[GtJ8PtIPDvGSAIHU.htm](pathfinder-bestiary-2-items/GtJ8PtIPDvGSAIHU.htm)|Sneak Attack|Attaque sournoise|changé|
|[guUOsE03Ck5JEoBu.htm](pathfinder-bestiary-2-items/guUOsE03Ck5JEoBu.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[gVLqh4wDw9v6njkz.htm](pathfinder-bestiary-2-items/gVLqh4wDw9v6njkz.htm)|Jaws|Mâchoires|changé|
|[Gz3flOxBg3X75q6s.htm](pathfinder-bestiary-2-items/Gz3flOxBg3X75q6s.htm)|Constant Spells|Sorts constants|changé|
|[HqwhBLhYDbCBbv8f.htm](pathfinder-bestiary-2-items/HqwhBLhYDbCBbv8f.htm)|Constant Spells|Sorts constants|changé|
|[Hu5L9AYzwJsXxIDY.htm](pathfinder-bestiary-2-items/Hu5L9AYzwJsXxIDY.htm)|Tremorsense 30 feet|Perception des vibrations|changé|
|[hwXxwT85s7KyKoWq.htm](pathfinder-bestiary-2-items/hwXxwT85s7KyKoWq.htm)|Lifesense 30 feet|Perception de la vie|changé|
|[HXybLZsE68ejwZbJ.htm](pathfinder-bestiary-2-items/HXybLZsE68ejwZbJ.htm)|Limb Extension|Extension de membres|changé|
|[hychIP30Jt5HOYED.htm](pathfinder-bestiary-2-items/hychIP30Jt5HOYED.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[I1XiJaJSXxln1l7V.htm](pathfinder-bestiary-2-items/I1XiJaJSXxln1l7V.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[i8TVY4kXRvQIaLLG.htm](pathfinder-bestiary-2-items/i8TVY4kXRvQIaLLG.htm)|Pollen Touch|Contact pollinifère|changé|
|[IM4bochbTpbrXxfY.htm](pathfinder-bestiary-2-items/IM4bochbTpbrXxfY.htm)|Sneak Attack|Attaque sournoise|changé|
|[iRZgPtkf6Lw3MzYm.htm](pathfinder-bestiary-2-items/iRZgPtkf6Lw3MzYm.htm)|Constant Spells|Sorts constants|changé|
|[jGhebYMT6hWNMixj.htm](pathfinder-bestiary-2-items/jGhebYMT6hWNMixj.htm)|Constant Spells|Sorts constants|changé|
|[JtEaK5YV5AgypBAF.htm](pathfinder-bestiary-2-items/JtEaK5YV5AgypBAF.htm)|Change Shape|Changement de forme|changé|
|[K5BMaVGszyeB9sm8.htm](pathfinder-bestiary-2-items/K5BMaVGszyeB9sm8.htm)|Constant Spells|Sorts constants|changé|
|[K80GtqxqKN7Og68w.htm](pathfinder-bestiary-2-items/K80GtqxqKN7Og68w.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[kkVojFishI8AxnVn.htm](pathfinder-bestiary-2-items/kkVojFishI8AxnVn.htm)|Slime Rot|Pourriture vaseuse|changé|
|[kO1NYbSXz3VNkr4L.htm](pathfinder-bestiary-2-items/kO1NYbSXz3VNkr4L.htm)|Claw|Griffes|changé|
|[ktdazJyPHLCoWXur.htm](pathfinder-bestiary-2-items/ktdazJyPHLCoWXur.htm)|Sneak Attack|Attaque sournoise|changé|
|[laXpsxFAPFs8EXkA.htm](pathfinder-bestiary-2-items/laXpsxFAPFs8EXkA.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[lhaBLbyRbWQceU9t.htm](pathfinder-bestiary-2-items/lhaBLbyRbWQceU9t.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[LVDdXXZ03SwDUNZ8.htm](pathfinder-bestiary-2-items/LVDdXXZ03SwDUNZ8.htm)|Jaws|Mâchoires|changé|
|[ma50pIyMGEpOtjhi.htm](pathfinder-bestiary-2-items/ma50pIyMGEpOtjhi.htm)|Darkvision|Vision dans le noir|changé|
|[MdeM9TcCMZeHBGEs.htm](pathfinder-bestiary-2-items/MdeM9TcCMZeHBGEs.htm)|Constant Spells|Sorts constants|changé|
|[MeT5lDeI05fBDoW3.htm](pathfinder-bestiary-2-items/MeT5lDeI05fBDoW3.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[MH7wNM2dBDOtmzVO.htm](pathfinder-bestiary-2-items/MH7wNM2dBDOtmzVO.htm)|Sneak Attack|Attaque sournoise|changé|
|[myXt3SwrT2L8VMW4.htm](pathfinder-bestiary-2-items/myXt3SwrT2L8VMW4.htm)|Stygian Guardian|Gardien stygien|changé|
|[nBbFutvNlcdf1cgZ.htm](pathfinder-bestiary-2-items/nBbFutvNlcdf1cgZ.htm)|Drown|Noyade|changé|
|[NDQPoKh0SP7tR1Ss.htm](pathfinder-bestiary-2-items/NDQPoKh0SP7tR1Ss.htm)|Tentacle Stab|Tentacule poignardeur|changé|
|[nQV7pNdpGD2GDrgl.htm](pathfinder-bestiary-2-items/nQV7pNdpGD2GDrgl.htm)|Sneak Attack|Attaque sournoise|changé|
|[oE4IMiBnWcfVyg7X.htm](pathfinder-bestiary-2-items/oE4IMiBnWcfVyg7X.htm)|Scent (Imprecise) 30 feet|Odorat|changé|
|[oPWhso0y4ezQpGaJ.htm](pathfinder-bestiary-2-items/oPWhso0y4ezQpGaJ.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[P0VFneuZMPCfZ35e.htm](pathfinder-bestiary-2-items/P0VFneuZMPCfZ35e.htm)|Constant Spells|Sorts constants|changé|
|[pmfr4MB1abUN5RbW.htm](pathfinder-bestiary-2-items/pmfr4MB1abUN5RbW.htm)|Feed|Alimentation|changé|
|[PMVviZt2FjDHt7QV.htm](pathfinder-bestiary-2-items/PMVviZt2FjDHt7QV.htm)|Telepathy 100 feet|Télépathie|changé|
|[psRycYQDP439XLGm.htm](pathfinder-bestiary-2-items/psRycYQDP439XLGm.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes|changé|
|[pvqYJHUkKVxUx3Q2.htm](pathfinder-bestiary-2-items/pvqYJHUkKVxUx3Q2.htm)|Claw|Griffe|changé|
|[QB6MLb6aO9rvCPbS.htm](pathfinder-bestiary-2-items/QB6MLb6aO9rvCPbS.htm)|Constant Spells|Sorts constants|changé|
|[Qhdl7tHPD3bhAihm.htm](pathfinder-bestiary-2-items/Qhdl7tHPD3bhAihm.htm)|Lash Out|Frappe fulgurante|changé|
|[QItaAvzvKUUHn205.htm](pathfinder-bestiary-2-items/QItaAvzvKUUHn205.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[QNvBVG4wk0atHVo9.htm](pathfinder-bestiary-2-items/QNvBVG4wk0atHVo9.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[QQgEGkDjynHqQ53g.htm](pathfinder-bestiary-2-items/QQgEGkDjynHqQ53g.htm)|Constant Spells|Sorts constants|changé|
|[qu9kOE72tZH2eGQ1.htm](pathfinder-bestiary-2-items/qu9kOE72tZH2eGQ1.htm)|Constant Spells|Sorts constants|changé|
|[QZup3O3zcKH6TFZd.htm](pathfinder-bestiary-2-items/QZup3O3zcKH6TFZd.htm)|Sneak Attack|Attaque sournoise|changé|
|[RLJbJxbajEsFLapw.htm](pathfinder-bestiary-2-items/RLJbJxbajEsFLapw.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[RMCsutKfNLtikzWK.htm](pathfinder-bestiary-2-items/RMCsutKfNLtikzWK.htm)|Constant Spells|Sorts constants|changé|
|[suaF5gRPb1TibfpQ.htm](pathfinder-bestiary-2-items/suaF5gRPb1TibfpQ.htm)|Hooked|Hammeçonné|changé|
|[tlcUEtC6qZlc69zv.htm](pathfinder-bestiary-2-items/tlcUEtC6qZlc69zv.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[tqSmdxFVDmY2CLns.htm](pathfinder-bestiary-2-items/tqSmdxFVDmY2CLns.htm)|Heavy Aura|Aura pesante|changé|
|[TYoj6whlYb3mxSVJ.htm](pathfinder-bestiary-2-items/TYoj6whlYb3mxSVJ.htm)|Constant Spells|Sorts constants|changé|
|[U5OMtZB6VwyAnL04.htm](pathfinder-bestiary-2-items/U5OMtZB6VwyAnL04.htm)|Constant Spells|Sorts constants|changé|
|[U69lb7BfCPLfTsfG.htm](pathfinder-bestiary-2-items/U69lb7BfCPLfTsfG.htm)|Attack of Opportunity (Tail Only)|Attaque d'opportunité|changé|
|[uIPRSgEue1olkirr.htm](pathfinder-bestiary-2-items/uIPRSgEue1olkirr.htm)|Constant Spells|Sorts constants|changé|
|[UpD8OqCE3oJ41eCz.htm](pathfinder-bestiary-2-items/UpD8OqCE3oJ41eCz.htm)|Constant Spells|Sorts constants|changé|
|[uQDUAoudAP9pgxja.htm](pathfinder-bestiary-2-items/uQDUAoudAP9pgxja.htm)|Soul Crush|Broyage d’âme|changé|
|[vmyBkhvE34HW6vpl.htm](pathfinder-bestiary-2-items/vmyBkhvE34HW6vpl.htm)|Glowing Mucus|Mucus luminescent|changé|
|[vPzVhI5rLf9ZTI5N.htm](pathfinder-bestiary-2-items/vPzVhI5rLf9ZTI5N.htm)|Constant Spells|Sorts constants|changé|
|[VyJyNz5V3NR5Gylr.htm](pathfinder-bestiary-2-items/VyJyNz5V3NR5Gylr.htm)|Axe Vulnerability 5|Vulnérabilité aux haches|changé|
|[vYo1emHFZe2lLLBo.htm](pathfinder-bestiary-2-items/vYo1emHFZe2lLLBo.htm)|Constant Spells|Sorts constants|changé|
|[w0XMZ6GmkzJcCRyO.htm](pathfinder-bestiary-2-items/w0XMZ6GmkzJcCRyO.htm)|Piercing Shriek|Cri perçant|changé|
|[W1abGwpcG9yJOB9j.htm](pathfinder-bestiary-2-items/W1abGwpcG9yJOB9j.htm)|Constant Spells|Sorts constants|changé|
|[w7tx24OC6vP2T7De.htm](pathfinder-bestiary-2-items/w7tx24OC6vP2T7De.htm)|Spray Pollen|Projection de pollen|changé|
|[wEzrLhyzfEwcfMlc.htm](pathfinder-bestiary-2-items/wEzrLhyzfEwcfMlc.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[WJHihy42QBNSUonW.htm](pathfinder-bestiary-2-items/WJHihy42QBNSUonW.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[wrisTXltKN2ggF1B.htm](pathfinder-bestiary-2-items/wrisTXltKN2ggF1B.htm)|Mandragora Venom|Venin de mandragore|changé|
|[WVChw6DFgtcfsrAf.htm](pathfinder-bestiary-2-items/WVChw6DFgtcfsrAf.htm)|Swarming Beaks|Déluge de becs|changé|
|[Wyf2nzKbV2LhVCMO.htm](pathfinder-bestiary-2-items/Wyf2nzKbV2LhVCMO.htm)|Scent (Imprecise) 60 feet|Odorat|changé|
|[x4vlJSFL6WnsbK82.htm](pathfinder-bestiary-2-items/x4vlJSFL6WnsbK82.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|changé|
|[XCr9jxxE59Egb2R1.htm](pathfinder-bestiary-2-items/XCr9jxxE59Egb2R1.htm)|Woodland Ambush|Embuscade sylvestre|changé|
|[xx6zXcdGLkuW5RmE.htm](pathfinder-bestiary-2-items/xx6zXcdGLkuW5RmE.htm)|Flooding Thrust|Injection d'eau|changé|
|[y0g8S6A5Uri1G8xe.htm](pathfinder-bestiary-2-items/y0g8S6A5Uri1G8xe.htm)|Drill|Foreuse|changé|
|[y7wcFxhYodNrBPwL.htm](pathfinder-bestiary-2-items/y7wcFxhYodNrBPwL.htm)|Wraith Spawn|Rejeton d'âme-en-peine|changé|
|[yKM6d3CSPA4ymKUk.htm](pathfinder-bestiary-2-items/yKM6d3CSPA4ymKUk.htm)|Constant Spells|Sorts constants|changé|
|[yW0bi5z1ngsdOjTv.htm](pathfinder-bestiary-2-items/yW0bi5z1ngsdOjTv.htm)|Constant Spells|Sorts constants|changé|
|[YwEDGRZpJYEj2PKw.htm](pathfinder-bestiary-2-items/YwEDGRZpJYEj2PKw.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[Z4dJO56WCJUBTpyf.htm](pathfinder-bestiary-2-items/Z4dJO56WCJUBTpyf.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|changé|
|[ZD69ngyFRQ3FHnUr.htm](pathfinder-bestiary-2-items/ZD69ngyFRQ3FHnUr.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegardes contre la magie|changé|
|[zp27Hiz99lnmpDcm.htm](pathfinder-bestiary-2-items/zp27Hiz99lnmpDcm.htm)|Constant Spells|Sorts constants|changé|
|[zVVCyjKNSDxZD2Oy.htm](pathfinder-bestiary-2-items/zVVCyjKNSDxZD2Oy.htm)|Grabbing Trunk|Trompe préhensile|changé|

## Liste des éléments vides ne pouvant pas être traduits

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[8qNsivFnCww0jFZo.htm](pathfinder-bestiary-2-items/8qNsivFnCww0jFZo.htm)|Electric Missile|vide|
|[AHglfvlj1P2w1uM8.htm](pathfinder-bestiary-2-items/AHglfvlj1P2w1uM8.htm)|Nightmare Tendril|vide|
|[AySAWmXmOwHrjkuG.htm](pathfinder-bestiary-2-items/AySAWmXmOwHrjkuG.htm)|Vine|vide|
|[bOe2DBNsKt2ZttfX.htm](pathfinder-bestiary-2-items/bOe2DBNsKt2ZttfX.htm)|Spectral Hand|vide|
|[c74H6Oxpa5sZzsN5.htm](pathfinder-bestiary-2-items/c74H6Oxpa5sZzsN5.htm)|+4 Status Bonus on Saves vs Mental|vide|
|[CdF08U1FHM2c2DIB.htm](pathfinder-bestiary-2-items/CdF08U1FHM2c2DIB.htm)|Athletics|vide|
|[e3XyV4HjxKBZucFh.htm](pathfinder-bestiary-2-items/e3XyV4HjxKBZucFh.htm)|Stealth|vide|
|[EPlZiO8molN6azkO.htm](pathfinder-bestiary-2-items/EPlZiO8molN6azkO.htm)|Spittle|vide|
|[g56aPVqelhyIjRuv.htm](pathfinder-bestiary-2-items/g56aPVqelhyIjRuv.htm)|+4 Status to All Saves vs Sonic|vide|
|[giVESjdTpw33NygU.htm](pathfinder-bestiary-2-items/giVESjdTpw33NygU.htm)|Norn Shears|vide|
|[ihi2vOTXGjRtjggj.htm](pathfinder-bestiary-2-items/ihi2vOTXGjRtjggj.htm)|Performance|vide|
|[ii5QAwwyYnQHUwH2.htm](pathfinder-bestiary-2-items/ii5QAwwyYnQHUwH2.htm)|Athletics|vide|
|[ISU9cvmrd38iqIo6.htm](pathfinder-bestiary-2-items/ISU9cvmrd38iqIo6.htm)|Kimono|vide|
|[jaF864bUYkmqmrC8.htm](pathfinder-bestiary-2-items/jaF864bUYkmqmrC8.htm)|Stealth|vide|
|[jICQ6Y5JgzCUZXC3.htm](pathfinder-bestiary-2-items/jICQ6Y5JgzCUZXC3.htm)|Hurled Barb|vide|
|[jsZXrLmJJQhLrRL0.htm](pathfinder-bestiary-2-items/jsZXrLmJJQhLrRL0.htm)|Stealth|vide|
|[k8MjR75C6SWEa6gd.htm](pathfinder-bestiary-2-items/k8MjR75C6SWEa6gd.htm)|Flytrap Mouth|vide|
|[k8sBT0SGwUD3ZUuE.htm](pathfinder-bestiary-2-items/k8sBT0SGwUD3ZUuE.htm)|+2 Status to All Saves vs Divine Magic|vide|
|[KF8TJluc2GTgVJhl.htm](pathfinder-bestiary-2-items/KF8TJluc2GTgVJhl.htm)|Paddle|vide|
|[kHGiew7m0qu0wSCx.htm](pathfinder-bestiary-2-items/kHGiew7m0qu0wSCx.htm)|Stealth|vide|
|[kk0U4TyvtgBAzAAR.htm](pathfinder-bestiary-2-items/kk0U4TyvtgBAzAAR.htm)|Stealth|vide|
|[KwNXLXzskiVB9RLh.htm](pathfinder-bestiary-2-items/KwNXLXzskiVB9RLh.htm)|Stealth|vide|
|[KxJ1GZ31oVRmv1XD.htm](pathfinder-bestiary-2-items/KxJ1GZ31oVRmv1XD.htm)|+1 Status Bonus on Saves vs Positive|vide|
|[L3qJCQDZiZu5wjEW.htm](pathfinder-bestiary-2-items/L3qJCQDZiZu5wjEW.htm)|Stealth|vide|
|[l6mRpAGFCZ7gTIb1.htm](pathfinder-bestiary-2-items/l6mRpAGFCZ7gTIb1.htm)|Rasping Tongue|vide|
|[lflIfQcsvaJJEVL7.htm](pathfinder-bestiary-2-items/lflIfQcsvaJJEVL7.htm)|Proboscis|vide|
|[LkKOdtSgJydv1rSu.htm](pathfinder-bestiary-2-items/LkKOdtSgJydv1rSu.htm)|Flytrap Hand|vide|
|[mLaikYbAsRTru36r.htm](pathfinder-bestiary-2-items/mLaikYbAsRTru36r.htm)|Arms|vide|
|[mNQAWCGl6DU8cQUP.htm](pathfinder-bestiary-2-items/mNQAWCGl6DU8cQUP.htm)|Jagged Jaws|vide|
|[NEQLZhd63z6qQPXJ.htm](pathfinder-bestiary-2-items/NEQLZhd63z6qQPXJ.htm)|Stealth|vide|
|[neZCdGIJBKYWKN5Q.htm](pathfinder-bestiary-2-items/neZCdGIJBKYWKN5Q.htm)|Deception|vide|
|[nuV1GkHwu5pqVXnI.htm](pathfinder-bestiary-2-items/nuV1GkHwu5pqVXnI.htm)|Barb|vide|
|[nWSidIDFacSNApv8.htm](pathfinder-bestiary-2-items/nWSidIDFacSNApv8.htm)|Stealth|vide|
|[quht5jPh2LiFjJft.htm](pathfinder-bestiary-2-items/quht5jPh2LiFjJft.htm)|+1 Status to All Saves vs Magic|vide|
|[qWgsbVHkbEipjoS7.htm](pathfinder-bestiary-2-items/qWgsbVHkbEipjoS7.htm)|+2 Status Bonus on Saves vs Divine Magic|vide|
|[SaS7qHeGtLYWa1rX.htm](pathfinder-bestiary-2-items/SaS7qHeGtLYWa1rX.htm)|Tail Claw|vide|
|[SlJZiCxXuBW155d5.htm](pathfinder-bestiary-2-items/SlJZiCxXuBW155d5.htm)|Athletics|vide|
|[Sv1H3JJKwu64RfI1.htm](pathfinder-bestiary-2-items/Sv1H3JJKwu64RfI1.htm)|Athletics|vide|
|[TZThNzsi8RUQFhrT.htm](pathfinder-bestiary-2-items/TZThNzsi8RUQFhrT.htm)|Stealth|vide|
|[u6kTO6HI4KtrGtds.htm](pathfinder-bestiary-2-items/u6kTO6HI4KtrGtds.htm)|+1 Status to All Saves vs Magic|vide|
|[uLuSAexGwbZSdlkF.htm](pathfinder-bestiary-2-items/uLuSAexGwbZSdlkF.htm)|Black Flame Knife|vide|
|[UPUXBFdc12OlpoBV.htm](pathfinder-bestiary-2-items/UPUXBFdc12OlpoBV.htm)|Gaff|vide|
|[ViIeviMg3Zbi0PdS.htm](pathfinder-bestiary-2-items/ViIeviMg3Zbi0PdS.htm)|Crafting|vide|
|[wmiM8DltOKYLxzz0.htm](pathfinder-bestiary-2-items/wmiM8DltOKYLxzz0.htm)|Stealth|vide|
|[x1avaIq02Cr7LmNs.htm](pathfinder-bestiary-2-items/x1avaIq02Cr7LmNs.htm)|+2 Status Bonus on Saves vs Enchantment and Illusion Effects|vide|
|[XhhI5NeGekRv5yOP.htm](pathfinder-bestiary-2-items/XhhI5NeGekRv5yOP.htm)|Stealth|vide|
|[XqbsPpZkRYLwrQDg.htm](pathfinder-bestiary-2-items/XqbsPpZkRYLwrQDg.htm)|Tresses|vide|
|[yH19s0CEMs71KZgv.htm](pathfinder-bestiary-2-items/yH19s0CEMs71KZgv.htm)|+1 Status to All Saves vs Positive|vide|

## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00kCy3dtoRU8EnP9.htm](pathfinder-bestiary-2-items/00kCy3dtoRU8EnP9.htm)|Longspear|Pique|libre|
|[00zSyT0a2tdDQnmZ.htm](pathfinder-bestiary-2-items/00zSyT0a2tdDQnmZ.htm)|Low-Light Vision|Vision nocturne|libre|
|[05N8sEltz4b9htCX.htm](pathfinder-bestiary-2-items/05N8sEltz4b9htCX.htm)|Dagger|Dague|libre|
|[072UUA8WXrWV6tPc.htm](pathfinder-bestiary-2-items/072UUA8WXrWV6tPc.htm)|Horn|Corne|libre|
|[090u49BTBz9TdQi7.htm](pathfinder-bestiary-2-items/090u49BTBz9TdQi7.htm)|Regeneration 15 (deactivated by cold iron)|Régénération 15 (désactivée par le fer froid)|officielle|
|[0a9bxpI79rMpqyXh.htm](pathfinder-bestiary-2-items/0a9bxpI79rMpqyXh.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[0BVpj3wkSrFlhgdK.htm](pathfinder-bestiary-2-items/0BVpj3wkSrFlhgdK.htm)|Agile Swimmer|Nageuse agile|officielle|
|[0D7BuUymOeEx7VEM.htm](pathfinder-bestiary-2-items/0D7BuUymOeEx7VEM.htm)|Athletics|Athlétisme|libre|
|[0DDWtKSDSWdXnkgj.htm](pathfinder-bestiary-2-items/0DDWtKSDSWdXnkgj.htm)|Bloodfire Fever|Fièvre du sang chaud|officielle|
|[0dNqhk5KI3nAaTlu.htm](pathfinder-bestiary-2-items/0dNqhk5KI3nAaTlu.htm)|Demon Hunter|Chasseur de démon|officielle|
|[0DOPtcRBzrEKku1I.htm](pathfinder-bestiary-2-items/0DOPtcRBzrEKku1I.htm)|Cloud Walk|Marche sur les nuages|officielle|
|[0DR36G32rpPd9PIN.htm](pathfinder-bestiary-2-items/0DR36G32rpPd9PIN.htm)|Whispered Despair|Désespoir susurré|officielle|
|[0dTEwPV9TXqDzFno.htm](pathfinder-bestiary-2-items/0dTEwPV9TXqDzFno.htm)|Soulsense|Perception de l'âme|officielle|
|[0DWjvOoTNOT0cP4O.htm](pathfinder-bestiary-2-items/0DWjvOoTNOT0cP4O.htm)|Stealth|Discrétion|libre|
|[0eRwevoxA5zdvqb9.htm](pathfinder-bestiary-2-items/0eRwevoxA5zdvqb9.htm)|Breath Weapon|Arme de souffle|officielle|
|[0F96cEU48FezVgKB.htm](pathfinder-bestiary-2-items/0F96cEU48FezVgKB.htm)|Bite|Morsure|officielle|
|[0gNZbOZYyi010Pow.htm](pathfinder-bestiary-2-items/0gNZbOZYyi010Pow.htm)|Curse of Stolen Breath|Malédiction du souffle volé|officielle|
|[0hkOF2WHkLhHAEjQ.htm](pathfinder-bestiary-2-items/0hkOF2WHkLhHAEjQ.htm)|Descend on a Web|Descendre sur un fil de toile|officielle|
|[0IfzoP16zRxLRIJ3.htm](pathfinder-bestiary-2-items/0IfzoP16zRxLRIJ3.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[0IwQxSLxEqwPcB3q.htm](pathfinder-bestiary-2-items/0IwQxSLxEqwPcB3q.htm)|Steam Vision|Vision malgré la vapeur|officielle|
|[0jpjkgun2zt97YLL.htm](pathfinder-bestiary-2-items/0jpjkgun2zt97YLL.htm)|Deceptive Reposition|Feinte de repositionnement|officielle|
|[0llDJD2BXJrr95lm.htm](pathfinder-bestiary-2-items/0llDJD2BXJrr95lm.htm)|Smoke Form|Forme de fumée|officielle|
|[0mzc60K0kDFbeLL4.htm](pathfinder-bestiary-2-items/0mzc60K0kDFbeLL4.htm)|Fast Swallow|Gober rapidement|officielle|
|[0NKzpP896UuTepW0.htm](pathfinder-bestiary-2-items/0NKzpP896UuTepW0.htm)|Jaws|Mâchoires|libre|
|[0Nlywj3cWzzzNsFd.htm](pathfinder-bestiary-2-items/0Nlywj3cWzzzNsFd.htm)|Bastard Sword|Épée bâtarde|libre|
|[0Q0S3Hd1xOjt3Ked.htm](pathfinder-bestiary-2-items/0Q0S3Hd1xOjt3Ked.htm)|Low-Light Vision|Vision nocturne|libre|
|[0q3s792vZI0wANuc.htm](pathfinder-bestiary-2-items/0q3s792vZI0wANuc.htm)|Tentacle|Tentacule|libre|
|[0QCPtkVtKzuuVVaK.htm](pathfinder-bestiary-2-items/0QCPtkVtKzuuVVaK.htm)|Bladed Limb|Membre tranchant|libre|
|[0s0zoTqBFyl1ZTjC.htm](pathfinder-bestiary-2-items/0s0zoTqBFyl1ZTjC.htm)|Frightful Presence|Présence terrifiante|officielle|
|[0s7Elyqvdpp0ck2s.htm](pathfinder-bestiary-2-items/0s7Elyqvdpp0ck2s.htm)|Darkvision|Vision dans le noir|libre|
|[0SmjbTrLS51rwGcc.htm](pathfinder-bestiary-2-items/0SmjbTrLS51rwGcc.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[0TCobRYKQCosWffG.htm](pathfinder-bestiary-2-items/0TCobRYKQCosWffG.htm)|Claw|Griffe|libre|
|[0ToPGrofmnFDeqOe.htm](pathfinder-bestiary-2-items/0ToPGrofmnFDeqOe.htm)|Slithering Attack|Attaque reptante|officielle|
|[0UKsJtM3fEth21iR.htm](pathfinder-bestiary-2-items/0UKsJtM3fEth21iR.htm)|Lifesense|Perception de la vie|libre|
|[0USgUg4kKiE4rcKV.htm](pathfinder-bestiary-2-items/0USgUg4kKiE4rcKV.htm)|Club|Gourdin|libre|
|[0uU20iNCnRv4spmg.htm](pathfinder-bestiary-2-items/0uU20iNCnRv4spmg.htm)|Darkvision|Vision dans le noir|libre|
|[0v7H9KCVbUUdkdU7.htm](pathfinder-bestiary-2-items/0v7H9KCVbUUdkdU7.htm)|Cloud Form|Forme de nuage|officielle|
|[0vByzvft4IIwjZh2.htm](pathfinder-bestiary-2-items/0vByzvft4IIwjZh2.htm)|At-Will Spells|Sorts à volonté|libre|
|[0vrX1z0w3GFCktsF.htm](pathfinder-bestiary-2-items/0vrX1z0w3GFCktsF.htm)|Darkvision|Vision dans le noir|libre|
|[0wGgpxEStAnmfi0k.htm](pathfinder-bestiary-2-items/0wGgpxEStAnmfi0k.htm)|Foot|Pied|libre|
|[0xGnS3VP3WvvpnSI.htm](pathfinder-bestiary-2-items/0xGnS3VP3WvvpnSI.htm)|Darkvision|Vision dans le noir|libre|
|[0XjG2HkUAja2e7pE.htm](pathfinder-bestiary-2-items/0XjG2HkUAja2e7pE.htm)|Jaws|Mâchoires|libre|
|[0Y47YuHl29LzryME.htm](pathfinder-bestiary-2-items/0Y47YuHl29LzryME.htm)|Trunk|Trompe|libre|
|[109W7aPnwhNHkrHE.htm](pathfinder-bestiary-2-items/109W7aPnwhNHkrHE.htm)|Claw|Griffe|libre|
|[12e8csvZLJdg2OS8.htm](pathfinder-bestiary-2-items/12e8csvZLJdg2OS8.htm)|Darkvision|Vision dans le noir|libre|
|[19Ayra6rX2M49iPG.htm](pathfinder-bestiary-2-items/19Ayra6rX2M49iPG.htm)|Feed on Blood|Se nourrir de sang|officielle|
|[19bztZWgNzgUNmTs.htm](pathfinder-bestiary-2-items/19bztZWgNzgUNmTs.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[1A17VJOGWG0BFypA.htm](pathfinder-bestiary-2-items/1A17VJOGWG0BFypA.htm)|Club|Gourdin|libre|
|[1abnSJGdHEvG6Lyl.htm](pathfinder-bestiary-2-items/1abnSJGdHEvG6Lyl.htm)|Bramble Jump|Saut dans les ronces|officielle|
|[1BD5jRohvhbGdDwq.htm](pathfinder-bestiary-2-items/1BD5jRohvhbGdDwq.htm)|Fangs|Crocs|libre|
|[1BrHXN7JmOnmyeOk.htm](pathfinder-bestiary-2-items/1BrHXN7JmOnmyeOk.htm)|Grab|Empoignade/Agrippement|libre|
|[1ChXT4DL7N5JhHEq.htm](pathfinder-bestiary-2-items/1ChXT4DL7N5JhHEq.htm)|Tail|Queue|libre|
|[1deHNHPL9laFtZgR.htm](pathfinder-bestiary-2-items/1deHNHPL9laFtZgR.htm)|Tremorsense|Perception des vibrations|libre|
|[1DfgmZUiWL8m4tiP.htm](pathfinder-bestiary-2-items/1DfgmZUiWL8m4tiP.htm)|Low-Light Vision|Vision nocturne|libre|
|[1eb07HNvWyOOELI3.htm](pathfinder-bestiary-2-items/1eb07HNvWyOOELI3.htm)|Darkvision|Vision dans le noir|libre|
|[1ENGsx3BESdgdHnk.htm](pathfinder-bestiary-2-items/1ENGsx3BESdgdHnk.htm)|Lightning Drinker|Buveur de foudre|officielle|
|[1FR8UtVfDAC3RoWh.htm](pathfinder-bestiary-2-items/1FR8UtVfDAC3RoWh.htm)|Mortasheen|Mortéclat|officielle|
|[1fTRsJAGDuFgPeXF.htm](pathfinder-bestiary-2-items/1fTRsJAGDuFgPeXF.htm)|Earth Glide|Glissement de terrain|officielle|
|[1gu2mF6EgXl6v3eM.htm](pathfinder-bestiary-2-items/1gu2mF6EgXl6v3eM.htm)|Crafting|Artisanat|libre|
|[1IvCoMHs0qGTlw8J.htm](pathfinder-bestiary-2-items/1IvCoMHs0qGTlw8J.htm)|Ripping Gaze|Regard lacérant|officielle|
|[1Kmvtn64DZibqUls.htm](pathfinder-bestiary-2-items/1Kmvtn64DZibqUls.htm)|Elemental Assault|Assaut élémentaire|officielle|
|[1LmOy2NMwi2RqSKT.htm](pathfinder-bestiary-2-items/1LmOy2NMwi2RqSKT.htm)|Golem Antimagic|Antimagie golem|libre|
|[1lSOq5grKxoZhTWw.htm](pathfinder-bestiary-2-items/1lSOq5grKxoZhTWw.htm)|Constant Spells|Sorts constants|libre|
|[1lYZ7v9sRWUarCls.htm](pathfinder-bestiary-2-items/1lYZ7v9sRWUarCls.htm)|Wing Deflection|Déflection de l'aile|officielle|
|[1M2I2905EVmXay9H.htm](pathfinder-bestiary-2-items/1M2I2905EVmXay9H.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[1qBTVbPZxVREvpnT.htm](pathfinder-bestiary-2-items/1qBTVbPZxVREvpnT.htm)|Telepathy (Touch only)|Télépathie (Contact seulement)|libre|
|[1RA0WbiyBjX7Iegz.htm](pathfinder-bestiary-2-items/1RA0WbiyBjX7Iegz.htm)|Darkvision|Vision dans le noir|libre|
|[1RcwWRNvn63MNFhB.htm](pathfinder-bestiary-2-items/1RcwWRNvn63MNFhB.htm)|Darkvision|Vision dans le noir|libre|
|[1Re8gJ0YTH6TxfLa.htm](pathfinder-bestiary-2-items/1Re8gJ0YTH6TxfLa.htm)|Darkvision|Vision dans le noir|libre|
|[1RpmI2FY6QNDsOVh.htm](pathfinder-bestiary-2-items/1RpmI2FY6QNDsOVh.htm)|Scent|Odorat|libre|
|[1UzzAnKlRhYDkzDO.htm](pathfinder-bestiary-2-items/1UzzAnKlRhYDkzDO.htm)|Telepathy|Télépathie|libre|
|[1V1IFVEwiJA8p72B.htm](pathfinder-bestiary-2-items/1V1IFVEwiJA8p72B.htm)|Baleful Glow|Lueur funeste|officielle|
|[1w9sOvRdSaIEpLNe.htm](pathfinder-bestiary-2-items/1w9sOvRdSaIEpLNe.htm)|Tail|Queue|libre|
|[1ynGuwCvWvX0EOiE.htm](pathfinder-bestiary-2-items/1ynGuwCvWvX0EOiE.htm)|Frightful Presence|Présence terrifiante|officielle|
|[1yoCf7iLNmn7SD5D.htm](pathfinder-bestiary-2-items/1yoCf7iLNmn7SD5D.htm)|Stealth|Discrétion|libre|
|[1Z3FZ39brRJH2cIs.htm](pathfinder-bestiary-2-items/1Z3FZ39brRJH2cIs.htm)|Recall Weapon|Rappeler une arme|officielle|
|[1zvM86hRPPnGLOEW.htm](pathfinder-bestiary-2-items/1zvM86hRPPnGLOEW.htm)|Scent (Imprecise) 60 feet|Odorat (imprécis) 18 m|libre|
|[20d4bCP4x4uHaNkd.htm](pathfinder-bestiary-2-items/20d4bCP4x4uHaNkd.htm)|Claw|Griffe|libre|
|[27ggnHd24eNbhg9e.htm](pathfinder-bestiary-2-items/27ggnHd24eNbhg9e.htm)|Darkvision|Vision dans le noir|libre|
|[28i8LDUqvwLQGZFc.htm](pathfinder-bestiary-2-items/28i8LDUqvwLQGZFc.htm)|Darkvision|Vision dans le noir|libre|
|[28TZFvjgwHVKS489.htm](pathfinder-bestiary-2-items/28TZFvjgwHVKS489.htm)|Filament|Filament|libre|
|[2a4l18G9fqQ03iFA.htm](pathfinder-bestiary-2-items/2a4l18G9fqQ03iFA.htm)|Tail|Queue|libre|
|[2ABnFaydFGtobnZC.htm](pathfinder-bestiary-2-items/2ABnFaydFGtobnZC.htm)|At-Will Spells|Sorts à volonté|libre|
|[2aU81mgG4GTqpajQ.htm](pathfinder-bestiary-2-items/2aU81mgG4GTqpajQ.htm)|Foot|Pied|libre|
|[2avn7E9oKYvrvNGl.htm](pathfinder-bestiary-2-items/2avn7E9oKYvrvNGl.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[2B1o26m73tHgV6UJ.htm](pathfinder-bestiary-2-items/2B1o26m73tHgV6UJ.htm)|Low-Light Vision|Vision nocturne|libre|
|[2bevQZIcuccKMxmq.htm](pathfinder-bestiary-2-items/2bevQZIcuccKMxmq.htm)|Jaws|Mâchoires|libre|
|[2BHwzsnFNAsPoULp.htm](pathfinder-bestiary-2-items/2BHwzsnFNAsPoULp.htm)|Fangs|Crocs|libre|
|[2bthXOLfHVbIarRo.htm](pathfinder-bestiary-2-items/2bthXOLfHVbIarRo.htm)|At-Will Spells|Sorts à volonté|libre|
|[2D3ZN6sbGBpo3oKC.htm](pathfinder-bestiary-2-items/2D3ZN6sbGBpo3oKC.htm)|Scent|Odorat|libre|
|[2dHPDZu3BYN1GauB.htm](pathfinder-bestiary-2-items/2dHPDZu3BYN1GauB.htm)|No Breath|Ne respire pas|officielle|
|[2doPyPIBLoDzjzk9.htm](pathfinder-bestiary-2-items/2doPyPIBLoDzjzk9.htm)|Paralysis|Paralysie|officielle|
|[2ESgkLReH2zyxUIl.htm](pathfinder-bestiary-2-items/2ESgkLReH2zyxUIl.htm)|Pestilential Aura|Aura pestilentielle|libre|
|[2FR8eBARbXfskcJR.htm](pathfinder-bestiary-2-items/2FR8eBARbXfskcJR.htm)|Low-Light Vision|Vision nocturne|libre|
|[2GNdGYBXQOD8VlC7.htm](pathfinder-bestiary-2-items/2GNdGYBXQOD8VlC7.htm)|Darkvision|Vision dans le noir|libre|
|[2hG90mAiBnKHWoxc.htm](pathfinder-bestiary-2-items/2hG90mAiBnKHWoxc.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[2HOhVTSCzGdbcfjr.htm](pathfinder-bestiary-2-items/2HOhVTSCzGdbcfjr.htm)|Jaws|Mâchoires|libre|
|[2hyOsL9cmbYO1koQ.htm](pathfinder-bestiary-2-items/2hyOsL9cmbYO1koQ.htm)|Frightful Presence|Présence terrifiante|libre|
|[2IZWaM2O4amLWbye.htm](pathfinder-bestiary-2-items/2IZWaM2O4amLWbye.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[2J8nRu7ryhdxrn6Q.htm](pathfinder-bestiary-2-items/2J8nRu7ryhdxrn6Q.htm)|Jaws|Mâchoires|libre|
|[2JHGP2W5F09SgDOr.htm](pathfinder-bestiary-2-items/2JHGP2W5F09SgDOr.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[2JNLdz4nnbNkOkbO.htm](pathfinder-bestiary-2-items/2JNLdz4nnbNkOkbO.htm)|Claw|Griffe|libre|
|[2KNgUCdMbbr8kaE7.htm](pathfinder-bestiary-2-items/2KNgUCdMbbr8kaE7.htm)|Barbed Tongue|Langue barbelée|officielle|
|[2KSiu60tktdQLiFE.htm](pathfinder-bestiary-2-items/2KSiu60tktdQLiFE.htm)|Ultimate Sacrifice|Sacrifice ultime|officielle|
|[2M8fHq1jWvJybtt9.htm](pathfinder-bestiary-2-items/2M8fHq1jWvJybtt9.htm)|Frightful Presence|Présence terrifiante|libre|
|[2MrVVGinZvjfZN22.htm](pathfinder-bestiary-2-items/2MrVVGinZvjfZN22.htm)|Tremorsense|Perception des vibrations|libre|
|[2NXWhRmjG27cSo87.htm](pathfinder-bestiary-2-items/2NXWhRmjG27cSo87.htm)|Starknife|Lamétoile|libre|
|[2O7YGmWG9wjbFcRM.htm](pathfinder-bestiary-2-items/2O7YGmWG9wjbFcRM.htm)|Scent|Odorat|libre|
|[2oF9dlmEoHZ6o4U9.htm](pathfinder-bestiary-2-items/2oF9dlmEoHZ6o4U9.htm)|Bastard Sword|Épée bâtarde|libre|
|[2OIZKsg9vZrNrBBw.htm](pathfinder-bestiary-2-items/2OIZKsg9vZrNrBBw.htm)|Basidirond Spores|Spores de basidirond|officielle|
|[2ooN4XI8c3aPZmLa.htm](pathfinder-bestiary-2-items/2ooN4XI8c3aPZmLa.htm)|Grab|Empoignade/Agrippement|libre|
|[2oTY3Ohbh0C59vOv.htm](pathfinder-bestiary-2-items/2oTY3Ohbh0C59vOv.htm)|Curse of Drowning|Malédiction de la noyade|officielle|
|[2R71xX6zTcFcPntf.htm](pathfinder-bestiary-2-items/2R71xX6zTcFcPntf.htm)|Greater Constrict|Constriction supérieure|libre|
|[2rJdPiiHOrVnLUyK.htm](pathfinder-bestiary-2-items/2rJdPiiHOrVnLUyK.htm)|Fish Hook|Hameçon|libre|
|[2SEpmpzrfI3iTOKV.htm](pathfinder-bestiary-2-items/2SEpmpzrfI3iTOKV.htm)|Jaws|Mâchoires|libre|
|[2ShdE5g6oMqrL464.htm](pathfinder-bestiary-2-items/2ShdE5g6oMqrL464.htm)|Scent|Odorat|libre|
|[2SKe9OcSUJ5AXTPf.htm](pathfinder-bestiary-2-items/2SKe9OcSUJ5AXTPf.htm)|Archon's Door|Porte de l'archon|libre|
|[2svyNegNoV205Fbp.htm](pathfinder-bestiary-2-items/2svyNegNoV205Fbp.htm)|Icy Demise|Destruction glaciale|officielle|
|[2szfgkG5m4pZQ6z8.htm](pathfinder-bestiary-2-items/2szfgkG5m4pZQ6z8.htm)|At-Will Spells|Sorts à volonté|libre|
|[2uyZOjTUnURaOWQp.htm](pathfinder-bestiary-2-items/2uyZOjTUnURaOWQp.htm)|Tail|Queue|libre|
|[2wJro1SjxNwEf7TQ.htm](pathfinder-bestiary-2-items/2wJro1SjxNwEf7TQ.htm)|Hatchet|Hachette|libre|
|[2wZs6OKCKOsMAU1a.htm](pathfinder-bestiary-2-items/2wZs6OKCKOsMAU1a.htm)|Throw Rock|Lancer de rocher|libre|
|[2X26aYIXJXcffHKZ.htm](pathfinder-bestiary-2-items/2X26aYIXJXcffHKZ.htm)|Throw Rock|Lancer de rocher|libre|
|[2xM4lSOsw2nfs2tu.htm](pathfinder-bestiary-2-items/2xM4lSOsw2nfs2tu.htm)|Mauler|Écharpeur|officielle|
|[2YDmS6mB4lt2kYvF.htm](pathfinder-bestiary-2-items/2YDmS6mB4lt2kYvF.htm)|Jaws|Mâchoires|libre|
|[2yuX2a5ufNBYmWHK.htm](pathfinder-bestiary-2-items/2yuX2a5ufNBYmWHK.htm)|Fangs|Crocs|libre|
|[3141UeEi3hFz0moy.htm](pathfinder-bestiary-2-items/3141UeEi3hFz0moy.htm)|Darkvision|Vision dans le noir|libre|
|[32RMKlAce7FfxW8e.htm](pathfinder-bestiary-2-items/32RMKlAce7FfxW8e.htm)|Darkvision|Vision dans le noir|libre|
|[34NmTbXjw24GQVju.htm](pathfinder-bestiary-2-items/34NmTbXjw24GQVju.htm)|Mind-Rending Sting|Dard déchireur d'esprit|officielle|
|[37D2wZ5XwqQ5zjsE.htm](pathfinder-bestiary-2-items/37D2wZ5XwqQ5zjsE.htm)|Negative Healing|Guérison négative|libre|
|[38cGo2E7vYZ2PNb5.htm](pathfinder-bestiary-2-items/38cGo2E7vYZ2PNb5.htm)|+2 Status Bonus to Saves vs Fear|Bonus de statut de +2 à tous les jets de sauvegarde contre la terreur|libre|
|[38rv8OhglPWCKDoM.htm](pathfinder-bestiary-2-items/38rv8OhglPWCKDoM.htm)|Low-Light Vision|Vision nocturne|libre|
|[3AK8xWwQNAfn8GZR.htm](pathfinder-bestiary-2-items/3AK8xWwQNAfn8GZR.htm)|Bite|Morsure|officielle|
|[3AqKPdexSndZxd5X.htm](pathfinder-bestiary-2-items/3AqKPdexSndZxd5X.htm)|Rend|Éventration|libre|
|[3bHndJ6sRVOw7gwC.htm](pathfinder-bestiary-2-items/3bHndJ6sRVOw7gwC.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[3c01H24sjgfJHyvI.htm](pathfinder-bestiary-2-items/3c01H24sjgfJHyvI.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[3cCdl7Qgc7kTT6at.htm](pathfinder-bestiary-2-items/3cCdl7Qgc7kTT6at.htm)|Darkvision|Vision dans le noir|libre|
|[3cCzIUJ7cXhefnQ1.htm](pathfinder-bestiary-2-items/3cCzIUJ7cXhefnQ1.htm)|Whirling Death|Mort tourbillonnante|officielle|
|[3CKGWY5tOmbNJznT.htm](pathfinder-bestiary-2-items/3CKGWY5tOmbNJznT.htm)|Fist|Poing|libre|
|[3DQQAVyagZHfe2VD.htm](pathfinder-bestiary-2-items/3DQQAVyagZHfe2VD.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[3ekp1Wmg9yNIy9iX.htm](pathfinder-bestiary-2-items/3ekp1Wmg9yNIy9iX.htm)|Crafting|Artisanat|libre|
|[3EzYscL7vFw5pVbn.htm](pathfinder-bestiary-2-items/3EzYscL7vFw5pVbn.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[3f1Y7j76I98KPIWK.htm](pathfinder-bestiary-2-items/3f1Y7j76I98KPIWK.htm)|Ice Missile|Projectile de glace|libre|
|[3F8tYJDQeeHlPY6p.htm](pathfinder-bestiary-2-items/3F8tYJDQeeHlPY6p.htm)|Darkvision|Vision dans le noir|libre|
|[3gCf4mj9ynEcSXVi.htm](pathfinder-bestiary-2-items/3gCf4mj9ynEcSXVi.htm)|Constrict|Constriction|libre|
|[3GOGRqIhDzmef7he.htm](pathfinder-bestiary-2-items/3GOGRqIhDzmef7he.htm)|Hallucinogenic Cloud|Nuage hallucinatoire|officielle|
|[3IExZ0tOTpB3Neuc.htm](pathfinder-bestiary-2-items/3IExZ0tOTpB3Neuc.htm)|Whirling Frenzy|Frénésie tourbillonnante|officielle|
|[3IKSOIIWyaxP7Xbh.htm](pathfinder-bestiary-2-items/3IKSOIIWyaxP7Xbh.htm)|Stealth|Discrétion|libre|
|[3jVmH3McNsX8B0Od.htm](pathfinder-bestiary-2-items/3jVmH3McNsX8B0Od.htm)|Jaws|Mâchoires|libre|
|[3KdhYZrwApupcqo8.htm](pathfinder-bestiary-2-items/3KdhYZrwApupcqo8.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[3MKqU2vGsb11kD4j.htm](pathfinder-bestiary-2-items/3MKqU2vGsb11kD4j.htm)|Negative Healing|Guérison négative|libre|
|[3ryftdIzkfXriKLV.htm](pathfinder-bestiary-2-items/3ryftdIzkfXriKLV.htm)|Scent|Odorat|libre|
|[3s66FP7raqgSCnP1.htm](pathfinder-bestiary-2-items/3s66FP7raqgSCnP1.htm)|Mucus Trail|Traînée de mucus|officielle|
|[3t3ulXs6r1EL76F0.htm](pathfinder-bestiary-2-items/3t3ulXs6r1EL76F0.htm)|Breath Weapon|Arme de souffle|libre|
|[3t8p1OnCR5iUrhxt.htm](pathfinder-bestiary-2-items/3t8p1OnCR5iUrhxt.htm)|Pounce|Bond|officielle|
|[3tFe6InN38WdT6ps.htm](pathfinder-bestiary-2-items/3tFe6InN38WdT6ps.htm)|Darkvision|Vision dans le noir|libre|
|[3Ut3Piu4ATI3yyUU.htm](pathfinder-bestiary-2-items/3Ut3Piu4ATI3yyUU.htm)|Regeneration 25 (deactivated by good or silver)|Régénération 25 (désactivé par bon ou argent)|officielle|
|[3vycv9ThXOQbgNBf.htm](pathfinder-bestiary-2-items/3vycv9ThXOQbgNBf.htm)|Instinctual Tinker|Bricoleur instinctif|officielle|
|[3WqEPpUcLFd5pJv6.htm](pathfinder-bestiary-2-items/3WqEPpUcLFd5pJv6.htm)|Protean Anatomy 6|Anatomie protéenne 6|officielle|
|[3z9Sgd2sbQUaFIUK.htm](pathfinder-bestiary-2-items/3z9Sgd2sbQUaFIUK.htm)|Dagger|Dague|libre|
|[3zjA9MZy3tcHkLRD.htm](pathfinder-bestiary-2-items/3zjA9MZy3tcHkLRD.htm)|At-Will Spells|Sorts à volonté|libre|
|[40BuHxitDl0hJDTK.htm](pathfinder-bestiary-2-items/40BuHxitDl0hJDTK.htm)|Toxic Skin|Peau toxique|officielle|
|[41jyyqtM7TidzhJx.htm](pathfinder-bestiary-2-items/41jyyqtM7TidzhJx.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[42LsnRw0ara1e2xW.htm](pathfinder-bestiary-2-items/42LsnRw0ara1e2xW.htm)|Darkvision|Vision dans le noir|libre|
|[43K5JpJ2j1VBI70k.htm](pathfinder-bestiary-2-items/43K5JpJ2j1VBI70k.htm)|Darkvision|Vision dans le noir|libre|
|[44O3eEgI5o0mZpbU.htm](pathfinder-bestiary-2-items/44O3eEgI5o0mZpbU.htm)|Telepathy|Télépathie|libre|
|[48eItql3Drlle3Rb.htm](pathfinder-bestiary-2-items/48eItql3Drlle3Rb.htm)|Athletics|Athlétisme|libre|
|[4AlhbRnSYawCodcB.htm](pathfinder-bestiary-2-items/4AlhbRnSYawCodcB.htm)|Darkvision|Vision dans le noir|libre|
|[4au76ROiAjb1dHdY.htm](pathfinder-bestiary-2-items/4au76ROiAjb1dHdY.htm)|Draconic Resistance|Résistance draconique|officielle|
|[4b9FtRKJfWmjYbQR.htm](pathfinder-bestiary-2-items/4b9FtRKJfWmjYbQR.htm)|Focus Gaze|Focaliser le regard|officielle|
|[4BH9uwuYigIuBuzB.htm](pathfinder-bestiary-2-items/4BH9uwuYigIuBuzB.htm)|Lightning Blast|Frappé par la foudre|officielle|
|[4CALESo8Bdrqe3vE.htm](pathfinder-bestiary-2-items/4CALESo8Bdrqe3vE.htm)|Spill Venom|Répandre du venin|officielle|
|[4cuSJxJLUg7icXmj.htm](pathfinder-bestiary-2-items/4cuSJxJLUg7icXmj.htm)|Vulnerable to Shatter|Vulnérable à fracassement|officielle|
|[4cwmHfbkRG2Hqswy.htm](pathfinder-bestiary-2-items/4cwmHfbkRG2Hqswy.htm)|Darkvision|Vision dans le noir|libre|
|[4CyhH5ZkHNlxJtIP.htm](pathfinder-bestiary-2-items/4CyhH5ZkHNlxJtIP.htm)|Regeneration 10 (deactivated by cold iron)|Régénération 10 (désactivée par le fer froid)|libre|
|[4d37i0818MYTgBAF.htm](pathfinder-bestiary-2-items/4d37i0818MYTgBAF.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegardes contre la magie|libre|
|[4DpvbfaL4gw6x5Iy.htm](pathfinder-bestiary-2-items/4DpvbfaL4gw6x5Iy.htm)|Scent|Odorat|libre|
|[4eJg9N0UmfD2Yt6C.htm](pathfinder-bestiary-2-items/4eJg9N0UmfD2Yt6C.htm)|Fist|Poing|libre|
|[4fUKQrOX75rYdFV8.htm](pathfinder-bestiary-2-items/4fUKQrOX75rYdFV8.htm)|Entropy Sense|Perception de l'entropie (imprécis) 18 m|officielle|
|[4J4DJgMerRgq1mfr.htm](pathfinder-bestiary-2-items/4J4DJgMerRgq1mfr.htm)|Claw|Griffe|libre|
|[4jUkeNIE92J0asLl.htm](pathfinder-bestiary-2-items/4jUkeNIE92J0asLl.htm)|Negative Healing|Guérison négative|libre|
|[4KxhR1H4vfF5teNe.htm](pathfinder-bestiary-2-items/4KxhR1H4vfF5teNe.htm)|Breath Weapon|Arme de souffle|libre|
|[4LgyNabFsMIjCO9s.htm](pathfinder-bestiary-2-items/4LgyNabFsMIjCO9s.htm)|Scent (Imprecise) 60 feet)|Odorat (imprécis) 18 m|libre|
|[4MEkxras2sA4aWfg.htm](pathfinder-bestiary-2-items/4MEkxras2sA4aWfg.htm)|Spiked Chain|Chaîne cloutée|libre|
|[4mqhPMJ1Yzc6mTFk.htm](pathfinder-bestiary-2-items/4mqhPMJ1Yzc6mTFk.htm)|Draconic Momentum|Élan draconique|libre|
|[4NE1llNXfbsRiHmP.htm](pathfinder-bestiary-2-items/4NE1llNXfbsRiHmP.htm)|Savage Jaws|Mâchoires sauvages|officielle|
|[4Ok3T9qk3DEMSviw.htm](pathfinder-bestiary-2-items/4Ok3T9qk3DEMSviw.htm)|Darkvision|Vision dans le noir|libre|
|[4oL2L4CT1FG6IAEu.htm](pathfinder-bestiary-2-items/4oL2L4CT1FG6IAEu.htm)|Grab|Empoignade/Agrippement|libre|
|[4PAeJHUBsggCTLYw.htm](pathfinder-bestiary-2-items/4PAeJHUBsggCTLYw.htm)|Tail|Queue|libre|
|[4PafOhYr090tdIQE.htm](pathfinder-bestiary-2-items/4PafOhYr090tdIQE.htm)|Aging Strikes|Frappes vieillissantes|officielle|
|[4PmFH52nJw8E0Xwl.htm](pathfinder-bestiary-2-items/4PmFH52nJw8E0Xwl.htm)|Grab|Empoignade/Agrippement|libre|
|[4tddCK0S1FejzHcG.htm](pathfinder-bestiary-2-items/4tddCK0S1FejzHcG.htm)|Wild Empathy|Empathie sauvage|officielle|
|[4tJ8KAPk36CVF7E2.htm](pathfinder-bestiary-2-items/4tJ8KAPk36CVF7E2.htm)|Savage Jaws|Mâchoires sauvage|officielle|
|[4ujIS89zTfcMbbaX.htm](pathfinder-bestiary-2-items/4ujIS89zTfcMbbaX.htm)|Jaws|Mâchoires|libre|
|[4UMDwIzgdxumhvIf.htm](pathfinder-bestiary-2-items/4UMDwIzgdxumhvIf.htm)|Swarm Mind|Esprit de nuée|libre|
|[4Uoy7JHv0Ea9SrCr.htm](pathfinder-bestiary-2-items/4Uoy7JHv0Ea9SrCr.htm)|Baffling Bluff|Bluff déconcertant|officielle|
|[4w3vvxZvUVrs2iWm.htm](pathfinder-bestiary-2-items/4w3vvxZvUVrs2iWm.htm)|Darkvision|Vision dans le noir|libre|
|[4W4LiMhJykn0tAnC.htm](pathfinder-bestiary-2-items/4W4LiMhJykn0tAnC.htm)|Darkvision|Vision dans le noir|libre|
|[4z5ULy6UhFHErn4I.htm](pathfinder-bestiary-2-items/4z5ULy6UhFHErn4I.htm)|Darkvision|Vision dans le noir|libre|
|[4ZdMoPLM5L40W4TK.htm](pathfinder-bestiary-2-items/4ZdMoPLM5L40W4TK.htm)|Claw|Griffe|libre|
|[4zEyOGGm0vwVrtmi.htm](pathfinder-bestiary-2-items/4zEyOGGm0vwVrtmi.htm)|Cunning|Astucieux|officielle|
|[52OH24WzpzdT3ptJ.htm](pathfinder-bestiary-2-items/52OH24WzpzdT3ptJ.htm)|Envisioning|Conceptualisation|officielle|
|[54mSt8tnFXfSUjhU.htm](pathfinder-bestiary-2-items/54mSt8tnFXfSUjhU.htm)|Cling|S’accrocher|officielle|
|[54xjK61C3DBrqHe7.htm](pathfinder-bestiary-2-items/54xjK61C3DBrqHe7.htm)|Constant Spells|Sorts constants|libre|
|[58HvfTTw56mTEDsj.htm](pathfinder-bestiary-2-items/58HvfTTw56mTEDsj.htm)|Breath Weapon|Arme de souffle|officielle|
|[5aqMBttykiS0SKAb.htm](pathfinder-bestiary-2-items/5aqMBttykiS0SKAb.htm)|Petrifying Glance|Coup d'oeil pétrifiant|officielle|
|[5ASzxj6oyeWu6gWM.htm](pathfinder-bestiary-2-items/5ASzxj6oyeWu6gWM.htm)|Darkvision|Vision dans le noir|libre|
|[5cC1uj7hBSGN8tix.htm](pathfinder-bestiary-2-items/5cC1uj7hBSGN8tix.htm)|Lifesense|Perception de la vie|libre|
|[5CgVbeYygEXhZf6y.htm](pathfinder-bestiary-2-items/5CgVbeYygEXhZf6y.htm)|Bully's Bludgeon|Matraquage au morgenstern|officielle|
|[5d1k1jwifXVc23o0.htm](pathfinder-bestiary-2-items/5d1k1jwifXVc23o0.htm)|Claw|Griffe|libre|
|[5djM8sO4U2PY23bG.htm](pathfinder-bestiary-2-items/5djM8sO4U2PY23bG.htm)|Unstoppable|Impossible à arrêter|officielle|
|[5Ds8GMXoIjWg9j7Q.htm](pathfinder-bestiary-2-items/5Ds8GMXoIjWg9j7Q.htm)|Tentacle|Tentacule|libre|
|[5e5FND78feSKXP99.htm](pathfinder-bestiary-2-items/5e5FND78feSKXP99.htm)|Impaling Chain|Chaîne empaleuse|officielle|
|[5EcCWK4rV7lqrPqY.htm](pathfinder-bestiary-2-items/5EcCWK4rV7lqrPqY.htm)|+2 Status Bonus on Saves vs Mental|Bonus de statut de +2 aux jets de sauvegarde contre les effets mentaux|libre|
|[5f6mJgvaVADXCaAz.htm](pathfinder-bestiary-2-items/5f6mJgvaVADXCaAz.htm)|Scent|Odorat|libre|
|[5gLIoGOoPN1ReZMD.htm](pathfinder-bestiary-2-items/5gLIoGOoPN1ReZMD.htm)|Wing|Aile|libre|
|[5IQ8tiyhCdSfyPEo.htm](pathfinder-bestiary-2-items/5IQ8tiyhCdSfyPEo.htm)|Grab|Empoignade/Agrippement|libre|
|[5kEI04PwOkPj2l0j.htm](pathfinder-bestiary-2-items/5kEI04PwOkPj2l0j.htm)|At-Will Spells|Sorts à volonté|libre|
|[5lCoU2D6djePIJZT.htm](pathfinder-bestiary-2-items/5lCoU2D6djePIJZT.htm)|Web Sense|Perception sur les toiles|officielle|
|[5lPaA6GKal2YHAjj.htm](pathfinder-bestiary-2-items/5lPaA6GKal2YHAjj.htm)|Jaws|Mâchoires|libre|
|[5nazHNTE9SHWxEH5.htm](pathfinder-bestiary-2-items/5nazHNTE9SHWxEH5.htm)|Claw|Griffe|libre|
|[5OLDndk87U2qEJ5p.htm](pathfinder-bestiary-2-items/5OLDndk87U2qEJ5p.htm)|Pincer|Pince|libre|
|[5PvfqR4GcUsfw2mR.htm](pathfinder-bestiary-2-items/5PvfqR4GcUsfw2mR.htm)|Rend|Éventration|libre|
|[5qArY8oNtsZLv3Lg.htm](pathfinder-bestiary-2-items/5qArY8oNtsZLv3Lg.htm)|Jaws|Mâchoires|libre|
|[5qgx9qhXAceS6uLh.htm](pathfinder-bestiary-2-items/5qgx9qhXAceS6uLh.htm)|Enraged Cunning|Astucieux enragés|officielle|
|[5QKXjFlhvGGdNcxf.htm](pathfinder-bestiary-2-items/5QKXjFlhvGGdNcxf.htm)|Claw|Griffe|libre|
|[5QMBo1Vy2JZTTsNV.htm](pathfinder-bestiary-2-items/5QMBo1Vy2JZTTsNV.htm)|Tremorsense|Perception des vibrations|libre|
|[5rN2CIypbGA4N34M.htm](pathfinder-bestiary-2-items/5rN2CIypbGA4N34M.htm)|Fangs|Crocs|libre|
|[5RsDoTAVa9W8gKnx.htm](pathfinder-bestiary-2-items/5RsDoTAVa9W8gKnx.htm)|Draconic Resistance|Résistance draconique|libre|
|[5SaRw4TGk2D0VBTZ.htm](pathfinder-bestiary-2-items/5SaRw4TGk2D0VBTZ.htm)|Draconic Momentum|Élan draconique|libre|
|[5t9D4gKM7JVpBSf6.htm](pathfinder-bestiary-2-items/5t9D4gKM7JVpBSf6.htm)|Wild Empathy|Empathie sauvage|officielle|
|[5tiASMinDzyHUAEv.htm](pathfinder-bestiary-2-items/5tiASMinDzyHUAEv.htm)|Jaws|Mâchoires|libre|
|[5tqNBlspN6txKrz9.htm](pathfinder-bestiary-2-items/5tqNBlspN6txKrz9.htm)|Jaws|Mâchoires|libre|
|[5UFt8Dv6NCEOpZlE.htm](pathfinder-bestiary-2-items/5UFt8Dv6NCEOpZlE.htm)|Constant Spells|Sorts constants|libre|
|[5Vw9VTB3I1sie1g1.htm](pathfinder-bestiary-2-items/5Vw9VTB3I1sie1g1.htm)|Sarglagon Venom|Venin de sarglagon|officielle|
|[5WLytdwKYQa3JLh7.htm](pathfinder-bestiary-2-items/5WLytdwKYQa3JLh7.htm)|Malleable|Malléable|officielle|
|[5wq581MuD4fc7xDD.htm](pathfinder-bestiary-2-items/5wq581MuD4fc7xDD.htm)|Lifesense 30 feet|Perception de la vie (9 m)|libre|
|[5XOy3zqts39I3bzo.htm](pathfinder-bestiary-2-items/5XOy3zqts39I3bzo.htm)|Low-Light Vision|Vision nocturne|libre|
|[5xuP57kLx1Xu16ZO.htm](pathfinder-bestiary-2-items/5xuP57kLx1Xu16ZO.htm)|Fist|Poing|libre|
|[5YDssRfPpgId4gTE.htm](pathfinder-bestiary-2-items/5YDssRfPpgId4gTE.htm)|Low-Light Vision|Vision nocturne|libre|
|[5Ygh7NnWAZBamFaS.htm](pathfinder-bestiary-2-items/5Ygh7NnWAZBamFaS.htm)|Jaws|Mâchoires|libre|
|[5yWmBrLhwYvStUFi.htm](pathfinder-bestiary-2-items/5yWmBrLhwYvStUFi.htm)|At-Will Spells|Sorts à volonté|libre|
|[60d6TRr07HgWYE5B.htm](pathfinder-bestiary-2-items/60d6TRr07HgWYE5B.htm)|Current|Courant|libre|
|[61EkmT4Ba451hMDC.htm](pathfinder-bestiary-2-items/61EkmT4Ba451hMDC.htm)|Jaws|Mâchoires|libre|
|[61GyOh9BdFBPZCUD.htm](pathfinder-bestiary-2-items/61GyOh9BdFBPZCUD.htm)|Low-Light Vision|Vision nocturne|libre|
|[61nmK3yvcoeX2PbG.htm](pathfinder-bestiary-2-items/61nmK3yvcoeX2PbG.htm)|Low-Light Vision|Vision nocturne|libre|
|[68hstcNYAfYsk65j.htm](pathfinder-bestiary-2-items/68hstcNYAfYsk65j.htm)|Claw|Griffe|libre|
|[68IxixgMddYHirAR.htm](pathfinder-bestiary-2-items/68IxixgMddYHirAR.htm)|Constrict|Constriction|libre|
|[68RVtURJJY5N5dTK.htm](pathfinder-bestiary-2-items/68RVtURJJY5N5dTK.htm)|Darkvision|Vision dans le noir|libre|
|[6a1pfDWvzEsw0JWE.htm](pathfinder-bestiary-2-items/6a1pfDWvzEsw0JWE.htm)|Darkvision|Vision dans le noir|libre|
|[6CGO8ENM5nFAurjN.htm](pathfinder-bestiary-2-items/6CGO8ENM5nFAurjN.htm)|Ranseur|Corsèque|libre|
|[6dch35LAYgze01qE.htm](pathfinder-bestiary-2-items/6dch35LAYgze01qE.htm)|Stealth|Discrétion|libre|
|[6dcoGxoSd8A6QFv2.htm](pathfinder-bestiary-2-items/6dcoGxoSd8A6QFv2.htm)|Twisting Tail|Queue sinueuse|officielle|
|[6gwNFiImkw2H0QWA.htm](pathfinder-bestiary-2-items/6gwNFiImkw2H0QWA.htm)|Grab|Empoignade/Agrippement|libre|
|[6i7GkvP5XmSn8g6G.htm](pathfinder-bestiary-2-items/6i7GkvP5XmSn8g6G.htm)|Breath Weapon|Arme de souffle|officielle|
|[6jVS5MIWVjrSMBki.htm](pathfinder-bestiary-2-items/6jVS5MIWVjrSMBki.htm)|Telepathy|Télépathie|libre|
|[6lnr0C4QK4LAkOa1.htm](pathfinder-bestiary-2-items/6lnr0C4QK4LAkOa1.htm)|Low-Light Vision|Vision nocturne|libre|
|[6LuECgP1a3GGskbB.htm](pathfinder-bestiary-2-items/6LuECgP1a3GGskbB.htm)|Jaws|Mâchoires|libre|
|[6Mfkl1CejuYcVcXf.htm](pathfinder-bestiary-2-items/6Mfkl1CejuYcVcXf.htm)|Seed|Graine|libre|
|[6NgDd3kvRPxleBuE.htm](pathfinder-bestiary-2-items/6NgDd3kvRPxleBuE.htm)|Blinding Sand|Sable aveuglant|officielle|
|[6no2LBNfSHP8w9DB.htm](pathfinder-bestiary-2-items/6no2LBNfSHP8w9DB.htm)|Negative Healing|Guérison négative|libre|
|[6oTpSA3IXew7MulO.htm](pathfinder-bestiary-2-items/6oTpSA3IXew7MulO.htm)|Hand Of Fate|Main du destin|libre|
|[6qaFJjq9JjlhZbaw.htm](pathfinder-bestiary-2-items/6qaFJjq9JjlhZbaw.htm)|Ogre Spider Venom|Venin d'araignée ogre|officielle|
|[6qDqRA2Nq93rsvx9.htm](pathfinder-bestiary-2-items/6qDqRA2Nq93rsvx9.htm)|Greater Electrolocation|Électrolocalisation supérieure|officielle|
|[6rslY3YCHH1EYxYL.htm](pathfinder-bestiary-2-items/6rslY3YCHH1EYxYL.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[6Sms9YOa60rjevVP.htm](pathfinder-bestiary-2-items/6Sms9YOa60rjevVP.htm)|Jaws|Mâchoires|libre|
|[6t0iHm0ru1bcVGTw.htm](pathfinder-bestiary-2-items/6t0iHm0ru1bcVGTw.htm)|Entangling Tendrils|Vrilles enchevêtrantes|officielle|
|[6U0xCdudnhZPawQv.htm](pathfinder-bestiary-2-items/6U0xCdudnhZPawQv.htm)|Web War Flail|Bola de toile|libre|
|[6UhOo5jcpQz76GcZ.htm](pathfinder-bestiary-2-items/6UhOo5jcpQz76GcZ.htm)|Darkvision|Vision dans le noir|libre|
|[6VEsckJo4laa2uls.htm](pathfinder-bestiary-2-items/6VEsckJo4laa2uls.htm)|Claw|Griffe|libre|
|[6vnexEfGJBfKrtHp.htm](pathfinder-bestiary-2-items/6vnexEfGJBfKrtHp.htm)|Tusk Sweep|Balayage avec les défenses|officielle|
|[6XMcWttuyRBueX73.htm](pathfinder-bestiary-2-items/6XMcWttuyRBueX73.htm)|Knockdown|Renversement|libre|
|[6Yl0sNRGgIqfvRj8.htm](pathfinder-bestiary-2-items/6Yl0sNRGgIqfvRj8.htm)|Darkvision|Vision dans le noir|libre|
|[6ZtjeB9jdEW4o2QJ.htm](pathfinder-bestiary-2-items/6ZtjeB9jdEW4o2QJ.htm)|Bite|Morsure|officielle|
|[71Xez8c9broSGKbf.htm](pathfinder-bestiary-2-items/71Xez8c9broSGKbf.htm)|Quill Barrage|Barrage de piquants|officielle|
|[72YvqHEAOHV34zLK.htm](pathfinder-bestiary-2-items/72YvqHEAOHV34zLK.htm)|Mucus|Mucus|officielle|
|[76jTiA45mzSvkuz1.htm](pathfinder-bestiary-2-items/76jTiA45mzSvkuz1.htm)|Antler|Bois|libre|
|[78oc41E029XUkMoS.htm](pathfinder-bestiary-2-items/78oc41E029XUkMoS.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[7AMBKbBXmPFp0RlP.htm](pathfinder-bestiary-2-items/7AMBKbBXmPFp0RlP.htm)|Darkvision|Vision dans le noir|libre|
|[7B1Q0p1BKuDwOEoo.htm](pathfinder-bestiary-2-items/7B1Q0p1BKuDwOEoo.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[7cdnWouJZ6KDhfxj.htm](pathfinder-bestiary-2-items/7cdnWouJZ6KDhfxj.htm)|Club|Gourdin|libre|
|[7D3xnmgdDZGumRA4.htm](pathfinder-bestiary-2-items/7D3xnmgdDZGumRA4.htm)|Breath Weapon|Arme de souffle|libre|
|[7dxhRfITiiUW00kr.htm](pathfinder-bestiary-2-items/7dxhRfITiiUW00kr.htm)|Darkvision|Vision dans le noir|libre|
|[7FksKj3ieU9yJEu0.htm](pathfinder-bestiary-2-items/7FksKj3ieU9yJEu0.htm)|Darkvision|Vision dans le noir|libre|
|[7GHhkRMg6SxILo1W.htm](pathfinder-bestiary-2-items/7GHhkRMg6SxILo1W.htm)|Lifesense|Perception de la vie|libre|
|[7i3BZvYdZOVTki7v.htm](pathfinder-bestiary-2-items/7i3BZvYdZOVTki7v.htm)|Throw Rock|Lancer de rocher|libre|
|[7ibCw6e1BSbQ94NW.htm](pathfinder-bestiary-2-items/7ibCw6e1BSbQ94NW.htm)|Lurker's Glow|Lueur de rôdeur|officielle|
|[7iLw2emMidGNxyzJ.htm](pathfinder-bestiary-2-items/7iLw2emMidGNxyzJ.htm)|Stealth|Discrétion|officielle|
|[7j5B40DMWHBTShLl.htm](pathfinder-bestiary-2-items/7j5B40DMWHBTShLl.htm)|Darkvision|Vision dans le noir|libre|
|[7kGu1ORgE4Nay79B.htm](pathfinder-bestiary-2-items/7kGu1ORgE4Nay79B.htm)|Sneak Attack|Attaque sournoise|officielle|
|[7KzbzFV96YELFVZA.htm](pathfinder-bestiary-2-items/7KzbzFV96YELFVZA.htm)|Darkvision|Vision dans le noir|libre|
|[7MiqMH6uob5zPB4e.htm](pathfinder-bestiary-2-items/7MiqMH6uob5zPB4e.htm)|Spotlight|Projecteur|officielle|
|[7mVYoMlm7knbjqzo.htm](pathfinder-bestiary-2-items/7mVYoMlm7knbjqzo.htm)|Tail|Queue|libre|
|[7NjsgQKMg0FvxDmJ.htm](pathfinder-bestiary-2-items/7NjsgQKMg0FvxDmJ.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[7ovDTTZtTBqn2eJ6.htm](pathfinder-bestiary-2-items/7ovDTTZtTBqn2eJ6.htm)|Performance|Représentation|libre|
|[7PG4MVtAvT1kH55R.htm](pathfinder-bestiary-2-items/7PG4MVtAvT1kH55R.htm)|Telepathy (Touch)|Télépathie (Contact)|libre|
|[7Qq2wYdCdaBf6zs4.htm](pathfinder-bestiary-2-items/7Qq2wYdCdaBf6zs4.htm)|Darkvision|Vision dans le noir|libre|
|[7QTkBSoDwgvNiBiC.htm](pathfinder-bestiary-2-items/7QTkBSoDwgvNiBiC.htm)|Chain|Chaînes|libre|
|[7RlCpEdZ5Lw3Omd2.htm](pathfinder-bestiary-2-items/7RlCpEdZ5Lw3Omd2.htm)|Gust|Rafale|libre|
|[7rYYffte0B1eDFDt.htm](pathfinder-bestiary-2-items/7rYYffte0B1eDFDt.htm)|Telepathy|Télépathie|libre|
|[7RZaK7BB8AYtaXJb.htm](pathfinder-bestiary-2-items/7RZaK7BB8AYtaXJb.htm)|At-Will Spells|Sorts à volonté|libre|
|[7TyKfwJXlkKJmSp1.htm](pathfinder-bestiary-2-items/7TyKfwJXlkKJmSp1.htm)|Darkvision|Vision dans le noir|libre|
|[7x3Iu1Xd8r44ZR48.htm](pathfinder-bestiary-2-items/7x3Iu1Xd8r44ZR48.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[8BGsf6kEIrCtKGgo.htm](pathfinder-bestiary-2-items/8BGsf6kEIrCtKGgo.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[8CRzC395t7TeEnQO.htm](pathfinder-bestiary-2-items/8CRzC395t7TeEnQO.htm)|Darkvision|Vision dans le noir|libre|
|[8EaMfqK0UpEoUqcg.htm](pathfinder-bestiary-2-items/8EaMfqK0UpEoUqcg.htm)|Acrobatics|Acrobatie|libre|
|[8F42ZUECGwbl9nmU.htm](pathfinder-bestiary-2-items/8F42ZUECGwbl9nmU.htm)|Darkvision|Vision dans le noir|libre|
|[8g9m2H7zav2ekCFO.htm](pathfinder-bestiary-2-items/8g9m2H7zav2ekCFO.htm)|Darkvision|Vision dans le noir|libre|
|[8GQKAYkj3NIJHurF.htm](pathfinder-bestiary-2-items/8GQKAYkj3NIJHurF.htm)|Children of the Night|Enfants de la nuit|libre|
|[8H0a3j4x2wevxTU0.htm](pathfinder-bestiary-2-items/8H0a3j4x2wevxTU0.htm)|Rock|Rocher|libre|
|[8J1VrbhC0fQxs0Xu.htm](pathfinder-bestiary-2-items/8J1VrbhC0fQxs0Xu.htm)|Muddy Field|Bourbier|officielle|
|[8JhtIGFYB5PcTSIt.htm](pathfinder-bestiary-2-items/8JhtIGFYB5PcTSIt.htm)|Ice Stride|Marche sur la glace|officielle|
|[8JPlEU0ps7X2hfP6.htm](pathfinder-bestiary-2-items/8JPlEU0ps7X2hfP6.htm)|Flower|Fleur|libre|
|[8Mkj5lFXbki11UgV.htm](pathfinder-bestiary-2-items/8Mkj5lFXbki11UgV.htm)|Infernal Mindlink|Lien mental infernal|officielle|
|[8PnlI5NzEir5W7Il.htm](pathfinder-bestiary-2-items/8PnlI5NzEir5W7Il.htm)|Derghodaemon's Stare|Regard fixe du derghodaémon|officielle|
|[8PpUoIkvn5HDKBBp.htm](pathfinder-bestiary-2-items/8PpUoIkvn5HDKBBp.htm)|Darkvision|Vision dans le noir|libre|
|[8qlQbmcwGPfRmLYu.htm](pathfinder-bestiary-2-items/8qlQbmcwGPfRmLYu.htm)|Low-Light Vision|Vision nocturne|libre|
|[8qYHl9oKTV79IZ6r.htm](pathfinder-bestiary-2-items/8qYHl9oKTV79IZ6r.htm)|Constrict|Constriction|officielle|
|[8r5XXqPZOhI0JSfB.htm](pathfinder-bestiary-2-items/8r5XXqPZOhI0JSfB.htm)|Darkvision|Vision dans le noir|libre|
|[8RpmS3nHFfxBiGaY.htm](pathfinder-bestiary-2-items/8RpmS3nHFfxBiGaY.htm)|Swarm Mind|Esprit de nuée|libre|
|[8SCH000Zv6X9BFPf.htm](pathfinder-bestiary-2-items/8SCH000Zv6X9BFPf.htm)|Fist|Poing|libre|
|[8SI23rDwK2QYJaxP.htm](pathfinder-bestiary-2-items/8SI23rDwK2QYJaxP.htm)|Catch Rock|Interception de rochers|libre|
|[8sphOZAnVIS4eVZ6.htm](pathfinder-bestiary-2-items/8sphOZAnVIS4eVZ6.htm)|Wing|Aile|libre|
|[8tPI0ksCv4zvd6WV.htm](pathfinder-bestiary-2-items/8tPI0ksCv4zvd6WV.htm)|Corkscrew|Foreuse|libre|
|[8UGnwgNXjFG8VCZU.htm](pathfinder-bestiary-2-items/8UGnwgNXjFG8VCZU.htm)|Hoof|Sabot|libre|
|[8UmPplmBw6ibPa9v.htm](pathfinder-bestiary-2-items/8UmPplmBw6ibPa9v.htm)|Explosion|Déflagration|officielle|
|[8UOucEsn2oF9O2uX.htm](pathfinder-bestiary-2-items/8UOucEsn2oF9O2uX.htm)|Frightful Presence|Présence terrifiante|libre|
|[8wrcvKK8xk5AIIva.htm](pathfinder-bestiary-2-items/8wrcvKK8xk5AIIva.htm)|Muddy Field|Champ boueux|officielle|
|[8WsmugIanltHKUg1.htm](pathfinder-bestiary-2-items/8WsmugIanltHKUg1.htm)|Low-Light Vision|Vision nocturne|libre|
|[8y1uIv1RL7FsHZzu.htm](pathfinder-bestiary-2-items/8y1uIv1RL7FsHZzu.htm)|Swallow Whole|Gober|officielle|
|[8yVRt0M6F9WbgdWV.htm](pathfinder-bestiary-2-items/8yVRt0M6F9WbgdWV.htm)|Cairn Linnorm Venom|Venin de linnorm des cairns|officielle|
|[8Zo1SkheQdiIdsNs.htm](pathfinder-bestiary-2-items/8Zo1SkheQdiIdsNs.htm)|Darkvision|Vision dans le noir|libre|
|[90V4TwzjxTH7bK7r.htm](pathfinder-bestiary-2-items/90V4TwzjxTH7bK7r.htm)|Spear|Lance|libre|
|[92YIjzpVzdYw3ZQC.htm](pathfinder-bestiary-2-items/92YIjzpVzdYw3ZQC.htm)|Change Shape|Changement de forme|officielle|
|[967GKOeV0EqG2hzn.htm](pathfinder-bestiary-2-items/967GKOeV0EqG2hzn.htm)|Grab|Empoignade/Agrippement|libre|
|[981IVfc2IS4Qnd3U.htm](pathfinder-bestiary-2-items/981IVfc2IS4Qnd3U.htm)|Aura of Sobs|Aura de pleurs|officielle|
|[98N9e0IqCp7WoRHr.htm](pathfinder-bestiary-2-items/98N9e0IqCp7WoRHr.htm)|Darkvision|Vision dans le noir|libre|
|[98pkTquG5m1vBxbt.htm](pathfinder-bestiary-2-items/98pkTquG5m1vBxbt.htm)|Tail|Queue|libre|
|[9atqlz9j100vC4uo.htm](pathfinder-bestiary-2-items/9atqlz9j100vC4uo.htm)|Jaws|Mâchoires|libre|
|[9bkFg1e33lRT8urN.htm](pathfinder-bestiary-2-items/9bkFg1e33lRT8urN.htm)|Sneak Attack|Attaque sournoise|officielle|
|[9bMQUXIt8V0ivYCg.htm](pathfinder-bestiary-2-items/9bMQUXIt8V0ivYCg.htm)|Darkvision|Vision dans le noir|libre|
|[9bSMDZ283g6Zq6IU.htm](pathfinder-bestiary-2-items/9bSMDZ283g6Zq6IU.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[9E67q4C5Qrwk0OqK.htm](pathfinder-bestiary-2-items/9E67q4C5Qrwk0OqK.htm)|Survival|Survie|libre|
|[9EJRdPBHkHMyY0yk.htm](pathfinder-bestiary-2-items/9EJRdPBHkHMyY0yk.htm)|Trample|Piétinement|officielle|
|[9eONvr5by0xfPFcf.htm](pathfinder-bestiary-2-items/9eONvr5by0xfPFcf.htm)|Witchflame Caress|Caresse de flammes ensorcelées|officielle|
|[9FcMZnkENPj4z3q0.htm](pathfinder-bestiary-2-items/9FcMZnkENPj4z3q0.htm)|Warpwave Strike|Frappe de vague de distorsion|officielle|
|[9G77nycfkHc93HGi.htm](pathfinder-bestiary-2-items/9G77nycfkHc93HGi.htm)|Dagger|Dague|libre|
|[9H6kMOzk61FQZoC9.htm](pathfinder-bestiary-2-items/9H6kMOzk61FQZoC9.htm)|Tail|Queue|libre|
|[9hRQJlQwiVSfsdc2.htm](pathfinder-bestiary-2-items/9hRQJlQwiVSfsdc2.htm)|Undulate|Ondulation|officielle|
|[9k7o784yI4Bka8ys.htm](pathfinder-bestiary-2-items/9k7o784yI4Bka8ys.htm)|Crossbow|Arbalète|libre|
|[9KZUjyVAnGC6gEbC.htm](pathfinder-bestiary-2-items/9KZUjyVAnGC6gEbC.htm)|Darkvision|Vision dans le noir|libre|
|[9O71o5KF846nZWVa.htm](pathfinder-bestiary-2-items/9O71o5KF846nZWVa.htm)|Swarming Bites|Nuée de morsures|officielle|
|[9oJwSlJ6tyt3KuRl.htm](pathfinder-bestiary-2-items/9oJwSlJ6tyt3KuRl.htm)|Spear|Lance|libre|
|[9ONTjzUo2qngSIHO.htm](pathfinder-bestiary-2-items/9ONTjzUo2qngSIHO.htm)|Grab|Empoignade/Agrippement|libre|
|[9PvfKc3Z0sztsiJ6.htm](pathfinder-bestiary-2-items/9PvfKc3Z0sztsiJ6.htm)|+1 Status Bonus on Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|libre|
|[9Q05lkXgY0PrqSQz.htm](pathfinder-bestiary-2-items/9Q05lkXgY0PrqSQz.htm)|At-Will Spells|Sorts à volonté|libre|
|[9rLnLLOFCIgXRAK8.htm](pathfinder-bestiary-2-items/9rLnLLOFCIgXRAK8.htm)|Jaws|Mâchoires|libre|
|[9tmJIJ6vy9JEPaL3.htm](pathfinder-bestiary-2-items/9tmJIJ6vy9JEPaL3.htm)|Tail|Queue|libre|
|[9tpWx4QY08q4Nv5G.htm](pathfinder-bestiary-2-items/9tpWx4QY08q4Nv5G.htm)|Telepathy|Télépathie|libre|
|[9tukJ8Am3f00bHUu.htm](pathfinder-bestiary-2-items/9tukJ8Am3f00bHUu.htm)|Savage Jaws|Mâchoires sauvages|officielle|
|[9UD8fQB0IDkl7w1X.htm](pathfinder-bestiary-2-items/9UD8fQB0IDkl7w1X.htm)|Staggering Servitude|Asservissement vertigineux|officielle|
|[9wolaUuOroFk0cOg.htm](pathfinder-bestiary-2-items/9wolaUuOroFk0cOg.htm)|Frightful Presence|Présence terrifiante|libre|
|[9WzRmRmcle5Fmp7N.htm](pathfinder-bestiary-2-items/9WzRmRmcle5Fmp7N.htm)|Alter Weather|Altération du climat|officielle|
|[9Xg9EXT1XEoi38zM.htm](pathfinder-bestiary-2-items/9Xg9EXT1XEoi38zM.htm)|Spear|Lance|libre|
|[9XxEDCpiqnYKds7V.htm](pathfinder-bestiary-2-items/9XxEDCpiqnYKds7V.htm)|Draconic Resistance|Résistance draconique|officielle|
|[9yu44cM7W0Xg2Q36.htm](pathfinder-bestiary-2-items/9yu44cM7W0Xg2Q36.htm)|Planar Incarnation - Astral Plane|Incarnation planaire|officielle|
|[9zIzERdEdvnnEjwe.htm](pathfinder-bestiary-2-items/9zIzERdEdvnnEjwe.htm)|Claw|Griffe|libre|
|[A18yDTtE84tJ1jhJ.htm](pathfinder-bestiary-2-items/A18yDTtE84tJ1jhJ.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[a29GFo7lbKhSTAYm.htm](pathfinder-bestiary-2-items/a29GFo7lbKhSTAYm.htm)|Fast Healing 2 (when touching ice or snow)|Guérison accélérée 2 (au contact de la glace ou de la neige)|officielle|
|[A2KzHDp53CpDVtv5.htm](pathfinder-bestiary-2-items/A2KzHDp53CpDVtv5.htm)|At-Will Spells|Sorts à volonté|libre|
|[A2lqtFMyanvkN6wM.htm](pathfinder-bestiary-2-items/A2lqtFMyanvkN6wM.htm)|Low-Light Vision|Vision nocturne|libre|
|[a3cYrmsIGVchwgtR.htm](pathfinder-bestiary-2-items/a3cYrmsIGVchwgtR.htm)|Pseudopod|Pseudopode|libre|
|[a3nL0MNgY6YXvvP9.htm](pathfinder-bestiary-2-items/a3nL0MNgY6YXvvP9.htm)|Jaws|Mâchoires|libre|
|[A3RVgZkJQrtRoYi2.htm](pathfinder-bestiary-2-items/A3RVgZkJQrtRoYi2.htm)|Tusk|Défense|libre|
|[A5NdjSB3IdJS5DAb.htm](pathfinder-bestiary-2-items/A5NdjSB3IdJS5DAb.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[a8kHbScnPzkgg3j3.htm](pathfinder-bestiary-2-items/a8kHbScnPzkgg3j3.htm)|Darkvision|Vision dans le noir|libre|
|[a9NvSrt50HRCakM8.htm](pathfinder-bestiary-2-items/a9NvSrt50HRCakM8.htm)|Telepathy|Télépathie|libre|
|[A9SIfdrB8Hk28o23.htm](pathfinder-bestiary-2-items/A9SIfdrB8Hk28o23.htm)|Jaws|Mâchoires|libre|
|[AasR87d76iiM7CAd.htm](pathfinder-bestiary-2-items/AasR87d76iiM7CAd.htm)|Mandibles|Mandibules|libre|
|[AAyJQ8CViWWMXWPU.htm](pathfinder-bestiary-2-items/AAyJQ8CViWWMXWPU.htm)|Jaws|Mâchoires|libre|
|[Ab1sWqDegn71eEve.htm](pathfinder-bestiary-2-items/Ab1sWqDegn71eEve.htm)|Magma Tomb|Cercueil de magma|officielle|
|[Ab4r9QqPgjwOMjaW.htm](pathfinder-bestiary-2-items/Ab4r9QqPgjwOMjaW.htm)|Tail|Queue|libre|
|[abGmJgduMKED2VeX.htm](pathfinder-bestiary-2-items/abGmJgduMKED2VeX.htm)|Chupar|Chupar|officielle|
|[aBIEnRecRBiBmpU4.htm](pathfinder-bestiary-2-items/aBIEnRecRBiBmpU4.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[AbJYjoA74dUGnY4h.htm](pathfinder-bestiary-2-items/AbJYjoA74dUGnY4h.htm)|Grab|Empoignade/Agrippement|libre|
|[ABqHj13JWyqTUohA.htm](pathfinder-bestiary-2-items/ABqHj13JWyqTUohA.htm)|Misty Tendril|Rinceau de brume|libre|
|[acasfdr7NXNxUTjJ.htm](pathfinder-bestiary-2-items/acasfdr7NXNxUTjJ.htm)|At-Will Spells|Sorts à volonté|libre|
|[AcMqqRGXMA4crSZt.htm](pathfinder-bestiary-2-items/AcMqqRGXMA4crSZt.htm)|Burning Lash|Fouet incandescent|libre|
|[adAFWbGgq7jnZcih.htm](pathfinder-bestiary-2-items/adAFWbGgq7jnZcih.htm)|Tail|Queue|libre|
|[aDF1WE7DJM17CR29.htm](pathfinder-bestiary-2-items/aDF1WE7DJM17CR29.htm)|Darkvision|Vision dans le noir|libre|
|[ADUDwILKarUV2YPp.htm](pathfinder-bestiary-2-items/ADUDwILKarUV2YPp.htm)|Slow|Lent|officielle|
|[aFiPts0pbbuzRaLf.htm](pathfinder-bestiary-2-items/aFiPts0pbbuzRaLf.htm)|Sudden Shove|Poussée soudaine|officielle|
|[AfkzOHwQLaNCyPZt.htm](pathfinder-bestiary-2-items/AfkzOHwQLaNCyPZt.htm)|Jaws|Mâchoires|libre|
|[AFlgcQtl6dJr1rtT.htm](pathfinder-bestiary-2-items/AFlgcQtl6dJr1rtT.htm)|Wolverine Rage|Rage du glouton|officielle|
|[AfwRVW7rPHMx2CVt.htm](pathfinder-bestiary-2-items/AfwRVW7rPHMx2CVt.htm)|Darkvision|Vision dans le noir|libre|
|[AGDYcCgG4mkmvihe.htm](pathfinder-bestiary-2-items/AGDYcCgG4mkmvihe.htm)|Breath Weapon|Arme de souffle|libre|
|[AGm4GoMUxx5xAmpo.htm](pathfinder-bestiary-2-items/AGm4GoMUxx5xAmpo.htm)|Swarm Mind|Esprit de nuée|libre|
|[AGZBKBnJMkp5NrlX.htm](pathfinder-bestiary-2-items/AGZBKBnJMkp5NrlX.htm)|Fangs|Crocs|libre|
|[aHkhx52xZZTtPI25.htm](pathfinder-bestiary-2-items/aHkhx52xZZTtPI25.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[AhkuOBFHV5aW2vlI.htm](pathfinder-bestiary-2-items/AhkuOBFHV5aW2vlI.htm)|Grab|Empoignade/Agrippement|libre|
|[Ai8J0A4OuhkDr7Cx.htm](pathfinder-bestiary-2-items/Ai8J0A4OuhkDr7Cx.htm)|Confusing Gaze|Regard déconcertant|officielle|
|[AiGQomtSZwNKsTr9.htm](pathfinder-bestiary-2-items/AiGQomtSZwNKsTr9.htm)|Speedy Sabotage|Sabotage express|officielle|
|[aisovzPKcYvcneL7.htm](pathfinder-bestiary-2-items/aisovzPKcYvcneL7.htm)|Throw Rock|Lancer de rocher|libre|
|[AJ6r1HXEEIIyvlnh.htm](pathfinder-bestiary-2-items/AJ6r1HXEEIIyvlnh.htm)|Regeneration 20 (deactivated by cold)|Régénération 20 (désactivée par le froid)|libre|
|[AJ75IwgP2pUKWDa2.htm](pathfinder-bestiary-2-items/AJ75IwgP2pUKWDa2.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[aJhXMbTAQPFT4ySw.htm](pathfinder-bestiary-2-items/aJhXMbTAQPFT4ySw.htm)|Scurry|Hâte|officielle|
|[AjxbFK9De8rRPATh.htm](pathfinder-bestiary-2-items/AjxbFK9De8rRPATh.htm)|Darkvision|Vision dans le noir|libre|
|[AjxhxqAsiBgHNq9P.htm](pathfinder-bestiary-2-items/AjxhxqAsiBgHNq9P.htm)|Frightful Presence|Présence terrifiante|libre|
|[AKHyZQZIqFBrYLLC.htm](pathfinder-bestiary-2-items/AKHyZQZIqFBrYLLC.htm)|Tilting Strike|Frappe inclinée|officielle|
|[AKOKPl5zfFti3EZh.htm](pathfinder-bestiary-2-items/AKOKPl5zfFti3EZh.htm)|Moon Frenzy|Frénésie lunaire|officielle|
|[ALbbJNuSFbicqOfo.htm](pathfinder-bestiary-2-items/ALbbJNuSFbicqOfo.htm)|Ripping Disarm|Désarmement par arrachage|officielle|
|[AldxbbHYC1u6qB4A.htm](pathfinder-bestiary-2-items/AldxbbHYC1u6qB4A.htm)|Vulnerable to Gentle Repose|Vulnérable à la préservation des morts|officielle|
|[Am3HvCv3WfgNu9sm.htm](pathfinder-bestiary-2-items/Am3HvCv3WfgNu9sm.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[AmgNn4ZAhXxVpdtv.htm](pathfinder-bestiary-2-items/AmgNn4ZAhXxVpdtv.htm)|Jaws|Mâchoires|libre|
|[AMnTAvqDDSvhK4rN.htm](pathfinder-bestiary-2-items/AMnTAvqDDSvhK4rN.htm)|Salt Water Vulnerability|Vulnérabilité à l’eau salée|officielle|
|[aNHpJbgvbOZz2Kpc.htm](pathfinder-bestiary-2-items/aNHpJbgvbOZz2Kpc.htm)|Fist|Poing|libre|
|[AoE6IxAfyxcUD4Jo.htm](pathfinder-bestiary-2-items/AoE6IxAfyxcUD4Jo.htm)|Fist|Poing|libre|
|[aOFgYZJ7qXDOoWDH.htm](pathfinder-bestiary-2-items/aOFgYZJ7qXDOoWDH.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[AoPor6Ll5rYacPzT.htm](pathfinder-bestiary-2-items/AoPor6Ll5rYacPzT.htm)|Club|Gourdin|libre|
|[APChG7D4rcYDlQ8m.htm](pathfinder-bestiary-2-items/APChG7D4rcYDlQ8m.htm)|Skrik Nettle Venom|Venin sur skrik urticant|officielle|
|[APd8zemC3kgQClDQ.htm](pathfinder-bestiary-2-items/APd8zemC3kgQClDQ.htm)|Darkvision|Vision dans le noir|libre|
|[aPrPuIOSdVOf8LwX.htm](pathfinder-bestiary-2-items/aPrPuIOSdVOf8LwX.htm)|Scent|Odorat|libre|
|[Aq9Qe7xGRm2JMoFA.htm](pathfinder-bestiary-2-items/Aq9Qe7xGRm2JMoFA.htm)|Low-Light Vision|Vision nocturne|libre|
|[AQeKaKMvD6zIOMpq.htm](pathfinder-bestiary-2-items/AQeKaKMvD6zIOMpq.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[aQUfPBPzpqLlkRM0.htm](pathfinder-bestiary-2-items/aQUfPBPzpqLlkRM0.htm)|Stygian Inquisitor|Inquisiteur stygien|officielle|
|[Ar1bSS3EJOZsaTMx.htm](pathfinder-bestiary-2-items/Ar1bSS3EJOZsaTMx.htm)|Jaws|Mâchoires|libre|
|[Ar58HUSjCtqUCBWd.htm](pathfinder-bestiary-2-items/Ar58HUSjCtqUCBWd.htm)|Jaws|Mâchoires|libre|
|[ARmbgc05YhfVz4Vz.htm](pathfinder-bestiary-2-items/ARmbgc05YhfVz4Vz.htm)|Scent|Odorat|libre|
|[aRmGycM99xyVkKnJ.htm](pathfinder-bestiary-2-items/aRmGycM99xyVkKnJ.htm)|Caustic Blood|Sang caustique|officielle|
|[aRTvrHip6K9YL4qq.htm](pathfinder-bestiary-2-items/aRTvrHip6K9YL4qq.htm)|Darkvision|Vision dans le noir|libre|
|[aSfa8TllpzF0cq1p.htm](pathfinder-bestiary-2-items/aSfa8TllpzF0cq1p.htm)|Jaws|Mâchoires|libre|
|[aSFQMPlhrSWqprUV.htm](pathfinder-bestiary-2-items/aSFQMPlhrSWqprUV.htm)|Tremorsense|Perception des vibrations|libre|
|[aT4z6HhNe6Rlop3y.htm](pathfinder-bestiary-2-items/aT4z6HhNe6Rlop3y.htm)|Tail|Queue|libre|
|[atrOWDXVF87vS3KI.htm](pathfinder-bestiary-2-items/atrOWDXVF87vS3KI.htm)|Curse of the Crooked Cane|Malédiction de la canne tordue|officielle|
|[aTTUtcIk2CfsuqEX.htm](pathfinder-bestiary-2-items/aTTUtcIk2CfsuqEX.htm)|Purity Vulnerability|Vulnérabilité à la pureté|officielle|
|[AtW9S7uW8rnbEEtY.htm](pathfinder-bestiary-2-items/AtW9S7uW8rnbEEtY.htm)|Darkvision|Vision dans le noir|libre|
|[AU14pBO1YZPWAEH5.htm](pathfinder-bestiary-2-items/AU14pBO1YZPWAEH5.htm)|Telepathy|Télépathie|libre|
|[aUo4Cxd0YTiFEabx.htm](pathfinder-bestiary-2-items/aUo4Cxd0YTiFEabx.htm)|Fist|Poing|libre|
|[AUp2Wnd4EQNf6jKb.htm](pathfinder-bestiary-2-items/AUp2Wnd4EQNf6jKb.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[auQwzWmfz2NSN0Rk.htm](pathfinder-bestiary-2-items/auQwzWmfz2NSN0Rk.htm)|Swarm Mind|Esprit de la nuée|libre|
|[AuRaykn2A42pUIob.htm](pathfinder-bestiary-2-items/AuRaykn2A42pUIob.htm)|Jaws|Mâchoires|libre|
|[avWzQnKIoQnTzxDr.htm](pathfinder-bestiary-2-items/avWzQnKIoQnTzxDr.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[AWGPMKkIerwV9Q8R.htm](pathfinder-bestiary-2-items/AWGPMKkIerwV9Q8R.htm)|Freezing Breath|Souffle glacial|officielle|
|[AWNveQn0EQ1aYORU.htm](pathfinder-bestiary-2-items/AWNveQn0EQ1aYORU.htm)|Rise Up|Soulèvement|officielle|
|[awrAfsHw9vT0Y612.htm](pathfinder-bestiary-2-items/awrAfsHw9vT0Y612.htm)|Grab|Empoignade/Agrippement|libre|
|[Ax2F85HRXfPDtO6x.htm](pathfinder-bestiary-2-items/Ax2F85HRXfPDtO6x.htm)|At-Will Spells|Sorts à volonté|libre|
|[ax8IJyIaesSgfUVD.htm](pathfinder-bestiary-2-items/ax8IJyIaesSgfUVD.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[axUDNy9X18Sypfa8.htm](pathfinder-bestiary-2-items/axUDNy9X18Sypfa8.htm)|+1 Status Bonus on Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegardes contre la magie|libre|
|[aYcG8JHXY8UJvVcF.htm](pathfinder-bestiary-2-items/aYcG8JHXY8UJvVcF.htm)|Sticky Feet|Pattes collantes|officielle|
|[ayFG0wkvOvyHre59.htm](pathfinder-bestiary-2-items/ayFG0wkvOvyHre59.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[AYVzOquWiUdDuNN3.htm](pathfinder-bestiary-2-items/AYVzOquWiUdDuNN3.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[azVkPps7BUkHzups.htm](pathfinder-bestiary-2-items/azVkPps7BUkHzups.htm)|Baleful Shriek|Hurlement sinistre|officielle|
|[B0ACGOBzm6iH5fdI.htm](pathfinder-bestiary-2-items/B0ACGOBzm6iH5fdI.htm)|Push|Bousculade|officielle|
|[B0vz4xw1pLJ9FeyD.htm](pathfinder-bestiary-2-items/B0vz4xw1pLJ9FeyD.htm)|Pounce|Bond|officielle|
|[B26U4dSQygOAKMgv.htm](pathfinder-bestiary-2-items/B26U4dSQygOAKMgv.htm)|Archon's Door|Porte de l'archon|libre|
|[B2rF8mguredueRbP.htm](pathfinder-bestiary-2-items/B2rF8mguredueRbP.htm)|Hoof|Sabot|libre|
|[b342tA8Gkde6f6zo.htm](pathfinder-bestiary-2-items/b342tA8Gkde6f6zo.htm)|Jaws|Mâchoires|libre|
|[B4lZzVAkZocFD4OC.htm](pathfinder-bestiary-2-items/B4lZzVAkZocFD4OC.htm)|Hoof|Sabot|libre|
|[b67N2gXUAWetXEVH.htm](pathfinder-bestiary-2-items/b67N2gXUAWetXEVH.htm)|Grab|Empoigner/Agripper|libre|
|[B73QNqTjGdZQ9uba.htm](pathfinder-bestiary-2-items/B73QNqTjGdZQ9uba.htm)|Petrifying Glance|Coup d'oeil pétrifiant|officielle|
|[B7bDAaNEMatWBkDD.htm](pathfinder-bestiary-2-items/B7bDAaNEMatWBkDD.htm)|Camouflage|Camouflage|officielle|
|[b7ci3cZP3HOyvnjf.htm](pathfinder-bestiary-2-items/b7ci3cZP3HOyvnjf.htm)|Constant Spells|Sorts constants|libre|
|[b9pe5dbiY9560pAA.htm](pathfinder-bestiary-2-items/b9pe5dbiY9560pAA.htm)|At-Will Spells|Sorts à volonté|officielle|
|[BA1HIu1Y9Xs74B9s.htm](pathfinder-bestiary-2-items/BA1HIu1Y9Xs74B9s.htm)|Darkvision|Vision dans le noir|libre|
|[ba5N4O9vroJScmAq.htm](pathfinder-bestiary-2-items/ba5N4O9vroJScmAq.htm)|Misty Form|Forme de brume|officielle|
|[BbeKEw6uMpYbSxue.htm](pathfinder-bestiary-2-items/BbeKEw6uMpYbSxue.htm)|Mandibles|Mandibules|libre|
|[bBQMy1lr5xW6rBgj.htm](pathfinder-bestiary-2-items/bBQMy1lr5xW6rBgj.htm)|Temporal Strike|Frappe temporelle|officielle|
|[BbzYprMJVagq4zOn.htm](pathfinder-bestiary-2-items/BbzYprMJVagq4zOn.htm)|Gust|Bourrasque|libre|
|[bcYwNYN6KGrhljcb.htm](pathfinder-bestiary-2-items/bcYwNYN6KGrhljcb.htm)|Darkvision|Vision dans le noir|libre|
|[BczEX9fZ2q5qXxKI.htm](pathfinder-bestiary-2-items/BczEX9fZ2q5qXxKI.htm)|Rock Tunneler|Foreur de roche|officielle|
|[BdObP5U0rM3c59uD.htm](pathfinder-bestiary-2-items/BdObP5U0rM3c59uD.htm)|Scent|Odorat|libre|
|[BdPJLN1AK4im8nIu.htm](pathfinder-bestiary-2-items/BdPJLN1AK4im8nIu.htm)|Breath Weapon|Arme de souffle|libre|
|[bEAhPwJXnrqxPt5i.htm](pathfinder-bestiary-2-items/bEAhPwJXnrqxPt5i.htm)|Blend with Light|Se fondre dans la lumière|officielle|
|[bezkLxOASRaFHd9O.htm](pathfinder-bestiary-2-items/bezkLxOASRaFHd9O.htm)|Composite Longbow|Arc long composite|libre|
|[BfQDGpd8cSNGgENd.htm](pathfinder-bestiary-2-items/BfQDGpd8cSNGgENd.htm)|Retract Tongue|Rétractation de la langue|officielle|
|[bIfz2rvBHXjIjmyK.htm](pathfinder-bestiary-2-items/bIfz2rvBHXjIjmyK.htm)|Darkvision|Vision dans le noir|libre|
|[BIIDNJTjlW3lJ1b2.htm](pathfinder-bestiary-2-items/BIIDNJTjlW3lJ1b2.htm)|Volcanic Veins|Veines volcaniques|officielle|
|[BIjOxLctW7PgHsOt.htm](pathfinder-bestiary-2-items/BIjOxLctW7PgHsOt.htm)|Compsognathus Venom|Venin de compsognathus|officielle|
|[bj3LBfUDYYqFHskT.htm](pathfinder-bestiary-2-items/bj3LBfUDYYqFHskT.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[bJUTz62klSlUEuTo.htm](pathfinder-bestiary-2-items/bJUTz62klSlUEuTo.htm)|Drink Blood|Boire le sang|officielle|
|[bJVD9JQb7YrlkdAs.htm](pathfinder-bestiary-2-items/bJVD9JQb7YrlkdAs.htm)|Void Child|Enfant du vide|officielle|
|[bJzQGcgwU0vilj1V.htm](pathfinder-bestiary-2-items/bJzQGcgwU0vilj1V.htm)|Culdewen's Curse|Malédiction du Culdewen|officielle|
|[bk1rrI0FiHN6P4an.htm](pathfinder-bestiary-2-items/bk1rrI0FiHN6P4an.htm)|Rapid Stinging|Piqûre rapide|officielle|
|[Bk63QeRIxXqBysrQ.htm](pathfinder-bestiary-2-items/Bk63QeRIxXqBysrQ.htm)|At-Will Spells|Sorts à volonté|libre|
|[bKdGH4QNZyOm2rhg.htm](pathfinder-bestiary-2-items/bKdGH4QNZyOm2rhg.htm)|Land the Fish|Belle prise|libre|
|[bKkmonWLmkkxYnh2.htm](pathfinder-bestiary-2-items/bKkmonWLmkkxYnh2.htm)|Camouflage|Camouflage|officielle|
|[BkLQtw75VewmqYSQ.htm](pathfinder-bestiary-2-items/BkLQtw75VewmqYSQ.htm)|Jaws|Mâchoires|libre|
|[BkmdLBAIea36UOWC.htm](pathfinder-bestiary-2-items/BkmdLBAIea36UOWC.htm)|Darkvision|Vision dans le noir|libre|
|[BKpd3Xf4hwQIvXYT.htm](pathfinder-bestiary-2-items/BKpd3Xf4hwQIvXYT.htm)|Darkvision|Vision dans le noir|libre|
|[bl1xW54BlfiFnU8h.htm](pathfinder-bestiary-2-items/bl1xW54BlfiFnU8h.htm)|Catch Rock|Interception de rochers|libre|
|[bLGN7Biu5pB1bqtL.htm](pathfinder-bestiary-2-items/bLGN7Biu5pB1bqtL.htm)|Tongue|Langue|libre|
|[bMLV80N79GMyKDyo.htm](pathfinder-bestiary-2-items/bMLV80N79GMyKDyo.htm)|Hand Crossbow (Serpentfolk Venom)|Arbalète de poing (Venin des Hommes-serpents)|officielle|
|[BmSUiM1ndU9nWVml.htm](pathfinder-bestiary-2-items/BmSUiM1ndU9nWVml.htm)|Darkvision|Vision dans le noir|libre|
|[BMv1jkvT4dSbEL8X.htm](pathfinder-bestiary-2-items/BMv1jkvT4dSbEL8X.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[bMW69DQKTMLqLasp.htm](pathfinder-bestiary-2-items/bMW69DQKTMLqLasp.htm)|Rider's Bond|Lien avec le cavalier|officielle|
|[bN7RCHuZpCHYpb2L.htm](pathfinder-bestiary-2-items/bN7RCHuZpCHYpb2L.htm)|Enraged Growth|Croissance de l'enragé|officielle|
|[bNbPPpEgnT6EWV5B.htm](pathfinder-bestiary-2-items/bNbPPpEgnT6EWV5B.htm)|Claw|Griffe|libre|
|[bo6bsp0fKrJ9ddNK.htm](pathfinder-bestiary-2-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|Discrétion|libre|
|[Bo7Y34vNrq537uhA.htm](pathfinder-bestiary-2-items/Bo7Y34vNrq537uhA.htm)|Tail|Queue|libre|
|[boC55Edo1J7Heynq.htm](pathfinder-bestiary-2-items/boC55Edo1J7Heynq.htm)|Darkvision|Vision dans le noir|libre|
|[BOGJAY8I1abQRm7S.htm](pathfinder-bestiary-2-items/BOGJAY8I1abQRm7S.htm)|At-Will Spells|Sorts à volonté|libre|
|[Bpm9xvzcp3dBpCvp.htm](pathfinder-bestiary-2-items/Bpm9xvzcp3dBpCvp.htm)|Sneak Attack|Attaque sournoise|officielle|
|[bQ7xqBShE3D6pEMf.htm](pathfinder-bestiary-2-items/bQ7xqBShE3D6pEMf.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[BQb8kxk0ZDelZ82e.htm](pathfinder-bestiary-2-items/BQb8kxk0ZDelZ82e.htm)|Darkvision|Vision dans le noir|libre|
|[BQDQsCBmg1gB3VSr.htm](pathfinder-bestiary-2-items/BQDQsCBmg1gB3VSr.htm)|Petrifying Gaze|Regard pétrifiant|officielle|
|[BRAhQrI56OQkBhNy.htm](pathfinder-bestiary-2-items/BRAhQrI56OQkBhNy.htm)|Draconic Momentum|Élan draconique|libre|
|[bRIQtOJZdj1Z9gcD.htm](pathfinder-bestiary-2-items/bRIQtOJZdj1Z9gcD.htm)|Low-Light Vision|Vision nocturne|libre|
|[brun3T7XF65PG2xu.htm](pathfinder-bestiary-2-items/brun3T7XF65PG2xu.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[bS27ihuZ0Xs3mU7a.htm](pathfinder-bestiary-2-items/bS27ihuZ0Xs3mU7a.htm)|Swipe|Frappe transversale|officielle|
|[BsKlvqGTEEIQo1cx.htm](pathfinder-bestiary-2-items/BsKlvqGTEEIQo1cx.htm)|+4 Bonus on Perception to Detect Illusions|Bonus de +4 en Perception pour détecter les Illusions|libre|
|[BsxGfhkuyAGxetGI.htm](pathfinder-bestiary-2-items/BsxGfhkuyAGxetGI.htm)|Frightful Presence|Présence terrifiante|libre|
|[BtLt92JdieUMFygu.htm](pathfinder-bestiary-2-items/BtLt92JdieUMFygu.htm)|Regeneration 20 (deactivated by acid or sonic)|Régénération 20 (désactivée par l’acide ou le son)|libre|
|[bu97rur2OngJhlHv.htm](pathfinder-bestiary-2-items/bu97rur2OngJhlHv.htm)|Titan Centipede Venom|Venin de mille-pattes titan|officielle|
|[bW6X64ql7gjX9zqQ.htm](pathfinder-bestiary-2-items/bW6X64ql7gjX9zqQ.htm)|Entangling Tendrils|Vrilles enchevêtrantes|officielle|
|[bwZ7BYouujVO39kJ.htm](pathfinder-bestiary-2-items/bwZ7BYouujVO39kJ.htm)|Draconic Resistance|Résistance draconique|officielle|
|[bxjYZ892TLi9AizW.htm](pathfinder-bestiary-2-items/bxjYZ892TLi9AizW.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[BXp3Iqh8AykJCF9i.htm](pathfinder-bestiary-2-items/BXp3Iqh8AykJCF9i.htm)|Talon|Ergot|libre|
|[ByGE43xowJglhHxP.htm](pathfinder-bestiary-2-items/ByGE43xowJglhHxP.htm)|Reactive Strikes|Frappe de réaction|officielle|
|[BYqZd7dW5USVQMnx.htm](pathfinder-bestiary-2-items/BYqZd7dW5USVQMnx.htm)|Rip and Tear|Déchirer et arracher|officielle|
|[bysIJ8d5SoGqSABO.htm](pathfinder-bestiary-2-items/bysIJ8d5SoGqSABO.htm)|Implant|Implantation|officielle|
|[bYuZrlYxVWLd0lD3.htm](pathfinder-bestiary-2-items/bYuZrlYxVWLd0lD3.htm)|Low-Light Vision|Vision nocturne|libre|
|[BZfKFoVLNm5bmKHc.htm](pathfinder-bestiary-2-items/BZfKFoVLNm5bmKHc.htm)|Mimic Shadow|Imiter les ombres|officielle|
|[c2nQF7qTMD3mwnrM.htm](pathfinder-bestiary-2-items/c2nQF7qTMD3mwnrM.htm)|Stingray Venom|Venin de Pastenague|officielle|
|[c4ByZjVeW0km1U7W.htm](pathfinder-bestiary-2-items/c4ByZjVeW0km1U7W.htm)|Stealth|Discrétion|libre|
|[c4mDZBMrkh9kwIHn.htm](pathfinder-bestiary-2-items/c4mDZBMrkh9kwIHn.htm)|Negative Healing|Guérison négative|libre|
|[C4MfMpmGULlKKhXy.htm](pathfinder-bestiary-2-items/C4MfMpmGULlKKhXy.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[C6IiDSyc4X1neQwS.htm](pathfinder-bestiary-2-items/C6IiDSyc4X1neQwS.htm)|Darkvision|Vision dans le noir|libre|
|[C6LpIB1Gy5mjBk2G.htm](pathfinder-bestiary-2-items/C6LpIB1Gy5mjBk2G.htm)|Jaws|Mâchoires|libre|
|[c6VHX9kLiVuag6R0.htm](pathfinder-bestiary-2-items/c6VHX9kLiVuag6R0.htm)|Foot|Pied|libre|
|[C7C9dLMFwNtAccbA.htm](pathfinder-bestiary-2-items/C7C9dLMFwNtAccbA.htm)|At-Will Spells|Sorts à volonté|libre|
|[c7V9HgCpZq3Unkja.htm](pathfinder-bestiary-2-items/c7V9HgCpZq3Unkja.htm)|Web|Toile|libre|
|[C91ggah5Ye65LrMH.htm](pathfinder-bestiary-2-items/C91ggah5Ye65LrMH.htm)|Leng Ruby|Rubis de Leng|officielle|
|[c9Aq0MMPKh2wKXXy.htm](pathfinder-bestiary-2-items/c9Aq0MMPKh2wKXXy.htm)|At-Will Spells|Sorts à volonté|libre|
|[CAKfIK6WRhkgpCW2.htm](pathfinder-bestiary-2-items/CAKfIK6WRhkgpCW2.htm)|Living Footsteps|Empreinte vivace|officielle|
|[cb6s0BQEA2EVpswg.htm](pathfinder-bestiary-2-items/cb6s0BQEA2EVpswg.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[cbhhzA952sTW7Nlb.htm](pathfinder-bestiary-2-items/cbhhzA952sTW7Nlb.htm)|Stealth|Discrétion|libre|
|[cCv1bOaozCwd4fB5.htm](pathfinder-bestiary-2-items/cCv1bOaozCwd4fB5.htm)|Claw|Griffe|libre|
|[CDAurAI8XPZBeHsL.htm](pathfinder-bestiary-2-items/CDAurAI8XPZBeHsL.htm)|Low-Light Vision|Vision nocturne|libre|
|[CDI8zDOfYxM1bdM6.htm](pathfinder-bestiary-2-items/CDI8zDOfYxM1bdM6.htm)|Aura of Order's Ruin|Aura de ruine de l’ordre|officielle|
|[cDS4azJaDaRZITrQ.htm](pathfinder-bestiary-2-items/cDS4azJaDaRZITrQ.htm)|Cold Vulnerability|Vulnérabilité au froid|officielle|
|[CeExWr00cFSdrZ4J.htm](pathfinder-bestiary-2-items/CeExWr00cFSdrZ4J.htm)|Buck|Désarçonner|libre|
|[CEREyXabBiMwUJlQ.htm](pathfinder-bestiary-2-items/CEREyXabBiMwUJlQ.htm)|Push|Bousculade|officielle|
|[CGd3B4KwGynCutU4.htm](pathfinder-bestiary-2-items/CGd3B4KwGynCutU4.htm)|Vulnerable to Shape Wood|Vulnérable à façonnage du bois|officielle|
|[cGXZfGlgZYjzMBP8.htm](pathfinder-bestiary-2-items/cGXZfGlgZYjzMBP8.htm)|Tremorsense|Perception des vibrations|libre|
|[ChobeFNcesw2DX01.htm](pathfinder-bestiary-2-items/ChobeFNcesw2DX01.htm)|Fangs|Crocs|libre|
|[chTQW4y1K5vGGjVn.htm](pathfinder-bestiary-2-items/chTQW4y1K5vGGjVn.htm)|Foot|Pied|libre|
|[CiQKBNNHPATwPDUX.htm](pathfinder-bestiary-2-items/CiQKBNNHPATwPDUX.htm)|Shortsword|Épée courte|libre|
|[cJ2jYiJRZR43AJCJ.htm](pathfinder-bestiary-2-items/cJ2jYiJRZR43AJCJ.htm)|Double Slash|Double taille|officielle|
|[Cj8UIWLOhEQ5yNgA.htm](pathfinder-bestiary-2-items/Cj8UIWLOhEQ5yNgA.htm)|Body Strike|Frappe corporelle|officielle|
|[cJFgwvR7wtsldJBw.htm](pathfinder-bestiary-2-items/cJFgwvR7wtsldJBw.htm)|Darkvision|Vision dans le noir|libre|
|[cjFyZppcDxVUaRJD.htm](pathfinder-bestiary-2-items/cjFyZppcDxVUaRJD.htm)|Drink Blood|Boire le sang|officielle|
|[cjYLiElA3muRcJuo.htm](pathfinder-bestiary-2-items/cjYLiElA3muRcJuo.htm)|Grotesque Gift|Don grotesque|officielle|
|[CkFeQyjFwtsMRJY8.htm](pathfinder-bestiary-2-items/CkFeQyjFwtsMRJY8.htm)|Darkvision|Vision dans le noir|libre|
|[ckpzKVXKEEwbtEV4.htm](pathfinder-bestiary-2-items/ckpzKVXKEEwbtEV4.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Cl2tanuHvlyyksIz.htm](pathfinder-bestiary-2-items/Cl2tanuHvlyyksIz.htm)|Frightful Presence|Présence terrifiante|libre|
|[cLUNjff4ATgKuIj8.htm](pathfinder-bestiary-2-items/cLUNjff4ATgKuIj8.htm)|Wicked Bite|Méchante morsure|officielle|
|[cLWUZTt9knGddxzH.htm](pathfinder-bestiary-2-items/cLWUZTt9knGddxzH.htm)|Constant Spells|Sorts constants|libre|
|[clxrPF7TPJ4sS2A6.htm](pathfinder-bestiary-2-items/clxrPF7TPJ4sS2A6.htm)|Extend Mandibles|Extension de mandibules|officielle|
|[cM6xjoHii8NbgRz1.htm](pathfinder-bestiary-2-items/cM6xjoHii8NbgRz1.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[cMrE7oz3K11aJZtC.htm](pathfinder-bestiary-2-items/cMrE7oz3K11aJZtC.htm)|Glimpse of Stolen Flesh|Vision de chair volée|officielle|
|[CNilhCQNrj60lElM.htm](pathfinder-bestiary-2-items/CNilhCQNrj60lElM.htm)|Beak|Bec|libre|
|[cNnj1mdRc6n47fUX.htm](pathfinder-bestiary-2-items/cNnj1mdRc6n47fUX.htm)|Darkvision|Vision dans le noir|libre|
|[coCCw9uVbyTHhKfl.htm](pathfinder-bestiary-2-items/coCCw9uVbyTHhKfl.htm)|Jaws|Mâchoires|libre|
|[cOypeEDeaznFsSdO.htm](pathfinder-bestiary-2-items/cOypeEDeaznFsSdO.htm)|Ravenous Attack|Attaque vorace|officielle|
|[Cp1loMnqFXw0uGPS.htm](pathfinder-bestiary-2-items/Cp1loMnqFXw0uGPS.htm)|Jaws|Mâchoires|libre|
|[cPD60wC9FBtIdzPL.htm](pathfinder-bestiary-2-items/cPD60wC9FBtIdzPL.htm)|Head Regrowth|Repousse de la tête|officielle|
|[cpSC9RcShIFK28EU.htm](pathfinder-bestiary-2-items/cpSC9RcShIFK28EU.htm)|Snout|Rostre|libre|
|[CpTRDv42ZCA5jHZL.htm](pathfinder-bestiary-2-items/CpTRDv42ZCA5jHZL.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[CpVMSme8RWwiT0UY.htm](pathfinder-bestiary-2-items/CpVMSme8RWwiT0UY.htm)|Fast Healing 2 (in boiling water or steam)|Guérison accélérée 2 (dans la vapeur ou l'eau bouillante)|officielle|
|[CQ9x9wMLcGSsM5wK.htm](pathfinder-bestiary-2-items/CQ9x9wMLcGSsM5wK.htm)|Web|Toile|libre|
|[cQH5uYYBW0hseijL.htm](pathfinder-bestiary-2-items/cQH5uYYBW0hseijL.htm)|Darkvision|Vision dans le noir|libre|
|[cqI7kqooduGDEqz6.htm](pathfinder-bestiary-2-items/cqI7kqooduGDEqz6.htm)|Warhammer|Marteau de guerre|officielle|
|[cQTIij6l7HjJwqQW.htm](pathfinder-bestiary-2-items/cQTIij6l7HjJwqQW.htm)|Telepathy|Télépathie|libre|
|[CquGR1meZf8Szzmo.htm](pathfinder-bestiary-2-items/CquGR1meZf8Szzmo.htm)|Splinter|Écharde|officielle|
|[CR1qaOcpKevyIuQj.htm](pathfinder-bestiary-2-items/CR1qaOcpKevyIuQj.htm)|Darkvision|Vision dans le noir|libre|
|[cRTNLnVuzisUG7kl.htm](pathfinder-bestiary-2-items/cRTNLnVuzisUG7kl.htm)|Improved Knockdown|Renversement amélioré|libre|
|[CRuPtzSYQoeDAWbH.htm](pathfinder-bestiary-2-items/CRuPtzSYQoeDAWbH.htm)|Swarm Mind|Esprit de nuée|libre|
|[cS4RQnXm1y1xugCt.htm](pathfinder-bestiary-2-items/cS4RQnXm1y1xugCt.htm)|Claw|Griffe|libre|
|[CSHQvYrfiR0GmczO.htm](pathfinder-bestiary-2-items/CSHQvYrfiR0GmczO.htm)|Brine Spit|Crachat d'acide|officielle|
|[CsNzwFbWatN4ivRP.htm](pathfinder-bestiary-2-items/CsNzwFbWatN4ivRP.htm)|Hurl Weapon|Projeter une arme|officielle|
|[CSSsFGyexhD5YUX3.htm](pathfinder-bestiary-2-items/CSSsFGyexhD5YUX3.htm)|Darkvision|Vision dans le noir|libre|
|[CsYqT7LTtX34B3Yz.htm](pathfinder-bestiary-2-items/CsYqT7LTtX34B3Yz.htm)|Breath Weapon|Arme de souffle|officielle|
|[CTeYc8gFkSPfWH7F.htm](pathfinder-bestiary-2-items/CTeYc8gFkSPfWH7F.htm)|Jaws|Mâchoires|libre|
|[CThMS3QhSA3Obh7Z.htm](pathfinder-bestiary-2-items/CThMS3QhSA3Obh7Z.htm)|Breath Weapon|Arme de souffle|libre|
|[Ctng3iJDIMkFh490.htm](pathfinder-bestiary-2-items/Ctng3iJDIMkFh490.htm)|Tail|Queue|libre|
|[cttVijuQGlQJk5Ux.htm](pathfinder-bestiary-2-items/cttVijuQGlQJk5Ux.htm)|Grab|Empoignade/Agrippement|libre|
|[CUcADH9CrR1t2kkL.htm](pathfinder-bestiary-2-items/CUcADH9CrR1t2kkL.htm)|Darkvision|Vision dans le noir|libre|
|[cuHB5zR7MVWma2MD.htm](pathfinder-bestiary-2-items/cuHB5zR7MVWma2MD.htm)|Cloud Form|Forme de nuage|officielle|
|[CUl4eRbkbnzMJ2kZ.htm](pathfinder-bestiary-2-items/CUl4eRbkbnzMJ2kZ.htm)|Vulnerable to Endure Elements|Vulnérable à endurance aux éléments|officielle|
|[CuMZTwnGZC3P7sm5.htm](pathfinder-bestiary-2-items/CuMZTwnGZC3P7sm5.htm)|Spiked Tail|Queue hérissée|libre|
|[cUtNYZ935fPsgj8x.htm](pathfinder-bestiary-2-items/cUtNYZ935fPsgj8x.htm)|Brine Spit|Crachat d'eau salée|officielle|
|[cVEH8toDyk2cQnEC.htm](pathfinder-bestiary-2-items/cVEH8toDyk2cQnEC.htm)|Breath Weapon|Arme de souffle|libre|
|[cvtGewKiksGkDNKz.htm](pathfinder-bestiary-2-items/cvtGewKiksGkDNKz.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[CwEVrNGC8oHJQrv0.htm](pathfinder-bestiary-2-items/CwEVrNGC8oHJQrv0.htm)|Isqulugia|Isqulugia|officielle|
|[cWj2CNZYq3EWCKZF.htm](pathfinder-bestiary-2-items/cWj2CNZYq3EWCKZF.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[CwjWGtOVvGZH0bP8.htm](pathfinder-bestiary-2-items/CwjWGtOVvGZH0bP8.htm)|Darkvision|Vision dans le noir|libre|
|[CwNuoWOgc2U8qCLd.htm](pathfinder-bestiary-2-items/CwNuoWOgc2U8qCLd.htm)|Knockdown|Renversement|libre|
|[CXc8oBEPlsp8HDTd.htm](pathfinder-bestiary-2-items/CXc8oBEPlsp8HDTd.htm)|Staff|Bâton|libre|
|[Cxm87AR1TJom2drt.htm](pathfinder-bestiary-2-items/Cxm87AR1TJom2drt.htm)|Double Claw|Double griffe|officielle|
|[CYtByJ7KaQiXTAWv.htm](pathfinder-bestiary-2-items/CYtByJ7KaQiXTAWv.htm)|Retributive Strike|Frappe punitive|officielle|
|[czBySBdDeH7WSORn.htm](pathfinder-bestiary-2-items/czBySBdDeH7WSORn.htm)|Spell Pilfer|Vol-sort|officielle|
|[D13sjvjuSrxFEe35.htm](pathfinder-bestiary-2-items/D13sjvjuSrxFEe35.htm)|No Breath|Ne respire pas|officielle|
|[D1QeVZDcMpO7hdmk.htm](pathfinder-bestiary-2-items/D1QeVZDcMpO7hdmk.htm)|Darkvision|Vision dans le noir|libre|
|[D1R03gCQbWQLLgtL.htm](pathfinder-bestiary-2-items/D1R03gCQbWQLLgtL.htm)|Splinter Volley|Volée d'échardes|officielle|
|[D3ePCbVgWGtm6c2e.htm](pathfinder-bestiary-2-items/D3ePCbVgWGtm6c2e.htm)|At-Will Spells|Sorts à volonté|libre|
|[D3z26xV27YGuSjtp.htm](pathfinder-bestiary-2-items/D3z26xV27YGuSjtp.htm)|Glaive|Coutille|libre|
|[d5CWT1AwKpYtsajX.htm](pathfinder-bestiary-2-items/d5CWT1AwKpYtsajX.htm)|Darkvision|Vision dans le noir|libre|
|[D5jAYC0XRC68X1gB.htm](pathfinder-bestiary-2-items/D5jAYC0XRC68X1gB.htm)|Greater Constrict|Constriction supérieure|libre|
|[D5p47qiv8jIAcjoL.htm](pathfinder-bestiary-2-items/D5p47qiv8jIAcjoL.htm)|Claw|Griffe|libre|
|[d68HrWYDDAvuRFIR.htm](pathfinder-bestiary-2-items/d68HrWYDDAvuRFIR.htm)|Redirect Fire|Réorientation du feu|officielle|
|[d7HHzvbGGdWbPEMo.htm](pathfinder-bestiary-2-items/d7HHzvbGGdWbPEMo.htm)|Claw|Griffe|libre|
|[D7nU92x03rn3cnoU.htm](pathfinder-bestiary-2-items/D7nU92x03rn3cnoU.htm)|Jaws|Mâchoires|libre|
|[D8xygwrc0WQBOje8.htm](pathfinder-bestiary-2-items/D8xygwrc0WQBOje8.htm)|Negative Healing|Guérison négative|libre|
|[Da6b96ZUkpdT5y8i.htm](pathfinder-bestiary-2-items/Da6b96ZUkpdT5y8i.htm)|Reel In|Enroulement|officielle|
|[Dax6qfY1KhJqryzV.htm](pathfinder-bestiary-2-items/Dax6qfY1KhJqryzV.htm)|Deep Breath|Inspiration profonde|officielle|
|[Dbrk210Q9t44P8vP.htm](pathfinder-bestiary-2-items/Dbrk210Q9t44P8vP.htm)|Claw|Griffe|libre|
|[DBzHso7FlJtYCeiU.htm](pathfinder-bestiary-2-items/DBzHso7FlJtYCeiU.htm)|At-Will Spells|Sorts à volonté|libre|
|[DC3068Ne4eQKeeW1.htm](pathfinder-bestiary-2-items/DC3068Ne4eQKeeW1.htm)|Create Spawn|Création de rejeton|officielle|
|[dCgNsIZTnOJTOkca.htm](pathfinder-bestiary-2-items/dCgNsIZTnOJTOkca.htm)|Telepathy|Télépathie|libre|
|[DcQizfyqoLcJASk7.htm](pathfinder-bestiary-2-items/DcQizfyqoLcJASk7.htm)|Surprise Attacker|Agresseur surprise|officielle|
|[DDI0cQeKx8SLhTUR.htm](pathfinder-bestiary-2-items/DDI0cQeKx8SLhTUR.htm)|Swift Swimmer|Nageur véloce|officielle|
|[de8ASlT2fs4snv5M.htm](pathfinder-bestiary-2-items/de8ASlT2fs4snv5M.htm)|Knockdown|Renversement|libre|
|[DEac5zIyNqdQYdnc.htm](pathfinder-bestiary-2-items/DEac5zIyNqdQYdnc.htm)|Grab|Empoignade/Agrippement|libre|
|[dEiJsJoECS4hvDnB.htm](pathfinder-bestiary-2-items/dEiJsJoECS4hvDnB.htm)|Sneak Attack|Attaque sournoise|officielle|
|[deytOmtiZjHFzGRU.htm](pathfinder-bestiary-2-items/deytOmtiZjHFzGRU.htm)|Athach Venom|Venin d’athach|officielle|
|[dF7LLSCwf5DeGcNU.htm](pathfinder-bestiary-2-items/dF7LLSCwf5DeGcNU.htm)|Rend|Éventration|libre|
|[DfbcYHdJzDXVkgEs.htm](pathfinder-bestiary-2-items/DfbcYHdJzDXVkgEs.htm)|Swallow Whole|Gober|libre|
|[DfefDz8ZxlBybpoa.htm](pathfinder-bestiary-2-items/DfefDz8ZxlBybpoa.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[dFKXMQ8FbHDHNhTC.htm](pathfinder-bestiary-2-items/dFKXMQ8FbHDHNhTC.htm)|Quick Bomber|Artificier rapide|officielle|
|[dFTh6F51ONxZqLHO.htm](pathfinder-bestiary-2-items/dFTh6F51ONxZqLHO.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[dG6wdlMkywKsurgA.htm](pathfinder-bestiary-2-items/dG6wdlMkywKsurgA.htm)|Fist|Poing|libre|
|[DI1RbgDDypEQJCMq.htm](pathfinder-bestiary-2-items/DI1RbgDDypEQJCMq.htm)|Steal Breath|Vol de souffle|officielle|
|[DJ4DzpxUnMokZPId.htm](pathfinder-bestiary-2-items/DJ4DzpxUnMokZPId.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[dj9YzVTLAtPASMHB.htm](pathfinder-bestiary-2-items/dj9YzVTLAtPASMHB.htm)|Creeping Cold|Froid insidieux|officielle|
|[djpxVgfWCno4NZwq.htm](pathfinder-bestiary-2-items/djpxVgfWCno4NZwq.htm)|Breath Weapon|Arme de souffle|libre|
|[dkeTBVoAjjtnQkuW.htm](pathfinder-bestiary-2-items/dkeTBVoAjjtnQkuW.htm)|Scent|Odorat|libre|
|[dkFTU2VIAk9BBNw3.htm](pathfinder-bestiary-2-items/dkFTU2VIAk9BBNw3.htm)|Greater Constrict|Constriction supérieure|libre|
|[dkgK1et0DFUYLpwT.htm](pathfinder-bestiary-2-items/dkgK1et0DFUYLpwT.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[DL16zUuUzuYySm31.htm](pathfinder-bestiary-2-items/DL16zUuUzuYySm31.htm)|Wing|Aile|libre|
|[DLcmdOFICEUjArKs.htm](pathfinder-bestiary-2-items/DLcmdOFICEUjArKs.htm)|Jaws|Mâchoires|libre|
|[DlQoJTMEjgmsWgCa.htm](pathfinder-bestiary-2-items/DlQoJTMEjgmsWgCa.htm)|Mandibles|Mandibules|libre|
|[DmYndlktCCjOahA2.htm](pathfinder-bestiary-2-items/DmYndlktCCjOahA2.htm)|Spiritual Warden|Gardien spirituel|officielle|
|[DnurghQjCJRhyW00.htm](pathfinder-bestiary-2-items/DnurghQjCJRhyW00.htm)|Lifesense|Perception de la vie|libre|
|[dNZMLg4zcPVX3fSU.htm](pathfinder-bestiary-2-items/dNZMLg4zcPVX3fSU.htm)|Darkvision|Vision dans le noir|libre|
|[dO0urnF4xfwl5Yp8.htm](pathfinder-bestiary-2-items/dO0urnF4xfwl5Yp8.htm)|Ravenous Embrace|Étreinte vorace|officielle|
|[dOcDYCNGkTD6GGcZ.htm](pathfinder-bestiary-2-items/dOcDYCNGkTD6GGcZ.htm)|Splinter Spray|Jet d'échardes|officielle|
|[dOkhtnPU3mvJbRar.htm](pathfinder-bestiary-2-items/dOkhtnPU3mvJbRar.htm)|Tail|Queue|libre|
|[DPcRdZIL38qcDj2I.htm](pathfinder-bestiary-2-items/DPcRdZIL38qcDj2I.htm)|Snatch|Saisir|officielle|
|[DpIIIQSKrhsFKToW.htm](pathfinder-bestiary-2-items/DpIIIQSKrhsFKToW.htm)|Holy Beam|Rayon saint|officielle|
|[dplDPboVsTPEngpW.htm](pathfinder-bestiary-2-items/dplDPboVsTPEngpW.htm)|Beak|Bec|libre|
|[DQRNNHu5GqCVAno4.htm](pathfinder-bestiary-2-items/DQRNNHu5GqCVAno4.htm)|Fangs|Crocs|libre|
|[DQT6UjypWovf9CmL.htm](pathfinder-bestiary-2-items/DQT6UjypWovf9CmL.htm)|Darkvision|Vision dans le noir|libre|
|[drwHha4i2p4cqBa7.htm](pathfinder-bestiary-2-items/drwHha4i2p4cqBa7.htm)|Tail|Queue|libre|
|[DryXaAfUqz1HN8lZ.htm](pathfinder-bestiary-2-items/DryXaAfUqz1HN8lZ.htm)|Fist|Poing|libre|
|[dSkIJ0gsh7XlH2OA.htm](pathfinder-bestiary-2-items/dSkIJ0gsh7XlH2OA.htm)|Claw|Griffe|libre|
|[DsM9HKBiut2efUZZ.htm](pathfinder-bestiary-2-items/DsM9HKBiut2efUZZ.htm)|Swarming Slither|Reptation de nuée|officielle|
|[DSxcWM211sMMf5q3.htm](pathfinder-bestiary-2-items/DSxcWM211sMMf5q3.htm)|Regurgitate Gastrolith|Régurgitation de gastrolithes|officielle|
|[Dsz0LBZEAi6Jkmmo.htm](pathfinder-bestiary-2-items/Dsz0LBZEAi6Jkmmo.htm)|Pincer|Pince|libre|
|[dsZl9DioIJswY3Gs.htm](pathfinder-bestiary-2-items/dsZl9DioIJswY3Gs.htm)|At-Will Spells|Sorts à volonté|libre|
|[DT1P11EFBYdmCUeQ.htm](pathfinder-bestiary-2-items/DT1P11EFBYdmCUeQ.htm)|Claw|Griffe|libre|
|[DtSPqabFqLrQDfGC.htm](pathfinder-bestiary-2-items/DtSPqabFqLrQDfGC.htm)|Low-Light Vision|Vision nocturne|libre|
|[dTZURdZLg8n5enSz.htm](pathfinder-bestiary-2-items/dTZURdZLg8n5enSz.htm)|Lifesense|Perception de la vie|libre|
|[DU6e2UkT2fc49Zk3.htm](pathfinder-bestiary-2-items/DU6e2UkT2fc49Zk3.htm)|Lesser Alchemist's Fire|Feu grégois inférieur|officielle|
|[dUfRUEqARe3yHUZM.htm](pathfinder-bestiary-2-items/dUfRUEqARe3yHUZM.htm)|Venom Spray|Projection de venin|officielle|
|[DUJLW3KPZ4HNw8je.htm](pathfinder-bestiary-2-items/DUJLW3KPZ4HNw8je.htm)|+4 Status Bonus on Will Saves vs Mental|Bonus de statut de +4 contre le mental|officielle|
|[DuUso0h1BlAS0Ch3.htm](pathfinder-bestiary-2-items/DuUso0h1BlAS0Ch3.htm)|Darkvision|Vision dans le noir|libre|
|[DVIJX5RWBUIXJUk5.htm](pathfinder-bestiary-2-items/DVIJX5RWBUIXJUk5.htm)|Scent|Odorat|libre|
|[dW28Q4hMeEWpDtay.htm](pathfinder-bestiary-2-items/dW28Q4hMeEWpDtay.htm)|At-Will Spells|Sorts à volonté|libre|
|[dx1pnJnp1Gvp5XLN.htm](pathfinder-bestiary-2-items/dx1pnJnp1Gvp5XLN.htm)|Darkvision|Vision dans le noir|libre|
|[dXKhtDCdlXRIWq2O.htm](pathfinder-bestiary-2-items/dXKhtDCdlXRIWq2O.htm)|Fangs|Crocs|libre|
|[dydhZgaxaoJL5dXx.htm](pathfinder-bestiary-2-items/dydhZgaxaoJL5dXx.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[dYdv3Ex4iHyirwUo.htm](pathfinder-bestiary-2-items/dYdv3Ex4iHyirwUo.htm)|Darkvision|Vision dans le noir|libre|
|[DyfwddYc7CkBo0HK.htm](pathfinder-bestiary-2-items/DyfwddYc7CkBo0HK.htm)|At-Will Spells|Sorts à volonté|libre|
|[dYHg3Ty9ZIAGqZaE.htm](pathfinder-bestiary-2-items/dYHg3Ty9ZIAGqZaE.htm)|At-Will Spells|Sorts à volonté|libre|
|[dyis1hyzjJyWz9Sg.htm](pathfinder-bestiary-2-items/dyis1hyzjJyWz9Sg.htm)|Rock|Rocher|libre|
|[dZoSOjCNPr4EIzGG.htm](pathfinder-bestiary-2-items/dZoSOjCNPr4EIzGG.htm)|Petrifying Glance|Coup d'oeil pétrifiant|officielle|
|[E1FAcXClkBLXXYC9.htm](pathfinder-bestiary-2-items/E1FAcXClkBLXXYC9.htm)|Claw|Griffe|libre|
|[E5KXLULpkukpqygI.htm](pathfinder-bestiary-2-items/E5KXLULpkukpqygI.htm)|Claw|Griffe|libre|
|[E7WX10Amh46NUWCC.htm](pathfinder-bestiary-2-items/E7WX10Amh46NUWCC.htm)|Low-Light Vision|Vision nocturne|libre|
|[E89TNZAj6eZb2Z9E.htm](pathfinder-bestiary-2-items/E89TNZAj6eZb2Z9E.htm)|Tentacle|Tentacule|libre|
|[E8ICm8MwDDWyjgKR.htm](pathfinder-bestiary-2-items/E8ICm8MwDDWyjgKR.htm)|Rock|Rocher|libre|
|[e8r1Ct8F3Oee48fo.htm](pathfinder-bestiary-2-items/e8r1Ct8F3Oee48fo.htm)|Tremorsense 60 feet|Perception des vibrations 18 m|libre|
|[E9aF47rwjTOQ1OTC.htm](pathfinder-bestiary-2-items/E9aF47rwjTOQ1OTC.htm)|Scent|Odorat|libre|
|[E9d31Pl4TeWAIjWG.htm](pathfinder-bestiary-2-items/E9d31Pl4TeWAIjWG.htm)|At-Will Spells|Sorts à volonté|libre|
|[E9FVtTu2qyp5HgnY.htm](pathfinder-bestiary-2-items/E9FVtTu2qyp5HgnY.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[e9VfAq0loJF4o49U.htm](pathfinder-bestiary-2-items/e9VfAq0loJF4o49U.htm)|Throw Rock|Lancer de rocher|libre|
|[EaiT42eaTUXQMPIu.htm](pathfinder-bestiary-2-items/EaiT42eaTUXQMPIu.htm)|Fist|Poing|libre|
|[EakALyMQUSW9bDgO.htm](pathfinder-bestiary-2-items/EakALyMQUSW9bDgO.htm)|Painsight|Vision de la douleur|officielle|
|[eb6RXroslhq0fMd9.htm](pathfinder-bestiary-2-items/eb6RXroslhq0fMd9.htm)|Extra Reaction|Surcroît de réaction|officielle|
|[eBkXc5S4Spo7zZ3w.htm](pathfinder-bestiary-2-items/eBkXc5S4Spo7zZ3w.htm)|Darkvision|Vision dans le noir|libre|
|[EBnYEijsgTINPs3Y.htm](pathfinder-bestiary-2-items/EBnYEijsgTINPs3Y.htm)|Sickle|Serpe|libre|
|[eBR0R2mRoSAc5xwp.htm](pathfinder-bestiary-2-items/eBR0R2mRoSAc5xwp.htm)|Claw|Griffe|libre|
|[ed300vUjktB7yhD7.htm](pathfinder-bestiary-2-items/ed300vUjktB7yhD7.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[ED7Sb3O9SLxFnTRr.htm](pathfinder-bestiary-2-items/ED7Sb3O9SLxFnTRr.htm)|Holy Greatsword|Épée à deux mains Sainte|libre|
|[edczs6WrO42Ttglo.htm](pathfinder-bestiary-2-items/edczs6WrO42Ttglo.htm)|Constrict|Constriction|officielle|
|[edEJeTPVOOPPaCiE.htm](pathfinder-bestiary-2-items/edEJeTPVOOPPaCiE.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[EDGqAUdAUwkbCzQK.htm](pathfinder-bestiary-2-items/EDGqAUdAUwkbCzQK.htm)|Spew Mud|Crachat de boue|officielle|
|[eE4KRcdYFo35NqFR.htm](pathfinder-bestiary-2-items/eE4KRcdYFo35NqFR.htm)|Capsize|Chavirer|officielle|
|[EelpjpvvyRcCSTCg.htm](pathfinder-bestiary-2-items/EelpjpvvyRcCSTCg.htm)|Darkvision|Vision dans le noir|libre|
|[EEQTDYHn0GwNCqRL.htm](pathfinder-bestiary-2-items/EEQTDYHn0GwNCqRL.htm)|Retributive Strike|Frappe punitive|officielle|
|[eESVKEI54htLp14a.htm](pathfinder-bestiary-2-items/eESVKEI54htLp14a.htm)|Low-Light Vision|Vision nocturne|libre|
|[EEVNlijtDn2A6Y9J.htm](pathfinder-bestiary-2-items/EEVNlijtDn2A6Y9J.htm)|At-Will Spells|Sorts à volonté|libre|
|[EF6jFpXwBUM3i2gf.htm](pathfinder-bestiary-2-items/EF6jFpXwBUM3i2gf.htm)|Fist|Poing|libre|
|[eF8APed2EwYOr5dF.htm](pathfinder-bestiary-2-items/eF8APed2EwYOr5dF.htm)|Rupturing Venom|Venin de rupture|officielle|
|[efL2PrxRnB9GLHmZ.htm](pathfinder-bestiary-2-items/efL2PrxRnB9GLHmZ.htm)|At-Will Spells|Sorts à volonté|libre|
|[eFL7EhhQB0m79mqu.htm](pathfinder-bestiary-2-items/eFL7EhhQB0m79mqu.htm)|Claw|Griffe|libre|
|[eFQH0YR5OMYZyD7n.htm](pathfinder-bestiary-2-items/eFQH0YR5OMYZyD7n.htm)|At-Will Spells|Sorts à volonté|libre|
|[EG42MTaDcKnzDi6h.htm](pathfinder-bestiary-2-items/EG42MTaDcKnzDi6h.htm)|Jaws|Mâchoires|libre|
|[EHjIsHVK3BSAzKZW.htm](pathfinder-bestiary-2-items/EHjIsHVK3BSAzKZW.htm)|Focus Gaze|Focaliser le regard|officielle|
|[ehOgYBBQ86ktUisv.htm](pathfinder-bestiary-2-items/ehOgYBBQ86ktUisv.htm)|Grab|Empoignade/Agrippement|libre|
|[ehUknW9WZvN9eNjb.htm](pathfinder-bestiary-2-items/ehUknW9WZvN9eNjb.htm)|Darkvision|Vision dans le noir|libre|
|[Ehx0cPu5CtggKCGE.htm](pathfinder-bestiary-2-items/Ehx0cPu5CtggKCGE.htm)|Low-Light Vision|Vision nocturne|libre|
|[eiefYuDeu60ebYAN.htm](pathfinder-bestiary-2-items/eiefYuDeu60ebYAN.htm)|+1 Circumstance Bonus on all Saving Throws|Bonus de circonstance de +1 sur tous les jets de sauvegarde|libre|
|[EIhaPhUMiN88cQT6.htm](pathfinder-bestiary-2-items/EIhaPhUMiN88cQT6.htm)|Aura of Righteousness|Aura de vertu|officielle|
|[EILiYrpIKrSD8lNU.htm](pathfinder-bestiary-2-items/EILiYrpIKrSD8lNU.htm)|Bite|Morsure|libre|
|[EIqtF3pL4wgzl7kp.htm](pathfinder-bestiary-2-items/EIqtF3pL4wgzl7kp.htm)|Lifesense|Perception de la vie|libre|
|[eIV0ZcgzUZI0e8VM.htm](pathfinder-bestiary-2-items/eIV0ZcgzUZI0e8VM.htm)|Earth Glide|Glissement de terrain|officielle|
|[EK9tHuUdNF8lzwwK.htm](pathfinder-bestiary-2-items/EK9tHuUdNF8lzwwK.htm)|Club|Gourdin|libre|
|[eKrrD4BYO7xl9vPo.htm](pathfinder-bestiary-2-items/eKrrD4BYO7xl9vPo.htm)|Fearful Attack|Attaque effrayante|officielle|
|[eKSzSgT3W8A9oNzs.htm](pathfinder-bestiary-2-items/eKSzSgT3W8A9oNzs.htm)|Multiple Opportunities|Opportunités multiples|officielle|
|[El38bC5IzKKtBdcW.htm](pathfinder-bestiary-2-items/El38bC5IzKKtBdcW.htm)|Steal Voice|Vol de voix|officielle|
|[ElDo5C9tGNaFEyUo.htm](pathfinder-bestiary-2-items/ElDo5C9tGNaFEyUo.htm)|Tail|Queue|libre|
|[ELI8vuFBdrN99fE3.htm](pathfinder-bestiary-2-items/ELI8vuFBdrN99fE3.htm)|Tendril|Vrille|libre|
|[elLeiMASiyy7jPxz.htm](pathfinder-bestiary-2-items/elLeiMASiyy7jPxz.htm)|Spirit Touch|Contact spirituel|officielle|
|[ENtN1odPFGnOKcBk.htm](pathfinder-bestiary-2-items/ENtN1odPFGnOKcBk.htm)|Telepathy|Télépathie|libre|
|[Eo3e73BdyU4oMYEq.htm](pathfinder-bestiary-2-items/Eo3e73BdyU4oMYEq.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[EO9iiH6RJCSv55DM.htm](pathfinder-bestiary-2-items/EO9iiH6RJCSv55DM.htm)|Root|Racine|libre|
|[eohTEKJnqW3cF8U8.htm](pathfinder-bestiary-2-items/eohTEKJnqW3cF8U8.htm)|Force Bolt|Trait de force|officielle|
|[ephUPtGMB5sA3OWQ.htm](pathfinder-bestiary-2-items/ephUPtGMB5sA3OWQ.htm)|Tremorsense|Perception des vibrations|libre|
|[eqGZhn9lsZI6JDYE.htm](pathfinder-bestiary-2-items/eqGZhn9lsZI6JDYE.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[Eqk7JKBf2mGkZbCf.htm](pathfinder-bestiary-2-items/Eqk7JKBf2mGkZbCf.htm)|Darkvision|Vision dans le noir|libre|
|[Er19GkrGFluxc3FN.htm](pathfinder-bestiary-2-items/Er19GkrGFluxc3FN.htm)|At-Will Spells|Sorts à volonté|libre|
|[eR2U38j5LSvSk5t6.htm](pathfinder-bestiary-2-items/eR2U38j5LSvSk5t6.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[eR8AFynWU8vfsTA9.htm](pathfinder-bestiary-2-items/eR8AFynWU8vfsTA9.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[ErIhDSVTBd7aGHeC.htm](pathfinder-bestiary-2-items/ErIhDSVTBd7aGHeC.htm)|Stinger|Dard|libre|
|[ERoazbmYgnuBSYsk.htm](pathfinder-bestiary-2-items/ERoazbmYgnuBSYsk.htm)|Xill Paralysis|Paralysie xille|officielle|
|[ERoYflv6bErPQNP2.htm](pathfinder-bestiary-2-items/ERoYflv6bErPQNP2.htm)|Tail|Queue|libre|
|[eTAHPb22Vfb1ZIIA.htm](pathfinder-bestiary-2-items/eTAHPb22Vfb1ZIIA.htm)|Grab|Empoignade/Agrippement|libre|
|[eTTwVRhxdeVx0PRD.htm](pathfinder-bestiary-2-items/eTTwVRhxdeVx0PRD.htm)|Low-Light Vision|Vision nocturne|libre|
|[eu0SQkCWm2vp3Yy6.htm](pathfinder-bestiary-2-items/eu0SQkCWm2vp3Yy6.htm)|Claw|Griffe|libre|
|[eUBZ8xXpWH2Gvg39.htm](pathfinder-bestiary-2-items/eUBZ8xXpWH2Gvg39.htm)|Kukri|Kukri|libre|
|[eugKdHe7dmAkjXP0.htm](pathfinder-bestiary-2-items/eugKdHe7dmAkjXP0.htm)|Witchflame Bolt|Trait de flammes ensorcelées|officielle|
|[EuS6s6eoD5RfY7jI.htm](pathfinder-bestiary-2-items/EuS6s6eoD5RfY7jI.htm)|Cloud Walk|Marche sur les nuages|officielle|
|[Ev5luBuqFKW5s9MN.htm](pathfinder-bestiary-2-items/Ev5luBuqFKW5s9MN.htm)|Water Glide|Glisser sur l'eau|officielle|
|[eVlxUKYnPRCGVP58.htm](pathfinder-bestiary-2-items/eVlxUKYnPRCGVP58.htm)|Wavesense|Perception des ondes|libre|
|[EW5LH6Dw5jEKEVOU.htm](pathfinder-bestiary-2-items/EW5LH6Dw5jEKEVOU.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[Ew7CBGn58KzAoHsQ.htm](pathfinder-bestiary-2-items/Ew7CBGn58KzAoHsQ.htm)|Bite|Morsure|officielle|
|[eWih1h98kRb5uo3E.htm](pathfinder-bestiary-2-items/eWih1h98kRb5uo3E.htm)|Telepathy|Télépathie|libre|
|[ewIhUHZqcxVqsG7x.htm](pathfinder-bestiary-2-items/ewIhUHZqcxVqsG7x.htm)|Dagger|Dague|libre|
|[EWOsMDPiDiwsUVSL.htm](pathfinder-bestiary-2-items/EWOsMDPiDiwsUVSL.htm)|At-Will Spells|Sorts à volonté|libre|
|[ewPaxkwRS28GoetB.htm](pathfinder-bestiary-2-items/ewPaxkwRS28GoetB.htm)|Jaws|Mâchoires|libre|
|[EXLaDRArP4bsh7XH.htm](pathfinder-bestiary-2-items/EXLaDRArP4bsh7XH.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[ExsxFy6Bmcgsy0GX.htm](pathfinder-bestiary-2-items/ExsxFy6Bmcgsy0GX.htm)|Low-Light Vision|Vision nocturne|libre|
|[eyEiaf2KwHBA5miS.htm](pathfinder-bestiary-2-items/eyEiaf2KwHBA5miS.htm)|Scent|Odorat|libre|
|[EYtHIPWgwGTdjCBp.htm](pathfinder-bestiary-2-items/EYtHIPWgwGTdjCBp.htm)|Fascination of Flame|Fascination de flammes|officielle|
|[ez4TlFP9ir8diYv7.htm](pathfinder-bestiary-2-items/ez4TlFP9ir8diYv7.htm)|Scuttle|Escampette|officielle|
|[eZpQaoruvmngTW6T.htm](pathfinder-bestiary-2-items/eZpQaoruvmngTW6T.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[eZt77JcDJLXdqusp.htm](pathfinder-bestiary-2-items/eZt77JcDJLXdqusp.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[f1adZO1FX8WkPN1h.htm](pathfinder-bestiary-2-items/f1adZO1FX8WkPN1h.htm)|Violet Rot|Putréfaction violette|officielle|
|[F1hR4vcfoAB63QNp.htm](pathfinder-bestiary-2-items/F1hR4vcfoAB63QNp.htm)|Frightful Presence|Présence terrifiante|libre|
|[F2mPoxLl7PTJpc3I.htm](pathfinder-bestiary-2-items/F2mPoxLl7PTJpc3I.htm)|All-Around Vision|Vision à 360°|libre|
|[f3X4SLxmMSok3p65.htm](pathfinder-bestiary-2-items/f3X4SLxmMSok3p65.htm)|Fate Drain|Drain du destin|officielle|
|[F40AJYqQ0BCC2TF0.htm](pathfinder-bestiary-2-items/F40AJYqQ0BCC2TF0.htm)|Chain|Chaîne|libre|
|[f4JPwXqfZM8fy4Zp.htm](pathfinder-bestiary-2-items/f4JPwXqfZM8fy4Zp.htm)|Absorb Force|Absorbtion de force|officielle|
|[F4z6xO5BYtoag9hK.htm](pathfinder-bestiary-2-items/F4z6xO5BYtoag9hK.htm)|Jaws|Mâchoires|libre|
|[f61dlQmeP2vZs1Go.htm](pathfinder-bestiary-2-items/f61dlQmeP2vZs1Go.htm)|Darkvision|Vision dans le noir|libre|
|[F7GzmZlJ6PXy8Cm5.htm](pathfinder-bestiary-2-items/F7GzmZlJ6PXy8Cm5.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[f7niepbS8VyknoBK.htm](pathfinder-bestiary-2-items/f7niepbS8VyknoBK.htm)|Scintillating Aura|Aura scintillante|officielle|
|[fakAfk3puTMpw1vT.htm](pathfinder-bestiary-2-items/fakAfk3puTMpw1vT.htm)|Slow Metabolism|Métabolisme lent|officielle|
|[FapwAUPhHeXc1NKj.htm](pathfinder-bestiary-2-items/FapwAUPhHeXc1NKj.htm)|Warlord's Training|Entraînement du seigneur de guerre|officielle|
|[FBiCgn8XESVcHSPx.htm](pathfinder-bestiary-2-items/FBiCgn8XESVcHSPx.htm)|Jaws|Mâchoires|libre|
|[FBO25NSysfQagiQl.htm](pathfinder-bestiary-2-items/FBO25NSysfQagiQl.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[FBYVc1nHfCqd2hLL.htm](pathfinder-bestiary-2-items/FBYVc1nHfCqd2hLL.htm)|Morningstar|Morgenstern|libre|
|[Fc0hTwlWHz8gcaaN.htm](pathfinder-bestiary-2-items/Fc0hTwlWHz8gcaaN.htm)|Tail|Queue|libre|
|[FcafMwq1Bll5o21K.htm](pathfinder-bestiary-2-items/FcafMwq1Bll5o21K.htm)|Deep Breath|Inspiration profonde|officielle|
|[fcCuiTHr7ed8LhqK.htm](pathfinder-bestiary-2-items/fcCuiTHr7ed8LhqK.htm)|Fangs|Crocs|libre|
|[FcLtb9IKlH2X8wBl.htm](pathfinder-bestiary-2-items/FcLtb9IKlH2X8wBl.htm)|Darkvision|Vision dans le noir|libre|
|[FcnwHTEpIdO7oCXf.htm](pathfinder-bestiary-2-items/FcnwHTEpIdO7oCXf.htm)|Darkvision|Vision dans le noir|libre|
|[Fd4JoDpXliyIpcvN.htm](pathfinder-bestiary-2-items/Fd4JoDpXliyIpcvN.htm)|Horn|Corne|libre|
|[fDD7cIf8ZsYLF8fv.htm](pathfinder-bestiary-2-items/fDD7cIf8ZsYLF8fv.htm)|Impaling Critical|Empalement critique|officielle|
|[fDEMKsDuC7SNDbx6.htm](pathfinder-bestiary-2-items/fDEMKsDuC7SNDbx6.htm)|Unbodied Possession|Possession sans corps|officielle|
|[FdZZPAWhuFULerJd.htm](pathfinder-bestiary-2-items/FdZZPAWhuFULerJd.htm)|Trumpet Blast|Coup de trompette|officielle|
|[fFFM3BNDIa0yq0lg.htm](pathfinder-bestiary-2-items/fFFM3BNDIa0yq0lg.htm)|Regeneration 20 (deactivated by good or silver)|Régénération 20 (désactivée par bon ou argent)|libre|
|[FFRvTDGH1FEnAKjH.htm](pathfinder-bestiary-2-items/FFRvTDGH1FEnAKjH.htm)|Splintering Death|Mort par échardes|officielle|
|[ffxbLuQioRQZCSba.htm](pathfinder-bestiary-2-items/ffxbLuQioRQZCSba.htm)|Darkvision|Vision dans le noir|libre|
|[fg8mmyOMmhcaaO4s.htm](pathfinder-bestiary-2-items/fg8mmyOMmhcaaO4s.htm)|Fast Healing 2 (when touching mud or slime)|Guérison accélérée 2 (au contact de la boue ou du limon)|libre|
|[fHcej8vlhHnCFoab.htm](pathfinder-bestiary-2-items/fHcej8vlhHnCFoab.htm)|Tremorsense|Perception des vibrations|libre|
|[FHQiK6c7jZUggMXg.htm](pathfinder-bestiary-2-items/FHQiK6c7jZUggMXg.htm)|Darkvision|Vision dans le noir|libre|
|[FHyqgRwAZouh37Gw.htm](pathfinder-bestiary-2-items/FHyqgRwAZouh37Gw.htm)|Fangs|Crocs|libre|
|[FIBgwJl6ghhZJB6Y.htm](pathfinder-bestiary-2-items/FIBgwJl6ghhZJB6Y.htm)|Quetz Couatl Venom|Venin de quetz couatl|officielle|
|[fiJwhvqSCYbkgaJg.htm](pathfinder-bestiary-2-items/fiJwhvqSCYbkgaJg.htm)|Swear|Juron|officielle|
|[filzxUgupqvL1ZQq.htm](pathfinder-bestiary-2-items/filzxUgupqvL1ZQq.htm)|Gouging Lunge|Feinte de lacération|officielle|
|[FjdhY5Rvi0x7AdX0.htm](pathfinder-bestiary-2-items/FjdhY5Rvi0x7AdX0.htm)|Darkvision|Vision dans le noir|libre|
|[fJJc56GDg0EB3oit.htm](pathfinder-bestiary-2-items/fJJc56GDg0EB3oit.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[FKd8OWhPrCEcBKFC.htm](pathfinder-bestiary-2-items/FKd8OWhPrCEcBKFC.htm)|Darkvision|Vision dans le noir|libre|
|[fKeX8OGCBc8j0eJu.htm](pathfinder-bestiary-2-items/fKeX8OGCBc8j0eJu.htm)|Low-Light Vision|Vision nocturne|libre|
|[FKgN8Ya6AS8vCOry.htm](pathfinder-bestiary-2-items/FKgN8Ya6AS8vCOry.htm)|Darkvision|Vision dans le noir|libre|
|[FKpHGfQVea81uMo2.htm](pathfinder-bestiary-2-items/FKpHGfQVea81uMo2.htm)|Motionsense|Perception du mouvement|officielle|
|[fKqimVsLOPsE2xN0.htm](pathfinder-bestiary-2-items/fKqimVsLOPsE2xN0.htm)|Menace to Magic|Menace pour la magie|officielle|
|[FlbbYL5EQllXYKjc.htm](pathfinder-bestiary-2-items/FlbbYL5EQllXYKjc.htm)|Susceptible to Desiccation|Sensible à la dessiccation|officielle|
|[fm5cOYL11qhzHV3G.htm](pathfinder-bestiary-2-items/fm5cOYL11qhzHV3G.htm)|Low-Light Vision|Vision nocturne|libre|
|[fmHvsMYoy6LDxLkK.htm](pathfinder-bestiary-2-items/fmHvsMYoy6LDxLkK.htm)|Tentacle Mouth|Gueule du tentacule|libre|
|[FNBiiooVEkzVDOTE.htm](pathfinder-bestiary-2-items/FNBiiooVEkzVDOTE.htm)|Regeneration 10 (Deactivated by Good or Silver)|Régénération 10 (neutralisée par le bon ou l'argent)|libre|
|[FnRuOLk9xVqNWtrD.htm](pathfinder-bestiary-2-items/FnRuOLk9xVqNWtrD.htm)|Constant Spells|Sorts constants|libre|
|[FNSLMfCQHDGPvjsv.htm](pathfinder-bestiary-2-items/FNSLMfCQHDGPvjsv.htm)|Sudden Retreat|Retraite soudaine|officielle|
|[FnwUcB0l68viXYtQ.htm](pathfinder-bestiary-2-items/FnwUcB0l68viXYtQ.htm)|Fish Hook|Hameçon|libre|
|[fO8E1sNI1DHDU5Dq.htm](pathfinder-bestiary-2-items/fO8E1sNI1DHDU5Dq.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|libre|
|[foIpp0GPCbsHL0dl.htm](pathfinder-bestiary-2-items/foIpp0GPCbsHL0dl.htm)|Cave Scorpion Venom|Venin de scorpion des cavernes|officielle|
|[Fp4obYTKAlnrpeFi.htm](pathfinder-bestiary-2-items/Fp4obYTKAlnrpeFi.htm)|Change Shape|Changement de forme|officielle|
|[FqRKnuudm93xTutr.htm](pathfinder-bestiary-2-items/FqRKnuudm93xTutr.htm)|At-Will Spells|Sorts à volonté|libre|
|[fqXSW2GPcFXzcz2P.htm](pathfinder-bestiary-2-items/fqXSW2GPcFXzcz2P.htm)|Constant Spells|Sorts constants|libre|
|[FrDnLG8ehjXFC6Ey.htm](pathfinder-bestiary-2-items/FrDnLG8ehjXFC6Ey.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[frIks1GEvtVQAPPw.htm](pathfinder-bestiary-2-items/frIks1GEvtVQAPPw.htm)|Vine|Liane|libre|
|[FrTdGO6BqbRvBLu9.htm](pathfinder-bestiary-2-items/FrTdGO6BqbRvBLu9.htm)|Fangs|Crocs|libre|
|[fRtL8wadKYNqacu6.htm](pathfinder-bestiary-2-items/fRtL8wadKYNqacu6.htm)|Lifesense|Perception de la vie|libre|
|[fRtY0qdhDDIRBRWj.htm](pathfinder-bestiary-2-items/fRtY0qdhDDIRBRWj.htm)|Fist|Poing|libre|
|[FsgtuZo9KF7fxlp5.htm](pathfinder-bestiary-2-items/FsgtuZo9KF7fxlp5.htm)|Jaws|Mâchoires|libre|
|[FTeRIF51hvaZAOzR.htm](pathfinder-bestiary-2-items/FTeRIF51hvaZAOzR.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[FTil5NFptMbuNK7y.htm](pathfinder-bestiary-2-items/FTil5NFptMbuNK7y.htm)|Dagger|Dague|libre|
|[ftJSskhqjZaMBNyZ.htm](pathfinder-bestiary-2-items/ftJSskhqjZaMBNyZ.htm)|Jet|Propulsion|libre|
|[FUbGjkLswPM3bxUL.htm](pathfinder-bestiary-2-items/FUbGjkLswPM3bxUL.htm)|Jaws|Mâchoires|libre|
|[FuTchIYowgou1HIa.htm](pathfinder-bestiary-2-items/FuTchIYowgou1HIa.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[fuxGM2FMqoHqXPJ4.htm](pathfinder-bestiary-2-items/fuxGM2FMqoHqXPJ4.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[fUyM5YcNhV5dlwTV.htm](pathfinder-bestiary-2-items/fUyM5YcNhV5dlwTV.htm)|Ice Burrow|Creusement dans la glace|officielle|
|[fVaZtygxTBUfppLk.htm](pathfinder-bestiary-2-items/fVaZtygxTBUfppLk.htm)|Swarming Bites|Nuée de morsures|officielle|
|[fx1cIpK2KgNbm7Rz.htm](pathfinder-bestiary-2-items/fx1cIpK2KgNbm7Rz.htm)|Magma Swim|Nage dans le magma|officielle|
|[FXPT5cs0LSYkZOku.htm](pathfinder-bestiary-2-items/FXPT5cs0LSYkZOku.htm)|Claw|Griffe|libre|
|[FXSgcv7xK9yqmz6Q.htm](pathfinder-bestiary-2-items/FXSgcv7xK9yqmz6Q.htm)|Jaws|Mâchoires|libre|
|[fxuqMIJcFuZQworq.htm](pathfinder-bestiary-2-items/fxuqMIJcFuZQworq.htm)|Fangs|Crocs|libre|
|[fxZztiCJybeYYeOI.htm](pathfinder-bestiary-2-items/fxZztiCJybeYYeOI.htm)|Captivating Dance|Danse captivante|officielle|
|[FYiiFhvRvIKkTu5v.htm](pathfinder-bestiary-2-items/FYiiFhvRvIKkTu5v.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[fysusPtWaa25MvUW.htm](pathfinder-bestiary-2-items/fysusPtWaa25MvUW.htm)|Claw|Griffe|libre|
|[FZaWBsctyw8LDdfD.htm](pathfinder-bestiary-2-items/FZaWBsctyw8LDdfD.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[G0091T8nIwBjmZUF.htm](pathfinder-bestiary-2-items/G0091T8nIwBjmZUF.htm)|Low-Light Vision|Vision nocturne|libre|
|[G0D3kQHUd2mouA90.htm](pathfinder-bestiary-2-items/G0D3kQHUd2mouA90.htm)|Dominate Animal|Domination animale|officielle|
|[G0pgg8gIhgw18USv.htm](pathfinder-bestiary-2-items/G0pgg8gIhgw18USv.htm)|Trample|Piétinement|officielle|
|[g2gB3rKzB7uL7Kxn.htm](pathfinder-bestiary-2-items/g2gB3rKzB7uL7Kxn.htm)|Claw|Griffes|libre|
|[G2PIvLQqH98WKNaK.htm](pathfinder-bestiary-2-items/G2PIvLQqH98WKNaK.htm)|Planar Acclimation|Acclimation planaire|officielle|
|[g3Gb5EYfu1jFqndC.htm](pathfinder-bestiary-2-items/g3Gb5EYfu1jFqndC.htm)|Ink Cloud|Nuage d'encre|officielle|
|[G3Qoqz5IMBvs9EYb.htm](pathfinder-bestiary-2-items/G3Qoqz5IMBvs9EYb.htm)|Verdant Burst|Explosion verdoyante|officielle|
|[G412UtVloe7PIpa4.htm](pathfinder-bestiary-2-items/G412UtVloe7PIpa4.htm)|+2 Status to All Saves vs Disease and Poison|Bonus de statut de +2 à tous les jets de sauvegarde contre la Maladie et le Poison|libre|
|[g41jvLpQ6exKQBgd.htm](pathfinder-bestiary-2-items/g41jvLpQ6exKQBgd.htm)|Spines|Épines|officielle|
|[G5e1Cf5jNqFiqF6y.htm](pathfinder-bestiary-2-items/G5e1Cf5jNqFiqF6y.htm)|Grab|Empoignade/Agrippement|libre|
|[G5NJxlLVYCrXX8Jv.htm](pathfinder-bestiary-2-items/G5NJxlLVYCrXX8Jv.htm)|Twisting Tail|Queue sinueuse|officielle|
|[g5TUPRPR9gHfXrfU.htm](pathfinder-bestiary-2-items/g5TUPRPR9gHfXrfU.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[G6O55FgFD95SRjPP.htm](pathfinder-bestiary-2-items/G6O55FgFD95SRjPP.htm)|Claw|Griffe|libre|
|[G7P9My2HYy017m1O.htm](pathfinder-bestiary-2-items/G7P9My2HYy017m1O.htm)|36 AC vs. Non-Magical|36 contre non-magique|libre|
|[g9enKXrvCUkwck8W.htm](pathfinder-bestiary-2-items/g9enKXrvCUkwck8W.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[gADgaaM8ZZlYZcXg.htm](pathfinder-bestiary-2-items/gADgaaM8ZZlYZcXg.htm)|Jaws|Mâchoires|libre|
|[gAdwnZ4OP8sPpuOg.htm](pathfinder-bestiary-2-items/gAdwnZ4OP8sPpuOg.htm)|Painsight|Vision de la douleur|officielle|
|[GBBCSikxVuTyL14m.htm](pathfinder-bestiary-2-items/GBBCSikxVuTyL14m.htm)|Foot|Pied|libre|
|[gBFFibeJFk4MB72C.htm](pathfinder-bestiary-2-items/gBFFibeJFk4MB72C.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[GbfpFzRZunqiRxI5.htm](pathfinder-bestiary-2-items/GbfpFzRZunqiRxI5.htm)|Catch Rock|Interception de rochers|libre|
|[GBMDJeuQSv0Q6T5o.htm](pathfinder-bestiary-2-items/GBMDJeuQSv0Q6T5o.htm)|Breath Weapon|Arme de souffle|officielle|
|[GCC6Zf1NaPihfEJ0.htm](pathfinder-bestiary-2-items/GCC6Zf1NaPihfEJ0.htm)|Tangle Spores|Enchevêtrement de spores|officielle|
|[GcML4HwZyqVy7XVw.htm](pathfinder-bestiary-2-items/GcML4HwZyqVy7XVw.htm)|Talon|Ergot|libre|
|[GCu6OFgRjem6F0sI.htm](pathfinder-bestiary-2-items/GCu6OFgRjem6F0sI.htm)|Witchflame|Flammes ensorcelées|officielle|
|[gCwKF48GST0z9EEa.htm](pathfinder-bestiary-2-items/gCwKF48GST0z9EEa.htm)|Grab|Empoignade/Agrippement|libre|
|[GcxVApKgeD5WO9Wh.htm](pathfinder-bestiary-2-items/GcxVApKgeD5WO9Wh.htm)|Desiccating Bite|Morsure déshydratante|officielle|
|[GEb3KusxBDvL8CkC.htm](pathfinder-bestiary-2-items/GEb3KusxBDvL8CkC.htm)|Stench|Puanteur|officielle|
|[GeIadA4vgUJvJ926.htm](pathfinder-bestiary-2-items/GeIadA4vgUJvJ926.htm)|Composite Shortbow|Arc court composite|libre|
|[gfmpjwebcEUzSXoN.htm](pathfinder-bestiary-2-items/gfmpjwebcEUzSXoN.htm)|Stealth|Discrétion|libre|
|[GFW3IASGrcBSyETb.htm](pathfinder-bestiary-2-items/GFW3IASGrcBSyETb.htm)|Worm Chill|Givre du ver|officielle|
|[gGIkwe9BtDFc6qdx.htm](pathfinder-bestiary-2-items/gGIkwe9BtDFc6qdx.htm)|Tremorsense|Perception des vibrations|libre|
|[ggo5fsSPiaSiT5ot.htm](pathfinder-bestiary-2-items/ggo5fsSPiaSiT5ot.htm)|At-Will Spells|Sorts à volonté|libre|
|[gGqDItdgQIf2RRPw.htm](pathfinder-bestiary-2-items/gGqDItdgQIf2RRPw.htm)|Falchion|Cimeterre à deux mains|libre|
|[gH06P6rp2bswuGya.htm](pathfinder-bestiary-2-items/gH06P6rp2bswuGya.htm)|Jaws|Mâchoires|libre|
|[GHbCLo2MkEFt1qai.htm](pathfinder-bestiary-2-items/GHbCLo2MkEFt1qai.htm)|Scimitar|Cimeterre|libre|
|[gI0Pt3FCseRtdPpl.htm](pathfinder-bestiary-2-items/gI0Pt3FCseRtdPpl.htm)|Swarming Bites|Nuée de morsures|officielle|
|[GIj7axAUfLMpl5DN.htm](pathfinder-bestiary-2-items/GIj7axAUfLMpl5DN.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[gINTMtPPDP3qyAhd.htm](pathfinder-bestiary-2-items/gINTMtPPDP3qyAhd.htm)|At-Will Spells|Sorts à volonté|libre|
|[GIop4yLHoN44KXN5.htm](pathfinder-bestiary-2-items/GIop4yLHoN44KXN5.htm)|Trample|Piétinement|libre|
|[GJECjnP6onECplMu.htm](pathfinder-bestiary-2-items/GJECjnP6onECplMu.htm)|Fangs|Crocs|libre|
|[gJMFRPJuO2fffH1c.htm](pathfinder-bestiary-2-items/gJMFRPJuO2fffH1c.htm)|Deflecting Cloud|Nuage déflecteur|officielle|
|[GjmjXrm0BYOCXs5E.htm](pathfinder-bestiary-2-items/GjmjXrm0BYOCXs5E.htm)|Telepathy|Télépathie|libre|
|[gkiQGmXDkxDHGY7j.htm](pathfinder-bestiary-2-items/gkiQGmXDkxDHGY7j.htm)|Mask of Power|Masque de pouvoir|officielle|
|[GkMCAxjpf2Y5nfdv.htm](pathfinder-bestiary-2-items/GkMCAxjpf2Y5nfdv.htm)|Draconic Momentum|Élan draconique|libre|
|[gKqI8aZDucHlpC9C.htm](pathfinder-bestiary-2-items/gKqI8aZDucHlpC9C.htm)|Cowering Fear|Recroquevillé de terreur|officielle|
|[gKujbrE9tdbREcZi.htm](pathfinder-bestiary-2-items/gKujbrE9tdbREcZi.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[gL7E79l3yS72fXPo.htm](pathfinder-bestiary-2-items/gL7E79l3yS72fXPo.htm)|Petrifying Gaze|Regard pétrifiant|officielle|
|[gm4ckzxne8UYVaZg.htm](pathfinder-bestiary-2-items/gm4ckzxne8UYVaZg.htm)|Feral Possession|Corruption sauvage|officielle|
|[GmbRFl9ktqpdH1WE.htm](pathfinder-bestiary-2-items/GmbRFl9ktqpdH1WE.htm)|At-Will Spells|Sorts à volonté|libre|
|[gmjig3Ov1aG6tBOx.htm](pathfinder-bestiary-2-items/gmjig3Ov1aG6tBOx.htm)|Mandibles|Mandibules|libre|
|[gmtlsmLh9eNdpNrk.htm](pathfinder-bestiary-2-items/gmtlsmLh9eNdpNrk.htm)|Mud Puddle|Flaque de boue|officielle|
|[GnAoSJeodjtFoanm.htm](pathfinder-bestiary-2-items/GnAoSJeodjtFoanm.htm)|Feral Possession|Corruption sauvage|officielle|
|[gNhak3Nf3ve1ouhK.htm](pathfinder-bestiary-2-items/gNhak3Nf3ve1ouhK.htm)|Claw|Griffe|libre|
|[gnL5MFzMfrbfAapT.htm](pathfinder-bestiary-2-items/gnL5MFzMfrbfAapT.htm)|Holy Armaments|Armement saint|officielle|
|[gOjlMNwWXWNHohGo.htm](pathfinder-bestiary-2-items/gOjlMNwWXWNHohGo.htm)|Mark Quarry|Proie marquée|officielle|
|[goYkQxQdewpmD26j.htm](pathfinder-bestiary-2-items/goYkQxQdewpmD26j.htm)|Jaws|Mâchoires|libre|
|[GoYV1mj4XKxDiBOM.htm](pathfinder-bestiary-2-items/GoYV1mj4XKxDiBOM.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegardes contre la magie|libre|
|[gozNFecLMRX00Wzz.htm](pathfinder-bestiary-2-items/gozNFecLMRX00Wzz.htm)|Frightful Presence|Présence terrifiante|libre|
|[gPcAYqbYyOADoHwc.htm](pathfinder-bestiary-2-items/gPcAYqbYyOADoHwc.htm)|Top-Heavy|Sourdeur supérieure|officielle|
|[gPCpNSHZ6SvXUhij.htm](pathfinder-bestiary-2-items/gPCpNSHZ6SvXUhij.htm)|Painful Strikes|Frappes douloureuses|officielle|
|[GpD6rwvIzYFSvtUC.htm](pathfinder-bestiary-2-items/GpD6rwvIzYFSvtUC.htm)|Draconic Momentum|Élan draconique|libre|
|[GPRRfLJKZALqfKud.htm](pathfinder-bestiary-2-items/GPRRfLJKZALqfKud.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[GPSk2dGRxJdcBtW7.htm](pathfinder-bestiary-2-items/GPSk2dGRxJdcBtW7.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[gpZkw015Ns4Dbplk.htm](pathfinder-bestiary-2-items/gpZkw015Ns4Dbplk.htm)|Drowning Touch|Noyer par le toucher|officielle|
|[GQ4af1lP9hWhVhkx.htm](pathfinder-bestiary-2-items/GQ4af1lP9hWhVhkx.htm)|Frightful Presence|Présence terrifiante|libre|
|[gQnxWI3DPgkoWjmj.htm](pathfinder-bestiary-2-items/gQnxWI3DPgkoWjmj.htm)|Darkvision|Vision dans le noir|libre|
|[grFGZyZ8952tQ9yF.htm](pathfinder-bestiary-2-items/grFGZyZ8952tQ9yF.htm)|Entombing Breath|Souffle ensevelissant|officielle|
|[GRiHxWvHx9rf0zcF.htm](pathfinder-bestiary-2-items/GRiHxWvHx9rf0zcF.htm)|Expel Infestation|Infestation expulsée|officielle|
|[GryMTmkSRqTAW3ol.htm](pathfinder-bestiary-2-items/GryMTmkSRqTAW3ol.htm)|Wing|Aile|libre|
|[gS1EUP1J8sgiDyR1.htm](pathfinder-bestiary-2-items/gS1EUP1J8sgiDyR1.htm)|Consume Death|Consommer la mort|officielle|
|[GsaoA7w1bfpGuWvA.htm](pathfinder-bestiary-2-items/GsaoA7w1bfpGuWvA.htm)|Tentacle|Tentacule|libre|
|[gsfeTAQtmgDaEQI7.htm](pathfinder-bestiary-2-items/gsfeTAQtmgDaEQI7.htm)|Darkvision|Vision dans le noir|libre|
|[GSglY8uncoV8v0jg.htm](pathfinder-bestiary-2-items/GSglY8uncoV8v0jg.htm)|Jet|Propulsion|officielle|
|[gSm7LVM6KgueNn8S.htm](pathfinder-bestiary-2-items/gSm7LVM6KgueNn8S.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[Gsx5q76S7ZPL3iXd.htm](pathfinder-bestiary-2-items/Gsx5q76S7ZPL3iXd.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[GTW3qSprQnTyrwNU.htm](pathfinder-bestiary-2-items/GTW3qSprQnTyrwNU.htm)|Claw|Griffe|libre|
|[gu96K11Wqu5OcYew.htm](pathfinder-bestiary-2-items/gu96K11Wqu5OcYew.htm)|Jaws|Mâchoires|libre|
|[GVFq98KDosC4rgkt.htm](pathfinder-bestiary-2-items/GVFq98KDosC4rgkt.htm)|Jaws|Mâchoires|libre|
|[GVP9BghiDShSeHpL.htm](pathfinder-bestiary-2-items/GVP9BghiDShSeHpL.htm)|Alter Dweomer|Altération occulte|officielle|
|[gVRJ2KRIYk9slxtI.htm](pathfinder-bestiary-2-items/gVRJ2KRIYk9slxtI.htm)|Longbow|Arc long|libre|
|[gvU3ZMNnU7PcbF4v.htm](pathfinder-bestiary-2-items/gvU3ZMNnU7PcbF4v.htm)|Shield Block|Blocage au bouclier|libre|
|[GWbkONUjqsTF8hP7.htm](pathfinder-bestiary-2-items/GWbkONUjqsTF8hP7.htm)|Claw|Griffe|libre|
|[GwBXsOeV4FFdxDzQ.htm](pathfinder-bestiary-2-items/GwBXsOeV4FFdxDzQ.htm)|Composite Longbow|Arc long composite|libre|
|[gWsyeIZTyzTRH7z1.htm](pathfinder-bestiary-2-items/gWsyeIZTyzTRH7z1.htm)|Telepathy|Télépathie|libre|
|[GY6jeRcNdp5Fd9OB.htm](pathfinder-bestiary-2-items/GY6jeRcNdp5Fd9OB.htm)|Darkvision|Vision dans le noir|libre|
|[GYapjknILGOMiV84.htm](pathfinder-bestiary-2-items/GYapjknILGOMiV84.htm)|-2 Status Bonus on Saves vs Curses|Malus de statut de -2 à tous les jets de sauvegarde contre les malédictions|libre|
|[GyMOxyS6WyN6PNVC.htm](pathfinder-bestiary-2-items/GyMOxyS6WyN6PNVC.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[GYPODdG0fWWAPk5y.htm](pathfinder-bestiary-2-items/GYPODdG0fWWAPk5y.htm)|Frond|Frondaison|libre|
|[gYXpVKd4NC2QDwe3.htm](pathfinder-bestiary-2-items/gYXpVKd4NC2QDwe3.htm)|At-Will Spells|Sorts à volonté|libre|
|[GZeUcgFbUA7B68BW.htm](pathfinder-bestiary-2-items/GZeUcgFbUA7B68BW.htm)|Telepathy|Télépathie|libre|
|[gzM5E4i0UEzXZAoY.htm](pathfinder-bestiary-2-items/gzM5E4i0UEzXZAoY.htm)|Negative Healing|Guérison négative|libre|
|[h0VU0sBf5jz6KCu1.htm](pathfinder-bestiary-2-items/h0VU0sBf5jz6KCu1.htm)|The Sea's Revenge|Vengeance de la mer|officielle|
|[h10snW2DCZ2hS7fP.htm](pathfinder-bestiary-2-items/h10snW2DCZ2hS7fP.htm)|Claw|Griffe|libre|
|[h155XUVgiUJ5onse.htm](pathfinder-bestiary-2-items/h155XUVgiUJ5onse.htm)|Blink Resistances|Résistance de clignotement|libre|
|[h1bIOksYIZLff4Jb.htm](pathfinder-bestiary-2-items/h1bIOksYIZLff4Jb.htm)|Jaws|Mâchoires|libre|
|[H1CrtJTYHt1zROG7.htm](pathfinder-bestiary-2-items/H1CrtJTYHt1zROG7.htm)|Rend|Éventration|libre|
|[h20KMmp1wuDSm1Oa.htm](pathfinder-bestiary-2-items/h20KMmp1wuDSm1Oa.htm)|Seed Spray|Pulvérisation de graines|officielle|
|[h282lP6zRpC6R83o.htm](pathfinder-bestiary-2-items/h282lP6zRpC6R83o.htm)|Swarming Infestation|Infestation grouillante|officielle|
|[H2rT9wdiejx3F1b0.htm](pathfinder-bestiary-2-items/H2rT9wdiejx3F1b0.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[h35hRTaiSXOfVZFA.htm](pathfinder-bestiary-2-items/h35hRTaiSXOfVZFA.htm)|Echolocation|Écholocalisation|officielle|
|[h3krrxTbAiZjVs8m.htm](pathfinder-bestiary-2-items/h3krrxTbAiZjVs8m.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[H3y3IbgMpQWevqRk.htm](pathfinder-bestiary-2-items/H3y3IbgMpQWevqRk.htm)|Drain Life|Drain de vie|officielle|
|[h4MdAekw9ZoGZc2Q.htm](pathfinder-bestiary-2-items/h4MdAekw9ZoGZc2Q.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[H5oOhIYYYiEvkna6.htm](pathfinder-bestiary-2-items/H5oOhIYYYiEvkna6.htm)|At-Will Spells|Sorts à volonté|libre|
|[h5vOq3kLWQKQrs6h.htm](pathfinder-bestiary-2-items/h5vOq3kLWQKQrs6h.htm)|Swallow Whole|Gober|officielle|
|[h61KXB1CbfC2hTh2.htm](pathfinder-bestiary-2-items/h61KXB1CbfC2hTh2.htm)|Swallow Whole|Gober|libre|
|[h68Rp5GCPIpGGeyq.htm](pathfinder-bestiary-2-items/h68Rp5GCPIpGGeyq.htm)|Breath Weapon|Arme de souffle|libre|
|[h69jYWXNKYbuTyzE.htm](pathfinder-bestiary-2-items/h69jYWXNKYbuTyzE.htm)|Breath Weapon|Arme de souffle|officielle|
|[H6d3brisQR3MDHO6.htm](pathfinder-bestiary-2-items/H6d3brisQR3MDHO6.htm)|Low-Light Vision|Vision nocturne|libre|
|[H6WOpA1s4iRwT9Ik.htm](pathfinder-bestiary-2-items/H6WOpA1s4iRwT9Ik.htm)|Scent|Odorat|libre|
|[H7is94BX3CDB78hl.htm](pathfinder-bestiary-2-items/H7is94BX3CDB78hl.htm)|Fist|Poing|libre|
|[H7ovqSFCx0c885Da.htm](pathfinder-bestiary-2-items/H7ovqSFCx0c885Da.htm)|Tentacles|Tentacules|libre|
|[H7ZdigvTaI99Ab76.htm](pathfinder-bestiary-2-items/H7ZdigvTaI99Ab76.htm)|Darkvision|Vision dans le noir|libre|
|[H8b5ywUVrI7pjhB8.htm](pathfinder-bestiary-2-items/H8b5ywUVrI7pjhB8.htm)|Shortsword|Épée courte|libre|
|[H9cKHc9OAHiXKDzf.htm](pathfinder-bestiary-2-items/H9cKHc9OAHiXKDzf.htm)|Swarming Bites|Nuée de morsures|officielle|
|[H9Hu5uk0AogKrXF4.htm](pathfinder-bestiary-2-items/H9Hu5uk0AogKrXF4.htm)|Regeneration 15 (deactivated by evil)|Régénération 15 (désactivée par mauvais)|officielle|
|[h9Ugyirm1B1g6pt6.htm](pathfinder-bestiary-2-items/h9Ugyirm1B1g6pt6.htm)|Jaws|Mâchoires|libre|
|[hA4DPWIY7hQbSa0i.htm](pathfinder-bestiary-2-items/hA4DPWIY7hQbSa0i.htm)|Dart|Fléchette|libre|
|[HAh9hAsU8XCNWPOU.htm](pathfinder-bestiary-2-items/HAh9hAsU8XCNWPOU.htm)|Organ of Endless Water|Organe d'eau intarissable|libre|
|[HAxp8tP3tYXocejD.htm](pathfinder-bestiary-2-items/HAxp8tP3tYXocejD.htm)|Claw|Griffe|libre|
|[HaXqTHQH97u8nuJK.htm](pathfinder-bestiary-2-items/HaXqTHQH97u8nuJK.htm)|Fist|Poing|libre|
|[HaYQ8Cd4C0hYSFdc.htm](pathfinder-bestiary-2-items/HaYQ8Cd4C0hYSFdc.htm)|Leap Attack|Attaque bondissante|officielle|
|[hbf277SQgtMCjjWF.htm](pathfinder-bestiary-2-items/hbf277SQgtMCjjWF.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[HcJNEFmJC7L4Ku0M.htm](pathfinder-bestiary-2-items/HcJNEFmJC7L4Ku0M.htm)|Rock|Rocher|libre|
|[Hd5SG091X71rJV1D.htm](pathfinder-bestiary-2-items/Hd5SG091X71rJV1D.htm)|Confusing Display|Démonstration déroutante|officielle|
|[hDdpyAhdnjpO1Bp9.htm](pathfinder-bestiary-2-items/hDdpyAhdnjpO1Bp9.htm)|Low-Light Vision|Vision nocturne|libre|
|[HDyQbtYQNxLarefS.htm](pathfinder-bestiary-2-items/HDyQbtYQNxLarefS.htm)|Grab|Empoignade/Agrippement|libre|
|[he0x5StuDg4yge3c.htm](pathfinder-bestiary-2-items/he0x5StuDg4yge3c.htm)|Claw|Griffe|libre|
|[hegoW9J1eMFWn0I6.htm](pathfinder-bestiary-2-items/hegoW9J1eMFWn0I6.htm)|Grab|Empoignade/Agrippement|libre|
|[hEsUevgpSNSIcNzB.htm](pathfinder-bestiary-2-items/hEsUevgpSNSIcNzB.htm)|Pounce|Bond|officielle|
|[HFAfo9jbjahhbw5J.htm](pathfinder-bestiary-2-items/HFAfo9jbjahhbw5J.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[hFDR91zB1cjh1UHv.htm](pathfinder-bestiary-2-items/hFDR91zB1cjh1UHv.htm)|Dead Tree|Arbre mort|officielle|
|[hg3lMJ6qrkw8WpZ3.htm](pathfinder-bestiary-2-items/hg3lMJ6qrkw8WpZ3.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[HGVXqBgfTKK0Gu4n.htm](pathfinder-bestiary-2-items/HGVXqBgfTKK0Gu4n.htm)|Trunk|Tronc|officielle|
|[HHnlE5IJhxgzBkin.htm](pathfinder-bestiary-2-items/HHnlE5IJhxgzBkin.htm)|Darkvision|Vision dans le noir|libre|
|[hHqx5bavcH4BYz5s.htm](pathfinder-bestiary-2-items/hHqx5bavcH4BYz5s.htm)|Darkvision|Vision dans le noir|libre|
|[HHRUhEQ6DJPe6kyX.htm](pathfinder-bestiary-2-items/HHRUhEQ6DJPe6kyX.htm)|Claw|Griffe|libre|
|[HHscPOBzNRjg9IJ5.htm](pathfinder-bestiary-2-items/HHscPOBzNRjg9IJ5.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[HhUiMTB8Qa8ySlVM.htm](pathfinder-bestiary-2-items/HhUiMTB8Qa8ySlVM.htm)|Sneak Attack|Attaque sournoise|officielle|
|[HilJh22elkeshhLj.htm](pathfinder-bestiary-2-items/HilJh22elkeshhLj.htm)|Wing|Aile|libre|
|[HIn5PSEUsrEdOYWT.htm](pathfinder-bestiary-2-items/HIn5PSEUsrEdOYWT.htm)|Trunk|Trompe|libre|
|[HIRmU6WblxuDmWc6.htm](pathfinder-bestiary-2-items/HIRmU6WblxuDmWc6.htm)|Fleshgout|Xanthome purulent|officielle|
|[HL1pjayMZJpK1JID.htm](pathfinder-bestiary-2-items/HL1pjayMZJpK1JID.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[HLcMADTgT1nixxce.htm](pathfinder-bestiary-2-items/HLcMADTgT1nixxce.htm)|Breath of the Bog|Souffle de la tourbière|officielle|
|[hm7ls5Yo0n7QOWMl.htm](pathfinder-bestiary-2-items/hm7ls5Yo0n7QOWMl.htm)|Low-Light Vision|Vision nocturne|libre|
|[Hmb2WHTATQHONj7o.htm](pathfinder-bestiary-2-items/Hmb2WHTATQHONj7o.htm)|Tail|Queue|libre|
|[HMmnA0DwFNibve0c.htm](pathfinder-bestiary-2-items/HMmnA0DwFNibve0c.htm)|At-Will Spells|Sorts à volonté|libre|
|[HmU4KBbkb30WqDpU.htm](pathfinder-bestiary-2-items/HmU4KBbkb30WqDpU.htm)|Darkvision|Vision dans le noir|libre|
|[Hn5PgfCFMwbKdN20.htm](pathfinder-bestiary-2-items/Hn5PgfCFMwbKdN20.htm)|Self-Loathing|Auto-dénigrement|officielle|
|[hNAE5swsHtvsyity.htm](pathfinder-bestiary-2-items/hNAE5swsHtvsyity.htm)|Darkvision|Vision dans le noir|libre|
|[HNfXE0yusiYA2juN.htm](pathfinder-bestiary-2-items/HNfXE0yusiYA2juN.htm)|Darkvision|Vision dans le noir|libre|
|[HOVV49IZbhN2hIwN.htm](pathfinder-bestiary-2-items/HOVV49IZbhN2hIwN.htm)|Scent|Odorat|libre|
|[HphLKNRNlCVczAuz.htm](pathfinder-bestiary-2-items/HphLKNRNlCVczAuz.htm)|Claw|Griffe|libre|
|[hPjYRtGf041nnmLw.htm](pathfinder-bestiary-2-items/hPjYRtGf041nnmLw.htm)|Stinger|Dard|libre|
|[HpLHoXbsdK98PiDh.htm](pathfinder-bestiary-2-items/HpLHoXbsdK98PiDh.htm)|Solid Refrain|Refrain solide|libre|
|[HPPj6Nf4GtdwCAEx.htm](pathfinder-bestiary-2-items/HPPj6Nf4GtdwCAEx.htm)|No Hearing|Ouïe nulle|libre|
|[hPXSQxaDWNJRZg36.htm](pathfinder-bestiary-2-items/hPXSQxaDWNJRZg36.htm)|Constrict|Constriction|libre|
|[HQ205X1b6CWTHXfU.htm](pathfinder-bestiary-2-items/HQ205X1b6CWTHXfU.htm)|Slow|Lent|officielle|
|[hQdMdV8u5o7eWZEV.htm](pathfinder-bestiary-2-items/hQdMdV8u5o7eWZEV.htm)|Jaws|Mâchoires|libre|
|[HQfx53oV60UYo0lg.htm](pathfinder-bestiary-2-items/HQfx53oV60UYo0lg.htm)|Gremlin Snare|Piège de gremlin|officielle|
|[HQTX3r7nEv9YINQh.htm](pathfinder-bestiary-2-items/HQTX3r7nEv9YINQh.htm)|Ferocity|Férocité|libre|
|[HqvJxFfKrYlRxHWC.htm](pathfinder-bestiary-2-items/HqvJxFfKrYlRxHWC.htm)|Rend|Éventration|libre|
|[hr8UQcvJzWfTNGoH.htm](pathfinder-bestiary-2-items/hr8UQcvJzWfTNGoH.htm)|Planar Fast Healing 5|Guérison accélérée planaire 5|officielle|
|[HR9jZiX9oYUmGloZ.htm](pathfinder-bestiary-2-items/HR9jZiX9oYUmGloZ.htm)|Darkvision|Vision dans le noir|libre|
|[hREXXxR78zjxAtPW.htm](pathfinder-bestiary-2-items/hREXXxR78zjxAtPW.htm)|Shoal Linnorm Venom|Venin de linnorm des hauts-fonds|officielle|
|[HRJpFIoBwbJ1rwnG.htm](pathfinder-bestiary-2-items/HRJpFIoBwbJ1rwnG.htm)|Telepathy|Télépathie|libre|
|[HrUGB7et2xcTkWSB.htm](pathfinder-bestiary-2-items/HrUGB7et2xcTkWSB.htm)|Mandibles|Mandibules|libre|
|[hS50gj87pD1XnQk9.htm](pathfinder-bestiary-2-items/hS50gj87pD1XnQk9.htm)|Claw Storm|Tempête de griffes|officielle|
|[hSqvFZoc1oaDpRVt.htm](pathfinder-bestiary-2-items/hSqvFZoc1oaDpRVt.htm)|Darkvision|Vision dans le noir|libre|
|[hSvaFFK9ERUSfUry.htm](pathfinder-bestiary-2-items/hSvaFFK9ERUSfUry.htm)|Moon Frenzy|Frénésie lunaire|officielle|
|[hswGSODfM3HdK8zC.htm](pathfinder-bestiary-2-items/hswGSODfM3HdK8zC.htm)|Stench|Puanteur|officielle|
|[HT78d5cKcguY6IcL.htm](pathfinder-bestiary-2-items/HT78d5cKcguY6IcL.htm)|Pounce|Bond|officielle|
|[HTGkYCrWWOoIvAja.htm](pathfinder-bestiary-2-items/HTGkYCrWWOoIvAja.htm)|Serpentfolk Venom|Venin des hommes-serpent|officielle|
|[hV3hgI3sPR9ZrQeK.htm](pathfinder-bestiary-2-items/hV3hgI3sPR9ZrQeK.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[HViM44dkUyHEWK31.htm](pathfinder-bestiary-2-items/HViM44dkUyHEWK31.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[HvL4pznzaaZwsFkA.htm](pathfinder-bestiary-2-items/HvL4pznzaaZwsFkA.htm)|Claw|Griffe|libre|
|[hVmOEk3NrvvmFkKg.htm](pathfinder-bestiary-2-items/hVmOEk3NrvvmFkKg.htm)|Painsight|Vision de la douleur|officielle|
|[hvQmC6GTFeQoVTYG.htm](pathfinder-bestiary-2-items/hvQmC6GTFeQoVTYG.htm)|Breath Weapon|Arme de souffle|libre|
|[hWE70sgsZXn7ZEa7.htm](pathfinder-bestiary-2-items/hWE70sgsZXn7ZEa7.htm)|Greatsword|Épée à deux mains|libre|
|[hWi2Ff37mvsMC81Y.htm](pathfinder-bestiary-2-items/hWi2Ff37mvsMC81Y.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[HWKLKpUPvbnvqEcp.htm](pathfinder-bestiary-2-items/HWKLKpUPvbnvqEcp.htm)|Horn|Corne|libre|
|[hwNl8SFYu6kOmwly.htm](pathfinder-bestiary-2-items/hwNl8SFYu6kOmwly.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[hwQlpkEec2QqnSxR.htm](pathfinder-bestiary-2-items/hwQlpkEec2QqnSxR.htm)|Regeneration 25 (deactivated by acid or cold)|Régénération 25 (désactivée par l'acide ou le froid)|libre|
|[HWUmjgOASJXWhmgn.htm](pathfinder-bestiary-2-items/HWUmjgOASJXWhmgn.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[hwX8mlw73yOCgwYw.htm](pathfinder-bestiary-2-items/hwX8mlw73yOCgwYw.htm)|Braincloud|Cerveau embrumé|officielle|
|[hwZzmzZjVpUb64cH.htm](pathfinder-bestiary-2-items/hwZzmzZjVpUb64cH.htm)|Earth Glide|Glissement de terrain|officielle|
|[hX2hdrvbUdBKffJD.htm](pathfinder-bestiary-2-items/hX2hdrvbUdBKffJD.htm)|Stealth|Discrétion|libre|
|[hxGtO1RrJyi0gVvQ.htm](pathfinder-bestiary-2-items/hxGtO1RrJyi0gVvQ.htm)|Scent|Odorat|libre|
|[HXNPKNUC1fSgSuTM.htm](pathfinder-bestiary-2-items/HXNPKNUC1fSgSuTM.htm)|Mohrg Spawn|Rejeton de mohrg|officielle|
|[hykxILVZRpdseaRk.htm](pathfinder-bestiary-2-items/hykxILVZRpdseaRk.htm)|Regeneration 25 (deactivated by acid or fire)|Régénération 25 (désactivée par l'acide ou le feu)|libre|
|[hyT0NpcWHdpgo5lt.htm](pathfinder-bestiary-2-items/hyT0NpcWHdpgo5lt.htm)|Dagger|Dague|libre|
|[hzr2Jl7pTcCFNWlF.htm](pathfinder-bestiary-2-items/hzr2Jl7pTcCFNWlF.htm)|Greataxe|Grande hache|libre|
|[I06QLdoJMbnJD3Pu.htm](pathfinder-bestiary-2-items/I06QLdoJMbnJD3Pu.htm)|Create Spawn|Création de rejeton|officielle|
|[i0ue3QRUsYmGAJDm.htm](pathfinder-bestiary-2-items/i0ue3QRUsYmGAJDm.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[I1cXiDVfmHepyrXf.htm](pathfinder-bestiary-2-items/I1cXiDVfmHepyrXf.htm)|Low-Light Vision|Vision nocturne|libre|
|[i1qGIf7bGKRu3RUq.htm](pathfinder-bestiary-2-items/i1qGIf7bGKRu3RUq.htm)|Tail|Queue|libre|
|[I2ICHtXckQoNKHcm.htm](pathfinder-bestiary-2-items/I2ICHtXckQoNKHcm.htm)|Knockdown|Renversement|libre|
|[I2sDX6PHhRSYYUZN.htm](pathfinder-bestiary-2-items/I2sDX6PHhRSYYUZN.htm)|Fist|Poing|libre|
|[i47viSV57dRQC5ZG.htm](pathfinder-bestiary-2-items/i47viSV57dRQC5ZG.htm)|Tremorsense|Perception des vibrations|libre|
|[I4ZjVogUXmOSRkNn.htm](pathfinder-bestiary-2-items/I4ZjVogUXmOSRkNn.htm)|Low-Light Vision|Vision nocturne|libre|
|[i5Rlc5hJ4fbTWS7H.htm](pathfinder-bestiary-2-items/i5Rlc5hJ4fbTWS7H.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[I6QwINd18HEwJGWW.htm](pathfinder-bestiary-2-items/I6QwINd18HEwJGWW.htm)|Frightful Presence|Présence terrifiante|libre|
|[i8KkA2JGZTsQSxta.htm](pathfinder-bestiary-2-items/i8KkA2JGZTsQSxta.htm)|Engulf|Engloutir|libre|
|[i9n1VIjQaHqI1V9X.htm](pathfinder-bestiary-2-items/i9n1VIjQaHqI1V9X.htm)|Paralyzing Display|Démonstration paralysante|officielle|
|[i9N4Fg8nJJdpMQUf.htm](pathfinder-bestiary-2-items/i9N4Fg8nJJdpMQUf.htm)|Tongue|Langue|libre|
|[i9Rco0paxz9ohGlm.htm](pathfinder-bestiary-2-items/i9Rco0paxz9ohGlm.htm)|Scent|Odorat|libre|
|[Ic32ezDxqx8TwcBo.htm](pathfinder-bestiary-2-items/Ic32ezDxqx8TwcBo.htm)|Stealth|Discrétion|libre|
|[icFkORI0Y4bNqBbP.htm](pathfinder-bestiary-2-items/icFkORI0Y4bNqBbP.htm)|Tail|Queue|libre|
|[iCMR05ML275h0Gek.htm](pathfinder-bestiary-2-items/iCMR05ML275h0Gek.htm)|Rebirth|Renaissance|officielle|
|[ICtnIf0c4SkY2NnB.htm](pathfinder-bestiary-2-items/ICtnIf0c4SkY2NnB.htm)|Athletics|Athlétisme|libre|
|[IcWvzNL316a1QTak.htm](pathfinder-bestiary-2-items/IcWvzNL316a1QTak.htm)|Claw|Griffe|libre|
|[IeAmIUQMEYZdSbaq.htm](pathfinder-bestiary-2-items/IeAmIUQMEYZdSbaq.htm)|Grab|Empoignade/Agrippement|libre|
|[ieuwqdM4zXO2i22Z.htm](pathfinder-bestiary-2-items/ieuwqdM4zXO2i22Z.htm)|Low-Light Vision|Vision nocturne|libre|
|[iF2j8ZpPVcS7dRD7.htm](pathfinder-bestiary-2-items/iF2j8ZpPVcS7dRD7.htm)|All-Around Vision|Vision à 360°|libre|
|[IFp4VK6X80nXiTJu.htm](pathfinder-bestiary-2-items/IFp4VK6X80nXiTJu.htm)|Darkvision|Vision dans le noir|libre|
|[IHM0ZFUpH2qKKM7E.htm](pathfinder-bestiary-2-items/IHM0ZFUpH2qKKM7E.htm)|Regeneration 10 (deactivated by good or silver)|Régénération 10 (neutralisée par le bon et l'argent)|libre|
|[iHNeGlvXTDOXMF0H.htm](pathfinder-bestiary-2-items/iHNeGlvXTDOXMF0H.htm)|Negative Healing|Guérison négative|libre|
|[iHR7yRqYeZST3uB5.htm](pathfinder-bestiary-2-items/iHR7yRqYeZST3uB5.htm)|Constrict|Constriction|libre|
|[IieLHPPUSUSDsN8d.htm](pathfinder-bestiary-2-items/IieLHPPUSUSDsN8d.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|officielle|
|[iINgbpwr9zD0DJ0d.htm](pathfinder-bestiary-2-items/iINgbpwr9zD0DJ0d.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[IJc6CxwwAtsS27ry.htm](pathfinder-bestiary-2-items/IJc6CxwwAtsS27ry.htm)|Tremorsense|Perception des vibrations|libre|
|[iKbhKQt4aD8JDvDd.htm](pathfinder-bestiary-2-items/iKbhKQt4aD8JDvDd.htm)|Absorb Flame|Absorption de feu|officielle|
|[iKjpMK3lSakpUkFY.htm](pathfinder-bestiary-2-items/iKjpMK3lSakpUkFY.htm)|Jaws|Mâchoires|libre|
|[iKpGYrRKqAjGDO9B.htm](pathfinder-bestiary-2-items/iKpGYrRKqAjGDO9B.htm)|Constrict|Constriction|officielle|
|[IKRXYrbZOj22lXn8.htm](pathfinder-bestiary-2-items/IKRXYrbZOj22lXn8.htm)|Ferocity|Férocité|libre|
|[iKVdFpbg8QyKzl0S.htm](pathfinder-bestiary-2-items/iKVdFpbg8QyKzl0S.htm)|Darkvision|Vision dans le noir|libre|
|[imFNV40cUextfevX.htm](pathfinder-bestiary-2-items/imFNV40cUextfevX.htm)|Grasping Foliage|Feuillage aggripant|officielle|
|[ImVU4G37cmGJMDoA.htm](pathfinder-bestiary-2-items/ImVU4G37cmGJMDoA.htm)|Sickle|Serpe|libre|
|[injH2iWqg5QjQiqs.htm](pathfinder-bestiary-2-items/injH2iWqg5QjQiqs.htm)|Blade|Lame|libre|
|[InntbXg8SWCGpsZF.htm](pathfinder-bestiary-2-items/InntbXg8SWCGpsZF.htm)|Telepathy|Télépathie|officielle|
|[IoE3s3hLaVewsjSQ.htm](pathfinder-bestiary-2-items/IoE3s3hLaVewsjSQ.htm)|Storm Aura|Aura de tempête|officielle|
|[Ipe3znrfNfiAvRCl.htm](pathfinder-bestiary-2-items/Ipe3znrfNfiAvRCl.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 jds contre magie|libre|
|[Ipf389K0Mqyu4zcP.htm](pathfinder-bestiary-2-items/Ipf389K0Mqyu4zcP.htm)|Scent|Odorat|libre|
|[ipGw6L7E6GUS565c.htm](pathfinder-bestiary-2-items/ipGw6L7E6GUS565c.htm)|Hypnosis|Hypnose|officielle|
|[iPIBtZVZ5TaPEFp6.htm](pathfinder-bestiary-2-items/iPIBtZVZ5TaPEFp6.htm)|Darkvision|Vision dans le noir|libre|
|[IPLCR3K5wWzQiSHK.htm](pathfinder-bestiary-2-items/IPLCR3K5wWzQiSHK.htm)|Discharge|Décharge|officielle|
|[IpYr4SLkhnzKvpdC.htm](pathfinder-bestiary-2-items/IpYr4SLkhnzKvpdC.htm)|At-Will Spells|Sorts à volonté|libre|
|[iqDrSwSZaUvRJKuG.htm](pathfinder-bestiary-2-items/iqDrSwSZaUvRJKuG.htm)|Grab|Empoignade/Agrippement|libre|
|[IQsStyO8N3SIqIw2.htm](pathfinder-bestiary-2-items/IQsStyO8N3SIqIw2.htm)|Scent|Odorat|libre|
|[iqXZDQJZ5iZnj2D0.htm](pathfinder-bestiary-2-items/iqXZDQJZ5iZnj2D0.htm)|Stealth|Discrétion|libre|
|[iRcU8dloUGz1q7VC.htm](pathfinder-bestiary-2-items/iRcU8dloUGz1q7VC.htm)|Claw|Griffe|libre|
|[iRmxw9CTYdvk3WOX.htm](pathfinder-bestiary-2-items/iRmxw9CTYdvk3WOX.htm)|Gatekeeper Aura|Aura du gardien|officielle|
|[IRXDgrccWGdvA4YK.htm](pathfinder-bestiary-2-items/IRXDgrccWGdvA4YK.htm)|Ferocity|Férocité|libre|
|[IS4BmsREr7eTZUPO.htm](pathfinder-bestiary-2-items/IS4BmsREr7eTZUPO.htm)|Claw|Griffe|libre|
|[is74odjgQwqelbqi.htm](pathfinder-bestiary-2-items/is74odjgQwqelbqi.htm)|Volcanic Purge|Purge volcanique|officielle|
|[ISiKvYdK9qbVBsG0.htm](pathfinder-bestiary-2-items/ISiKvYdK9qbVBsG0.htm)|Fangs|Crocs|libre|
|[isUs3BDvVUggXstu.htm](pathfinder-bestiary-2-items/isUs3BDvVUggXstu.htm)|Shadow Traveler|Voyageur de l'ombre|officielle|
|[iSVPM2m77YXV78mJ.htm](pathfinder-bestiary-2-items/iSVPM2m77YXV78mJ.htm)|Badger Rage|Rage du blaireau|officielle|
|[iSvQy8CUUD5u4nsG.htm](pathfinder-bestiary-2-items/iSvQy8CUUD5u4nsG.htm)|Mandibles|Mandibules|libre|
|[iT766eWfAydCiJYK.htm](pathfinder-bestiary-2-items/iT766eWfAydCiJYK.htm)|Mandibles|Mandibules|libre|
|[iThujq9Fhag6Jad2.htm](pathfinder-bestiary-2-items/iThujq9Fhag6Jad2.htm)|Grip Throat|Gorge serrée|officielle|
|[itoyZJLBh98BavI1.htm](pathfinder-bestiary-2-items/itoyZJLBh98BavI1.htm)|Entrench|Enracinement|officielle|
|[IukwgU6DfXFrQy49.htm](pathfinder-bestiary-2-items/IukwgU6DfXFrQy49.htm)|Inflict Warpwave|Infliger des vagues de distorsion|officielle|
|[iuqPDrdlLQjQi1NU.htm](pathfinder-bestiary-2-items/iuqPDrdlLQjQi1NU.htm)|Bastard Sword|Épée bâtarde|libre|
|[IvFL4oT7wjrxQZqw.htm](pathfinder-bestiary-2-items/IvFL4oT7wjrxQZqw.htm)|Claw|Griffe|libre|
|[iVrKx2VXmKTgxWvi.htm](pathfinder-bestiary-2-items/iVrKx2VXmKTgxWvi.htm)|Claw|Griffe|libre|
|[IvU2EF3nyrIZUdjp.htm](pathfinder-bestiary-2-items/IvU2EF3nyrIZUdjp.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[IVUIIQ9KDB8DHfvL.htm](pathfinder-bestiary-2-items/IVUIIQ9KDB8DHfvL.htm)|Jaws|Mâchoires|libre|
|[ivy8kuAr2Ml8VdAF.htm](pathfinder-bestiary-2-items/ivy8kuAr2Ml8VdAF.htm)|Cold Adaptation|Adaptation au froid|officielle|
|[IW1NCko3S3NUTExU.htm](pathfinder-bestiary-2-items/IW1NCko3S3NUTExU.htm)|Knockdown|Renversement|libre|
|[iw7xcu7OkpYHYyQ2.htm](pathfinder-bestiary-2-items/iw7xcu7OkpYHYyQ2.htm)|Greater Web Sense|Perception supérieure sur les toiles|officielle|
|[IWFZQHALE4qFDWJv.htm](pathfinder-bestiary-2-items/IWFZQHALE4qFDWJv.htm)|Snow Vision|Vision malgré la neige|officielle|
|[IWIgUhii4TYY31l7.htm](pathfinder-bestiary-2-items/IWIgUhii4TYY31l7.htm)|At-Will Spells|Sorts à volonté|libre|
|[iWjW5DElqzkofyBS.htm](pathfinder-bestiary-2-items/iWjW5DElqzkofyBS.htm)|Fist|Poing|libre|
|[IwPCTJvgvDQC90Yj.htm](pathfinder-bestiary-2-items/IwPCTJvgvDQC90Yj.htm)|Hurl Net|Lancer de filet|officielle|
|[IWSVjUBcLTretOIM.htm](pathfinder-bestiary-2-items/IWSVjUBcLTretOIM.htm)|Claw|Griffe|libre|
|[Ixt0Bpsg2FgwTGiy.htm](pathfinder-bestiary-2-items/Ixt0Bpsg2FgwTGiy.htm)|Dispelling Roar|Rugissement de dissipation|officielle|
|[ixXRVHF3jg0IMuKw.htm](pathfinder-bestiary-2-items/ixXRVHF3jg0IMuKw.htm)|Leprechaun Magic|Magie de leprechaun|officielle|
|[iYT6OEbdoPMem0XE.htm](pathfinder-bestiary-2-items/iYT6OEbdoPMem0XE.htm)|Children of the Night|Enfants de la nuit|officielle|
|[iZA7ek0jbfpt5FDo.htm](pathfinder-bestiary-2-items/iZA7ek0jbfpt5FDo.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[IzbfeuZKm6hz1cWS.htm](pathfinder-bestiary-2-items/IzbfeuZKm6hz1cWS.htm)|Darkvision|Vision dans le noir|libre|
|[IZKppOuI7oVJKs5K.htm](pathfinder-bestiary-2-items/IZKppOuI7oVJKs5K.htm)|Scent|Odorat|libre|
|[IZVxmKtbXiti9BZx.htm](pathfinder-bestiary-2-items/IZVxmKtbXiti9BZx.htm)|Cacophonous Roar|Rugissement cacophonique|officielle|
|[iZxDrmolCkhIxWO6.htm](pathfinder-bestiary-2-items/iZxDrmolCkhIxWO6.htm)|Ranged Trip|Croc-en-jambe à distance|officielle|
|[J0PFKoQWakUEDyIJ.htm](pathfinder-bestiary-2-items/J0PFKoQWakUEDyIJ.htm)|Shortsword|Épée courte|libre|
|[J1OKxBmPeSehcjj9.htm](pathfinder-bestiary-2-items/J1OKxBmPeSehcjj9.htm)|Breath Weapon|Arme de souffle|officielle|
|[J2LUtQlsnbpMEQ1r.htm](pathfinder-bestiary-2-items/J2LUtQlsnbpMEQ1r.htm)|Tail|Queue|libre|
|[J2UWT1TZOpg7ODbW.htm](pathfinder-bestiary-2-items/J2UWT1TZOpg7ODbW.htm)|Stoke the Fervent|Raviver le fervent|officielle|
|[j2XC7qI2EMqGvGSs.htm](pathfinder-bestiary-2-items/j2XC7qI2EMqGvGSs.htm)|Low-Light Vision|Vision nocturne|libre|
|[j45FbeObppNsRIhN.htm](pathfinder-bestiary-2-items/j45FbeObppNsRIhN.htm)|Darkvision|Vision dans le noir|libre|
|[j4OHhvOuFnXfbDjB.htm](pathfinder-bestiary-2-items/j4OHhvOuFnXfbDjB.htm)|Spell Feedback|Choc magique en retour|libre|
|[J83izDOsrGU2y7y6.htm](pathfinder-bestiary-2-items/J83izDOsrGU2y7y6.htm)|Lance Arm|Bras-lance d'arçon|libre|
|[j8BUnBP4Ahs3QmLx.htm](pathfinder-bestiary-2-items/j8BUnBP4Ahs3QmLx.htm)|Swarm Mind|Esprit de nuée|libre|
|[j8GeF2Jp36lFRgyF.htm](pathfinder-bestiary-2-items/j8GeF2Jp36lFRgyF.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[j8i5xeCEDDiWbh3z.htm](pathfinder-bestiary-2-items/j8i5xeCEDDiWbh3z.htm)|Fist|Poing|libre|
|[J8KaQAnTGQqYov9e.htm](pathfinder-bestiary-2-items/J8KaQAnTGQqYov9e.htm)|Xill Eggs|Œufs de xill|officielle|
|[j8Wq9vNUrxbBHNGK.htm](pathfinder-bestiary-2-items/j8Wq9vNUrxbBHNGK.htm)|Throw Rock|Projection de rocher|libre|
|[J9iEljd9P9EPB4wm.htm](pathfinder-bestiary-2-items/J9iEljd9P9EPB4wm.htm)|Grab|Empoignade/Agrippement|libre|
|[j9L9BPR6t7eHpHAt.htm](pathfinder-bestiary-2-items/j9L9BPR6t7eHpHAt.htm)|Breath Weapon|Arme de souffle|officielle|
|[JAh7SWOTsIXvrckz.htm](pathfinder-bestiary-2-items/JAh7SWOTsIXvrckz.htm)|Water Travel|Voyage par l'eau|officielle|
|[JakacQ9SCpgoCpCp.htm](pathfinder-bestiary-2-items/JakacQ9SCpgoCpCp.htm)|Shocking Burst|Explosion choc|officielle|
|[jC3hvioFdJ495oDv.htm](pathfinder-bestiary-2-items/jC3hvioFdJ495oDv.htm)|Crystallize Flesh|Cristallisation de la chair|officielle|
|[JCHWJ2nJ8ZmLOBs5.htm](pathfinder-bestiary-2-items/JCHWJ2nJ8ZmLOBs5.htm)|Breath Weapon|Arme de souffle|officielle|
|[Jd5I0ORcv8JckftL.htm](pathfinder-bestiary-2-items/Jd5I0ORcv8JckftL.htm)|Stormsight|Vision malgré la tempête|officielle|
|[JdsDXPVL1v4JEQt5.htm](pathfinder-bestiary-2-items/JdsDXPVL1v4JEQt5.htm)|Wing|Aile|libre|
|[jDvNUVTVAmMni2Va.htm](pathfinder-bestiary-2-items/jDvNUVTVAmMni2Va.htm)|Aura of Vitality|Aura de vitalité|officielle|
|[JdyDKp2bgIArfQmi.htm](pathfinder-bestiary-2-items/JdyDKp2bgIArfQmi.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[jEaI3soXVIxnAcPe.htm](pathfinder-bestiary-2-items/jEaI3soXVIxnAcPe.htm)|Breath Weapon|Arme de souffle|officielle|
|[jeV0r6dAZqnh6sD8.htm](pathfinder-bestiary-2-items/jeV0r6dAZqnh6sD8.htm)|Claw|Griffe|libre|
|[jfL2tU9w9oSiQlmg.htm](pathfinder-bestiary-2-items/jfL2tU9w9oSiQlmg.htm)|Enhance Venom|Améliorer le venin|officielle|
|[JfR3IkhT8g6aU9ry.htm](pathfinder-bestiary-2-items/JfR3IkhT8g6aU9ry.htm)|Scythe Branch|Branche faux|libre|
|[jG3O965IQDZgxg9B.htm](pathfinder-bestiary-2-items/jG3O965IQDZgxg9B.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[JGCkaVnAvnwAxWOp.htm](pathfinder-bestiary-2-items/JGCkaVnAvnwAxWOp.htm)|Mauler|Écharpeur|officielle|
|[JGEy5BcX7MQ17sdA.htm](pathfinder-bestiary-2-items/JGEy5BcX7MQ17sdA.htm)|Vrykolakas Vulnerabilities|Vulnérabilités des vrykolakas|officielle|
|[jgUPBrXeWbjnI6f1.htm](pathfinder-bestiary-2-items/jgUPBrXeWbjnI6f1.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[JgV2QrmvVWjF51JT.htm](pathfinder-bestiary-2-items/JgV2QrmvVWjF51JT.htm)|At-Will Spells|Sorts à volonté|libre|
|[JIAc2IH8Ot3uIXfD.htm](pathfinder-bestiary-2-items/JIAc2IH8Ot3uIXfD.htm)|Trample|Piétinement|officielle|
|[JIKJPPU6aBax4pjD.htm](pathfinder-bestiary-2-items/JIKJPPU6aBax4pjD.htm)|Mist Vision|Vision malgré la brume|officielle|
|[JImKJVdKy1YbBpwf.htm](pathfinder-bestiary-2-items/JImKJVdKy1YbBpwf.htm)|Mandibles|Mandibules|libre|
|[jISdvlh9f3wIBxZ8.htm](pathfinder-bestiary-2-items/jISdvlh9f3wIBxZ8.htm)|Freezing Blood|Sang glacial|officielle|
|[jJAZZAqxbrSxGLXv.htm](pathfinder-bestiary-2-items/jJAZZAqxbrSxGLXv.htm)|Wing|Aile|libre|
|[jJMWnNtJ3dst8bcj.htm](pathfinder-bestiary-2-items/jJMWnNtJ3dst8bcj.htm)|Drain Vigor|Drain de vigueur|officielle|
|[jK8jhXaoNnEEDRLV.htm](pathfinder-bestiary-2-items/jK8jhXaoNnEEDRLV.htm)|Darkvision|Vision dans le noir|libre|
|[jKCuP9xlPQoqlnGq.htm](pathfinder-bestiary-2-items/jKCuP9xlPQoqlnGq.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[JkVCc433MJHRl0qG.htm](pathfinder-bestiary-2-items/JkVCc433MJHRl0qG.htm)|Darkvision|Vision dans le noir|libre|
|[JLgWzofKmiq0ajpJ.htm](pathfinder-bestiary-2-items/JLgWzofKmiq0ajpJ.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[jljKVq6TmhKOQUSh.htm](pathfinder-bestiary-2-items/jljKVq6TmhKOQUSh.htm)|Stealth|Discrétion|libre|
|[JLM9pHvLtJxtkDH5.htm](pathfinder-bestiary-2-items/JLM9pHvLtJxtkDH5.htm)|Breath Weapon|Arme de souffle|officielle|
|[JLqzN2RUPFKa8beH.htm](pathfinder-bestiary-2-items/JLqzN2RUPFKa8beH.htm)|Reflect Spell|Sort renvoyé|officielle|
|[jM9qG2FB1AcJMMiL.htm](pathfinder-bestiary-2-items/jM9qG2FB1AcJMMiL.htm)|Darkvision|Vision dans le noir|libre|
|[JMoMmAd0UH6b2Ckg.htm](pathfinder-bestiary-2-items/JMoMmAd0UH6b2Ckg.htm)|Telepathy|Télépathie|libre|
|[jN9qLXiQfaOBlFfC.htm](pathfinder-bestiary-2-items/jN9qLXiQfaOBlFfC.htm)|Ramming Speed|Vitesse d’éperonnage|officielle|
|[jNQXKdaz3dr7h6s3.htm](pathfinder-bestiary-2-items/jNQXKdaz3dr7h6s3.htm)|Jaws|Mâchoires|libre|
|[JoBMwImgYPvlh4zo.htm](pathfinder-bestiary-2-items/JoBMwImgYPvlh4zo.htm)|Darkvision|Vision dans le noir|libre|
|[joBz08Vy0TnC5ELN.htm](pathfinder-bestiary-2-items/joBz08Vy0TnC5ELN.htm)|Jaws|Mâchoires|libre|
|[JOeW89FGrajDYQsW.htm](pathfinder-bestiary-2-items/JOeW89FGrajDYQsW.htm)|Vrykolakas Vulnerabilities|Vulnérabilités des vrykolakas|officielle|
|[jOuIuz89du1DGTFh.htm](pathfinder-bestiary-2-items/jOuIuz89du1DGTFh.htm)|Tongue|Langue|libre|
|[jpeqjLFSGfQaHqwb.htm](pathfinder-bestiary-2-items/jpeqjLFSGfQaHqwb.htm)|Bite|Morsure|officielle|
|[jppcxZjbxQ0e8W30.htm](pathfinder-bestiary-2-items/jppcxZjbxQ0e8W30.htm)|At-Will Spells|Sorts à volonté|libre|
|[jQ0BoxOUmGdOkGIf.htm](pathfinder-bestiary-2-items/jQ0BoxOUmGdOkGIf.htm)|Constrict|Constriction|libre|
|[JqoRYjfcXOg9zHSr.htm](pathfinder-bestiary-2-items/JqoRYjfcXOg9zHSr.htm)|Darkvision|Vision dans le noir|libre|
|[JqWD8UssH8Z7OeZz.htm](pathfinder-bestiary-2-items/JqWD8UssH8Z7OeZz.htm)|Limb Extension|Extension de membres|officielle|
|[jr6lRjG0ZeZgW116.htm](pathfinder-bestiary-2-items/jr6lRjG0ZeZgW116.htm)|Soul Scream|Hurlement de l'âme|officielle|
|[JRi9pHjxAIEGIlv1.htm](pathfinder-bestiary-2-items/JRi9pHjxAIEGIlv1.htm)|Tentacle|Tentacule|libre|
|[jskRIHujbQ0xJ8oy.htm](pathfinder-bestiary-2-items/jskRIHujbQ0xJ8oy.htm)|Frightful Presence|Présence terrifiante|libre|
|[jtcA1Dklz2pAKWhI.htm](pathfinder-bestiary-2-items/jtcA1Dklz2pAKWhI.htm)|Ghost Bane|Fléau spectral|officielle|
|[jtcRrpxqpR8quMII.htm](pathfinder-bestiary-2-items/jtcRrpxqpR8quMII.htm)|Quick Invisibility|Invisibilité rapide|officielle|
|[jTkhIBakBrDpEBuu.htm](pathfinder-bestiary-2-items/jTkhIBakBrDpEBuu.htm)|At-Will Spells|Sorts à volonté|libre|
|[jToSRHKjIQK8vQjE.htm](pathfinder-bestiary-2-items/jToSRHKjIQK8vQjE.htm)|Claw|Griffe|libre|
|[jTPTmoj95k6FZSf8.htm](pathfinder-bestiary-2-items/jTPTmoj95k6FZSf8.htm)|Darkvision|Vision dans le noir|libre|
|[JVezHGtBp5knZ21Z.htm](pathfinder-bestiary-2-items/JVezHGtBp5knZ21Z.htm)|Jaws|Mâchoires|libre|
|[jVgZ19s2KzgHUo1a.htm](pathfinder-bestiary-2-items/jVgZ19s2KzgHUo1a.htm)|Tentacle|Tentacule|libre|
|[jVnAGrnHMY0ZZTAt.htm](pathfinder-bestiary-2-items/jVnAGrnHMY0ZZTAt.htm)|Negative Healing|Guérison négative|libre|
|[Jw5v3XqSesAoiBsp.htm](pathfinder-bestiary-2-items/Jw5v3XqSesAoiBsp.htm)|Reactive Shock|Décharge automatique|officielle|
|[Jw8Rmo5ZFh9ouq86.htm](pathfinder-bestiary-2-items/Jw8Rmo5ZFh9ouq86.htm)|Ice Burrow|Creusement dans la glace|officielle|
|[JWaVcBKV9F51VWja.htm](pathfinder-bestiary-2-items/JWaVcBKV9F51VWja.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|officielle|
|[jWc6dwkDJvfVXXtk.htm](pathfinder-bestiary-2-items/jWc6dwkDJvfVXXtk.htm)|Trample|Piétinement|libre|
|[jwprc3z4QABxyA1y.htm](pathfinder-bestiary-2-items/jwprc3z4QABxyA1y.htm)|Pyrexic Malaria|Malaria pyrétique|officielle|
|[JwstH3hUkDgNqT21.htm](pathfinder-bestiary-2-items/JwstH3hUkDgNqT21.htm)|Stinger|Dard|libre|
|[jwZ2PMloPddIckyK.htm](pathfinder-bestiary-2-items/jwZ2PMloPddIckyK.htm)|Spirit Touch|Contact spirituel|officielle|
|[jXcwI039VI8MCBIE.htm](pathfinder-bestiary-2-items/jXcwI039VI8MCBIE.htm)|Rock|Rocher|libre|
|[JxE1ccJO8ctrpivc.htm](pathfinder-bestiary-2-items/JxE1ccJO8ctrpivc.htm)|+2 Status to All Saves vs Magic|Bonus de statut de +2 à tous les jets de sauvegarde contre la magie|libre|
|[jXl0E4vOdMPzQ3Hi.htm](pathfinder-bestiary-2-items/jXl0E4vOdMPzQ3Hi.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[jzbdXMuXge2oT2ol.htm](pathfinder-bestiary-2-items/jzbdXMuXge2oT2ol.htm)|Wolverine Rage|Rage du glouton|officielle|
|[JzdZZiLkLG0WmOid.htm](pathfinder-bestiary-2-items/JzdZZiLkLG0WmOid.htm)|Low-Light Vision|Vision nocturne|libre|
|[jZkG8pIe4dAne06Q.htm](pathfinder-bestiary-2-items/jZkG8pIe4dAne06Q.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[jzVssayBq4mzMU9u.htm](pathfinder-bestiary-2-items/jzVssayBq4mzMU9u.htm)|Darkvision|Vision dans le noir|libre|
|[JzWnn3KoGale3jgX.htm](pathfinder-bestiary-2-items/JzWnn3KoGale3jgX.htm)|Claw|Griffe|libre|
|[k0yAlAVkZbGrXfX3.htm](pathfinder-bestiary-2-items/k0yAlAVkZbGrXfX3.htm)|Jaws|Mâchoires|libre|
|[K1woMBH8Ut9J50As.htm](pathfinder-bestiary-2-items/K1woMBH8Ut9J50As.htm)|Foot|Pied|libre|
|[k2d7PB0MeYOIfjcj.htm](pathfinder-bestiary-2-items/k2d7PB0MeYOIfjcj.htm)|Filth Fever|Fièvre de la fange|officielle|
|[k2HAX2N3qaYcbpb8.htm](pathfinder-bestiary-2-items/k2HAX2N3qaYcbpb8.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[K32GntqM2YIDwGgh.htm](pathfinder-bestiary-2-items/K32GntqM2YIDwGgh.htm)|Scent|Odorat|libre|
|[k3o7ZpWc6J6flWCQ.htm](pathfinder-bestiary-2-items/k3o7ZpWc6J6flWCQ.htm)|Breach|Brèche|officielle|
|[K45PxGQvJlwB2Y0j.htm](pathfinder-bestiary-2-items/K45PxGQvJlwB2Y0j.htm)|Shield Block|Blocage au bouclier|libre|
|[k4tbzCH8jMb0d1Qk.htm](pathfinder-bestiary-2-items/k4tbzCH8jMb0d1Qk.htm)|Leng Spider Venom|Venin de l'araignée de Leng|officielle|
|[k6tK5Cya0h5SEzNQ.htm](pathfinder-bestiary-2-items/k6tK5Cya0h5SEzNQ.htm)|Rend|Éventration|libre|
|[k7IjtCkUuIzOv0dz.htm](pathfinder-bestiary-2-items/k7IjtCkUuIzOv0dz.htm)|Frightful Presence|Présence terrifiante|libre|
|[K7yXQ4PZPINLQXnj.htm](pathfinder-bestiary-2-items/K7yXQ4PZPINLQXnj.htm)|Darkvision|Vision dans le noir|libre|
|[K8JEIxdXOOyv0Mm6.htm](pathfinder-bestiary-2-items/K8JEIxdXOOyv0Mm6.htm)|Darkvision|Vision dans le noir|libre|
|[k8uRKWJkW8KHtabq.htm](pathfinder-bestiary-2-items/k8uRKWJkW8KHtabq.htm)|Grab|Empoignade/Agrippement|libre|
|[kAgFhMe8TiBlKO3j.htm](pathfinder-bestiary-2-items/kAgFhMe8TiBlKO3j.htm)|At-Will Spells|Sorts à volonté|libre|
|[KBUEqfW6m7bI5a0g.htm](pathfinder-bestiary-2-items/KBUEqfW6m7bI5a0g.htm)|Thunderstrike|Coup de tonnerre|officielle|
|[kbzaYm7nhy7Ip1Tj.htm](pathfinder-bestiary-2-items/kbzaYm7nhy7Ip1Tj.htm)|Grab|Empoignade/Agrippement|libre|
|[kbzgtxi6mitCaLGP.htm](pathfinder-bestiary-2-items/kbzgtxi6mitCaLGP.htm)|Grant Desire|Accorder une voeu|officielle|
|[kCNQjJs87QDFBoGR.htm](pathfinder-bestiary-2-items/kCNQjJs87QDFBoGR.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[kcRvfKLlgq9sdmsj.htm](pathfinder-bestiary-2-items/kcRvfKLlgq9sdmsj.htm)|Golem Antimagic|Antimagie du golem|libre|
|[KdGuOd6jczb7aCb5.htm](pathfinder-bestiary-2-items/KdGuOd6jczb7aCb5.htm)|Scent|Odorat|libre|
|[KdVfLfgHwYXdIYXG.htm](pathfinder-bestiary-2-items/KdVfLfgHwYXdIYXG.htm)|Jaws|Mâchoires|libre|
|[kDZcpk5GUe5jUU73.htm](pathfinder-bestiary-2-items/kDZcpk5GUe5jUU73.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[keXbMIiyh6Q6tjbr.htm](pathfinder-bestiary-2-items/keXbMIiyh6Q6tjbr.htm)|Darkvision|Vision dans le noir|libre|
|[KF7pVWIzuG4o2wMP.htm](pathfinder-bestiary-2-items/KF7pVWIzuG4o2wMP.htm)|Holy Mace|Masse sainte|libre|
|[kFLwkDqwtyueOpyG.htm](pathfinder-bestiary-2-items/kFLwkDqwtyueOpyG.htm)|Breath Weapon|Arme de souffle|officielle|
|[kFT6c7OlCSLY8QF1.htm](pathfinder-bestiary-2-items/kFT6c7OlCSLY8QF1.htm)|Regeneration 15 (deactivated by acid or fire)|Régénération 15 (désactivée par l'acide ou le feu)|libre|
|[kGJwFA2RdVWHs6ty.htm](pathfinder-bestiary-2-items/kGJwFA2RdVWHs6ty.htm)|Fly Pox|Peste de la mouche|officielle|
|[kGnrd8Q7x3x8myE3.htm](pathfinder-bestiary-2-items/kGnrd8Q7x3x8myE3.htm)|Tremorsense|Perception des vibrations|libre|
|[KgYaEP7QLp7DpYYS.htm](pathfinder-bestiary-2-items/KgYaEP7QLp7DpYYS.htm)|Constant Spells|Sorts constants|libre|
|[kGZni5n9AHCuIa7c.htm](pathfinder-bestiary-2-items/kGZni5n9AHCuIa7c.htm)|Vicious Criticals|Critiques vicieux|officielle|
|[khgGPSxgTQcgev3o.htm](pathfinder-bestiary-2-items/khgGPSxgTQcgev3o.htm)|Deep Breath|Inspiration profonde|officielle|
|[kHid4k2E9vYIzPDP.htm](pathfinder-bestiary-2-items/kHid4k2E9vYIzPDP.htm)|Claw|Griffe|libre|
|[khwh43CDMROkIfgf.htm](pathfinder-bestiary-2-items/khwh43CDMROkIfgf.htm)|Tremorsense (Imprecise) 30 feet|Perception des vribations (imprécis) 9 m|libre|
|[ki1JmOjsmc5eRI42.htm](pathfinder-bestiary-2-items/ki1JmOjsmc5eRI42.htm)|Jaws|Mâchoires|libre|
|[KIhN4LqmxLYnSLJj.htm](pathfinder-bestiary-2-items/KIhN4LqmxLYnSLJj.htm)|Change Shape|Changement de forme|officielle|
|[kIvuH6nOKhB0zVQI.htm](pathfinder-bestiary-2-items/kIvuH6nOKhB0zVQI.htm)|Jaws|Mâchoires|libre|
|[kj7lFKS7xdj7h83n.htm](pathfinder-bestiary-2-items/kj7lFKS7xdj7h83n.htm)|Change Shape|Changement de forme|officielle|
|[kJAxB9HM1zsPkPDC.htm](pathfinder-bestiary-2-items/kJAxB9HM1zsPkPDC.htm)|Tentacle|Tentacule|libre|
|[KJj34nHnZfjz70Pj.htm](pathfinder-bestiary-2-items/KJj34nHnZfjz70Pj.htm)|At-Will Spells|Sorts à volonté|libre|
|[KJKzoE0ci2sMwyFX.htm](pathfinder-bestiary-2-items/KJKzoE0ci2sMwyFX.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[KJVejaONCwwGt9ka.htm](pathfinder-bestiary-2-items/KJVejaONCwwGt9ka.htm)|Buck|Désarçonner|libre|
|[Kk860My0b7DPBnAo.htm](pathfinder-bestiary-2-items/Kk860My0b7DPBnAo.htm)|Darkvision|Vision dans le noir|libre|
|[KkNlymsNYCYGKgtS.htm](pathfinder-bestiary-2-items/KkNlymsNYCYGKgtS.htm)|Rend|Éventration|libre|
|[kKubgrbJBPv5ke6D.htm](pathfinder-bestiary-2-items/kKubgrbJBPv5ke6D.htm)|Tail|Queue|libre|
|[kli2QsKUO2VSz2oy.htm](pathfinder-bestiary-2-items/kli2QsKUO2VSz2oy.htm)|Breath Weapon|Arme de souffle|libre|
|[Kmh2fOosCcnUFq1X.htm](pathfinder-bestiary-2-items/Kmh2fOosCcnUFq1X.htm)|Darkvision|Vision dans le noir|libre|
|[kN2uT5XwOWMjkzA5.htm](pathfinder-bestiary-2-items/kN2uT5XwOWMjkzA5.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (Imprécis) 9 mètres|libre|
|[KO7ETr6PiZ0HJeHK.htm](pathfinder-bestiary-2-items/KO7ETr6PiZ0HJeHK.htm)|Holy Greatsword|Épée à deux mains Sainte|libre|
|[KovaVbwNiKoGj4DJ.htm](pathfinder-bestiary-2-items/KovaVbwNiKoGj4DJ.htm)|Greater Constrict|Constriction supérieure|libre|
|[KpGCXx2t9NN3a85q.htm](pathfinder-bestiary-2-items/KpGCXx2t9NN3a85q.htm)|Fascinating Display|Démonstration fascinante|officielle|
|[KQ5qTss6bA4fQLVK.htm](pathfinder-bestiary-2-items/KQ5qTss6bA4fQLVK.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[KQAihkBcgo2L9lGv.htm](pathfinder-bestiary-2-items/KQAihkBcgo2L9lGv.htm)|Breath Weapon|Arme de souffle|officielle|
|[kqeAoYGDfSoyJUyC.htm](pathfinder-bestiary-2-items/kqeAoYGDfSoyJUyC.htm)|Web|Toile|libre|
|[KqYkMZZ4hM5T9rej.htm](pathfinder-bestiary-2-items/KqYkMZZ4hM5T9rej.htm)|Darkvision|Vision dans le noir|libre|
|[kRAvWqrWJqwc6KU3.htm](pathfinder-bestiary-2-items/kRAvWqrWJqwc6KU3.htm)|Jaws|Mâchoires|libre|
|[kryZwH2P5QUcvtyI.htm](pathfinder-bestiary-2-items/kryZwH2P5QUcvtyI.htm)|Split|Scission|officielle|
|[ksfJahOG38JYy8bt.htm](pathfinder-bestiary-2-items/ksfJahOG38JYy8bt.htm)|Scent|Odorat|libre|
|[KTc5frqwQnvcjRY9.htm](pathfinder-bestiary-2-items/KTc5frqwQnvcjRY9.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[KtIqgI2Oe8cbrguF.htm](pathfinder-bestiary-2-items/KtIqgI2Oe8cbrguF.htm)|Worm Trill|Trille du ver|officielle|
|[KtjrcbTPGagKVhSc.htm](pathfinder-bestiary-2-items/KtjrcbTPGagKVhSc.htm)|Desiccating Bite|Morsure déshydratante|officielle|
|[kUtQvCE4ePTqjluS.htm](pathfinder-bestiary-2-items/kUtQvCE4ePTqjluS.htm)|At-Will Spells|Sorts à volonté|libre|
|[kuV1kCq4ZEiH9DzH.htm](pathfinder-bestiary-2-items/kuV1kCq4ZEiH9DzH.htm)|Vulnerable to Curved Space|Vulnérable aux espaces courbes|officielle|
|[Kv1pkf5Ma30A8n58.htm](pathfinder-bestiary-2-items/Kv1pkf5Ma30A8n58.htm)|Sickening Display|Démonstration écœurante|officielle|
|[kV8qUZKG2gXQGCKq.htm](pathfinder-bestiary-2-items/kV8qUZKG2gXQGCKq.htm)|Tremorsense|Perception des vibrations|libre|
|[kVhpKM2gcWYaxWvb.htm](pathfinder-bestiary-2-items/kVhpKM2gcWYaxWvb.htm)|Rock|Rocher|libre|
|[kVPXtEyRgZTAdwiv.htm](pathfinder-bestiary-2-items/kVPXtEyRgZTAdwiv.htm)|Wing|Aile|libre|
|[KvQg0sUQsjltpY39.htm](pathfinder-bestiary-2-items/KvQg0sUQsjltpY39.htm)|Thunderbolt|Foudre|officielle|
|[kvsmf0ENkLJSdBP2.htm](pathfinder-bestiary-2-items/kvsmf0ENkLJSdBP2.htm)|Dominate Animal|Domination animale|officielle|
|[kxiMCVyD0e1fsT96.htm](pathfinder-bestiary-2-items/kxiMCVyD0e1fsT96.htm)|Soul Ward|Armure d'âmes|officielle|
|[kxo11YQf1psAw6Ig.htm](pathfinder-bestiary-2-items/kxo11YQf1psAw6Ig.htm)|At-Will Spells|Sorts à volonté|libre|
|[KXvcedAJNDy7eaCz.htm](pathfinder-bestiary-2-items/KXvcedAJNDy7eaCz.htm)|Foot|Pied|libre|
|[kynHGhmwcc8qJ5T8.htm](pathfinder-bestiary-2-items/kynHGhmwcc8qJ5T8.htm)|Stealth|Discrétion|libre|
|[KYPHjX89IxHU4Sz3.htm](pathfinder-bestiary-2-items/KYPHjX89IxHU4Sz3.htm)|Claw|Griffe|libre|
|[kzg3qoq55PGMljc3.htm](pathfinder-bestiary-2-items/kzg3qoq55PGMljc3.htm)|Spear Frog Venom|Venin de grenouille-lance|officielle|
|[kzI56kmfH5aQquo7.htm](pathfinder-bestiary-2-items/kzI56kmfH5aQquo7.htm)|Ice Claw|Griffe de glace|officielle|
|[kziZybKPrc34cUoX.htm](pathfinder-bestiary-2-items/kziZybKPrc34cUoX.htm)|Jungle Stride|Déplacement facilité dans la jungle|officielle|
|[Kzxltf72FUB0IXJp.htm](pathfinder-bestiary-2-items/Kzxltf72FUB0IXJp.htm)|Darkvision|Vision dans le noir|libre|
|[l0EHPtcSyp31Bf57.htm](pathfinder-bestiary-2-items/l0EHPtcSyp31Bf57.htm)|At-Will Spells|Sorts à volonté|libre|
|[l0ivHsOCu0wN1yYF.htm](pathfinder-bestiary-2-items/l0ivHsOCu0wN1yYF.htm)|Darkvision|Vision dans le noir|libre|
|[L0IZRmIosB8Icqo0.htm](pathfinder-bestiary-2-items/L0IZRmIosB8Icqo0.htm)|Athletics|Athlétisme|libre|
|[l0J79vRF4TUP2DXP.htm](pathfinder-bestiary-2-items/l0J79vRF4TUP2DXP.htm)|Tail|Queue|libre|
|[L17R2FDChGWE5pgD.htm](pathfinder-bestiary-2-items/L17R2FDChGWE5pgD.htm)|Void Death|Mort du vide|officielle|
|[L19PpXXqJh02tp7Q.htm](pathfinder-bestiary-2-items/L19PpXXqJh02tp7Q.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[l25YrJUMqQDtE3NT.htm](pathfinder-bestiary-2-items/l25YrJUMqQDtE3NT.htm)|Jaws|Mâchoires|libre|
|[L35cRaqjhItrc6Po.htm](pathfinder-bestiary-2-items/L35cRaqjhItrc6Po.htm)|Claw|Griffe|libre|
|[l3FDPjYAvQMi4tfJ.htm](pathfinder-bestiary-2-items/l3FDPjYAvQMi4tfJ.htm)|Darkvision|Vision dans le noir|libre|
|[l4eGICy0QK1EEERs.htm](pathfinder-bestiary-2-items/l4eGICy0QK1EEERs.htm)|Jaws|Mâchoires|libre|
|[l5R6X6sffDy41u9i.htm](pathfinder-bestiary-2-items/l5R6X6sffDy41u9i.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegardes contre la magie|libre|
|[l6ptuF1kw5FfVHfl.htm](pathfinder-bestiary-2-items/l6ptuF1kw5FfVHfl.htm)|Pus Burst|Éruption de pus|officielle|
|[l6Q79Qlb9vVTOERz.htm](pathfinder-bestiary-2-items/l6Q79Qlb9vVTOERz.htm)|Hurled Weapon|Arme projetée|libre|
|[l8iqEFTf53TY4e9n.htm](pathfinder-bestiary-2-items/l8iqEFTf53TY4e9n.htm)|Fist|Poing|libre|
|[L8tZphaG7qnpPLpQ.htm](pathfinder-bestiary-2-items/L8tZphaG7qnpPLpQ.htm)|Golem Antimagic|Antimagie du golem|libre|
|[L8z6SDNKm57PB639.htm](pathfinder-bestiary-2-items/L8z6SDNKm57PB639.htm)|Claw|Griffe|libre|
|[L97RozqSBXjCukbA.htm](pathfinder-bestiary-2-items/L97RozqSBXjCukbA.htm)|Darkvision|Vision dans le noir|libre|
|[L9XO3TUsAurnPduF.htm](pathfinder-bestiary-2-items/L9XO3TUsAurnPduF.htm)|Jaws|Mâchoires|libre|
|[LA4f99Yw7Dk1e16d.htm](pathfinder-bestiary-2-items/LA4f99Yw7Dk1e16d.htm)|Fast Healing 2 (in dust or sand)|Guérison accélérée 2 (dans la poussière ou le sable)|libre|
|[LaDtFmUzWtxdlYPv.htm](pathfinder-bestiary-2-items/LaDtFmUzWtxdlYPv.htm)|Scent|Odorat|libre|
|[lB6z7CfDLMd5lPpG.htm](pathfinder-bestiary-2-items/lB6z7CfDLMd5lPpG.htm)|Starknife|Lamétoile|libre|
|[lbG9FKqt6rVqqKrM.htm](pathfinder-bestiary-2-items/lbG9FKqt6rVqqKrM.htm)|Low-Light Vision|Vision nocturne|libre|
|[LbkW4zjvlyreuGX1.htm](pathfinder-bestiary-2-items/LbkW4zjvlyreuGX1.htm)|Fangs|Crocs|libre|
|[lbNjzpti2ihhuipU.htm](pathfinder-bestiary-2-items/lbNjzpti2ihhuipU.htm)|Pounce|Bond|officielle|
|[LbTesuVUtAbIzLRY.htm](pathfinder-bestiary-2-items/LbTesuVUtAbIzLRY.htm)|Darkvision|Vision dans le noir|libre|
|[LBtPHwiQcsiBIHG7.htm](pathfinder-bestiary-2-items/LBtPHwiQcsiBIHG7.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[lc7e1FfD0zeKHSkz.htm](pathfinder-bestiary-2-items/lc7e1FfD0zeKHSkz.htm)|Negative Healing|Guérison négative|libre|
|[lcdESp8kjoyZyuYE.htm](pathfinder-bestiary-2-items/lcdESp8kjoyZyuYE.htm)|Jaws|Mâchoires|libre|
|[LcG0o3PUyemEv5oK.htm](pathfinder-bestiary-2-items/LcG0o3PUyemEv5oK.htm)|Darkvision|Vision dans le noir|libre|
|[LcHF4msXaAScrZFk.htm](pathfinder-bestiary-2-items/LcHF4msXaAScrZFk.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[LcmY1jYUhcSTLbed.htm](pathfinder-bestiary-2-items/LcmY1jYUhcSTLbed.htm)|Stormflight|Vol par tempête|officielle|
|[le0p8FsNfX4jkVzn.htm](pathfinder-bestiary-2-items/le0p8FsNfX4jkVzn.htm)|Amplify Voltage|Amplification de voltage|officielle|
|[LFvM7oAwEJD1soNx.htm](pathfinder-bestiary-2-items/LFvM7oAwEJD1soNx.htm)|Low-Light Vision|Vision nocturne|libre|
|[lgHFmVhsmZOzCgFm.htm](pathfinder-bestiary-2-items/lgHFmVhsmZOzCgFm.htm)|Returning Starknife|Lamétoile boomerang|libre|
|[lgkWnI9fHF9WMU2Q.htm](pathfinder-bestiary-2-items/lgkWnI9fHF9WMU2Q.htm)|Scent|Odorat|libre|
|[LGmgWdigRZKEMwNq.htm](pathfinder-bestiary-2-items/LGmgWdigRZKEMwNq.htm)|Swallow Whole|Gober|libre|
|[LGq3APZstgY9LM9o.htm](pathfinder-bestiary-2-items/LGq3APZstgY9LM9o.htm)|Claw|Griffe|libre|
|[Lh6yYbwltdDOrlaP.htm](pathfinder-bestiary-2-items/Lh6yYbwltdDOrlaP.htm)|Pincers|Pince|libre|
|[lIiFggTlCNveUaJM.htm](pathfinder-bestiary-2-items/lIiFggTlCNveUaJM.htm)|Vrykolakas Vulnerabilities|Vulnérabilités des vrykolakas|officielle|
|[lIutGZGuGlSN1uvq.htm](pathfinder-bestiary-2-items/lIutGZGuGlSN1uvq.htm)|Fist|Poing|libre|
|[lJ35tuNxwPc194AM.htm](pathfinder-bestiary-2-items/lJ35tuNxwPc194AM.htm)|Jaws|Mâchoires|libre|
|[LJ98Uh0Kt9H0AdSx.htm](pathfinder-bestiary-2-items/LJ98Uh0Kt9H0AdSx.htm)|Grab|Empoignade/Agrippement|libre|
|[ljlBgsFIBKMNCZPZ.htm](pathfinder-bestiary-2-items/ljlBgsFIBKMNCZPZ.htm)|Camouflage|Camouflage|officielle|
|[LKQ6v7y7Jr6HEaPh.htm](pathfinder-bestiary-2-items/LKQ6v7y7Jr6HEaPh.htm)|Scent|Odorat|libre|
|[lL32oAteqyTNi0nT.htm](pathfinder-bestiary-2-items/lL32oAteqyTNi0nT.htm)|Hallucinatory Brine|Saumure hallucinatoire|officielle|
|[llgAm8MQ01n7hFrf.htm](pathfinder-bestiary-2-items/llgAm8MQ01n7hFrf.htm)|Swarm Mind|Esprit de nuée|libre|
|[LMv9Jl8QyuoKhMpw.htm](pathfinder-bestiary-2-items/LMv9Jl8QyuoKhMpw.htm)|Spirit Touch|Contact spirituel|officielle|
|[Lmym2mXsN2UYnCLA.htm](pathfinder-bestiary-2-items/Lmym2mXsN2UYnCLA.htm)|Regeneration 5 (deactivated by good or silver)|Régénération 5 (neutralisée par le bon ou l’argent)|officielle|
|[LN7JypAnmAunS0lg.htm](pathfinder-bestiary-2-items/LN7JypAnmAunS0lg.htm)|Twist the Blade|Retourner la lame|officielle|
|[lNktyL9mwqYhh5mh.htm](pathfinder-bestiary-2-items/lNktyL9mwqYhh5mh.htm)|Scent|Odorat|libre|
|[loahwrlFxvmp2CXy.htm](pathfinder-bestiary-2-items/loahwrlFxvmp2CXy.htm)|Jet|Propulsion|officielle|
|[LOsTy4ZSFM2RSN4k.htm](pathfinder-bestiary-2-items/LOsTy4ZSFM2RSN4k.htm)|Rev Up|Plein gaz|officielle|
|[loVBGCMa9ni2Xgvw.htm](pathfinder-bestiary-2-items/loVBGCMa9ni2Xgvw.htm)|Scent|Odorat|libre|
|[lP5ul9ZfSW3FEFpa.htm](pathfinder-bestiary-2-items/lP5ul9ZfSW3FEFpa.htm)|Swarm Mind|Esprit de nuée|libre|
|[LPf9DIArTBdgTKrW.htm](pathfinder-bestiary-2-items/LPf9DIArTBdgTKrW.htm)|Push|Bousculade|officielle|
|[lPGPRMZYecowSzGA.htm](pathfinder-bestiary-2-items/lPGPRMZYecowSzGA.htm)|Regeneration 10 (deactivated by cold iron)|Régénération 10 (désactivée par le fer froid)|officielle|
|[lPlAVVeLD4qqvZ8q.htm](pathfinder-bestiary-2-items/lPlAVVeLD4qqvZ8q.htm)|Trample|Piétinement|officielle|
|[LPLiXKoxMuJu4ifi.htm](pathfinder-bestiary-2-items/LPLiXKoxMuJu4ifi.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[LpnkFiQmJ2nxDYm5.htm](pathfinder-bestiary-2-items/LpnkFiQmJ2nxDYm5.htm)|Blinding Beams|Rayons aveuglants|officielle|
|[lPnyzUu12dzRoAGk.htm](pathfinder-bestiary-2-items/lPnyzUu12dzRoAGk.htm)|At-Will Spells|Sorts à volonté|libre|
|[lPV8I6T6I09MjTy3.htm](pathfinder-bestiary-2-items/lPV8I6T6I09MjTy3.htm)|Jaws|Mâchoires|libre|
|[LpxpKBeM4lLjGR0o.htm](pathfinder-bestiary-2-items/LpxpKBeM4lLjGR0o.htm)|Pincer|Pince|libre|
|[LQBlltvEb0rVnJYA.htm](pathfinder-bestiary-2-items/LQBlltvEb0rVnJYA.htm)|Heliotrope|Héliotrope|officielle|
|[LqIsXOTw0kTELToc.htm](pathfinder-bestiary-2-items/LqIsXOTw0kTELToc.htm)|Antler|Bois|libre|
|[lQZGx8hWnzcG2R6l.htm](pathfinder-bestiary-2-items/lQZGx8hWnzcG2R6l.htm)|Dazzling Brilliance|Éclat aveuglant|officielle|
|[LRAuzrNMjhe4ZYR9.htm](pathfinder-bestiary-2-items/LRAuzrNMjhe4ZYR9.htm)|Jaws|Mâchoires|libre|
|[lSdAwlGX28qPPhdE.htm](pathfinder-bestiary-2-items/lSdAwlGX28qPPhdE.htm)|Scent|Odorat|libre|
|[lSfQ3gfSsjhNS3HW.htm](pathfinder-bestiary-2-items/lSfQ3gfSsjhNS3HW.htm)|Tremorsense|Perception des vibrations|libre|
|[lssPHJEGxZtK2T3A.htm](pathfinder-bestiary-2-items/lssPHJEGxZtK2T3A.htm)|Jaws|Mâchoires|libre|
|[lt0GXB5vawxWhRLk.htm](pathfinder-bestiary-2-items/lt0GXB5vawxWhRLk.htm)|Fist|Poing|libre|
|[Lt44trsI3w4Nptsb.htm](pathfinder-bestiary-2-items/Lt44trsI3w4Nptsb.htm)|Jaws|Mâchoires|libre|
|[LTcf2lgijfe0Cv96.htm](pathfinder-bestiary-2-items/LTcf2lgijfe0Cv96.htm)|Darkvision|Vision dans le noir|libre|
|[LTW5DHnhGB6HMte6.htm](pathfinder-bestiary-2-items/LTW5DHnhGB6HMte6.htm)|Stealth|Discrétion|libre|
|[LUG7zInRwnS0c3vJ.htm](pathfinder-bestiary-2-items/LUG7zInRwnS0c3vJ.htm)|Cloud Walk|Marche sur les nuages|officielle|
|[lugrDlIHRd8kGDKs.htm](pathfinder-bestiary-2-items/lugrDlIHRd8kGDKs.htm)|Burning Swarm|Nuée brûlante|officielle|
|[LUKxBKPHrzVKWWNz.htm](pathfinder-bestiary-2-items/LUKxBKPHrzVKWWNz.htm)|Tongue|Langue|libre|
|[Lv1GZd4xpvCGfjkz.htm](pathfinder-bestiary-2-items/Lv1GZd4xpvCGfjkz.htm)|Pollen Touch|Contact pollinifère|officielle|
|[LV58uBPdmRP5wve2.htm](pathfinder-bestiary-2-items/LV58uBPdmRP5wve2.htm)|Negative Healing|Guérison négative|libre|
|[lVmzxYHnjQBBZTN4.htm](pathfinder-bestiary-2-items/lVmzxYHnjQBBZTN4.htm)|Claw|Griffe|libre|
|[lvSGUnxmrOKOF2tT.htm](pathfinder-bestiary-2-items/lvSGUnxmrOKOF2tT.htm)|Grab|Empoignade/Agrippement|libre|
|[LwAD4kqFgkuoaJ0b.htm](pathfinder-bestiary-2-items/LwAD4kqFgkuoaJ0b.htm)|Change Shape|Changement de forme|officielle|
|[LwBTFYgYLRf8gXzt.htm](pathfinder-bestiary-2-items/LwBTFYgYLRf8gXzt.htm)|Spell Reflection|Renvoi de sort|officielle|
|[lWr6fzaJ1ya4uUVM.htm](pathfinder-bestiary-2-items/lWr6fzaJ1ya4uUVM.htm)|Deflect Arrow|Parade de projectiles|officielle|
|[LXMgaHhqx5CpVHAu.htm](pathfinder-bestiary-2-items/LXMgaHhqx5CpVHAu.htm)|Thorn Volley|Volée d'échardes|officielle|
|[LXXg3Endpf7Vtn3r.htm](pathfinder-bestiary-2-items/LXXg3Endpf7Vtn3r.htm)|Constrict|Constriction|libre|
|[lY7Ggtouj0iOizqY.htm](pathfinder-bestiary-2-items/lY7Ggtouj0iOizqY.htm)|Chain|Chaîne|libre|
|[Ly9IqnJW6Zy3JQ4y.htm](pathfinder-bestiary-2-items/Ly9IqnJW6Zy3JQ4y.htm)|Pseudopod|Pseudopode|libre|
|[LYeMRFsmLZRCk3uE.htm](pathfinder-bestiary-2-items/LYeMRFsmLZRCk3uE.htm)|Sudden Retreat|Retraite soudaine|officielle|
|[LYl70MYcQUy11TXM.htm](pathfinder-bestiary-2-items/LYl70MYcQUy11TXM.htm)|Improved Knockdown|Renversement amélioré|libre|
|[lZ2HxJLCCy5TITMC.htm](pathfinder-bestiary-2-items/lZ2HxJLCCy5TITMC.htm)|At-Will Spells|Sorts à volonté|libre|
|[lZ9B89A9j1PjYcdf.htm](pathfinder-bestiary-2-items/lZ9B89A9j1PjYcdf.htm)|Volcanic Purge|Purge volcanique|officielle|
|[lzbAKcAmDlWaRGOF.htm](pathfinder-bestiary-2-items/lzbAKcAmDlWaRGOF.htm)|Telepathy|Télépathie|libre|
|[LzoVt6l20KHm15wv.htm](pathfinder-bestiary-2-items/LzoVt6l20KHm15wv.htm)|Radiant Beam|Rayon radiant|libre|
|[m0MCsCQ2HU7X7Rr2.htm](pathfinder-bestiary-2-items/m0MCsCQ2HU7X7Rr2.htm)|Claw|Griffe|libre|
|[M14sGTx3h9uOKf65.htm](pathfinder-bestiary-2-items/M14sGTx3h9uOKf65.htm)|Darkvision|Vision dans le noir|libre|
|[m1Ku7DrcoF8JnIil.htm](pathfinder-bestiary-2-items/m1Ku7DrcoF8JnIil.htm)|Beak|Bec|libre|
|[m39B0WvlTUpZ3tLS.htm](pathfinder-bestiary-2-items/m39B0WvlTUpZ3tLS.htm)|Darkvision|Vision dans le noir|libre|
|[M5bYj5m7ymoWlabG.htm](pathfinder-bestiary-2-items/M5bYj5m7ymoWlabG.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[M6lP0K1hMi81OdrJ.htm](pathfinder-bestiary-2-items/M6lP0K1hMi81OdrJ.htm)|Beak|Bec|libre|
|[M7sZToYvFNrig8TH.htm](pathfinder-bestiary-2-items/M7sZToYvFNrig8TH.htm)|Constant Spells|Sorts constants|libre|
|[m8k2uRU1UVmPgF03.htm](pathfinder-bestiary-2-items/m8k2uRU1UVmPgF03.htm)|Regeneration|Régénération 10 (désactivée par le froid)|libre|
|[m9kHAZ6q8lhsA9dC.htm](pathfinder-bestiary-2-items/m9kHAZ6q8lhsA9dC.htm)|Deep Breath|Inspiration profonde|officielle|
|[mBjJ5gUJvowlXzIH.htm](pathfinder-bestiary-2-items/mBjJ5gUJvowlXzIH.htm)|Breath Weapon|Arme de souffle|officielle|
|[mcIvTT80sYixnl9W.htm](pathfinder-bestiary-2-items/mcIvTT80sYixnl9W.htm)|Nauseating Display|Démonstration répugnante|officielle|
|[mcOn5wJPK3EDRCeU.htm](pathfinder-bestiary-2-items/mcOn5wJPK3EDRCeU.htm)|Sneak Attack|Attaque sournoise|officielle|
|[mcp20ANc2q921IjQ.htm](pathfinder-bestiary-2-items/mcp20ANc2q921IjQ.htm)|Claw|Griffe|libre|
|[mcsK2cK4pMrDIfZJ.htm](pathfinder-bestiary-2-items/mcsK2cK4pMrDIfZJ.htm)|Curse of Endless Storms|Malédiction des tempêtes sans fin|libre|
|[md179yuEkGpC91XD.htm](pathfinder-bestiary-2-items/md179yuEkGpC91XD.htm)|Claw|Griffe|libre|
|[mdEZdShzE63nlTQK.htm](pathfinder-bestiary-2-items/mdEZdShzE63nlTQK.htm)|Salt Water Vulnerability|Vulnérabilité à l'eau salée|officielle|
|[MDX9dw99EkjeyPM0.htm](pathfinder-bestiary-2-items/MDX9dw99EkjeyPM0.htm)|Grab|Empoignade/Agrippement|libre|
|[MEtJibVB3AjSqu2W.htm](pathfinder-bestiary-2-items/MEtJibVB3AjSqu2W.htm)|Improved Knockdown|Renversement amélioré|libre|
|[meXB4fiPOKy6eWwH.htm](pathfinder-bestiary-2-items/meXB4fiPOKy6eWwH.htm)|Longspear|Pique|libre|
|[mey0HoXRCPNAYCqa.htm](pathfinder-bestiary-2-items/mey0HoXRCPNAYCqa.htm)|Low-Light Vision|Vision nocturne|libre|
|[Mg0QNEtZXzZdfn88.htm](pathfinder-bestiary-2-items/Mg0QNEtZXzZdfn88.htm)|Low-Light Vision|Vision nocturne|libre|
|[mgA0es28Vb0TAr40.htm](pathfinder-bestiary-2-items/mgA0es28Vb0TAr40.htm)|Menacing Growl|Feulement menaçant|officielle|
|[mgF7v4Mg6J6e9WVv.htm](pathfinder-bestiary-2-items/mgF7v4Mg6J6e9WVv.htm)|Composite Longbow|Arc long composite|libre|
|[mGkbLuXtisEvFNNs.htm](pathfinder-bestiary-2-items/mGkbLuXtisEvFNNs.htm)|Fastening Leap|Saut de fixation|officielle|
|[MgWi09fDy6GgzHJM.htm](pathfinder-bestiary-2-items/MgWi09fDy6GgzHJM.htm)|Sinister Bite|Morsure sinistre|officielle|
|[mHMefxDyPXjW4mKt.htm](pathfinder-bestiary-2-items/mHMefxDyPXjW4mKt.htm)|Grab|Empoignade/Agrippement|libre|
|[MiX1rLReSocMJdta.htm](pathfinder-bestiary-2-items/MiX1rLReSocMJdta.htm)|Cold Lethargy|Léthargie dans le froid|officielle|
|[MJktJ33e2ejK5I1o.htm](pathfinder-bestiary-2-items/MJktJ33e2ejK5I1o.htm)|Debilitating Bite|Morsure débilitante|officielle|
|[mjWf3inPCmf7NzIh.htm](pathfinder-bestiary-2-items/mjWf3inPCmf7NzIh.htm)|Bite|Morsure|officielle|
|[mKdD7XKIAXJx8KnC.htm](pathfinder-bestiary-2-items/mKdD7XKIAXJx8KnC.htm)|Athletics|Athlétisme|libre|
|[MKjlk8JpKxpWjpPW.htm](pathfinder-bestiary-2-items/MKjlk8JpKxpWjpPW.htm)|Darkvision|Vision dans le noir|libre|
|[mKNSE9q3LtnCb49y.htm](pathfinder-bestiary-2-items/mKNSE9q3LtnCb49y.htm)|Scythe|Faux|libre|
|[MkwxsOzEbMfQNQfG.htm](pathfinder-bestiary-2-items/MkwxsOzEbMfQNQfG.htm)|At-Will Spells|Sorts à volonté|libre|
|[mkXd7hCmTsucWehM.htm](pathfinder-bestiary-2-items/mkXd7hCmTsucWehM.htm)|Fast Healing 30|Guérison accélérée 30|libre|
|[ml69ZMPwkVsbCTC7.htm](pathfinder-bestiary-2-items/ml69ZMPwkVsbCTC7.htm)|Storm of Tentacles|Tempête de tentacules|officielle|
|[mlnT94tqOlTGgqXM.htm](pathfinder-bestiary-2-items/mlnT94tqOlTGgqXM.htm)|Darkvision|Vision dans le noir|libre|
|[mLqIKRgLPV3i0SWY.htm](pathfinder-bestiary-2-items/mLqIKRgLPV3i0SWY.htm)|Claw|Griffe|libre|
|[MLvT2QA997KCHesX.htm](pathfinder-bestiary-2-items/MLvT2QA997KCHesX.htm)|Low-Light Vision|Vision nocturne|libre|
|[mmb4NGB4HueXi8Ty.htm](pathfinder-bestiary-2-items/mmb4NGB4HueXi8Ty.htm)|Bite|Morsure|officielle|
|[mMsy3KDfbrGBdYgB.htm](pathfinder-bestiary-2-items/mMsy3KDfbrGBdYgB.htm)|Low-Light Vision|Vision nocturne|libre|
|[mnNaEVfu4nEmEwem.htm](pathfinder-bestiary-2-items/mnNaEVfu4nEmEwem.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[mObSHLoIirMcASES.htm](pathfinder-bestiary-2-items/mObSHLoIirMcASES.htm)|Jaws|Mâchoires|libre|
|[mOfIGDW3g48Ft7w8.htm](pathfinder-bestiary-2-items/mOfIGDW3g48Ft7w8.htm)|Catch Rock|Interception de rochers|libre|
|[MopioBBWXr3np4t0.htm](pathfinder-bestiary-2-items/MopioBBWXr3np4t0.htm)|Darkvision|Vision dans le noir|libre|
|[MPbG99HGshwvN1vm.htm](pathfinder-bestiary-2-items/MPbG99HGshwvN1vm.htm)|Grab|Empoignade/Agrippement|libre|
|[MPJFQqOrPfQloQLr.htm](pathfinder-bestiary-2-items/MPJFQqOrPfQloQLr.htm)|Death Throes|Agonie fatale|officielle|
|[mPQXusMKyetxlwjx.htm](pathfinder-bestiary-2-items/mPQXusMKyetxlwjx.htm)|Scent|Odorat|libre|
|[mqgxK9jf9zGXE672.htm](pathfinder-bestiary-2-items/mqgxK9jf9zGXE672.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[MqN8V1YL2EczTFIG.htm](pathfinder-bestiary-2-items/MqN8V1YL2EczTFIG.htm)|Trident|Trident|libre|
|[mqqrT74LZMyFyuic.htm](pathfinder-bestiary-2-items/mqqrT74LZMyFyuic.htm)|Jaws|Mâchoires|libre|
|[MRVG5RH41oEJ41N3.htm](pathfinder-bestiary-2-items/MRVG5RH41oEJ41N3.htm)|Fist|Poing|libre|
|[MS8HSxdLBdDYo9WD.htm](pathfinder-bestiary-2-items/MS8HSxdLBdDYo9WD.htm)|Telepathy|Télépathie|libre|
|[msIyAn6AsHKdGxMu.htm](pathfinder-bestiary-2-items/msIyAn6AsHKdGxMu.htm)|Thundering Bite|Morsure assourdissante|officielle|
|[mSpMpLaDOLKbXWns.htm](pathfinder-bestiary-2-items/mSpMpLaDOLKbXWns.htm)|Piscovenom|Piscovenin|officielle|
|[Mtkrvum6bboSrXtq.htm](pathfinder-bestiary-2-items/Mtkrvum6bboSrXtq.htm)|Sanguine Mauling|Mutilation sanguine|officielle|
|[mTsKSxGS9jWZPCar.htm](pathfinder-bestiary-2-items/mTsKSxGS9jWZPCar.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[MUg2W7zTfRWFuMZY.htm](pathfinder-bestiary-2-items/MUg2W7zTfRWFuMZY.htm)|Low-Light Vision|Vision nocturne|libre|
|[mUjeve67uKUXgiEz.htm](pathfinder-bestiary-2-items/mUjeve67uKUXgiEz.htm)|Regeneration 15 (deactivated by cold iron)|Régénération 15 (désactivée par le fer froid)|libre|
|[mUpNn76jylw99mXr.htm](pathfinder-bestiary-2-items/mUpNn76jylw99mXr.htm)|Frightful Presence|Présence terrifiante|libre|
|[MvqmGM4ha7aQt1CR.htm](pathfinder-bestiary-2-items/MvqmGM4ha7aQt1CR.htm)|Change Shape|Changement de forme|officielle|
|[MVRIjPsJ2e4QYHM9.htm](pathfinder-bestiary-2-items/MVRIjPsJ2e4QYHM9.htm)|Wing Deflection|Déflection de l'aile|officielle|
|[MvwGFLtsnf8ZFqhG.htm](pathfinder-bestiary-2-items/MvwGFLtsnf8ZFqhG.htm)|Root|Racine|libre|
|[MvxbkaVb0xvmWEgv.htm](pathfinder-bestiary-2-items/MvxbkaVb0xvmWEgv.htm)|Constant Spells|Sorts constants|libre|
|[MWff0Ro0CLotg930.htm](pathfinder-bestiary-2-items/MWff0Ro0CLotg930.htm)|Constrict|Constriction|officielle|
|[mwrBvwI1EzNpPqAp.htm](pathfinder-bestiary-2-items/mwrBvwI1EzNpPqAp.htm)|Fist|Poing|libre|
|[MwuE7Ua1AaKVpFBo.htm](pathfinder-bestiary-2-items/MwuE7Ua1AaKVpFBo.htm)|Peluda Venom|Venin de pélude|officielle|
|[mXh2p5FT7dkLC4KJ.htm](pathfinder-bestiary-2-items/mXh2p5FT7dkLC4KJ.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[mXvv89TdLwyRKCuM.htm](pathfinder-bestiary-2-items/mXvv89TdLwyRKCuM.htm)|Tentacle Mouth|Gueule du tentacule|libre|
|[MY7iImbmJzLQjtsK.htm](pathfinder-bestiary-2-items/MY7iImbmJzLQjtsK.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[MYTvAqtXzKikO4tg.htm](pathfinder-bestiary-2-items/MYTvAqtXzKikO4tg.htm)|Stinger|Dard|libre|
|[mZb7eFQ1yel73da5.htm](pathfinder-bestiary-2-items/mZb7eFQ1yel73da5.htm)|Soulsense|Perception de l’âme|officielle|
|[mzcBU4P7MDB5Kav4.htm](pathfinder-bestiary-2-items/mzcBU4P7MDB5Kav4.htm)|Frightful Presence|Présence terrifiante|libre|
|[mzdRGmCygfrC7SXm.htm](pathfinder-bestiary-2-items/mzdRGmCygfrC7SXm.htm)|Tick Fever|Fièvre des tiques|officielle|
|[MZE5l2QagOF7ICxV.htm](pathfinder-bestiary-2-items/MZE5l2QagOF7ICxV.htm)|Scent|Odorat|libre|
|[MzmPJ00qwdaxKPhE.htm](pathfinder-bestiary-2-items/MzmPJ00qwdaxKPhE.htm)|Grab|Empoignade/Agrippement|libre|
|[MZYIlatBEX5yoG4G.htm](pathfinder-bestiary-2-items/MZYIlatBEX5yoG4G.htm)|Bite|Morsure|officielle|
|[N1bssKPKGqtiKwNk.htm](pathfinder-bestiary-2-items/N1bssKPKGqtiKwNk.htm)|Magma Swim|Nage dans le magma|libre|
|[N1nMBoBunIANgCSx.htm](pathfinder-bestiary-2-items/N1nMBoBunIANgCSx.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[n5nOO8opkHkd9ZDW.htm](pathfinder-bestiary-2-items/n5nOO8opkHkd9ZDW.htm)|Darkvision|Vision dans le noir|libre|
|[N5T2rvrlqZmn4pNy.htm](pathfinder-bestiary-2-items/N5T2rvrlqZmn4pNy.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[N5U9pyyTitN5UDgN.htm](pathfinder-bestiary-2-items/N5U9pyyTitN5UDgN.htm)|Jaws|Mâchoires|libre|
|[n5uTPxg5zF7PKMZF.htm](pathfinder-bestiary-2-items/n5uTPxg5zF7PKMZF.htm)|Yank|Permutation|libre|
|[n6zbDHFEoD3NyVNv.htm](pathfinder-bestiary-2-items/n6zbDHFEoD3NyVNv.htm)|Hypostome|Hypostome|libre|
|[N8OPQrQXn7YLXcis.htm](pathfinder-bestiary-2-items/N8OPQrQXn7YLXcis.htm)|Constant Spells|Sorts constants|libre|
|[n94Dl501br4UFyL1.htm](pathfinder-bestiary-2-items/n94Dl501br4UFyL1.htm)|Throw Rock|Lancer de rocher|libre|
|[N9DGOzWNdk9Q9GYm.htm](pathfinder-bestiary-2-items/N9DGOzWNdk9Q9GYm.htm)|+1 Status Bonus on Saves vs Magic|Bonus de statut de +1 aux jds contre la magie|libre|
|[N9O6ClFh2arDGR4l.htm](pathfinder-bestiary-2-items/N9O6ClFh2arDGR4l.htm)|Pseudopod|Pseudopode|libre|
|[NA3aEVVR6sNkB8KH.htm](pathfinder-bestiary-2-items/NA3aEVVR6sNkB8KH.htm)|Ice Shard|Éclat de glace|libre|
|[nceXlZk44iBehzvl.htm](pathfinder-bestiary-2-items/nceXlZk44iBehzvl.htm)|Tail|Queue|libre|
|[NCrNtLcBDV1QPiXn.htm](pathfinder-bestiary-2-items/NCrNtLcBDV1QPiXn.htm)|Rend|Éventration|libre|
|[nd0M4055DPNvjNvf.htm](pathfinder-bestiary-2-items/nd0M4055DPNvjNvf.htm)|Manifest Shawl|Créer un châle|officielle|
|[nD0QzJqbAecWv9ls.htm](pathfinder-bestiary-2-items/nD0QzJqbAecWv9ls.htm)|Low-Light Vision|Vision nocturne|libre|
|[ND8R6Z5H3eqFpdDz.htm](pathfinder-bestiary-2-items/ND8R6Z5H3eqFpdDz.htm)|Regeneration 40 (deactivated by acid or fire)|Régénération 40 (désactivée par l'acide ou le feu)|libre|
|[NDhBBYbkfa48lqzj.htm](pathfinder-bestiary-2-items/NDhBBYbkfa48lqzj.htm)|Mist Vision|Vision malgré la brume|officielle|
|[Ne1X80xmcoz2KP6a.htm](pathfinder-bestiary-2-items/Ne1X80xmcoz2KP6a.htm)|Darkvision|Vision dans le noir|libre|
|[NEE6cuy1OUxkBe2Q.htm](pathfinder-bestiary-2-items/NEE6cuy1OUxkBe2Q.htm)|Darkvision|Vision dans le noir|libre|
|[neGWW2ZDslwlxS5Y.htm](pathfinder-bestiary-2-items/neGWW2ZDslwlxS5Y.htm)|Tremorsense|Perception des vibrations|libre|
|[newbZPBnDfEIJX5y.htm](pathfinder-bestiary-2-items/newbZPBnDfEIJX5y.htm)|Feeding Tendril|Vrille d'alimentation|libre|
|[NEWDCp6ZApgJEtMx.htm](pathfinder-bestiary-2-items/NEWDCp6ZApgJEtMx.htm)|Dispelling Field|Champ de dissipation|officielle|
|[nEZNMODnNpwrnUPC.htm](pathfinder-bestiary-2-items/nEZNMODnNpwrnUPC.htm)|Final Judgment|Jugement dernier|officielle|
|[nflrLT1POlEJZu3H.htm](pathfinder-bestiary-2-items/nflrLT1POlEJZu3H.htm)|Claw|Griffe|libre|
|[ngIT9cdBAAOK2wzV.htm](pathfinder-bestiary-2-items/ngIT9cdBAAOK2wzV.htm)|Independent Brains|Cerveaux indépendants|officielle|
|[nhcCpe4q2AuE5rBl.htm](pathfinder-bestiary-2-items/nhcCpe4q2AuE5rBl.htm)|Fire Jelly Venom|Venin de la méduse de feu|officielle|
|[NHDhjNbpULppyjkP.htm](pathfinder-bestiary-2-items/NHDhjNbpULppyjkP.htm)|Darkvision|Vision dans le noir|libre|
|[nhn4UFoQny0X750B.htm](pathfinder-bestiary-2-items/nhn4UFoQny0X750B.htm)|Breath Weapon|Arme de souffle|libre|
|[nIDe1OaPgWDPyBej.htm](pathfinder-bestiary-2-items/nIDe1OaPgWDPyBej.htm)|Text Immersion|Immersion textuelle|officielle|
|[nj1sfrFbZHDVVn3y.htm](pathfinder-bestiary-2-items/nj1sfrFbZHDVVn3y.htm)|Darkvision|Vision dans le noir|libre|
|[njNLaG6BmpA7LInp.htm](pathfinder-bestiary-2-items/njNLaG6BmpA7LInp.htm)|Jaws|Mâchoires|libre|
|[NJorOVwoCKz6JGOq.htm](pathfinder-bestiary-2-items/NJorOVwoCKz6JGOq.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[NJS22AHrPTckdEPS.htm](pathfinder-bestiary-2-items/NJS22AHrPTckdEPS.htm)|Sense Murderer|Perception du meurtrier|officielle|
|[nKHp4Fk7EvpSS3N9.htm](pathfinder-bestiary-2-items/nKHp4Fk7EvpSS3N9.htm)|Jaws|Mâchoires|libre|
|[nKL6rZMmn2WdRk3g.htm](pathfinder-bestiary-2-items/nKL6rZMmn2WdRk3g.htm)|Cling|S'accrocher|officielle|
|[nKM6FVNsXbBwEPNq.htm](pathfinder-bestiary-2-items/nKM6FVNsXbBwEPNq.htm)|Gnaw|Ronger|officielle|
|[nKqb3ksLlia2sFBZ.htm](pathfinder-bestiary-2-items/nKqb3ksLlia2sFBZ.htm)|Infused Items|Objets imprégnés|officielle|
|[nM4ThlQUyejxc6bH.htm](pathfinder-bestiary-2-items/nM4ThlQUyejxc6bH.htm)|Stone Step|Pas de pierre|officielle|
|[nMVv9cAH945ESnI9.htm](pathfinder-bestiary-2-items/nMVv9cAH945ESnI9.htm)|Claw|Griffe|libre|
|[Nnavq7mhKrb8vMx5.htm](pathfinder-bestiary-2-items/Nnavq7mhKrb8vMx5.htm)|Low-Light Vision|Vision nocturne|libre|
|[nneVdsY6tUBGsScQ.htm](pathfinder-bestiary-2-items/nneVdsY6tUBGsScQ.htm)|Head Regrowth|Repousse de la tête|officielle|
|[NnMlHBLwjpkBjx4v.htm](pathfinder-bestiary-2-items/NnMlHBLwjpkBjx4v.htm)|Claw|Griffe|libre|
|[NNPWYZUGqcGbUeZI.htm](pathfinder-bestiary-2-items/NNPWYZUGqcGbUeZI.htm)|Summon Aquatic Ally|Convocation d'allié aquatique|officielle|
|[NOvlBahz080PaM4R.htm](pathfinder-bestiary-2-items/NOvlBahz080PaM4R.htm)|Tail|Queue|libre|
|[noxHcmgvGeBUJRX5.htm](pathfinder-bestiary-2-items/noxHcmgvGeBUJRX5.htm)|Telepathy|Télépathie|libre|
|[NP8RWxt3VPo7LZwb.htm](pathfinder-bestiary-2-items/NP8RWxt3VPo7LZwb.htm)|Beak|Bec|libre|
|[npIgzch2HZkhFifI.htm](pathfinder-bestiary-2-items/npIgzch2HZkhFifI.htm)|Claw|Griffe|libre|
|[nplfqbEUtcOBlV0b.htm](pathfinder-bestiary-2-items/nplfqbEUtcOBlV0b.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[nPZlZs1rfnERketf.htm](pathfinder-bestiary-2-items/nPZlZs1rfnERketf.htm)|Undying Vendetta|Vendetta immortelle|officielle|
|[NqdW9zuvmBwHZScr.htm](pathfinder-bestiary-2-items/NqdW9zuvmBwHZScr.htm)|Archon's Door|Porte de l'archon|libre|
|[NrafRXoflRlcVm6H.htm](pathfinder-bestiary-2-items/NrafRXoflRlcVm6H.htm)|At-Will Spells|Sorts à volonté|libre|
|[nrHgXy7XizFWsPFe.htm](pathfinder-bestiary-2-items/nrHgXy7XizFWsPFe.htm)|Dweomer Leap|Bond occulte|officielle|
|[NSdZME2VBfcmBDQA.htm](pathfinder-bestiary-2-items/NSdZME2VBfcmBDQA.htm)|Claw|Griffe|libre|
|[nSgOSBLM92y28WsS.htm](pathfinder-bestiary-2-items/nSgOSBLM92y28WsS.htm)|Darkvision|Vision dans le noir|libre|
|[NshCFgqhon7NfWbq.htm](pathfinder-bestiary-2-items/NshCFgqhon7NfWbq.htm)|Claw|Griffe|libre|
|[NSLdgH5UqohAav11.htm](pathfinder-bestiary-2-items/NSLdgH5UqohAav11.htm)|Tiger Empathy|Empathie avec les tigres|libre|
|[NsRn4K2Tli8P3WKm.htm](pathfinder-bestiary-2-items/NsRn4K2Tli8P3WKm.htm)|Ignore Pain|Ignorer la douleur|officielle|
|[NTWIBx9lvUwV7F1W.htm](pathfinder-bestiary-2-items/NTWIBx9lvUwV7F1W.htm)|Mundane Appearance|Apparence banale|officielle|
|[numgrwQUdlF3fy51.htm](pathfinder-bestiary-2-items/numgrwQUdlF3fy51.htm)|Tail|Queue|libre|
|[nuNIIa81vZ9PT2RL.htm](pathfinder-bestiary-2-items/nuNIIa81vZ9PT2RL.htm)|Dazzling Burst|Éclat éblouissant|officielle|
|[NUrrmWCh8zDwdKFw.htm](pathfinder-bestiary-2-items/NUrrmWCh8zDwdKFw.htm)|Grab|Empoignade/Agrippement|libre|
|[nvEMR0N4HLFwMTMQ.htm](pathfinder-bestiary-2-items/nvEMR0N4HLFwMTMQ.htm)|Tremorsense|Perception des vibrations|libre|
|[nVMaCED96f0puvS1.htm](pathfinder-bestiary-2-items/nVMaCED96f0puvS1.htm)|Apocalypse Breath|Souffle de l’apocalypse|officielle|
|[nwepl5Un9QYNK5Gk.htm](pathfinder-bestiary-2-items/nwepl5Un9QYNK5Gk.htm)|Crafting|Artisanat|libre|
|[nxagDqd1qxC8w8dl.htm](pathfinder-bestiary-2-items/nxagDqd1qxC8w8dl.htm)|Low-Light Vision|Vision nocturne|libre|
|[nxblqBzJUsncnJ9t.htm](pathfinder-bestiary-2-items/nxblqBzJUsncnJ9t.htm)|Noxious Burst|Explosion nocive|officielle|
|[NXk6QjCfO8eW6Txp.htm](pathfinder-bestiary-2-items/NXk6QjCfO8eW6Txp.htm)|Sneak Attack|Attaque sournnoise|officielle|
|[nYQgnHNVs60rcvUn.htm](pathfinder-bestiary-2-items/nYQgnHNVs60rcvUn.htm)|Mechanical Vulnerability|Vulnérabilité mécanique|officielle|
|[NYqhdSbPLiwcvxjs.htm](pathfinder-bestiary-2-items/NYqhdSbPLiwcvxjs.htm)|Darkvision|Vision dans le noir|libre|
|[Nz33adJeSgdyaAw6.htm](pathfinder-bestiary-2-items/Nz33adJeSgdyaAw6.htm)|Hoof|Sabot|libre|
|[Nz9xljPpg09kcxBl.htm](pathfinder-bestiary-2-items/Nz9xljPpg09kcxBl.htm)|Bite|Morsure|officielle|
|[NzMuO5EzlGKw4nGq.htm](pathfinder-bestiary-2-items/NzMuO5EzlGKw4nGq.htm)|Mouth|Gueule|libre|
|[NZRABrBe5xEzlWlP.htm](pathfinder-bestiary-2-items/NZRABrBe5xEzlWlP.htm)|Arrow of Mortality|Flèche de mortalité|officielle|
|[O0uewbc9915uqqlD.htm](pathfinder-bestiary-2-items/O0uewbc9915uqqlD.htm)|Tail|Queue|libre|
|[O0YSsVnEBCg6oJnT.htm](pathfinder-bestiary-2-items/O0YSsVnEBCg6oJnT.htm)|Horns|Cornes|libre|
|[o19lnL3xo6DpdVgZ.htm](pathfinder-bestiary-2-items/o19lnL3xo6DpdVgZ.htm)|Twisting Tail|Queue sinueuse|officielle|
|[o3nbXHISVkXC32B1.htm](pathfinder-bestiary-2-items/o3nbXHISVkXC32B1.htm)|Grab|Empoignade/Agrippement|libre|
|[o40ddeHAtx8cJ0uY.htm](pathfinder-bestiary-2-items/o40ddeHAtx8cJ0uY.htm)|Mind Lash|Fouet mental|officielle|
|[O4cx0ZHztve08mHY.htm](pathfinder-bestiary-2-items/O4cx0ZHztve08mHY.htm)|Darkvision|Vision dans le noir|libre|
|[o4iEKaEBIwAr18XF.htm](pathfinder-bestiary-2-items/o4iEKaEBIwAr18XF.htm)|+4 Status Bonus on Will Saves vs Mental|Bonus de statut de +4 contre le mental|libre|
|[O5P2324cJfYLLpmm.htm](pathfinder-bestiary-2-items/O5P2324cJfYLLpmm.htm)|Glass Armor|Armure de verre|officielle|
|[o6CQEcEj0w3RIaZB.htm](pathfinder-bestiary-2-items/o6CQEcEj0w3RIaZB.htm)|At-Will Spells|Sorts à volonté|libre|
|[oa0MYylUzdTPYei2.htm](pathfinder-bestiary-2-items/oa0MYylUzdTPYei2.htm)|Claw|Griffe|libre|
|[oabwo3FEBY2CjPRM.htm](pathfinder-bestiary-2-items/oabwo3FEBY2CjPRM.htm)|Darkvision|Vision dans le noir|libre|
|[oApMO7ZwPJkW1mn1.htm](pathfinder-bestiary-2-items/oApMO7ZwPJkW1mn1.htm)|Improved Push|Bousculade améliorée|officielle|
|[oAxtReNr1iwTRmKX.htm](pathfinder-bestiary-2-items/oAxtReNr1iwTRmKX.htm)|Negate Levitation|Annulation de la lévitation|officielle|
|[OB8l4HRZ5Zc4G42p.htm](pathfinder-bestiary-2-items/OB8l4HRZ5Zc4G42p.htm)|Scent|Odorat|libre|
|[OBayZaw80QHZ8uBf.htm](pathfinder-bestiary-2-items/OBayZaw80QHZ8uBf.htm)|Constant Spells|Sorts constants|libre|
|[OBbctD8lNz3bU0yf.htm](pathfinder-bestiary-2-items/OBbctD8lNz3bU0yf.htm)|At-Will Spells|Sorts à volonté|libre|
|[oBMQTX6kkJaK35ed.htm](pathfinder-bestiary-2-items/oBMQTX6kkJaK35ed.htm)|Claw|Griffe|libre|
|[OBn7syfr2t30QJaa.htm](pathfinder-bestiary-2-items/OBn7syfr2t30QJaa.htm)|Bite|Morsure|officielle|
|[ObPZcbNErqRJbu6p.htm](pathfinder-bestiary-2-items/ObPZcbNErqRJbu6p.htm)|Holy Longbow|Arc long Saint|libre|
|[oBq0zVqb67SoY8qy.htm](pathfinder-bestiary-2-items/oBq0zVqb67SoY8qy.htm)|Darkvision|Vision dans le noir|libre|
|[oBV1Ybwqj4dpXeff.htm](pathfinder-bestiary-2-items/oBV1Ybwqj4dpXeff.htm)|Spirit Touch|Contact spirituel|officielle|
|[OBXLT5RlRI6AauZ1.htm](pathfinder-bestiary-2-items/OBXLT5RlRI6AauZ1.htm)|Breath Weapon|Armes de souffle|libre|
|[oC4rXv11i7sVgw4k.htm](pathfinder-bestiary-2-items/oC4rXv11i7sVgw4k.htm)|Radula|Radula|libre|
|[OC4UGWpbAZyLbFb6.htm](pathfinder-bestiary-2-items/OC4UGWpbAZyLbFb6.htm)|Breath Weapon|Arme de souffle|officielle|
|[oC5vZAEFIISSmofG.htm](pathfinder-bestiary-2-items/oC5vZAEFIISSmofG.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[Od1YX31hXRHCzHS3.htm](pathfinder-bestiary-2-items/Od1YX31hXRHCzHS3.htm)|Rip and Tear|Déchirer et arracher|officielle|
|[odZw9mK4mxnlp6qz.htm](pathfinder-bestiary-2-items/odZw9mK4mxnlp6qz.htm)|Darkvision|Vision dans le noir|libre|
|[Oe7KNrqiGTrNfkV9.htm](pathfinder-bestiary-2-items/Oe7KNrqiGTrNfkV9.htm)|Jaws|Mâchoires|libre|
|[OeHuAJLeUtdJFHfo.htm](pathfinder-bestiary-2-items/OeHuAJLeUtdJFHfo.htm)|Rhinoceros Charge|Charge du rhinocéros|officielle|
|[Oem6sNhw2eizgyTW.htm](pathfinder-bestiary-2-items/Oem6sNhw2eizgyTW.htm)|Constant Spells|Sorts constants|libre|
|[OEYNgSplNvqswLPK.htm](pathfinder-bestiary-2-items/OEYNgSplNvqswLPK.htm)|Golem Antimagic|Antimagie du Golem|libre|
|[Of0URrwSMG72XvUL.htm](pathfinder-bestiary-2-items/Of0URrwSMG72XvUL.htm)|Lifesense|Perception de la vie|libre|
|[ofz49oiBkargWzh6.htm](pathfinder-bestiary-2-items/ofz49oiBkargWzh6.htm)|Jaws|Mâchoires|libre|
|[oG07EBWnvzNPv4hk.htm](pathfinder-bestiary-2-items/oG07EBWnvzNPv4hk.htm)|Darkvision|Vision dans le noir|libre|
|[OhlQui33tv2Wq8Ur.htm](pathfinder-bestiary-2-items/OhlQui33tv2Wq8Ur.htm)|Scent (Imprecise) 60 feet|Odorat (imprécis) 18 m|libre|
|[oifiHjSHVsJGyQiE.htm](pathfinder-bestiary-2-items/oifiHjSHVsJGyQiE.htm)|Darkvision|Vision dans le noir|libre|
|[OJA9CljFVtmCuzlZ.htm](pathfinder-bestiary-2-items/OJA9CljFVtmCuzlZ.htm)|Prudent Asterism|Astérisme avisé|officielle|
|[OJg4VMV8aMk40kBm.htm](pathfinder-bestiary-2-items/OJg4VMV8aMk40kBm.htm)|Shadow Blend|Fusion dans les ombres|officielle|
|[ojlZFCKNrXd0MjTG.htm](pathfinder-bestiary-2-items/ojlZFCKNrXd0MjTG.htm)|Blood Siphon|Siphon de sang|officielle|
|[oKEblFTjM5UV6Lwa.htm](pathfinder-bestiary-2-items/oKEblFTjM5UV6Lwa.htm)|Steep Weapon|Macération d'arme|officielle|
|[OKH1R1FoBSEm3oKP.htm](pathfinder-bestiary-2-items/OKH1R1FoBSEm3oKP.htm)|Enraged Growth|Croissance de l'enragé|officielle|
|[OL5iKgpKK0q9NrD1.htm](pathfinder-bestiary-2-items/OL5iKgpKK0q9NrD1.htm)|Low-Light Vision|Vision nocturne|libre|
|[OlCiQCdanvjrjD3s.htm](pathfinder-bestiary-2-items/OlCiQCdanvjrjD3s.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie contre la magie|libre|
|[OlnWMyIX3qedGUTu.htm](pathfinder-bestiary-2-items/OlnWMyIX3qedGUTu.htm)|Darkvision|Vision dans le noir|libre|
|[OLwbn1vkUwxatzY5.htm](pathfinder-bestiary-2-items/OLwbn1vkUwxatzY5.htm)|Lifesense|Perception de la vie|libre|
|[OlwLivQfLQIh0ut7.htm](pathfinder-bestiary-2-items/OlwLivQfLQIh0ut7.htm)|Magic Sense|Perception de la magie|officielle|
|[oLxdFwYmbkBdHCgB.htm](pathfinder-bestiary-2-items/oLxdFwYmbkBdHCgB.htm)|At-Will Spells|Sorts à volonté|libre|
|[ome4gBGaz20PH3OU.htm](pathfinder-bestiary-2-items/ome4gBGaz20PH3OU.htm)|Blood Draining Bites|Absorption de sang par morsures|officielle|
|[oMj0SiABPgORm6t3.htm](pathfinder-bestiary-2-items/oMj0SiABPgORm6t3.htm)|Breath Weapon|Arme de souffle|libre|
|[OMTKXoF2bm81nkS5.htm](pathfinder-bestiary-2-items/OMTKXoF2bm81nkS5.htm)|Longsword|Épée longue|libre|
|[onjhPnLTGiThJjTI.htm](pathfinder-bestiary-2-items/onjhPnLTGiThJjTI.htm)|Tremorsense|Perception des vibrations|libre|
|[oNpExBZGMP135Gh8.htm](pathfinder-bestiary-2-items/oNpExBZGMP135Gh8.htm)|Capsize|Chavirer|officielle|
|[OOgOPxe66wl8FqEm.htm](pathfinder-bestiary-2-items/OOgOPxe66wl8FqEm.htm)|Athletics|Athlétisme|libre|
|[ope3wpbKSAJOWCJD.htm](pathfinder-bestiary-2-items/ope3wpbKSAJOWCJD.htm)|Bony Hand|Main décharnée|libre|
|[OpUEqqVcx7trG4mu.htm](pathfinder-bestiary-2-items/OpUEqqVcx7trG4mu.htm)|Kukri|Kukri|libre|
|[oQcnbbUjvuJ6yIaK.htm](pathfinder-bestiary-2-items/oQcnbbUjvuJ6yIaK.htm)|Deep Breath|Inspiration profonde|officielle|
|[OQE9ap0Mj3pykdzk.htm](pathfinder-bestiary-2-items/OQE9ap0Mj3pykdzk.htm)|Acrobatics|Acrobaties|libre|
|[OqJWwPCeEzKsYH8X.htm](pathfinder-bestiary-2-items/OqJWwPCeEzKsYH8X.htm)|Darkvision|Vision dans le noir|libre|
|[OQMwQiE7vGLPMasL.htm](pathfinder-bestiary-2-items/OQMwQiE7vGLPMasL.htm)|Blood Drain|Absorption de sang|officielle|
|[oQNL2zFwRBLQE7vn.htm](pathfinder-bestiary-2-items/oQNL2zFwRBLQE7vn.htm)|Horns|Cornes|libre|
|[OR27Y1Vkv8bMFjHO.htm](pathfinder-bestiary-2-items/OR27Y1Vkv8bMFjHO.htm)|Dagger|Dague|libre|
|[orQ7bvmsL70g6qk9.htm](pathfinder-bestiary-2-items/orQ7bvmsL70g6qk9.htm)|Jaws|Mâchoires|libre|
|[osi4ctAQErnOa2BZ.htm](pathfinder-bestiary-2-items/osi4ctAQErnOa2BZ.htm)|Heart Ripper|Arrache-coeur|officielle|
|[OSiq1CXChzTT7udW.htm](pathfinder-bestiary-2-items/OSiq1CXChzTT7udW.htm)|Spectral Corruption|Corruption spectrale|officielle|
|[osogPaHrRMKqt4Fw.htm](pathfinder-bestiary-2-items/osogPaHrRMKqt4Fw.htm)|Trample|Piétinement|libre|
|[ot1RNObQMdnpWSrb.htm](pathfinder-bestiary-2-items/ot1RNObQMdnpWSrb.htm)|Catch Rock|Interception de rochers|libre|
|[Ot3tRiGqqRheUAKs.htm](pathfinder-bestiary-2-items/Ot3tRiGqqRheUAKs.htm)|Crystallize Flesh|Cristallisation de la chair|officielle|
|[OT4mRkq8mC5ESE4d.htm](pathfinder-bestiary-2-items/OT4mRkq8mC5ESE4d.htm)|Stealth|Discrétion|libre|
|[otBr1P6MKia09fCt.htm](pathfinder-bestiary-2-items/otBr1P6MKia09fCt.htm)|Catch Rock|Interception de rochers|libre|
|[OUEVYrICDhOs5NML.htm](pathfinder-bestiary-2-items/OUEVYrICDhOs5NML.htm)|Savage Jaws|Mâchoires sauvages|officielle|
|[OxEJ1NOQewlXZjZh.htm](pathfinder-bestiary-2-items/OxEJ1NOQewlXZjZh.htm)|Claw|Griffe|libre|
|[OXfHyEExXIhlnUp7.htm](pathfinder-bestiary-2-items/OXfHyEExXIhlnUp7.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[OxPw5pr9S1edjy8S.htm](pathfinder-bestiary-2-items/OxPw5pr9S1edjy8S.htm)|Lunar Naga Venom|Venin de Naga lunaire|officielle|
|[OxZGoHvhVubVXMm4.htm](pathfinder-bestiary-2-items/OxZGoHvhVubVXMm4.htm)|Petrifying Gaze|Regard pétrifiant|officielle|
|[OygWVWtANgUBztcL.htm](pathfinder-bestiary-2-items/OygWVWtANgUBztcL.htm)|At-Will Spells|Sorts à volonté|libre|
|[OymgeOQy6WJ5jvF1.htm](pathfinder-bestiary-2-items/OymgeOQy6WJ5jvF1.htm)|Scent|Odorat|libre|
|[OYTKATaPDdautdNt.htm](pathfinder-bestiary-2-items/OYTKATaPDdautdNt.htm)|Wing|Aile|libre|
|[oYtncLyCKuBAQThk.htm](pathfinder-bestiary-2-items/oYtncLyCKuBAQThk.htm)|Clawing Fear|Peur griffue|officielle|
|[Oz0PpydQ8KCagat3.htm](pathfinder-bestiary-2-items/Oz0PpydQ8KCagat3.htm)|Tail|Queue|libre|
|[OZmNooOK98jFnTUU.htm](pathfinder-bestiary-2-items/OZmNooOK98jFnTUU.htm)|Barbed Tentacles|Tentacules lamées|libre|
|[OzuFfQ0SSo89GdPZ.htm](pathfinder-bestiary-2-items/OzuFfQ0SSo89GdPZ.htm)|Hoof|Sabot|libre|
|[p18BSmeidbDDfJ2D.htm](pathfinder-bestiary-2-items/p18BSmeidbDDfJ2D.htm)|Kind Word|Discours bienveillant|officielle|
|[p1rxtsKx33vjytn0.htm](pathfinder-bestiary-2-items/p1rxtsKx33vjytn0.htm)|Thulgant Venom|Venin de thulgant|officielle|
|[P48WRIeVuuAB8JWK.htm](pathfinder-bestiary-2-items/P48WRIeVuuAB8JWK.htm)|Death Flood|Torrent mortel|officielle|
|[P4BV5OJUyj3rfr1E.htm](pathfinder-bestiary-2-items/P4BV5OJUyj3rfr1E.htm)|Telepathy|Télépathie|libre|
|[p5OQLHvMklzKmrbR.htm](pathfinder-bestiary-2-items/p5OQLHvMklzKmrbR.htm)|Scimitar|Cimeterre|libre|
|[P5TjcVC1dN5jLmFb.htm](pathfinder-bestiary-2-items/P5TjcVC1dN5jLmFb.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[P5tslJuQziddpFpV.htm](pathfinder-bestiary-2-items/P5tslJuQziddpFpV.htm)|Tooth Grind|Broyage avec les dents|officielle|
|[P6oJVM0MDCy82QgU.htm](pathfinder-bestiary-2-items/P6oJVM0MDCy82QgU.htm)|Shadow Blending|Fusion avec les ombres|officielle|
|[p8Evyqv8pdGxIXIQ.htm](pathfinder-bestiary-2-items/p8Evyqv8pdGxIXIQ.htm)|Rend|Éventration|libre|
|[P8P9UmFHbSmMnTnd.htm](pathfinder-bestiary-2-items/P8P9UmFHbSmMnTnd.htm)|Low-Light Vision|Vision nocturne|libre|
|[p9ZGDxOXXXlq8PuH.htm](pathfinder-bestiary-2-items/p9ZGDxOXXXlq8PuH.htm)|Jaws|Mâchoires|libre|
|[Pb3h2ToW8cyiBXN7.htm](pathfinder-bestiary-2-items/Pb3h2ToW8cyiBXN7.htm)|Tentacle|Tentacule|libre|
|[PBcYVDE3HZByWRI7.htm](pathfinder-bestiary-2-items/PBcYVDE3HZByWRI7.htm)|Black Scorpion Venom|Venin de scorpion noir|officielle|
|[PbnYiMI8IGyrAIej.htm](pathfinder-bestiary-2-items/PbnYiMI8IGyrAIej.htm)|Chain|Chaîne|libre|
|[PbQYJStaqe1nhRUn.htm](pathfinder-bestiary-2-items/PbQYJStaqe1nhRUn.htm)|Frightful Presence|Présence terrifiante|libre|
|[Pd7VzQqoVyctHCLg.htm](pathfinder-bestiary-2-items/Pd7VzQqoVyctHCLg.htm)|Magma Swim|Nage dans le magma|officielle|
|[PDKjx7hYxc0mQaVe.htm](pathfinder-bestiary-2-items/PDKjx7hYxc0mQaVe.htm)|At-Will Spells|Sorts à volonté|libre|
|[PDq2G4IiMdipukDg.htm](pathfinder-bestiary-2-items/PDq2G4IiMdipukDg.htm)|Beak|Bec|libre|
|[PFAC6dSt4RN7aRDz.htm](pathfinder-bestiary-2-items/PFAC6dSt4RN7aRDz.htm)|Stinger|Dard|libre|
|[pfYqWHlnkQUBpJIo.htm](pathfinder-bestiary-2-items/pfYqWHlnkQUBpJIo.htm)|Electric Torpor|Torpeur électrique|officielle|
|[pfzWyL0gCgqD1xRz.htm](pathfinder-bestiary-2-items/pfzWyL0gCgqD1xRz.htm)|Telepathy|Télépathie|libre|
|[pGaSnoNG83zKVZkX.htm](pathfinder-bestiary-2-items/pGaSnoNG83zKVZkX.htm)|Deflecting Cloud|Nuage déflecteur|officielle|
|[PGeuRIZhMLp88mYc.htm](pathfinder-bestiary-2-items/PGeuRIZhMLp88mYc.htm)|Fist|Poing|libre|
|[PgZtqgPtRR9DRaGz.htm](pathfinder-bestiary-2-items/PgZtqgPtRR9DRaGz.htm)|Vine|Liane|libre|
|[pheseepJ6Q1YF17G.htm](pathfinder-bestiary-2-items/pheseepJ6Q1YF17G.htm)|Darkvision|Vision dans le noir|libre|
|[pJ6eBDUVUixzX9UL.htm](pathfinder-bestiary-2-items/pJ6eBDUVUixzX9UL.htm)|Animate Chains|Animer les chaînes|officielle|
|[PjBdBCA31o7FcQvI.htm](pathfinder-bestiary-2-items/PjBdBCA31o7FcQvI.htm)|Telepathy|Télépathie|libre|
|[pke6bvuCCSuidMVb.htm](pathfinder-bestiary-2-items/pke6bvuCCSuidMVb.htm)|Poisonous Touch|Toucher vénéneux|libre|
|[pKF3w8Hj8QFO6ZMv.htm](pathfinder-bestiary-2-items/pKF3w8Hj8QFO6ZMv.htm)|Draconic Momentum|Élan draconique|libre|
|[PKHHueFUrRyFJPxL.htm](pathfinder-bestiary-2-items/PKHHueFUrRyFJPxL.htm)|Rend|Éventration|libre|
|[pkqZFkDwe6SFvUXX.htm](pathfinder-bestiary-2-items/pkqZFkDwe6SFvUXX.htm)|Serpentfolk Venom|Venin des hommes-serpents|officielle|
|[pL08UYEZoKBSYUzv.htm](pathfinder-bestiary-2-items/pL08UYEZoKBSYUzv.htm)|Darkvision|Vision dans le noir|libre|
|[pLNcYnrUhpwxDo9b.htm](pathfinder-bestiary-2-items/pLNcYnrUhpwxDo9b.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[PM9U1Ca1DQ2ZA7ZJ.htm](pathfinder-bestiary-2-items/PM9U1Ca1DQ2ZA7ZJ.htm)|Negative Healing|Guérison négative|libre|
|[PMbE9CQyp5uM7Whu.htm](pathfinder-bestiary-2-items/PMbE9CQyp5uM7Whu.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[pmHXQt1EgMIVwD1i.htm](pathfinder-bestiary-2-items/pmHXQt1EgMIVwD1i.htm)|Exquisite Pain|Douleur raffinée|officielle|
|[pntbmMiiU6IArfJX.htm](pathfinder-bestiary-2-items/pntbmMiiU6IArfJX.htm)|Grab|Empoignade/Agrippement|libre|
|[pnvcZVssnfksWBzv.htm](pathfinder-bestiary-2-items/pnvcZVssnfksWBzv.htm)|Quill Thrust|Assaut de piquants|officielle|
|[POIZ15lxh5FqbzaK.htm](pathfinder-bestiary-2-items/POIZ15lxh5FqbzaK.htm)|Fist|Poing|libre|
|[POLjXIhCSeQBrvcV.htm](pathfinder-bestiary-2-items/POLjXIhCSeQBrvcV.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[POVANlwhTG8kc8av.htm](pathfinder-bestiary-2-items/POVANlwhTG8kc8av.htm)|Claw|Griffe|libre|
|[pP0EwdXTDTjgDpZA.htm](pathfinder-bestiary-2-items/pP0EwdXTDTjgDpZA.htm)|Darkvision|Vision dans le noir|libre|
|[PpVuZdKsS2ms3K3h.htm](pathfinder-bestiary-2-items/PpVuZdKsS2ms3K3h.htm)|Shadow Stride|Marche des ombres|officielle|
|[Pq25te8zoxBzUMRN.htm](pathfinder-bestiary-2-items/Pq25te8zoxBzUMRN.htm)|Jaws|Mâchoires|libre|
|[PQ87JPdxzlCptXn6.htm](pathfinder-bestiary-2-items/PQ87JPdxzlCptXn6.htm)|Tick Fever|Fièvre des tiques|officielle|
|[pRC5IW45o7eWNos1.htm](pathfinder-bestiary-2-items/pRC5IW45o7eWNos1.htm)|Jaws|Mâchoires|libre|
|[pRxhn354ZvirgsU6.htm](pathfinder-bestiary-2-items/pRxhn354ZvirgsU6.htm)|Recall Venom|Rappeler le venin|officielle|
|[PskJTiEThXOJo4hH.htm](pathfinder-bestiary-2-items/PskJTiEThXOJo4hH.htm)|Claw|Griffe|libre|
|[PSqd8jopT7V35lJt.htm](pathfinder-bestiary-2-items/PSqd8jopT7V35lJt.htm)|Hands of the Murderer|Mains du meurtrier|officielle|
|[pT1xpkQICFklLesg.htm](pathfinder-bestiary-2-items/pT1xpkQICFklLesg.htm)|Tendril|Vrille|libre|
|[pTjXZs6ZwSF31KLb.htm](pathfinder-bestiary-2-items/pTjXZs6ZwSF31KLb.htm)|Drink Blood|Boire le sang|officielle|
|[pTowxMM6c9JCvVGF.htm](pathfinder-bestiary-2-items/pTowxMM6c9JCvVGF.htm)|Mind Bolt|Assaut mental|officielle|
|[pu67MUMU2zgOU5zE.htm](pathfinder-bestiary-2-items/pu67MUMU2zgOU5zE.htm)|Scent|Odorat|libre|
|[puOX1L9eYq450jRo.htm](pathfinder-bestiary-2-items/puOX1L9eYq450jRo.htm)|Scent|Odorat|libre|
|[pUwo9wumwrjtvid1.htm](pathfinder-bestiary-2-items/pUwo9wumwrjtvid1.htm)|Infernal Wound|Blessure infernale|officielle|
|[pV2UvWTozyuKQHGx.htm](pathfinder-bestiary-2-items/pV2UvWTozyuKQHGx.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[pVbWqNxbj50FvZ38.htm](pathfinder-bestiary-2-items/pVbWqNxbj50FvZ38.htm)|Low-Light Vision|Vision nocturne|libre|
|[pvidfrziJK8Olr4p.htm](pathfinder-bestiary-2-items/pvidfrziJK8Olr4p.htm)|Piercing Shot|Tir perforant|officielle|
|[pViUeTjTLhTubzOw.htm](pathfinder-bestiary-2-items/pViUeTjTLhTubzOw.htm)|Coven|Cercle|officielle|
|[Pvp0MGWk24EJhPud.htm](pathfinder-bestiary-2-items/Pvp0MGWk24EJhPud.htm)|Tentacle|Tentacule|libre|
|[pvTRbzLubEXLA5D5.htm](pathfinder-bestiary-2-items/pvTRbzLubEXLA5D5.htm)|Claw|Griffe|libre|
|[PWBjM1JecEFdjzo0.htm](pathfinder-bestiary-2-items/PWBjM1JecEFdjzo0.htm)|Savage Jaws|Mâchoires sauvages|officielle|
|[pwHxPg0sBxXkBROU.htm](pathfinder-bestiary-2-items/pwHxPg0sBxXkBROU.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[PwSEiS1mqX6rqjJa.htm](pathfinder-bestiary-2-items/PwSEiS1mqX6rqjJa.htm)|Bo Staff|Bô|libre|
|[PxJ09KlgYVpdaklS.htm](pathfinder-bestiary-2-items/PxJ09KlgYVpdaklS.htm)|Tail|Queue|libre|
|[pXklOYevcG58P7sJ.htm](pathfinder-bestiary-2-items/pXklOYevcG58P7sJ.htm)|Beak|Bec|libre|
|[pXVSNEwIRE2uBaw6.htm](pathfinder-bestiary-2-items/pXVSNEwIRE2uBaw6.htm)|Wing Flash|Battement d'aile|officielle|
|[Pxxc4fNbFR6zlG0y.htm](pathfinder-bestiary-2-items/Pxxc4fNbFR6zlG0y.htm)|Web Trap|Piège de toile|officielle|
|[Pxxr235CKBdgLZ6y.htm](pathfinder-bestiary-2-items/Pxxr235CKBdgLZ6y.htm)|Jaws|Mâchoires|libre|
|[Py8ycO501wOs8ujU.htm](pathfinder-bestiary-2-items/Py8ycO501wOs8ujU.htm)|Legs|Jambes|libre|
|[pYBvVxfTXqJhnpWd.htm](pathfinder-bestiary-2-items/pYBvVxfTXqJhnpWd.htm)|Claw|Griffe|libre|
|[pYiWNLrYh7MbvSRl.htm](pathfinder-bestiary-2-items/pYiWNLrYh7MbvSRl.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[PySanquTNKBp5Zkm.htm](pathfinder-bestiary-2-items/PySanquTNKBp5Zkm.htm)|Breath Weapon|Arme de souffle|officielle|
|[pzMdTIEh6A57hKG7.htm](pathfinder-bestiary-2-items/pzMdTIEh6A57hKG7.htm)|At-Will Spells|Sorts à volonté|libre|
|[pztBMGaCTnjDqJCY.htm](pathfinder-bestiary-2-items/pztBMGaCTnjDqJCY.htm)|At-Will Spells|Sorts à volonté|libre|
|[q0kSVZllWCY1CwD2.htm](pathfinder-bestiary-2-items/q0kSVZllWCY1CwD2.htm)|Darkvision|Vision dans le noir|libre|
|[Q0VRtsqm6etoZxCa.htm](pathfinder-bestiary-2-items/Q0VRtsqm6etoZxCa.htm)|Shadow Doubles|Doubles des ombres|officielle|
|[Q0zvzLaylr0AYBGR.htm](pathfinder-bestiary-2-items/Q0zvzLaylr0AYBGR.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[q2KJwlgmVEKQMJPb.htm](pathfinder-bestiary-2-items/q2KJwlgmVEKQMJPb.htm)|Maintain Disguise|Conserver le déguisement|officielle|
|[Q3683RHwk5skFzWi.htm](pathfinder-bestiary-2-items/Q3683RHwk5skFzWi.htm)|Dispelling Strike|Frappe de dissipation|officielle|
|[Q57wXsK3JdmLYoDR.htm](pathfinder-bestiary-2-items/Q57wXsK3JdmLYoDR.htm)|Blade of Justice|Lame de justice|officielle|
|[q5XLED0zxjExmJp2.htm](pathfinder-bestiary-2-items/q5XLED0zxjExmJp2.htm)|Tail|Queue|libre|
|[Q6Z6sPX8M68AUOXI.htm](pathfinder-bestiary-2-items/Q6Z6sPX8M68AUOXI.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[Q7nRRCF8dXONTqKt.htm](pathfinder-bestiary-2-items/Q7nRRCF8dXONTqKt.htm)|Shock|Choc|libre|
|[Q8lUTVhcaklSOhJn.htm](pathfinder-bestiary-2-items/Q8lUTVhcaklSOhJn.htm)|Scent|Odorat|libre|
|[QACVidUBGQoHHpqW.htm](pathfinder-bestiary-2-items/QACVidUBGQoHHpqW.htm)|Mark Quarry|Proie marquée|officielle|
|[QAiOrfL7q6tF2GUV.htm](pathfinder-bestiary-2-items/QAiOrfL7q6tF2GUV.htm)|Draconic Momentum|Impulsion draconique|libre|
|[QaqXp54jr3aKQjWN.htm](pathfinder-bestiary-2-items/QaqXp54jr3aKQjWN.htm)|Negative Healing|Soins négatifs|libre|
|[QaRhnU95T3Ie2dFP.htm](pathfinder-bestiary-2-items/QaRhnU95T3Ie2dFP.htm)|Stealth|Discrétion|libre|
|[qARQ5Sntem4thRiy.htm](pathfinder-bestiary-2-items/qARQ5Sntem4thRiy.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[QaWyk1afljYBga0c.htm](pathfinder-bestiary-2-items/QaWyk1afljYBga0c.htm)|Wind Gust|Bourrasque|libre|
|[qbm10J9qDUiFCQDn.htm](pathfinder-bestiary-2-items/qbm10J9qDUiFCQDn.htm)|Arc Lightning|Arc électrique|officielle|
|[qbn93YsujSFI6TMZ.htm](pathfinder-bestiary-2-items/qbn93YsujSFI6TMZ.htm)|Camouflage|Camouflage|officielle|
|[QBq3x8HUuGt0wVZm.htm](pathfinder-bestiary-2-items/QBq3x8HUuGt0wVZm.htm)|Sense Blood (Imprecise) 60 feet|Perception du sang (Imprécis) 18 m|officielle|
|[qcCotjUp5yYyWkjT.htm](pathfinder-bestiary-2-items/qcCotjUp5yYyWkjT.htm)|Claw|Griffe|libre|
|[qCZyXHHj5OMACPYY.htm](pathfinder-bestiary-2-items/qCZyXHHj5OMACPYY.htm)|Taiga Linnorm Venom|Venin de linnorm de la taïga|officielle|
|[QD2iwjozawGYYqOf.htm](pathfinder-bestiary-2-items/QD2iwjozawGYYqOf.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[qdKGH0iZztLLCCPz.htm](pathfinder-bestiary-2-items/qdKGH0iZztLLCCPz.htm)|Low-Light Vision|Vision nocturne|libre|
|[qebLFwBJf98hFcSv.htm](pathfinder-bestiary-2-items/qebLFwBJf98hFcSv.htm)|Darkvision|Vision dans le noir|libre|
|[qEh64WzRoljm53JB.htm](pathfinder-bestiary-2-items/qEh64WzRoljm53JB.htm)|Darkvision|Vision dans le noir|libre|
|[qeIQpw65yitumRkB.htm](pathfinder-bestiary-2-items/qeIQpw65yitumRkB.htm)|Jaws|Mâchoires|libre|
|[qfAGiyqMgrNFUoSw.htm](pathfinder-bestiary-2-items/qfAGiyqMgrNFUoSw.htm)|Fast Healing 20|Guérison accélérée 20|libre|
|[qfIg2ulL65DhV6X2.htm](pathfinder-bestiary-2-items/qfIg2ulL65DhV6X2.htm)|Sprint|Sprint|officielle|
|[qfkx0yR9a3Qh4LsT.htm](pathfinder-bestiary-2-items/qfkx0yR9a3Qh4LsT.htm)|Double Attack|Double attaque|officielle|
|[qfYFz5QrJfsbmJng.htm](pathfinder-bestiary-2-items/qfYFz5QrJfsbmJng.htm)|Darkvision|Vision dans le noir|libre|
|[Qgjku7yO52q21NCG.htm](pathfinder-bestiary-2-items/Qgjku7yO52q21NCG.htm)|Temporal Flurry|Frénésie tempmorelle|officielle|
|[qh8LFXVLWjILfZZx.htm](pathfinder-bestiary-2-items/qh8LFXVLWjILfZZx.htm)|Sunlight Powerlessness|Impuissance solaire|officielle|
|[QHdmWY86UyD9ECHR.htm](pathfinder-bestiary-2-items/QHdmWY86UyD9ECHR.htm)|Darkvision|Vision dans le noir|libre|
|[qHEzK4MngkQB6vhD.htm](pathfinder-bestiary-2-items/qHEzK4MngkQB6vhD.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[qhgcqwFp1hE2jFbu.htm](pathfinder-bestiary-2-items/qhgcqwFp1hE2jFbu.htm)|Claw|Griffe|libre|
|[QHvw3fI9gFNac33v.htm](pathfinder-bestiary-2-items/QHvw3fI9gFNac33v.htm)|Fangs|Crocs|libre|
|[QIzXNCE3GPZBj5sy.htm](pathfinder-bestiary-2-items/QIzXNCE3GPZBj5sy.htm)|Shortsword|Épée courte|libre|
|[Qj1AizQTHLeRQxPV.htm](pathfinder-bestiary-2-items/Qj1AizQTHLeRQxPV.htm)|Darkvision|Vision dans le noir|libre|
|[QjBqDZJ1zOlHWSEG.htm](pathfinder-bestiary-2-items/QjBqDZJ1zOlHWSEG.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[qJQGpBBEACoUjdFY.htm](pathfinder-bestiary-2-items/qJQGpBBEACoUjdFY.htm)|Claw|Griffe|libre|
|[ql5PVdfgunDKXI5N.htm](pathfinder-bestiary-2-items/ql5PVdfgunDKXI5N.htm)|Darkvision|Vision dans le noir|libre|
|[qlRNyTqzf6kMvBEV.htm](pathfinder-bestiary-2-items/qlRNyTqzf6kMvBEV.htm)|Quill|Piquants|libre|
|[QmK15RaSe9yfdYIR.htm](pathfinder-bestiary-2-items/QmK15RaSe9yfdYIR.htm)|Vulnerable Tail|Queue vulnérable|officielle|
|[QNMEwPCmUmz3bSOW.htm](pathfinder-bestiary-2-items/QNMEwPCmUmz3bSOW.htm)|Knockdown|Renversement|libre|
|[QoOV8oVWiooAilxA.htm](pathfinder-bestiary-2-items/QoOV8oVWiooAilxA.htm)|Sea Snake Venom|Venin de serpent de mer|officielle|
|[qoqJdxMXA4DPeMal.htm](pathfinder-bestiary-2-items/qoqJdxMXA4DPeMal.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[qoSUaPSmr9BrJU8V.htm](pathfinder-bestiary-2-items/qoSUaPSmr9BrJU8V.htm)|Breath Weapon|Arme de souffle|officielle|
|[qpAN2omg7HvCwOzk.htm](pathfinder-bestiary-2-items/qpAN2omg7HvCwOzk.htm)|Undetectable|Indétectable|officielle|
|[QPrrVmViA5QTr6tE.htm](pathfinder-bestiary-2-items/QPrrVmViA5QTr6tE.htm)|Bastion Aura|Aura du bastion|officielle|
|[QqggW4FlHDKMwZT4.htm](pathfinder-bestiary-2-items/QqggW4FlHDKMwZT4.htm)|Push|Bousculade|officielle|
|[QQSJHx4JIqEThZoC.htm](pathfinder-bestiary-2-items/QQSJHx4JIqEThZoC.htm)|Mandibles|Mandibules|libre|
|[qs95uKqjbpoxk48D.htm](pathfinder-bestiary-2-items/qs95uKqjbpoxk48D.htm)|Jaws|Mâchoires|libre|
|[QSAMSi294JHAumur.htm](pathfinder-bestiary-2-items/QSAMSi294JHAumur.htm)|Rock|Rocher|libre|
|[QSxWcw9SiRdGtKM7.htm](pathfinder-bestiary-2-items/QSxWcw9SiRdGtKM7.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[QTFJOKoZto5yRgDd.htm](pathfinder-bestiary-2-items/QTFJOKoZto5yRgDd.htm)|Darkvision|Vision dans le noir|libre|
|[QTxOTyLCST6Zaqb3.htm](pathfinder-bestiary-2-items/QTxOTyLCST6Zaqb3.htm)|At-Will Spells|Sorts à volonté|libre|
|[QuK27vpZ1PXmKcRW.htm](pathfinder-bestiary-2-items/QuK27vpZ1PXmKcRW.htm)|Fist|Poing|libre|
|[qUtjLPzfgoNg1aAP.htm](pathfinder-bestiary-2-items/qUtjLPzfgoNg1aAP.htm)|Mandibles|Mandibules|libre|
|[QUwEfng9Ey9oqGwE.htm](pathfinder-bestiary-2-items/QUwEfng9Ey9oqGwE.htm)|Darkvision|Vision dans le noir|libre|
|[qV74gNViCaiEVUy2.htm](pathfinder-bestiary-2-items/qV74gNViCaiEVUy2.htm)|Capsize|Chavirer|officielle|
|[QVlwvsf0cmEupKV2.htm](pathfinder-bestiary-2-items/QVlwvsf0cmEupKV2.htm)|Tremorsense|Perception des vibrations|libre|
|[qWb7LRbSfICoEdcJ.htm](pathfinder-bestiary-2-items/qWb7LRbSfICoEdcJ.htm)|Constant Spells|Sorts constants|libre|
|[qWcmE7mc3EOqNR3L.htm](pathfinder-bestiary-2-items/qWcmE7mc3EOqNR3L.htm)|Tentacle|Tentacule|libre|
|[qwdqondeCGj9321E.htm](pathfinder-bestiary-2-items/qwdqondeCGj9321E.htm)|Negative Healing|Guérison négative|libre|
|[qwrA1CcDM9QaJlc5.htm](pathfinder-bestiary-2-items/qwrA1CcDM9QaJlc5.htm)|Pounce|Bond|officielle|
|[QwsbmnGAaEHYU0ut.htm](pathfinder-bestiary-2-items/QwsbmnGAaEHYU0ut.htm)|Lightning Bolt|Éclair|libre|
|[Qx0Uh3ktQIh1dVKT.htm](pathfinder-bestiary-2-items/Qx0Uh3ktQIh1dVKT.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[qxh5yKErE5FpGPnf.htm](pathfinder-bestiary-2-items/qxh5yKErE5FpGPnf.htm)|Knockdown|Renversement|libre|
|[qxkuhugvyoxDpt7N.htm](pathfinder-bestiary-2-items/qxkuhugvyoxDpt7N.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[QxUxl1vPtZ72uSOi.htm](pathfinder-bestiary-2-items/QxUxl1vPtZ72uSOi.htm)|Mist Vision|Vision malgré la brume|officielle|
|[QXYKDwpj06Pd7sEo.htm](pathfinder-bestiary-2-items/QXYKDwpj06Pd7sEo.htm)|Scent|Odorat|libre|
|[qZaBb2z6mE2zLL2u.htm](pathfinder-bestiary-2-items/qZaBb2z6mE2zLL2u.htm)|Create Object|Créer un objet|officielle|
|[Qzr8MBWWm4DE5P8H.htm](pathfinder-bestiary-2-items/Qzr8MBWWm4DE5P8H.htm)|Discorporate|Dématérialisation|officielle|
|[R2a5AYr0RbUoKiK3.htm](pathfinder-bestiary-2-items/R2a5AYr0RbUoKiK3.htm)|Sard Venom|Venin de sard|officielle|
|[R2ziTTF38PwKFQr7.htm](pathfinder-bestiary-2-items/R2ziTTF38PwKFQr7.htm)|Frightful Presence|Présence terrifiante|libre|
|[R30Gee8hs5TPUNYY.htm](pathfinder-bestiary-2-items/R30Gee8hs5TPUNYY.htm)|Darkvision|Vision dans le noir|libre|
|[r3hGo7TAQ3TtGwLY.htm](pathfinder-bestiary-2-items/r3hGo7TAQ3TtGwLY.htm)|Electrolocation|Électrolocalisation|officielle|
|[r421CNwt0qBGoUVK.htm](pathfinder-bestiary-2-items/r421CNwt0qBGoUVK.htm)|Retributive Strike|Frappe punitive|officielle|
|[R4jXbSxWyqNtYJyp.htm](pathfinder-bestiary-2-items/R4jXbSxWyqNtYJyp.htm)|Thumb Spike|Pouce pointu|libre|
|[r5Mw7uM8br1L49y5.htm](pathfinder-bestiary-2-items/r5Mw7uM8br1L49y5.htm)|Wing|Aile|libre|
|[r7aL4DHJpi1R2n6Z.htm](pathfinder-bestiary-2-items/r7aL4DHJpi1R2n6Z.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[R8bzIOrsR2PFpnSQ.htm](pathfinder-bestiary-2-items/R8bzIOrsR2PFpnSQ.htm)|Noxious Fumes|Émanations nocives|officielle|
|[R8vIKnaOA34FiWMY.htm](pathfinder-bestiary-2-items/R8vIKnaOA34FiWMY.htm)|Water Jet|Jet d'eau|libre|
|[r8zcJu9p3TzgNHoU.htm](pathfinder-bestiary-2-items/r8zcJu9p3TzgNHoU.htm)|At-Will Spells|Sorts à volonté|libre|
|[r9UN2dDmxrL8ACPX.htm](pathfinder-bestiary-2-items/r9UN2dDmxrL8ACPX.htm)|All-Around Vision|Vision à 360°|libre|
|[rA2rDDvXjf2Cfr4w.htm](pathfinder-bestiary-2-items/rA2rDDvXjf2Cfr4w.htm)|Grab|Empoignade/Agrippement|libre|
|[ra2VPYkOtDIX5nt5.htm](pathfinder-bestiary-2-items/ra2VPYkOtDIX5nt5.htm)|Darkvision|Vision dans le noir|libre|
|[rajOpAe3FLG1J8uc.htm](pathfinder-bestiary-2-items/rajOpAe3FLG1J8uc.htm)|Starknife|Lamétoile|libre|
|[rASGf5YkW2lQXMFQ.htm](pathfinder-bestiary-2-items/rASGf5YkW2lQXMFQ.htm)|Trample|Piétinement|libre|
|[rasNjBJHTsaP5LFC.htm](pathfinder-bestiary-2-items/rasNjBJHTsaP5LFC.htm)|Tail|Queue|libre|
|[rB0kJuC158tvKsmj.htm](pathfinder-bestiary-2-items/rB0kJuC158tvKsmj.htm)|Garbled Thoughts|Pensées confuses|officielle|
|[rBN7VRWeDcgG0c1C.htm](pathfinder-bestiary-2-items/rBN7VRWeDcgG0c1C.htm)|Darkvision|Vision dans le noir|libre|
|[rCDCEl5KfuYha1bo.htm](pathfinder-bestiary-2-items/rCDCEl5KfuYha1bo.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[rCDEvA0MWLJZe3ba.htm](pathfinder-bestiary-2-items/rCDEvA0MWLJZe3ba.htm)|At-Will Spells|Sorts à volonté|libre|
|[rCFV63sVu1lfxToK.htm](pathfinder-bestiary-2-items/rCFV63sVu1lfxToK.htm)|Negative Healing|Soins négatifs|libre|
|[rDl9ZNp0ry5Tek3B.htm](pathfinder-bestiary-2-items/rDl9ZNp0ry5Tek3B.htm)|Dagger|Dague|libre|
|[rdri6pl6hY1glOfy.htm](pathfinder-bestiary-2-items/rdri6pl6hY1glOfy.htm)|Cold Healing|Guérison glacée|officielle|
|[reBkTZBqwYPgsiFb.htm](pathfinder-bestiary-2-items/reBkTZBqwYPgsiFb.htm)|Tail|Queue|libre|
|[ReQtBIuf6WBTNwEH.htm](pathfinder-bestiary-2-items/ReQtBIuf6WBTNwEH.htm)|Thorn|Échardes|libre|
|[rET5SfCVJHTK7d5a.htm](pathfinder-bestiary-2-items/rET5SfCVJHTK7d5a.htm)|Athletics|Athlétisme|libre|
|[reVIS3aes83WAuF2.htm](pathfinder-bestiary-2-items/reVIS3aes83WAuF2.htm)|Deep Breath|Inspiration profonde|officielle|
|[RF4DrP2dezs9NRn8.htm](pathfinder-bestiary-2-items/RF4DrP2dezs9NRn8.htm)|At-Will Spells|Sorts à volonté|libre|
|[RFcZ8BNKEh3WWl6j.htm](pathfinder-bestiary-2-items/RFcZ8BNKEh3WWl6j.htm)|Whispering Wounds|Blessures murmurantes|officielle|
|[RFGL24YZ4ufhBCRM.htm](pathfinder-bestiary-2-items/RFGL24YZ4ufhBCRM.htm)|At-Will Spells|Sorts à volonté|libre|
|[RFPvW7U5w3NRyp3q.htm](pathfinder-bestiary-2-items/RFPvW7U5w3NRyp3q.htm)|Envisioning|Conceptualisation|officielle|
|[RGaUdIjwMpgnNYGg.htm](pathfinder-bestiary-2-items/RGaUdIjwMpgnNYGg.htm)|Pain Starvation|Douloureusement affamé|officielle|
|[rGG9CbH3CnQNbztb.htm](pathfinder-bestiary-2-items/rGG9CbH3CnQNbztb.htm)|Darkvision|Vision dans le noir|libre|
|[rGvREWDBZrWzdUhd.htm](pathfinder-bestiary-2-items/rGvREWDBZrWzdUhd.htm)|Beak|Bec|libre|
|[RHRtFPGTQDqJ0cER.htm](pathfinder-bestiary-2-items/RHRtFPGTQDqJ0cER.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[ri6MKWBy7EmYlC3n.htm](pathfinder-bestiary-2-items/ri6MKWBy7EmYlC3n.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[Rid4PCaJM2edDiKh.htm](pathfinder-bestiary-2-items/Rid4PCaJM2edDiKh.htm)|Forest Step|Passage par les forêts|officielle|
|[RjfMaoY3Fp7Ouab8.htm](pathfinder-bestiary-2-items/RjfMaoY3Fp7Ouab8.htm)|Tremorsense|Perception des vibrations|libre|
|[RJUajmwcmdSx0tQb.htm](pathfinder-bestiary-2-items/RJUajmwcmdSx0tQb.htm)|Mesmerizing Melody|Mélodie envoûtante|officielle|
|[rkayE4XQbhVQPYCS.htm](pathfinder-bestiary-2-items/rkayE4XQbhVQPYCS.htm)|Crossbow|Arbalète|libre|
|[RkzJDhpWzxqQXiAq.htm](pathfinder-bestiary-2-items/RkzJDhpWzxqQXiAq.htm)|Strafing Rush|Ruée agressive|officielle|
|[rl4i6PCiSzEqRVyf.htm](pathfinder-bestiary-2-items/rl4i6PCiSzEqRVyf.htm)|Claw|Griffe|libre|
|[rLT3Kld8E7p9hG1y.htm](pathfinder-bestiary-2-items/rLT3Kld8E7p9hG1y.htm)|Low-Light Vision|Vision nocturne|libre|
|[rMTD8eLYVnpfvpCd.htm](pathfinder-bestiary-2-items/rMTD8eLYVnpfvpCd.htm)|Wing|Aile|libre|
|[rN5DBFuJma8wPkT1.htm](pathfinder-bestiary-2-items/rN5DBFuJma8wPkT1.htm)|Grab|Empoignade/Agrippement|libre|
|[RNDT57aD8UzBIRVk.htm](pathfinder-bestiary-2-items/RNDT57aD8UzBIRVk.htm)|Pack Attack|Attaque en meute|officielle|
|[rnvfOLxvrsqRTH2s.htm](pathfinder-bestiary-2-items/rnvfOLxvrsqRTH2s.htm)|Darkvision|Vision dans le noir|libre|
|[roCv7KuzhfzP7hQD.htm](pathfinder-bestiary-2-items/roCv7KuzhfzP7hQD.htm)|Claw|Griffe|libre|
|[ROfUBzcM8hE66OL5.htm](pathfinder-bestiary-2-items/ROfUBzcM8hE66OL5.htm)|At-Will Spells|Sorts à volonté|libre|
|[ROXOsK91xVXF9I3t.htm](pathfinder-bestiary-2-items/ROXOsK91xVXF9I3t.htm)|Claw|Griffe|libre|
|[rOyG05jMYSlAcSS5.htm](pathfinder-bestiary-2-items/rOyG05jMYSlAcSS5.htm)|Swiftness|Célérité|officielle|
|[Rp0zPvS2PAgfYdfn.htm](pathfinder-bestiary-2-items/Rp0zPvS2PAgfYdfn.htm)|Fist|Poing|libre|
|[rp6F9XDfGE9qOef0.htm](pathfinder-bestiary-2-items/rp6F9XDfGE9qOef0.htm)|Darkvision|Vision dans le noir|libre|
|[rPjj6ZCE5pwwVZg1.htm](pathfinder-bestiary-2-items/rPjj6ZCE5pwwVZg1.htm)|Jaws|Mâchoires|libre|
|[rpp6hEDYHYJ2EJ15.htm](pathfinder-bestiary-2-items/rpp6hEDYHYJ2EJ15.htm)|Claw|Griffe|libre|
|[RrDmY99gWZJtFtjo.htm](pathfinder-bestiary-2-items/RrDmY99gWZJtFtjo.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[rRJmbyj1idfcUw82.htm](pathfinder-bestiary-2-items/rRJmbyj1idfcUw82.htm)|Grab|Empoignade/Agrippement|libre|
|[RRuGVF2WBzHvPAD4.htm](pathfinder-bestiary-2-items/RRuGVF2WBzHvPAD4.htm)|Vine|liane|libre|
|[rs8cxUHAMnuhP7nv.htm](pathfinder-bestiary-2-items/rs8cxUHAMnuhP7nv.htm)|Stealth|Discrétion|libre|
|[rsf1q6ooMNJFjdfh.htm](pathfinder-bestiary-2-items/rsf1q6ooMNJFjdfh.htm)|No Hearing|Audition nulle|officielle|
|[RSov7mEf6h3jgcfL.htm](pathfinder-bestiary-2-items/RSov7mEf6h3jgcfL.htm)|Draconic Momentum|Élan draconique|libre|
|[rSpxgtewFq1FNp4L.htm](pathfinder-bestiary-2-items/rSpxgtewFq1FNp4L.htm)|Retune|Syntonisation|officielle|
|[RSXc4NTQGKjlqt3X.htm](pathfinder-bestiary-2-items/RSXc4NTQGKjlqt3X.htm)|Darkvision|Vision dans le noir|libre|
|[RTbPNwtAI2pRCjBX.htm](pathfinder-bestiary-2-items/RTbPNwtAI2pRCjBX.htm)|Sling|Fronde|libre|
|[Ru2Q5RTGsxQBBWMZ.htm](pathfinder-bestiary-2-items/Ru2Q5RTGsxQBBWMZ.htm)|Planar Fast Healing|Guérison accélérée planaire|officielle|
|[RUcTjfOyyFNcxgIK.htm](pathfinder-bestiary-2-items/RUcTjfOyyFNcxgIK.htm)|Reactive Chomp|Morsure réactive|officielle|
|[RUPrm8gIOS2f852a.htm](pathfinder-bestiary-2-items/RUPrm8gIOS2f852a.htm)|Ferocity|Férocité|libre|
|[rUvfMcCzgvir0ElO.htm](pathfinder-bestiary-2-items/rUvfMcCzgvir0ElO.htm)|Jaws|Mâchoires|libre|
|[RvyDbBs5muX2yHdI.htm](pathfinder-bestiary-2-items/RvyDbBs5muX2yHdI.htm)|Stolen Death|Mort volée|officielle|
|[RwPr1Ik2tKeFrL0r.htm](pathfinder-bestiary-2-items/RwPr1Ik2tKeFrL0r.htm)|Swiftness|Célérité|officielle|
|[RwVJwyaaTesWbfgR.htm](pathfinder-bestiary-2-items/RwVJwyaaTesWbfgR.htm)|Manipulate Flames|Manipulation des flammes|officielle|
|[rwYZxmONVe2JmBPn.htm](pathfinder-bestiary-2-items/rwYZxmONVe2JmBPn.htm)|Darkvision|Vision dans le noir|libre|
|[RXUhJpHPbpDMUlbJ.htm](pathfinder-bestiary-2-items/RXUhJpHPbpDMUlbJ.htm)|Change Shape|Changement de forme|officielle|
|[s0ekhcyN4jpM6jLQ.htm](pathfinder-bestiary-2-items/s0ekhcyN4jpM6jLQ.htm)|Rock|Rocher|libre|
|[S0ETALXebRGPPFPv.htm](pathfinder-bestiary-2-items/S0ETALXebRGPPFPv.htm)|Low-Light Vision|Vision nocturne|libre|
|[s1CEAQqJM8T6nqR4.htm](pathfinder-bestiary-2-items/s1CEAQqJM8T6nqR4.htm)|Claw|Griffe|libre|
|[s23ZMm1sJc7XLyKW.htm](pathfinder-bestiary-2-items/s23ZMm1sJc7XLyKW.htm)|Seed Spray|Pulvérisation de graines|officielle|
|[S2jsW7P3mTUPKBHZ.htm](pathfinder-bestiary-2-items/S2jsW7P3mTUPKBHZ.htm)|Darkvision|Vision dans le noir|libre|
|[s2VxShdmP0XyPcUl.htm](pathfinder-bestiary-2-items/s2VxShdmP0XyPcUl.htm)|Tremorsense|Perception des vibrations|libre|
|[S31hZB9EKGtsEZ7B.htm](pathfinder-bestiary-2-items/S31hZB9EKGtsEZ7B.htm)|Orrery|Planétaire|officielle|
|[S3MoMWkoEovX2zha.htm](pathfinder-bestiary-2-items/S3MoMWkoEovX2zha.htm)|Fist|Poing|libre|
|[s43Ks62ECjo1B5kX.htm](pathfinder-bestiary-2-items/s43Ks62ECjo1B5kX.htm)|Claw|Griffe|libre|
|[s4Ce80I5ui9yCmbG.htm](pathfinder-bestiary-2-items/s4Ce80I5ui9yCmbG.htm)|Grab|Empoignade/Agrippement|libre|
|[s4eMY3meCcZpGDrL.htm](pathfinder-bestiary-2-items/s4eMY3meCcZpGDrL.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[S6Avv1gbTIidozSY.htm](pathfinder-bestiary-2-items/S6Avv1gbTIidozSY.htm)|Low-Light Vision|Vision nocturne|libre|
|[s6qHGkntlKnGpY6s.htm](pathfinder-bestiary-2-items/s6qHGkntlKnGpY6s.htm)|Telepathy|Télépathie|libre|
|[s7GGq81IzCx73D2B.htm](pathfinder-bestiary-2-items/s7GGq81IzCx73D2B.htm)|Call to Blood|Appel du sang|officielle|
|[S7QInhdroOaf7k84.htm](pathfinder-bestiary-2-items/S7QInhdroOaf7k84.htm)|Tremorsense|Perception des vibrations|libre|
|[s8TihgQDAypdPOee.htm](pathfinder-bestiary-2-items/s8TihgQDAypdPOee.htm)|Darkvision|Vision dans le noir|libre|
|[s9Def6M7z5ceOIoj.htm](pathfinder-bestiary-2-items/s9Def6M7z5ceOIoj.htm)|Pounce|Bond|officielle|
|[s9PF3C48raLmLaNG.htm](pathfinder-bestiary-2-items/s9PF3C48raLmLaNG.htm)|Scent|Odorat|libre|
|[sA3oeWc14avc7CPO.htm](pathfinder-bestiary-2-items/sA3oeWc14avc7CPO.htm)|Wrath of Fate|Fureur du destin|officielle|
|[SaUKY4rTuI0N39ad.htm](pathfinder-bestiary-2-items/SaUKY4rTuI0N39ad.htm)|Scent|Odorat|libre|
|[SaVJfclmAsjFCBke.htm](pathfinder-bestiary-2-items/SaVJfclmAsjFCBke.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[SB54Z5VO15GfJd0X.htm](pathfinder-bestiary-2-items/SB54Z5VO15GfJd0X.htm)|Rend|Éventration|libre|
|[sBfOfFAVZjz8e2Sk.htm](pathfinder-bestiary-2-items/sBfOfFAVZjz8e2Sk.htm)|Grab|Empoignade/Agrippement|libre|
|[sbPAeP8uZ3oiRj7q.htm](pathfinder-bestiary-2-items/sbPAeP8uZ3oiRj7q.htm)|Tangling Chains|Enchevêtrement de chaînes|officielle|
|[SDF2zWv6zGkmcQH9.htm](pathfinder-bestiary-2-items/SDF2zWv6zGkmcQH9.htm)|Bite|Morsure|officielle|
|[sDghWo7WoJwJ3FPJ.htm](pathfinder-bestiary-2-items/sDghWo7WoJwJ3FPJ.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[sdQC9PYloWzpJd9P.htm](pathfinder-bestiary-2-items/sdQC9PYloWzpJd9P.htm)|Grab|Empoignade/Agrippement|libre|
|[Sei3FrQPmhMHMkTo.htm](pathfinder-bestiary-2-items/Sei3FrQPmhMHMkTo.htm)|Scent|Odorat|libre|
|[sfWdhP3dRBdtmiol.htm](pathfinder-bestiary-2-items/sfWdhP3dRBdtmiol.htm)|Blood Drain|Absorption de sang|officielle|
|[sGGppAnKyKB4kipq.htm](pathfinder-bestiary-2-items/sGGppAnKyKB4kipq.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[sguXTwhSIa1swJ1p.htm](pathfinder-bestiary-2-items/sguXTwhSIa1swJ1p.htm)|Tongue Grab|Agrippement avec la langue|officielle|
|[SHBwJIixLi2Gr8Lx.htm](pathfinder-bestiary-2-items/SHBwJIixLi2Gr8Lx.htm)|Savage Assault|Assaut féroce|officielle|
|[SIJWWzmQ2ZLmATdy.htm](pathfinder-bestiary-2-items/SIJWWzmQ2ZLmATdy.htm)|Claw|Griffe|libre|
|[sIL9AEoQV1qcrskK.htm](pathfinder-bestiary-2-items/sIL9AEoQV1qcrskK.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[sjk7gsZB7YAEVqjS.htm](pathfinder-bestiary-2-items/sjk7gsZB7YAEVqjS.htm)|Focus Gaze|Focaliser le regard|officielle|
|[sJmlL0pxIhzEUZxO.htm](pathfinder-bestiary-2-items/sJmlL0pxIhzEUZxO.htm)|Trample|Piétinement|libre|
|[sLqxJoKDLd1T2p2i.htm](pathfinder-bestiary-2-items/sLqxJoKDLd1T2p2i.htm)|Solid Blow|Coup puissant|officielle|
|[SLu0NlKYzhM3DVPZ.htm](pathfinder-bestiary-2-items/SLu0NlKYzhM3DVPZ.htm)|Aura of Doom|Aura de condamnation|officielle|
|[SLw6k2GamdDXVK6d.htm](pathfinder-bestiary-2-items/SLw6k2GamdDXVK6d.htm)|Scent|Odorat|libre|
|[sMafmLEOWxv5RH1h.htm](pathfinder-bestiary-2-items/sMafmLEOWxv5RH1h.htm)|Jaws|Mâchoires|libre|
|[sMNeAt05PFEaDVIK.htm](pathfinder-bestiary-2-items/sMNeAt05PFEaDVIK.htm)|Entropy Sense|Perception de l'entropie (imprécis) 9 m|officielle|
|[sMQewzNATahHc65F.htm](pathfinder-bestiary-2-items/sMQewzNATahHc65F.htm)|Death Implosion|Implosion finale|officielle|
|[Smr9bqHYQl1EI48C.htm](pathfinder-bestiary-2-items/Smr9bqHYQl1EI48C.htm)|Dragon Heat|Chaleur du dragon|officielle|
|[snHGvNo3Y3eluuUN.htm](pathfinder-bestiary-2-items/snHGvNo3Y3eluuUN.htm)|Compel Courage|Insuffler du courage|officielle|
|[snsO2nPGYHe6pZx0.htm](pathfinder-bestiary-2-items/snsO2nPGYHe6pZx0.htm)|Create Web Weaponry|Créer un arsenal de toile|officielle|
|[SNtv0J2CRa2RFhi9.htm](pathfinder-bestiary-2-items/SNtv0J2CRa2RFhi9.htm)|Entropic Touch|Toucher entropique|officielle|
|[so21bQRtejWVxF5S.htm](pathfinder-bestiary-2-items/so21bQRtejWVxF5S.htm)|Twisting Tail|Queue sinueuse|officielle|
|[So7Gr127G3zbRJAt.htm](pathfinder-bestiary-2-items/So7Gr127G3zbRJAt.htm)|Fist|Poing|libre|
|[SOgonEKuJHWkgeNc.htm](pathfinder-bestiary-2-items/SOgonEKuJHWkgeNc.htm)|Darkvision|Vision dans le noir|libre|
|[SpQLyhSHuzpd81GZ.htm](pathfinder-bestiary-2-items/SpQLyhSHuzpd81GZ.htm)|Jaws|Mâchoires|libre|
|[SqBptcrkfElc5p4M.htm](pathfinder-bestiary-2-items/SqBptcrkfElc5p4M.htm)|At-Will Spells|Sorts à volonté|libre|
|[SQM0OqkayL2Oywmm.htm](pathfinder-bestiary-2-items/SQM0OqkayL2Oywmm.htm)|Tail|Queue|libre|
|[SqmhCTwOKtI5c7BT.htm](pathfinder-bestiary-2-items/SqmhCTwOKtI5c7BT.htm)|Mote of Light|Particule de lumière|libre|
|[sqrdrxJ0d19wPysW.htm](pathfinder-bestiary-2-items/sqrdrxJ0d19wPysW.htm)|Catch Rock|Interception de rochers|libre|
|[sR5alX5hZopxwTLw.htm](pathfinder-bestiary-2-items/sR5alX5hZopxwTLw.htm)|Web Bola|Bola de toile|officielle|
|[sRJgd3DpiEABxebP.htm](pathfinder-bestiary-2-items/sRJgd3DpiEABxebP.htm)|Deflecting Gale|Bourrasque déviatrice|officielle|
|[SRNjrRDQg8BEgVDx.htm](pathfinder-bestiary-2-items/SRNjrRDQg8BEgVDx.htm)|Ferocity|Férocité|libre|
|[sSAVmevencpx75UB.htm](pathfinder-bestiary-2-items/sSAVmevencpx75UB.htm)|Pestilential Aura|Aura pestilentielle|officielle|
|[sSkLpLlTDlkKIKAw.htm](pathfinder-bestiary-2-items/sSkLpLlTDlkKIKAw.htm)|Planar Acclimation|Acclimatation planaire|officielle|
|[st5T6nXfjzQXbUOB.htm](pathfinder-bestiary-2-items/st5T6nXfjzQXbUOB.htm)|Lancing Charge|Charge à la lance|officielle|
|[stgGnScdknvWx6D2.htm](pathfinder-bestiary-2-items/stgGnScdknvWx6D2.htm)|Regeneration 15 (deactivated by acid or fire)|Régénération 15 (désactivée par l'acide ou le feu)|libre|
|[stKoZGNXuFFgUzxE.htm](pathfinder-bestiary-2-items/stKoZGNXuFFgUzxE.htm)|Fist|Poing|libre|
|[storTBRDZBrko2mc.htm](pathfinder-bestiary-2-items/storTBRDZBrko2mc.htm)|Swarm Mind|Esprit de nuée|libre|
|[stSp4FJbtoFzokE9.htm](pathfinder-bestiary-2-items/stSp4FJbtoFzokE9.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Stt8YZwfNCRzNgkQ.htm](pathfinder-bestiary-2-items/Stt8YZwfNCRzNgkQ.htm)|Powerful Charge|Charge puissante|officielle|
|[Su8xRpdikP4lRabN.htm](pathfinder-bestiary-2-items/Su8xRpdikP4lRabN.htm)|Fist|Poing|libre|
|[SU9JTCOQpNZUjegs.htm](pathfinder-bestiary-2-items/SU9JTCOQpNZUjegs.htm)|Darkvision|Vision dans le noir|libre|
|[sUall0U5UbL2KkEE.htm](pathfinder-bestiary-2-items/sUall0U5UbL2KkEE.htm)|Reef Octopus Venom|Venin de pieuvre de récif|officielle|
|[SuO1iTdYnYyeXOGq.htm](pathfinder-bestiary-2-items/SuO1iTdYnYyeXOGq.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[sUR4J0lzNGl0KfsK.htm](pathfinder-bestiary-2-items/sUR4J0lzNGl0KfsK.htm)|Tusk|Défense|libre|
|[SvghWJwmAo36V4Sx.htm](pathfinder-bestiary-2-items/SvghWJwmAo36V4Sx.htm)|Grab|Empoignade/Agrippement|libre|
|[sw8oj6ADIgUM0I7C.htm](pathfinder-bestiary-2-items/sw8oj6ADIgUM0I7C.htm)|Arm|Bras|libre|
|[sW9lqWGbyXr7xeXh.htm](pathfinder-bestiary-2-items/sW9lqWGbyXr7xeXh.htm)|Swallow Whole|Gober|libre|
|[swAJMaqujVUFqFKT.htm](pathfinder-bestiary-2-items/swAJMaqujVUFqFKT.htm)|Host Scent|Perception de l'hôte|officielle|
|[sWbaqjXgO4EigI5j.htm](pathfinder-bestiary-2-items/sWbaqjXgO4EigI5j.htm)|Lay Web Trap|Tendre un piège de toile|officielle|
|[swPjSgGgOFitRVM8.htm](pathfinder-bestiary-2-items/swPjSgGgOFitRVM8.htm)|Low-Light Vision|Vision nocturne|libre|
|[swUZOpPbIdXVMQ1X.htm](pathfinder-bestiary-2-items/swUZOpPbIdXVMQ1X.htm)|Horn|Corne|libre|
|[SXISaBxf4jsnV512.htm](pathfinder-bestiary-2-items/SXISaBxf4jsnV512.htm)|Breath Weapon|Arme de souffle|libre|
|[sy1d3enT3AxVwNJ9.htm](pathfinder-bestiary-2-items/sy1d3enT3AxVwNJ9.htm)|Telepathy|Télépathie|libre|
|[sy7gZgu0yKjVGQB8.htm](pathfinder-bestiary-2-items/sy7gZgu0yKjVGQB8.htm)|Ferocity|Férocité|libre|
|[sYzEwjtXD4GsP03b.htm](pathfinder-bestiary-2-items/sYzEwjtXD4GsP03b.htm)|Hibernation|Hibernation|officielle|
|[SZrUoVGK2e5HE1dm.htm](pathfinder-bestiary-2-items/SZrUoVGK2e5HE1dm.htm)|Brood Leech Swarm Venom|Venin de la nuée de sangsues d'élevage0|officielle|
|[T0KOWsF7QwJ1oDQq.htm](pathfinder-bestiary-2-items/T0KOWsF7QwJ1oDQq.htm)|Low-Light Vision|Vision nocturne|libre|
|[t0On0v7gyVoScNyY.htm](pathfinder-bestiary-2-items/t0On0v7gyVoScNyY.htm)|Staggering Sail|Voile déstabilisante|officielle|
|[T0vnmUhrqmtMKLQD.htm](pathfinder-bestiary-2-items/T0vnmUhrqmtMKLQD.htm)|Hostile Duet|Duo hostile|officielle|
|[T30LeVWTmp9xhLg7.htm](pathfinder-bestiary-2-items/T30LeVWTmp9xhLg7.htm)|Darkvision|Vision dans le noir|libre|
|[t3cnDaUX26zIroa8.htm](pathfinder-bestiary-2-items/t3cnDaUX26zIroa8.htm)|Focus Gaze|Focaliser le regard|officielle|
|[T4cC9H9vxNTUqnco.htm](pathfinder-bestiary-2-items/T4cC9H9vxNTUqnco.htm)|Deflecting Cloud|Nuage déflecteur|officielle|
|[t4DBIA1EMPiRHvkY.htm](pathfinder-bestiary-2-items/t4DBIA1EMPiRHvkY.htm)|Grab|Empoignade/Agrippement|libre|
|[T4TOIiPyHfghQy04.htm](pathfinder-bestiary-2-items/T4TOIiPyHfghQy04.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[T5aqHU7uB2IvWwrg.htm](pathfinder-bestiary-2-items/T5aqHU7uB2IvWwrg.htm)|Surgical Rend|Éventration chirurgicale|officielle|
|[T7L2FMHmpDoURw0s.htm](pathfinder-bestiary-2-items/T7L2FMHmpDoURw0s.htm)|Clinging Suckers|Ventouses de fixation|officielle|
|[T7rTM38nfnCsn85I.htm](pathfinder-bestiary-2-items/T7rTM38nfnCsn85I.htm)|Jaws|Mâchoires|libre|
|[T7tMkEUVA0Pq90Yo.htm](pathfinder-bestiary-2-items/T7tMkEUVA0Pq90Yo.htm)|At-Will Spells|Sorts à volonté|libre|
|[t8m638wy4P8pa4PZ.htm](pathfinder-bestiary-2-items/t8m638wy4P8pa4PZ.htm)|Jaws|Mâchoires|libre|
|[t8ToZMzymYUk47az.htm](pathfinder-bestiary-2-items/t8ToZMzymYUk47az.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[t9ReCb126y5KXuru.htm](pathfinder-bestiary-2-items/t9ReCb126y5KXuru.htm)|Opportune Witchflame|Flammes ensorcelées opportunistes|officielle|
|[TA0G6wsaR62bthsu.htm](pathfinder-bestiary-2-items/TA0G6wsaR62bthsu.htm)|Constant Spells|Sorts constants|libre|
|[tAdSWqK8N7bxwjab.htm](pathfinder-bestiary-2-items/tAdSWqK8N7bxwjab.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[taEoZD9fGbhrr6G9.htm](pathfinder-bestiary-2-items/taEoZD9fGbhrr6G9.htm)|Darkvision|Vision dans le noir|libre|
|[TAIcUgt3JKg5c7fV.htm](pathfinder-bestiary-2-items/TAIcUgt3JKg5c7fV.htm)|Scarecrow's Leer|Regard de l'épouvantail|officielle|
|[TBp1tYUPtYoOEonO.htm](pathfinder-bestiary-2-items/TBp1tYUPtYoOEonO.htm)|Aquatic Echolocation|Écholocalisation aquatique|officielle|
|[TBsuxObKIaeVagO5.htm](pathfinder-bestiary-2-items/TBsuxObKIaeVagO5.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[tByLKFHCnMANfCJV.htm](pathfinder-bestiary-2-items/tByLKFHCnMANfCJV.htm)|Fist|Poing|libre|
|[tcE3BBuNT8JtsDJQ.htm](pathfinder-bestiary-2-items/tcE3BBuNT8JtsDJQ.htm)|Low-Light Vision|Vision nocturne|libre|
|[tdnc6MMJXidby82v.htm](pathfinder-bestiary-2-items/tdnc6MMJXidby82v.htm)|Darkvision|Vision dans le noir|libre|
|[TdOAA9HgzD5psJwW.htm](pathfinder-bestiary-2-items/TdOAA9HgzD5psJwW.htm)|Pull Filament|Filament tractif|libre|
|[teonM0iI5aOvey0N.htm](pathfinder-bestiary-2-items/teonM0iI5aOvey0N.htm)|Fast Healing 1|Guérison accélérée 1|libre|
|[tF3KNEXj3uSK3cil.htm](pathfinder-bestiary-2-items/tF3KNEXj3uSK3cil.htm)|Throw Rock|Lancer de rocher|libre|
|[TF4ZUPn2jeFMb9Fz.htm](pathfinder-bestiary-2-items/TF4ZUPn2jeFMb9Fz.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[TfHuj8LvtfwsXfXQ.htm](pathfinder-bestiary-2-items/TfHuj8LvtfwsXfXQ.htm)|Tentacle Arm|Bras tentaculaire|libre|
|[TfK9D77zOoU9jjxj.htm](pathfinder-bestiary-2-items/TfK9D77zOoU9jjxj.htm)|Grab|Empoignade/Agrippement|libre|
|[tFkVmhokDW1zCny4.htm](pathfinder-bestiary-2-items/tFkVmhokDW1zCny4.htm)|Wing|Aile|libre|
|[tG4nT3YD8KPOsT5n.htm](pathfinder-bestiary-2-items/tG4nT3YD8KPOsT5n.htm)|Grab|Empoignade/Agrippement|libre|
|[tH3f4GsDerHRNPik.htm](pathfinder-bestiary-2-items/tH3f4GsDerHRNPik.htm)|Change Shape|Changement de forme|officielle|
|[tIiXnBCscCuQi4WY.htm](pathfinder-bestiary-2-items/tIiXnBCscCuQi4WY.htm)|Favored Host|Hôte de prédilection|officielle|
|[tikGFRXVhGNo3ahM.htm](pathfinder-bestiary-2-items/tikGFRXVhGNo3ahM.htm)|Hammer|Marteau|libre|
|[tIQTZDimC6ewCWZ7.htm](pathfinder-bestiary-2-items/tIQTZDimC6ewCWZ7.htm)|Jaws|Mâchoires|libre|
|[tisbaQ4bvix6pdXR.htm](pathfinder-bestiary-2-items/tisbaQ4bvix6pdXR.htm)|Tail|Queue|libre|
|[tIvtO7TbZgSNlr3F.htm](pathfinder-bestiary-2-items/tIvtO7TbZgSNlr3F.htm)|Constant Spells|Sorts constants|libre|
|[tjcUq4Pdk5hq6KqX.htm](pathfinder-bestiary-2-items/tjcUq4Pdk5hq6KqX.htm)|Boneshatter|Éclats d’os|officielle|
|[tJgewoTin1AzHV0a.htm](pathfinder-bestiary-2-items/tJgewoTin1AzHV0a.htm)|Stealth|Discrétion|libre|
|[tjNFJu8XOANHpzYB.htm](pathfinder-bestiary-2-items/tjNFJu8XOANHpzYB.htm)|At-Will Spells|Sorts à volonté|libre|
|[tjoCXlFjIMoeCEAO.htm](pathfinder-bestiary-2-items/tjoCXlFjIMoeCEAO.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[tjQCRx8UN4LQPnwJ.htm](pathfinder-bestiary-2-items/tjQCRx8UN4LQPnwJ.htm)|Witchflame Kindling|Flammes ensorcelées incandescentes|officielle|
|[tjv4Luss5zUtzkrn.htm](pathfinder-bestiary-2-items/tjv4Luss5zUtzkrn.htm)|Darkvision|Vision dans le noir|libre|
|[Tjvf8RD2s6FEQelo.htm](pathfinder-bestiary-2-items/Tjvf8RD2s6FEQelo.htm)|Rock|Rocher|libre|
|[tKbplFnGrHjbOkKF.htm](pathfinder-bestiary-2-items/tKbplFnGrHjbOkKF.htm)|Darkvision|Vision dans le noir|libre|
|[TKjiZlqXKYAz5MTX.htm](pathfinder-bestiary-2-items/TKjiZlqXKYAz5MTX.htm)|All-Around Vision|Vision à 360°|libre|
|[tkqzuUsreLrNt0sh.htm](pathfinder-bestiary-2-items/tkqzuUsreLrNt0sh.htm)|Aquatic Echolocation|Écholocalisation aquatique|officielle|
|[TlRvAbAXlF8M2LXl.htm](pathfinder-bestiary-2-items/TlRvAbAXlF8M2LXl.htm)|Jaws|Mâchoires|libre|
|[TM2S9CZ5bTyxKkXp.htm](pathfinder-bestiary-2-items/TM2S9CZ5bTyxKkXp.htm)|Unnerving Gaze|Regard déstabilisant|officielle|
|[Tml8KbX9Hdipnofj.htm](pathfinder-bestiary-2-items/Tml8KbX9Hdipnofj.htm)|Stealth|Discrétion|libre|
|[TnT0MH1u6Ixly1UZ.htm](pathfinder-bestiary-2-items/TnT0MH1u6Ixly1UZ.htm)|Pseudopod|Pseudopode|libre|
|[tnyUum6cGLUvry3c.htm](pathfinder-bestiary-2-items/tnyUum6cGLUvry3c.htm)|Jaws|Mâchoires|libre|
|[tO053EeFkQRNiOBR.htm](pathfinder-bestiary-2-items/tO053EeFkQRNiOBR.htm)|Draconic Momentum|Élan draconique|libre|
|[tO90cMW3Jx22uW0F.htm](pathfinder-bestiary-2-items/tO90cMW3Jx22uW0F.htm)|Arm|Bras|libre|
|[tOMw33x9d2qXTKwi.htm](pathfinder-bestiary-2-items/tOMw33x9d2qXTKwi.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[tONGWAraS1nSA49Y.htm](pathfinder-bestiary-2-items/tONGWAraS1nSA49Y.htm)|Wicked Bite|Méchante morsure|officielle|
|[TOzSzGQn4MrKg3wy.htm](pathfinder-bestiary-2-items/TOzSzGQn4MrKg3wy.htm)|Igniting Assault|Assaut brûlant|officielle|
|[tPa52hjkpaRccFn3.htm](pathfinder-bestiary-2-items/tPa52hjkpaRccFn3.htm)|Tentacle|Tentacule|libre|
|[TpfBgK9H4iQNfMgS.htm](pathfinder-bestiary-2-items/TpfBgK9H4iQNfMgS.htm)|Sting|Aiguillon|libre|
|[TPKGkvgCAQz8TKeC.htm](pathfinder-bestiary-2-items/TPKGkvgCAQz8TKeC.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[tpQDVH3NwAokh2NC.htm](pathfinder-bestiary-2-items/tpQDVH3NwAokh2NC.htm)|Breath Weapon|Arme de souffle|officielle|
|[tRLWCHj0DJ8Jb78B.htm](pathfinder-bestiary-2-items/tRLWCHj0DJ8Jb78B.htm)|Focus Gaze|Focaliser le regard|officielle|
|[tssacopT88cz2z8v.htm](pathfinder-bestiary-2-items/tssacopT88cz2z8v.htm)|Tail|Queue|libre|
|[tTPvWYYvp1PgybyK.htm](pathfinder-bestiary-2-items/tTPvWYYvp1PgybyK.htm)|Swift Tracker|Pistage accéléré|officielle|
|[TtqhvUO0vvhjbIFp.htm](pathfinder-bestiary-2-items/TtqhvUO0vvhjbIFp.htm)|Exorcism Vulnerability|Vulnérabilité à l’exorcisme|officielle|
|[TUaSPYJP2yuCkKh9.htm](pathfinder-bestiary-2-items/TUaSPYJP2yuCkKh9.htm)|Wing Deflection|Déflection de l'aile|officielle|
|[tuQ8FrXL4iChAQA1.htm](pathfinder-bestiary-2-items/tuQ8FrXL4iChAQA1.htm)|Sunlight Petrification|Pétrification solaire|officielle|
|[tVBujCEfyKPKjA2Q.htm](pathfinder-bestiary-2-items/tVBujCEfyKPKjA2Q.htm)|Flailing Tentacles|Frénésie de tentacules|officielle|
|[TvDY3FqPJsHE5Bz7.htm](pathfinder-bestiary-2-items/TvDY3FqPJsHE5Bz7.htm)|Stinger|Dard|libre|
|[TVOxysBVf9C1u5wF.htm](pathfinder-bestiary-2-items/TVOxysBVf9C1u5wF.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[TWEbrceKTc0P1GrD.htm](pathfinder-bestiary-2-items/TWEbrceKTc0P1GrD.htm)|Ember Ball|Boule de braise|libre|
|[twuwqjXSZmaqi54S.htm](pathfinder-bestiary-2-items/twuwqjXSZmaqi54S.htm)|Lifesense 30 feet|Perception de la vie 9 m|libre|
|[TxgMLxy2ap1ZIMWz.htm](pathfinder-bestiary-2-items/TxgMLxy2ap1ZIMWz.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[TxRwH6kafpwqJtmQ.htm](pathfinder-bestiary-2-items/TxRwH6kafpwqJtmQ.htm)|Truespeech|Language universel|officielle|
|[tygj3hMG88FeH6W1.htm](pathfinder-bestiary-2-items/tygj3hMG88FeH6W1.htm)|Draconic Resistance|Résistance draconique|libre|
|[tZ28zS8D4Ibrkmjl.htm](pathfinder-bestiary-2-items/tZ28zS8D4Ibrkmjl.htm)|Thunderbolt|Foudre|libre|
|[TZC0DNY97EGRDFbD.htm](pathfinder-bestiary-2-items/TZC0DNY97EGRDFbD.htm)|Horns|Cornes|libre|
|[tZhV19HIQBeAjdki.htm](pathfinder-bestiary-2-items/tZhV19HIQBeAjdki.htm)|Jaws|Mâchoires|libre|
|[TzQpE2J6oD8ijpuC.htm](pathfinder-bestiary-2-items/TzQpE2J6oD8ijpuC.htm)|Splinter|Écharde|libre|
|[u12odfdEyxk5GPY5.htm](pathfinder-bestiary-2-items/u12odfdEyxk5GPY5.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[u1o4MqU6HFWR8rVh.htm](pathfinder-bestiary-2-items/u1o4MqU6HFWR8rVh.htm)|Electrolocation|Électrolocalisation|officielle|
|[U1rCcNDpkbvZuuCb.htm](pathfinder-bestiary-2-items/U1rCcNDpkbvZuuCb.htm)|Darkvision|Vision dans le noir|libre|
|[U291oOsn8aI5u9Za.htm](pathfinder-bestiary-2-items/U291oOsn8aI5u9Za.htm)|Darkvision|Vision dans le noir|libre|
|[u3s5y4pE6L6HfEAq.htm](pathfinder-bestiary-2-items/u3s5y4pE6L6HfEAq.htm)|Malleability|Malléable|officielle|
|[u4BcgBmwi4zY27Ei.htm](pathfinder-bestiary-2-items/u4BcgBmwi4zY27Ei.htm)|Clacking Exoskeleton|Exosquelette cliquetant|officielle|
|[U4uu4gkB12yKgjWl.htm](pathfinder-bestiary-2-items/U4uu4gkB12yKgjWl.htm)|Jaws|Mâchoires|libre|
|[u58XzFLTCWAEQCJN.htm](pathfinder-bestiary-2-items/u58XzFLTCWAEQCJN.htm)|Writhing Arms|Membres grouillants|officielle|
|[u5ajL2rEGMJeJmYt.htm](pathfinder-bestiary-2-items/u5ajL2rEGMJeJmYt.htm)|Telepathy|Télépathie|libre|
|[U64zKLjgQG2MzhAS.htm](pathfinder-bestiary-2-items/U64zKLjgQG2MzhAS.htm)|Wind's Guidance|Soutien du vent|officielle|
|[u6jbIfe9u8XJ6Pe0.htm](pathfinder-bestiary-2-items/u6jbIfe9u8XJ6Pe0.htm)|Claw|Griffe|libre|
|[U6lHO1793L8PN4Hc.htm](pathfinder-bestiary-2-items/U6lHO1793L8PN4Hc.htm)|Grab|Empoignade/Agrippement|libre|
|[U6mlSBSkzcgtivnv.htm](pathfinder-bestiary-2-items/U6mlSBSkzcgtivnv.htm)|Scent|Odorat|libre|
|[u6T8tsZkOcFjvrzG.htm](pathfinder-bestiary-2-items/u6T8tsZkOcFjvrzG.htm)|Negative Healing|Guérison négative|libre|
|[u6tuxFGclP3Cf3u2.htm](pathfinder-bestiary-2-items/u6tuxFGclP3Cf3u2.htm)|Otherworldly Mind|Esprit d’un autre monde|officielle|
|[u7R9PDTQDTEJeQAX.htm](pathfinder-bestiary-2-items/u7R9PDTQDTEJeQAX.htm)|Keen Hearing|Ouïe fine|officielle|
|[U7WoIw4znbFwFj7t.htm](pathfinder-bestiary-2-items/U7WoIw4znbFwFj7t.htm)|Scent|Odorat|libre|
|[UaG0xt5nSwesLzma.htm](pathfinder-bestiary-2-items/UaG0xt5nSwesLzma.htm)|Stealth|Discrétion|libre|
|[uAKDE9WbARcraQJx.htm](pathfinder-bestiary-2-items/uAKDE9WbARcraQJx.htm)|Clobbering Charge|Charge percutante|officielle|
|[UatAhxaZk9Zo9da0.htm](pathfinder-bestiary-2-items/UatAhxaZk9Zo9da0.htm)|Coven|Cercle|officielle|
|[UatC9BxVJopPgwJd.htm](pathfinder-bestiary-2-items/UatC9BxVJopPgwJd.htm)|Rotting Curse|Malédiction de pourriture|officielle|
|[UbbiLTnNP4WGrar5.htm](pathfinder-bestiary-2-items/UbbiLTnNP4WGrar5.htm)|Mold Mulch|Paillage moisi|officielle|
|[ubHtu9kCddCNuEjt.htm](pathfinder-bestiary-2-items/ubHtu9kCddCNuEjt.htm)|Low-Light Vision|Vision nocturne|libre|
|[ubj0ypG0da0nRirb.htm](pathfinder-bestiary-2-items/ubj0ypG0da0nRirb.htm)|Fade into the Light|Disparition en pleine lumière|officielle|
|[ubttzSAG6qyejH25.htm](pathfinder-bestiary-2-items/ubttzSAG6qyejH25.htm)|Grab|Empoignade/Agrippement|libre|
|[UcQu3REAJGDNFfE0.htm](pathfinder-bestiary-2-items/UcQu3REAJGDNFfE0.htm)|Captivating Pollen|Pollen fascinant|officielle|
|[UcRrxW2Ay0vhDBrn.htm](pathfinder-bestiary-2-items/UcRrxW2Ay0vhDBrn.htm)|Darkvision|Vision dans le noir|libre|
|[uD7Sv3Sf3fTDECXG.htm](pathfinder-bestiary-2-items/uD7Sv3Sf3fTDECXG.htm)|Darkvision|Vision dans le noir|libre|
|[Udn39gtetBxEcwJn.htm](pathfinder-bestiary-2-items/Udn39gtetBxEcwJn.htm)|Telepathy|Télépathie|libre|
|[uDrkrfwjoPByW5Zs.htm](pathfinder-bestiary-2-items/uDrkrfwjoPByW5Zs.htm)|Warpwaves|Vagues de distorsion|officielle|
|[uDxnASCZGKnXfoQw.htm](pathfinder-bestiary-2-items/uDxnASCZGKnXfoQw.htm)|Trample|Piétinement|officielle|
|[UE4WlNSLrotRauZG.htm](pathfinder-bestiary-2-items/UE4WlNSLrotRauZG.htm)|Lightning-Struck Curse|Malédiction de foudre|officielle|
|[uGdTa0kjSsx6O3yO.htm](pathfinder-bestiary-2-items/uGdTa0kjSsx6O3yO.htm)|Scent (Imprecise) 60 feet|Odorat (imprécis) 18 m|libre|
|[ugIVlLPaqyvekXgN.htm](pathfinder-bestiary-2-items/ugIVlLPaqyvekXgN.htm)|Darkvision|Vision dans le noir|libre|
|[UGoT9Ax8vT4G0yrI.htm](pathfinder-bestiary-2-items/UGoT9Ax8vT4G0yrI.htm)|Dagger|Dague|libre|
|[UgqU2m5UQKRKewoU.htm](pathfinder-bestiary-2-items/UgqU2m5UQKRKewoU.htm)|Osyluth Venom|Venin d’osyluth|officielle|
|[UHFNjGmcb7jCzvBE.htm](pathfinder-bestiary-2-items/UHFNjGmcb7jCzvBE.htm)|Attach|Fixation|officielle|
|[UhnqMxX22JlPHncP.htm](pathfinder-bestiary-2-items/UhnqMxX22JlPHncP.htm)|Tentacle|Tentacule|libre|
|[UIgdlwmkkFjjq6Kl.htm](pathfinder-bestiary-2-items/UIgdlwmkkFjjq6Kl.htm)|Unsettled Aura|Aura de perturbations|officielle|
|[Ujc2l5skhi8bH1Ue.htm](pathfinder-bestiary-2-items/Ujc2l5skhi8bH1Ue.htm)|Holy Armaments|Armement Saint|officielle|
|[ujOn2cOoDTqdrh70.htm](pathfinder-bestiary-2-items/ujOn2cOoDTqdrh70.htm)|Claw|Griffe|libre|
|[UjQN53iNiipJJkHh.htm](pathfinder-bestiary-2-items/UjQN53iNiipJJkHh.htm)|Branch|Branche|libre|
|[uJuFK46gaxSPryGB.htm](pathfinder-bestiary-2-items/uJuFK46gaxSPryGB.htm)|Brine Spit|Crachat d'eau salée|officielle|
|[UK2ayZO6tmbwgHBL.htm](pathfinder-bestiary-2-items/UK2ayZO6tmbwgHBL.htm)|Jaws|Mâchoires|libre|
|[uKp0N4KDIGVFsJ1T.htm](pathfinder-bestiary-2-items/uKp0N4KDIGVFsJ1T.htm)|Darkvision|Vision dans le noir|libre|
|[ulrrbkt4l6SBi1z8.htm](pathfinder-bestiary-2-items/ulrrbkt4l6SBi1z8.htm)|Painsight|Vision de la douleur|officielle|
|[uLVKT1uotgIuFDA2.htm](pathfinder-bestiary-2-items/uLVKT1uotgIuFDA2.htm)|At-Will Spells|Sorts à volonté|libre|
|[um06dCu0E8W9LRDd.htm](pathfinder-bestiary-2-items/um06dCu0E8W9LRDd.htm)|Entropy Sense (Imprecise) 30 feet|Perception de l'entropie (imprécis) 9 m|officielle|
|[umE4aR6ccDxLqz3l.htm](pathfinder-bestiary-2-items/umE4aR6ccDxLqz3l.htm)|Hand Crossbow (Hunting Spider Venom)|arbalète de poing (Venin d'araignée chasseresse)|officielle|
|[UmF9ATrgifYyt9He.htm](pathfinder-bestiary-2-items/UmF9ATrgifYyt9He.htm)|Pounce|Bond|officielle|
|[uMPmwIYK4s395qvB.htm](pathfinder-bestiary-2-items/uMPmwIYK4s395qvB.htm)|Darkvision|Vision dans le noir|libre|
|[uMr0g5ycTJTsxsHh.htm](pathfinder-bestiary-2-items/uMr0g5ycTJTsxsHh.htm)|Darkvision|Vision dans le noir|libre|
|[uMVIaXXt18rDrOPn.htm](pathfinder-bestiary-2-items/uMVIaXXt18rDrOPn.htm)|Ravenous Breath Weapon|Souffle dévorant|officielle|
|[UN8cAG3SmdTtuDWe.htm](pathfinder-bestiary-2-items/UN8cAG3SmdTtuDWe.htm)|Smoke Slash|Lacération vaporeuse|officielle|
|[uNH8ZKqCgsh6qYis.htm](pathfinder-bestiary-2-items/uNH8ZKqCgsh6qYis.htm)|Bay|Abois|officielle|
|[uot20BQOy52S73qO.htm](pathfinder-bestiary-2-items/uot20BQOy52S73qO.htm)|Steal Breath|Vol de souffle|officielle|
|[up0HVVMUk86V0DdT.htm](pathfinder-bestiary-2-items/up0HVVMUk86V0DdT.htm)|Burning Rush|Ruée incandescente|officielle|
|[Up9zgawdSde0SLPP.htm](pathfinder-bestiary-2-items/Up9zgawdSde0SLPP.htm)|All-Around Vision|Vision à 360°|libre|
|[uQ1bRoDBDEldN87d.htm](pathfinder-bestiary-2-items/uQ1bRoDBDEldN87d.htm)|Darkvision|Vision dans le noir|libre|
|[uQFiXxjHXOCxLHe8.htm](pathfinder-bestiary-2-items/uQFiXxjHXOCxLHe8.htm)|Vulnerability to Supernatural Darkness|Vulnérabilité aux ténèbres surnaturelles|officielle|
|[UQft7JhW6zhtWCy2.htm](pathfinder-bestiary-2-items/UQft7JhW6zhtWCy2.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[Ur31aU2yqO6lWSV2.htm](pathfinder-bestiary-2-items/Ur31aU2yqO6lWSV2.htm)|At-Will Spells|Sorts à volonté|libre|
|[uR5sStuJbBbR9XK6.htm](pathfinder-bestiary-2-items/uR5sStuJbBbR9XK6.htm)|Hatchet|Hachette|libre|
|[urEbuoDUEyOj6f0A.htm](pathfinder-bestiary-2-items/urEbuoDUEyOj6f0A.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Us7BcRckPUy5WuaZ.htm](pathfinder-bestiary-2-items/Us7BcRckPUy5WuaZ.htm)|Swallow Whole|Gober|officielle|
|[UScYAVgddobTyqOw.htm](pathfinder-bestiary-2-items/UScYAVgddobTyqOw.htm)|Fist|Poing|libre|
|[UTaJ0rJXzssLeINs.htm](pathfinder-bestiary-2-items/UTaJ0rJXzssLeINs.htm)|Motion Sense|Perception du mouvement|officielle|
|[UuDDHSI5hH9lHpZv.htm](pathfinder-bestiary-2-items/UuDDHSI5hH9lHpZv.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[uuI2Msz0ObGGd1kj.htm](pathfinder-bestiary-2-items/uuI2Msz0ObGGd1kj.htm)|Scent|Odorat|libre|
|[Uus1jecX7kww8spI.htm](pathfinder-bestiary-2-items/Uus1jecX7kww8spI.htm)|Fast Healing 2|Guérison accélérée 2|libre|
|[UUVUKTE7xQsPxDNy.htm](pathfinder-bestiary-2-items/UUVUKTE7xQsPxDNy.htm)|Bone Shard|Fragment osseux|libre|
|[UuxtWMTDYGzdEzIA.htm](pathfinder-bestiary-2-items/UuxtWMTDYGzdEzIA.htm)|Glowing Touch|Touché lumineux|libre|
|[uvi6KWWlZsgw6kYw.htm](pathfinder-bestiary-2-items/uvi6KWWlZsgw6kYw.htm)|Sting|Dard|libre|
|[UvIa41hvZRkj3zM0.htm](pathfinder-bestiary-2-items/UvIa41hvZRkj3zM0.htm)|Tremorsense|Perception des vibrations|libre|
|[UVvsMOpzGvjOBeG1.htm](pathfinder-bestiary-2-items/UVvsMOpzGvjOBeG1.htm)|Protean Anatomy 15|Anatomie protéenne 15|officielle|
|[uW30KxL4NUlDPAi5.htm](pathfinder-bestiary-2-items/uW30KxL4NUlDPAi5.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[uXmhD5XQjEMFuZSJ.htm](pathfinder-bestiary-2-items/uXmhD5XQjEMFuZSJ.htm)|Darkvision|Vision dans le noir|libre|
|[uY2J1JrUYcVbFp2j.htm](pathfinder-bestiary-2-items/uY2J1JrUYcVbFp2j.htm)|Stunning Display|Démonstration étourdissante|officielle|
|[UYeaGbpn427tN1hJ.htm](pathfinder-bestiary-2-items/UYeaGbpn427tN1hJ.htm)|Oar|Rame|libre|
|[uYHwkATnIk2mUvIu.htm](pathfinder-bestiary-2-items/uYHwkATnIk2mUvIu.htm)|Tentacle|Tentacule|libre|
|[uYqNB5URDvfKjzHe.htm](pathfinder-bestiary-2-items/uYqNB5URDvfKjzHe.htm)|Morningstar|Morgenstern|libre|
|[uyXWXmerHoFxX1jU.htm](pathfinder-bestiary-2-items/uyXWXmerHoFxX1jU.htm)|Boiling Rain|Pluie bouillante|officielle|
|[uzav7YxjDugksaRl.htm](pathfinder-bestiary-2-items/uzav7YxjDugksaRl.htm)|Low-Light Vision|Vision nocturne|libre|
|[UZc2Evb7hxJkfGtx.htm](pathfinder-bestiary-2-items/UZc2Evb7hxJkfGtx.htm)|Breath Weapon|Arme de souffle|libre|
|[uze31xboBPgWCYgP.htm](pathfinder-bestiary-2-items/uze31xboBPgWCYgP.htm)|Shortsword|Épée courte|libre|
|[uzTaJDrIHuQpGZCH.htm](pathfinder-bestiary-2-items/uzTaJDrIHuQpGZCH.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[V01q458qlTH6dA7i.htm](pathfinder-bestiary-2-items/V01q458qlTH6dA7i.htm)|Darkvision|Vision dans le noir|libre|
|[V0hJjrywdDfeOIZf.htm](pathfinder-bestiary-2-items/V0hJjrywdDfeOIZf.htm)|Scent (Imprecise) 30 feet|Odorat (imprécis) 9 m|libre|
|[v0kg2s0AeP7Pv7zn.htm](pathfinder-bestiary-2-items/v0kg2s0AeP7Pv7zn.htm)|Ravenous Jaws|Mâchoires voraces|officielle|
|[V0XpLpHdJ1rYFrd8.htm](pathfinder-bestiary-2-items/V0XpLpHdJ1rYFrd8.htm)|Grab|Empoignade/Agrippement|libre|
|[v1Nae4WDwZunhsHt.htm](pathfinder-bestiary-2-items/v1Nae4WDwZunhsHt.htm)|Regeneration 15 (deactivated by chaotic)|Régénération 15 (désactivée par chaotique)|libre|
|[V1y5FVHfe3BpQRSu.htm](pathfinder-bestiary-2-items/V1y5FVHfe3BpQRSu.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[v2by5D3cMGyVu78a.htm](pathfinder-bestiary-2-items/v2by5D3cMGyVu78a.htm)|Ritual Gate|Portail rituel|officielle|
|[V4aoUtsug8TREjXX.htm](pathfinder-bestiary-2-items/V4aoUtsug8TREjXX.htm)|Swarm Spit|Crachat de nuées|libre|
|[V4PELj4i5rSeImC7.htm](pathfinder-bestiary-2-items/V4PELj4i5rSeImC7.htm)|Darkvision|Vision dans le noir|libre|
|[V4TPpwFX1g237Ekq.htm](pathfinder-bestiary-2-items/V4TPpwFX1g237Ekq.htm)|Claw|Griffe|libre|
|[V5AYyUYH7UjuXk8T.htm](pathfinder-bestiary-2-items/V5AYyUYH7UjuXk8T.htm)|Darkvision|Vision dans le noir|libre|
|[V5mIlHkiRCxhndVN.htm](pathfinder-bestiary-2-items/V5mIlHkiRCxhndVN.htm)|Feeding Frenzy|Frénésie alimentaire|officielle|
|[V5Py4m6Gm8m9Z8I2.htm](pathfinder-bestiary-2-items/V5Py4m6Gm8m9Z8I2.htm)|Draconic Momentum|Élan draconique|libre|
|[V5YgaBwWcZPdAj1X.htm](pathfinder-bestiary-2-items/V5YgaBwWcZPdAj1X.htm)|Darkvision|Vision dans le noir|libre|
|[v65HAbd7O3gZv2VP.htm](pathfinder-bestiary-2-items/v65HAbd7O3gZv2VP.htm)|Eerie Flexibility|Souplesse surnaturelle|officielle|
|[v6dHZF6ZJriIUH9V.htm](pathfinder-bestiary-2-items/v6dHZF6ZJriIUH9V.htm)|Low-Light Vision|Vision nocturne|libre|
|[v6WETOqHL0U4nwur.htm](pathfinder-bestiary-2-items/v6WETOqHL0U4nwur.htm)|Telepathy|Télépathie|libre|
|[V7yL5WF6xEMuHMIf.htm](pathfinder-bestiary-2-items/V7yL5WF6xEMuHMIf.htm)|Grab|Empoignade/Agrippement|libre|
|[v8A5DhitrEJIAAL1.htm](pathfinder-bestiary-2-items/v8A5DhitrEJIAAL1.htm)|Consume Soul|Dévorer les âmes|officielle|
|[V8J7D9aK5N4MpaBI.htm](pathfinder-bestiary-2-items/V8J7D9aK5N4MpaBI.htm)|Lifesense|Perception de la vie|libre|
|[V90pHqyo14mQzVuc.htm](pathfinder-bestiary-2-items/V90pHqyo14mQzVuc.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations (imprécis) 9 m|libre|
|[V9Ei1dwq0RUilrjT.htm](pathfinder-bestiary-2-items/V9Ei1dwq0RUilrjT.htm)|Telepathy 100 feet (With Spectral Thralls Only)|Télépathie 30 mètres (avec les Esclaves spectraux seulement)|libre|
|[V9VYMbFdFCqt2IqA.htm](pathfinder-bestiary-2-items/V9VYMbFdFCqt2IqA.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[vApfqAgqE1h1XDQe.htm](pathfinder-bestiary-2-items/vApfqAgqE1h1XDQe.htm)|Motion Sense|Perception du mouvement|officielle|
|[vAqYhN2FTor4CPFU.htm](pathfinder-bestiary-2-items/vAqYhN2FTor4CPFU.htm)|Tendril|Vrille|libre|
|[VaxEKl3gTn1iAnvf.htm](pathfinder-bestiary-2-items/VaxEKl3gTn1iAnvf.htm)|Claw|Griffe|libre|
|[vb0fFWLf39zgAI4N.htm](pathfinder-bestiary-2-items/vb0fFWLf39zgAI4N.htm)|Spear|Lance|libre|
|[vBhPWXgs8q8icc85.htm](pathfinder-bestiary-2-items/vBhPWXgs8q8icc85.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[vbL3yHO5ysgCJOcD.htm](pathfinder-bestiary-2-items/vbL3yHO5ysgCJOcD.htm)|Darkvision|Vision dans le noir|libre|
|[vC8tzGqfa79am1Xm.htm](pathfinder-bestiary-2-items/vC8tzGqfa79am1Xm.htm)|Tusk|Défense|libre|
|[vCEb36Sv06OtmLaz.htm](pathfinder-bestiary-2-items/vCEb36Sv06OtmLaz.htm)|Rhinoceros Charge|Charge du rhinocéros|officielle|
|[vCp6u7IXfs7VJRFF.htm](pathfinder-bestiary-2-items/vCp6u7IXfs7VJRFF.htm)|Scimitar|Cimeterre|libre|
|[VD3WAa6HAnP9FYx6.htm](pathfinder-bestiary-2-items/VD3WAa6HAnP9FYx6.htm)|Ferocity|Férocité|libre|
|[vd8w1UZaLid8WPrA.htm](pathfinder-bestiary-2-items/vd8w1UZaLid8WPrA.htm)|Claw|Griffe|libre|
|[vda7kHsnuwSQWtXd.htm](pathfinder-bestiary-2-items/vda7kHsnuwSQWtXd.htm)|Grab|Empoignade/Agrippement|libre|
|[VDa8nmlZXUFf1Y7z.htm](pathfinder-bestiary-2-items/VDa8nmlZXUFf1Y7z.htm)|Jaws|Mâchoires|libre|
|[VdCpMDU3TyIElL0X.htm](pathfinder-bestiary-2-items/VdCpMDU3TyIElL0X.htm)|Darkvision|Vision dans le noir|libre|
|[VdCXQ44TAdADVOEd.htm](pathfinder-bestiary-2-items/VdCXQ44TAdADVOEd.htm)|Low-Light Vision|Vision nocturne|libre|
|[VDFewubyHTGzI0bL.htm](pathfinder-bestiary-2-items/VDFewubyHTGzI0bL.htm)|Scent (Imprecise) 60 feet|Odorat (imprécis) 18 mètres|libre|
|[VDW4W2k1N88INhBp.htm](pathfinder-bestiary-2-items/VDW4W2k1N88INhBp.htm)|Breath Weapon|Arme de souffle|libre|
|[VDwn97NFRX24iTjv.htm](pathfinder-bestiary-2-items/VDwn97NFRX24iTjv.htm)|Claw|Griffe|libre|
|[Vf0GMSeZbKtccykV.htm](pathfinder-bestiary-2-items/Vf0GMSeZbKtccykV.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[VFprYdFvs654DtJ6.htm](pathfinder-bestiary-2-items/VFprYdFvs654DtJ6.htm)|Grab|Empoignade/Agrippement|libre|
|[VGBhMtipbBzkkHbO.htm](pathfinder-bestiary-2-items/VGBhMtipbBzkkHbO.htm)|Lifesense|Perception de la vie|libre|
|[VH3uvuMCquGbT6Wx.htm](pathfinder-bestiary-2-items/VH3uvuMCquGbT6Wx.htm)|Regeneration 10 (Deactivated by Good or Silver)|Régénération 10 (neutralisée par le bon ou l'argent)|libre|
|[VHBwi6X1gEpQ8YsT.htm](pathfinder-bestiary-2-items/VHBwi6X1gEpQ8YsT.htm)|Flaming Bastard Sword|Épée bâtarde enflammée|libre|
|[VhRB4rDZ9bnYht7Z.htm](pathfinder-bestiary-2-items/VhRB4rDZ9bnYht7Z.htm)|Constrict|Constriction|officielle|
|[vHtf6k3iRMSkYTZd.htm](pathfinder-bestiary-2-items/vHtf6k3iRMSkYTZd.htm)|Darkvision|Vision dans le noir|libre|
|[vID81IzA60CV6yD2.htm](pathfinder-bestiary-2-items/vID81IzA60CV6yD2.htm)|Frightful Presence|Présence terrifiante|libre|
|[vIzThDo5KtpM1fnu.htm](pathfinder-bestiary-2-items/vIzThDo5KtpM1fnu.htm)|Fangs|Crocs|libre|
|[vKcQxEXLQx45iyeq.htm](pathfinder-bestiary-2-items/vKcQxEXLQx45iyeq.htm)|Grimstalker Sap|Résine de rôdeur sombre|officielle|
|[VKysBYdqHBzZ3aOg.htm](pathfinder-bestiary-2-items/VKysBYdqHBzZ3aOg.htm)|Breath Weapon|Arme de souffle|officielle|
|[VL2af2GD7VsPiI1u.htm](pathfinder-bestiary-2-items/VL2af2GD7VsPiI1u.htm)|Jaws|Mâchoires|libre|
|[vl5TAyZBV7YfjwyO.htm](pathfinder-bestiary-2-items/vl5TAyZBV7YfjwyO.htm)|Scent|Odorat|libre|
|[VLQ2Vi92N8KogGpN.htm](pathfinder-bestiary-2-items/VLQ2Vi92N8KogGpN.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[vLuAkberNsYeDSD0.htm](pathfinder-bestiary-2-items/vLuAkberNsYeDSD0.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[VLwTahhYo1OObMUv.htm](pathfinder-bestiary-2-items/VLwTahhYo1OObMUv.htm)|Greater Constrict|Constriction supérieure|libre|
|[vLZK0xL9O8b27GxA.htm](pathfinder-bestiary-2-items/vLZK0xL9O8b27GxA.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes|libre|
|[Vm3TYdn6BMYCQ0MR.htm](pathfinder-bestiary-2-items/Vm3TYdn6BMYCQ0MR.htm)|Claw|Griffe|libre|
|[vm77jtXXqIjODOmX.htm](pathfinder-bestiary-2-items/vm77jtXXqIjODOmX.htm)|Rebuke Soul|Réprimander une âme|officielle|
|[vMozJKWMEqEZvBOl.htm](pathfinder-bestiary-2-items/vMozJKWMEqEZvBOl.htm)|Jaws|Mâchoires|libre|
|[VN24JoB39HXPdS3r.htm](pathfinder-bestiary-2-items/VN24JoB39HXPdS3r.htm)|Ravenous Repast|Repas vorace|officielle|
|[VnUmVnOCMIOvu7ja.htm](pathfinder-bestiary-2-items/VnUmVnOCMIOvu7ja.htm)|Flaming Armaments|Armement enflammé|officielle|
|[vOhew2ogdaZmv4TY.htm](pathfinder-bestiary-2-items/vOhew2ogdaZmv4TY.htm)|Claw|Griffe|libre|
|[voMMMHUogUu7LSeF.htm](pathfinder-bestiary-2-items/voMMMHUogUu7LSeF.htm)|Mocking Touch|Toucher moqueur|officielle|
|[vorcYiRkw5YIs0Qy.htm](pathfinder-bestiary-2-items/vorcYiRkw5YIs0Qy.htm)|Trample|Piétinement|officielle|
|[vOVPKQFfOLMWV7F2.htm](pathfinder-bestiary-2-items/vOVPKQFfOLMWV7F2.htm)|Stealth|Discrétion|libre|
|[VOzKuytxsOHWyPEJ.htm](pathfinder-bestiary-2-items/VOzKuytxsOHWyPEJ.htm)|Darkvision|Vision dans le noir|libre|
|[vP6KGkCy0KpMQarQ.htm](pathfinder-bestiary-2-items/vP6KGkCy0KpMQarQ.htm)|Sense Portal|Perception des portails|officielle|
|[VQZuQhcA0A15y8kQ.htm](pathfinder-bestiary-2-items/VQZuQhcA0A15y8kQ.htm)|Trample|Piétinement|libre|
|[VRDZl6y3aAge5VyP.htm](pathfinder-bestiary-2-items/VRDZl6y3aAge5VyP.htm)|Raise Serpent|Résurrection de serpent|officielle|
|[vrswuJTbGgycV77r.htm](pathfinder-bestiary-2-items/vrswuJTbGgycV77r.htm)|Vomit|Régurgitation|libre|
|[VrxOb3MaxvXl1OR3.htm](pathfinder-bestiary-2-items/VrxOb3MaxvXl1OR3.htm)|Claw|Griffe|libre|
|[VS237lvefLjMX0lY.htm](pathfinder-bestiary-2-items/VS237lvefLjMX0lY.htm)|Tremorsense|Perception des vibrations|libre|
|[vtC1BWVjxbgjcJ2H.htm](pathfinder-bestiary-2-items/vtC1BWVjxbgjcJ2H.htm)|Darkvision|Vision dans le noir|libre|
|[VTmtaRNVlzTpmhif.htm](pathfinder-bestiary-2-items/VTmtaRNVlzTpmhif.htm)|Scimitar|Cimeterre|libre|
|[VtQnDFC3P9Efec5Z.htm](pathfinder-bestiary-2-items/VtQnDFC3P9Efec5Z.htm)|Darkvision|Vision dans le noir|libre|
|[vu2E8xDlE75Jm0Mt.htm](pathfinder-bestiary-2-items/vu2E8xDlE75Jm0Mt.htm)|Low-Light Vision|Vision nocturne|libre|
|[vu4nBmfeMF19jeah.htm](pathfinder-bestiary-2-items/vu4nBmfeMF19jeah.htm)|Radiant Blow|Coup radiant|officielle|
|[vu53NeOG28PzBXPL.htm](pathfinder-bestiary-2-items/vu53NeOG28PzBXPL.htm)|Darkvision|Vision dans le noir|libre|
|[vuAvJlvQLWub8gyW.htm](pathfinder-bestiary-2-items/vuAvJlvQLWub8gyW.htm)|Strangling Fingers|Poigne asphyxiante|officielle|
|[vvGfXGSxCsVkyVlu.htm](pathfinder-bestiary-2-items/vvGfXGSxCsVkyVlu.htm)|Scent|Odorat|libre|
|[vVt7KeVPbqT0jHZQ.htm](pathfinder-bestiary-2-items/vVt7KeVPbqT0jHZQ.htm)|Clutch|Étreinte|officielle|
|[VwEDItuYmaW2BAkI.htm](pathfinder-bestiary-2-items/VwEDItuYmaW2BAkI.htm)|Draconic Momentum|Élan draconique|libre|
|[VwEyLSgIQwBeoPyV.htm](pathfinder-bestiary-2-items/VwEyLSgIQwBeoPyV.htm)|Nature's Infusion|Imprégnation naturelle|officielle|
|[VxDq9bJ1tuymcMTt.htm](pathfinder-bestiary-2-items/VxDq9bJ1tuymcMTt.htm)|At-Will Spells|Sorts à volonté|libre|
|[vZ2zVHToKsyYJOTI.htm](pathfinder-bestiary-2-items/vZ2zVHToKsyYJOTI.htm)|Low-Light Vision|Vision nocturne|libre|
|[VzK5CD8g2ofWRIkk.htm](pathfinder-bestiary-2-items/VzK5CD8g2ofWRIkk.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[VzMnFSYbgzk4CXes.htm](pathfinder-bestiary-2-items/VzMnFSYbgzk4CXes.htm)|Trackless|Piste effacée|officielle|
|[W0kFuPmuWmicQuUc.htm](pathfinder-bestiary-2-items/W0kFuPmuWmicQuUc.htm)|Darkvision|Vision dans le noir|libre|
|[W0OH5fsPmoPURYsD.htm](pathfinder-bestiary-2-items/W0OH5fsPmoPURYsD.htm)|Claw|Griffe|libre|
|[W1bCVUm0sI8Wl7Bs.htm](pathfinder-bestiary-2-items/W1bCVUm0sI8Wl7Bs.htm)|Ghost Bane|Fléau spectral|officielle|
|[W1PoKy0P1n1NYE7q.htm](pathfinder-bestiary-2-items/W1PoKy0P1n1NYE7q.htm)|Low-Light Vision|Vision nocturne|libre|
|[w1UuelNH4N0BPFBe.htm](pathfinder-bestiary-2-items/w1UuelNH4N0BPFBe.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[w2BCKUoJdnaAE7ty.htm](pathfinder-bestiary-2-items/w2BCKUoJdnaAE7ty.htm)|Stealth|Discrétion|libre|
|[W3rITonxbyLEARlP.htm](pathfinder-bestiary-2-items/W3rITonxbyLEARlP.htm)|Breath Weapon|Arme de souffle|libre|
|[W5c599K6o0Z10LCq.htm](pathfinder-bestiary-2-items/W5c599K6o0Z10LCq.htm)|Jaws|Mâchoires|libre|
|[W6gUfzomGXvlnDjI.htm](pathfinder-bestiary-2-items/W6gUfzomGXvlnDjI.htm)|Telepathy|Télépathie|libre|
|[W7bXRQuNuOabEMJg.htm](pathfinder-bestiary-2-items/W7bXRQuNuOabEMJg.htm)|Negative Healing|Guérison négative|libre|
|[w7j7MOc866dBtx82.htm](pathfinder-bestiary-2-items/w7j7MOc866dBtx82.htm)|Scent|Odorat|libre|
|[W7Wjl7sNOZeiCQDu.htm](pathfinder-bestiary-2-items/W7Wjl7sNOZeiCQDu.htm)|Splinter|Écharde|libre|
|[W86zITZjQ9sKIM3N.htm](pathfinder-bestiary-2-items/W86zITZjQ9sKIM3N.htm)|Petrifying Gaze|Regard pétrifiant|officielle|
|[W8H9WsxFN6hIlghm.htm](pathfinder-bestiary-2-items/W8H9WsxFN6hIlghm.htm)|Darkvision|Vision dans le noir|libre|
|[W9urt04ZY9TmtKEz.htm](pathfinder-bestiary-2-items/W9urt04ZY9TmtKEz.htm)|Jaws|Mâchoires|libre|
|[W9vOqV3RGCnKCbHu.htm](pathfinder-bestiary-2-items/W9vOqV3RGCnKCbHu.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[wA3oz36wHS6e53Sa.htm](pathfinder-bestiary-2-items/wA3oz36wHS6e53Sa.htm)|Destructive Smash|Fracassement destructeur|officielle|
|[WbbgLFwKjXAiRHsC.htm](pathfinder-bestiary-2-items/WbbgLFwKjXAiRHsC.htm)|Whipping Tentacles|Flagellation de tentacules|officielle|
|[Wbgjzf3goWRWkItA.htm](pathfinder-bestiary-2-items/Wbgjzf3goWRWkItA.htm)|Claw|Griffe|libre|
|[wcSlJpWbxhokd7Vc.htm](pathfinder-bestiary-2-items/wcSlJpWbxhokd7Vc.htm)|Pseudopod|Pseudopode|libre|
|[wdSBw6qGzwcD9eZu.htm](pathfinder-bestiary-2-items/wdSBw6qGzwcD9eZu.htm)|Gory Hydration|Hydratation sanglante|officielle|
|[WdzuTa2nPDcswvjz.htm](pathfinder-bestiary-2-items/WdzuTa2nPDcswvjz.htm)|Disgorged Mucus|Régurgitation de mucus|libre|
|[weN7yMZk4CWGAypb.htm](pathfinder-bestiary-2-items/weN7yMZk4CWGAypb.htm)|Beak|Bec|libre|
|[Wez6v2oJoatkzzVp.htm](pathfinder-bestiary-2-items/Wez6v2oJoatkzzVp.htm)|Throw Rock|Projection de rocher|libre|
|[wFvcOmqLVFa5RuAY.htm](pathfinder-bestiary-2-items/wFvcOmqLVFa5RuAY.htm)|Longspear|Pique|libre|
|[WIlbTCI5PaEEizfq.htm](pathfinder-bestiary-2-items/WIlbTCI5PaEEizfq.htm)|Hidden Movement|Déplacement caché|officielle|
|[wjVFu1tX56VVEB8G.htm](pathfinder-bestiary-2-items/wjVFu1tX56VVEB8G.htm)|Darkvision|Vision dans le noir|libre|
|[WKRlqiMNA7n19s8U.htm](pathfinder-bestiary-2-items/WKRlqiMNA7n19s8U.htm)|Hoof|Sabot|libre|
|[wKTyf3x01s0VjUOw.htm](pathfinder-bestiary-2-items/wKTyf3x01s0VjUOw.htm)|Darkvision|Vision dans le noir|libre|
|[WKWzTrWyiPJNihmZ.htm](pathfinder-bestiary-2-items/WKWzTrWyiPJNihmZ.htm)|Thoughtsense|Perception des pensées|officielle|
|[WLcykyc9hp2QiTO4.htm](pathfinder-bestiary-2-items/WLcykyc9hp2QiTO4.htm)|Serpentfolk Venom|Venin d'hommes-serpents|officielle|
|[wleIqE8aRKNBcgOU.htm](pathfinder-bestiary-2-items/wleIqE8aRKNBcgOU.htm)|Darkvision|Vision dans le noir|libre|
|[WleXuHzXfn7II29b.htm](pathfinder-bestiary-2-items/WleXuHzXfn7II29b.htm)|Yamaraj Venom|Venin du yamaraje|officielle|
|[WmPbJSiwT9lndOlq.htm](pathfinder-bestiary-2-items/WmPbJSiwT9lndOlq.htm)|Grab|Empoignade/Agrippement|libre|
|[wnZi8QCOMG86Db4l.htm](pathfinder-bestiary-2-items/wnZi8QCOMG86Db4l.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[WocBezkZj15D3pxg.htm](pathfinder-bestiary-2-items/WocBezkZj15D3pxg.htm)|Tremorsense 30 feet|Perception des vibrations 9 m|libre|
|[wOmBOkeQmHwUzF7p.htm](pathfinder-bestiary-2-items/wOmBOkeQmHwUzF7p.htm)|Darkvision|Vision dans le noir|libre|
|[WopfIMXZtm7V3YUE.htm](pathfinder-bestiary-2-items/WopfIMXZtm7V3YUE.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[WOqo2UC99RfWFqA0.htm](pathfinder-bestiary-2-items/WOqo2UC99RfWFqA0.htm)|Darkvision|Vision dans le noir|libre|
|[wOW7iWJBYxXBP4Dm.htm](pathfinder-bestiary-2-items/wOW7iWJBYxXBP4Dm.htm)|Darkvision|Vision dans le noir|libre|
|[WPlqW6SIzbL8jscs.htm](pathfinder-bestiary-2-items/WPlqW6SIzbL8jscs.htm)|Focused Gaze|Regard focalisé|officielle|
|[wPLUpFiWxDX00EQJ.htm](pathfinder-bestiary-2-items/wPLUpFiWxDX00EQJ.htm)|Horn|Corne|libre|
|[WpPjPwdGV3sTxsSN.htm](pathfinder-bestiary-2-items/WpPjPwdGV3sTxsSN.htm)|Claw|Griffe|libre|
|[WpPQsA9BHFxmr54k.htm](pathfinder-bestiary-2-items/WpPQsA9BHFxmr54k.htm)|Skull|Crâne|libre|
|[WPVTRIdQSTbDZImk.htm](pathfinder-bestiary-2-items/WPVTRIdQSTbDZImk.htm)|Feral Possession|Corruption sauvage|officielle|
|[WpWYDh7Z8YVMtkCU.htm](pathfinder-bestiary-2-items/WpWYDh7Z8YVMtkCU.htm)|Negative Healing|Guérison négative|libre|
|[Wq62yRjG1oubi4Cf.htm](pathfinder-bestiary-2-items/Wq62yRjG1oubi4Cf.htm)|Grab|Empoignade/Agrippement|libre|
|[WqAfn6xBB95rkCnJ.htm](pathfinder-bestiary-2-items/WqAfn6xBB95rkCnJ.htm)|Barbed Chain|Chaîne barbelée|libre|
|[wQeIEyoNfxwL2mn1.htm](pathfinder-bestiary-2-items/wQeIEyoNfxwL2mn1.htm)|Grab|Empoignade/Agrippement|libre|
|[wqGETtOyNtSqKkGC.htm](pathfinder-bestiary-2-items/wqGETtOyNtSqKkGC.htm)|Grab|Empoignade/Agrippement|libre|
|[wrrWSyBXlSizOWeT.htm](pathfinder-bestiary-2-items/wrrWSyBXlSizOWeT.htm)|Lifesense 30 feet|Perception de la vie (9 m)|libre|
|[Wrwp9diMkabKorJn.htm](pathfinder-bestiary-2-items/Wrwp9diMkabKorJn.htm)|Beak|Bec|libre|
|[ws9YoHlMJ3zrjnM1.htm](pathfinder-bestiary-2-items/ws9YoHlMJ3zrjnM1.htm)|Tail Sweep|Balayage caudal|officielle|
|[wSYxanQKAbTVaazk.htm](pathfinder-bestiary-2-items/wSYxanQKAbTVaazk.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre la magie|libre|
|[WtMCu7aRag8Z4ewh.htm](pathfinder-bestiary-2-items/WtMCu7aRag8Z4ewh.htm)|Swamp Stride|Déplacement facilité dans les marais|officielle|
|[WtPKkX18bEjTQBFy.htm](pathfinder-bestiary-2-items/WtPKkX18bEjTQBFy.htm)|Soul Harvest|Moisson des âmes|officielle|
|[WTUxYAzq1Nr3FFwN.htm](pathfinder-bestiary-2-items/WTUxYAzq1Nr3FFwN.htm)|Jaws|Mâchoires|libre|
|[wtvDPCazUVSsqJ2O.htm](pathfinder-bestiary-2-items/wtvDPCazUVSsqJ2O.htm)|Negative Healing|Guérison négative|libre|
|[WU1jExSnDYhncwTy.htm](pathfinder-bestiary-2-items/WU1jExSnDYhncwTy.htm)|Darkvision|Vision dans le noir|libre|
|[wugLYHXdCUg05j8X.htm](pathfinder-bestiary-2-items/wugLYHXdCUg05j8X.htm)|Sportlebore Infestation|Infestation d’imitapéros|officielle|
|[WuwpqYb5cVctrCUQ.htm](pathfinder-bestiary-2-items/WuwpqYb5cVctrCUQ.htm)|Claw|Griffe|libre|
|[wveW1LFrPr2tdNpt.htm](pathfinder-bestiary-2-items/wveW1LFrPr2tdNpt.htm)|Tentacle|Tentacule|libre|
|[Wvss2Toe2q47z9GS.htm](pathfinder-bestiary-2-items/Wvss2Toe2q47z9GS.htm)|Leg|Jambe|libre|
|[wwbxcuqrl4U2FZiP.htm](pathfinder-bestiary-2-items/wwbxcuqrl4U2FZiP.htm)|Claw|Griffe|libre|
|[wWilHEuB4DGYHQdO.htm](pathfinder-bestiary-2-items/wWilHEuB4DGYHQdO.htm)|Jaws|Mâchoires|libre|
|[Wx80gWdCaIlQR8Yn.htm](pathfinder-bestiary-2-items/Wx80gWdCaIlQR8Yn.htm)|Darkvision|Vision dans le noir|libre|
|[WXEuGWzgAk5JMfEE.htm](pathfinder-bestiary-2-items/WXEuGWzgAk5JMfEE.htm)|Vulnerable to Prone|Vulnérabilité à retournement|officielle|
|[WxKg83hAxh4B0VJo.htm](pathfinder-bestiary-2-items/WxKg83hAxh4B0VJo.htm)|Flaming Ghost Touch Longspear|Pique spectrale enflammée|libre|
|[wy1mkkY0rhH0B5oL.htm](pathfinder-bestiary-2-items/wy1mkkY0rhH0B5oL.htm)|Defender of the Seas|Défenseurs des mers|officielle|
|[wY6wIEuFcKjcTbGS.htm](pathfinder-bestiary-2-items/wY6wIEuFcKjcTbGS.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[wY7gOSBgpUJ4wcUI.htm](pathfinder-bestiary-2-items/wY7gOSBgpUJ4wcUI.htm)|Shortbow|Arc court|libre|
|[wYU3mjAP9KQSofkn.htm](pathfinder-bestiary-2-items/wYU3mjAP9KQSofkn.htm)|Ghost Bane|Fléau spectral|officielle|
|[wyy8wN5QiLr4fV2L.htm](pathfinder-bestiary-2-items/wyy8wN5QiLr4fV2L.htm)|Fjord Linnorm Venom|Venin de linnorm des fjords|officielle|
|[WyzkB1NHDFF0YsAm.htm](pathfinder-bestiary-2-items/WyzkB1NHDFF0YsAm.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[WzWOnae18jvHd2C0.htm](pathfinder-bestiary-2-items/WzWOnae18jvHd2C0.htm)|Sadistic Strike|Frappe sadique|officielle|
|[x0tIw8t8mhFgH7ab.htm](pathfinder-bestiary-2-items/x0tIw8t8mhFgH7ab.htm)|At-Will Spells|Sorts à volonté|libre|
|[x183YuDiYXsrOlKc.htm](pathfinder-bestiary-2-items/x183YuDiYXsrOlKc.htm)|Bone-Chilling Screech|Hurlement à glacer le sang|officielle|
|[X1Hh7DXiCGoPXh2j.htm](pathfinder-bestiary-2-items/X1Hh7DXiCGoPXh2j.htm)|Jaws|Mâchoires|libre|
|[x1kqxURcucjG3BaP.htm](pathfinder-bestiary-2-items/x1kqxURcucjG3BaP.htm)|Change Shape|Changement de forme|officielle|
|[x2IF1MX8EodRyzeW.htm](pathfinder-bestiary-2-items/x2IF1MX8EodRyzeW.htm)|Rend|Éventration|libre|
|[x3aksJFYweZk98OP.htm](pathfinder-bestiary-2-items/x3aksJFYweZk98OP.htm)|Retributive Strike|Frappe punitive|libre|
|[x3sRV0Fub8ixWfBV.htm](pathfinder-bestiary-2-items/x3sRV0Fub8ixWfBV.htm)|Toss|Projection|officielle|
|[X3x7z0KsDNoqFpey.htm](pathfinder-bestiary-2-items/X3x7z0KsDNoqFpey.htm)|At-Will Spells|Sorts à volonté|libre|
|[x5lmNBtvBd1z0Azh.htm](pathfinder-bestiary-2-items/x5lmNBtvBd1z0Azh.htm)|Temporal Reversion|Inversion temporelle|officielle|
|[X5uycRcoYkauIGYQ.htm](pathfinder-bestiary-2-items/X5uycRcoYkauIGYQ.htm)|Darkvision|Vision dans le noir|libre|
|[x6cH9h8wAkR78uX9.htm](pathfinder-bestiary-2-items/x6cH9h8wAkR78uX9.htm)|Fist|Poing|libre|
|[X6lteRmVzKPr3P02.htm](pathfinder-bestiary-2-items/X6lteRmVzKPr3P02.htm)|Jaws|Mâchoires|libre|
|[x7o1u8JU3bkG8XjR.htm](pathfinder-bestiary-2-items/x7o1u8JU3bkG8XjR.htm)|Claw|Griffe|libre|
|[x7QZQO50H5BxEvkk.htm](pathfinder-bestiary-2-items/x7QZQO50H5BxEvkk.htm)|Starvation Vulnerability|Vulnérabilité à la famine|officielle|
|[x8IEFlb3LI5XDtkm.htm](pathfinder-bestiary-2-items/x8IEFlb3LI5XDtkm.htm)|Darkvision|Vision dans le noir|libre|
|[X8NPEY2f1Z9LKe53.htm](pathfinder-bestiary-2-items/X8NPEY2f1Z9LKe53.htm)|Scent|Odorat|libre|
|[x8pzzu8VRuRa5sdg.htm](pathfinder-bestiary-2-items/x8pzzu8VRuRa5sdg.htm)|Stench|Puanteur|officielle|
|[x97vWevUTEMqeOCJ.htm](pathfinder-bestiary-2-items/x97vWevUTEMqeOCJ.htm)|Painsight|Vision de la douleur|officielle|
|[X9B0ICfO4bmtJ9pv.htm](pathfinder-bestiary-2-items/X9B0ICfO4bmtJ9pv.htm)|Death-Stealing Gaze|Regard voleur de mort|officielle|
|[X9SDgPuuF9AwEVsK.htm](pathfinder-bestiary-2-items/X9SDgPuuF9AwEVsK.htm)|Darkvision|Vision dans le noir|libre|
|[xa0P6lS6jAgU3iPX.htm](pathfinder-bestiary-2-items/xa0P6lS6jAgU3iPX.htm)|Archon's Door|Porte de l'archon|libre|
|[xA6uZnqBzxOvjJYs.htm](pathfinder-bestiary-2-items/xA6uZnqBzxOvjJYs.htm)|Tongue|Langue|libre|
|[XaAIKVkn76K7fFFS.htm](pathfinder-bestiary-2-items/XaAIKVkn76K7fFFS.htm)|Change Shape|Changement de forme|officielle|
|[xAlvzCWOjUqsTXiK.htm](pathfinder-bestiary-2-items/xAlvzCWOjUqsTXiK.htm)|Scent|Odorat|libre|
|[xAtGnv1FFFsyE06f.htm](pathfinder-bestiary-2-items/xAtGnv1FFFsyE06f.htm)|At-Will Spells|Sorts à volonté|libre|
|[xAxdPG2ftegyTaUj.htm](pathfinder-bestiary-2-items/xAxdPG2ftegyTaUj.htm)|Water Jet|Jet d'eau|libre|
|[Xb5fg9Wf3Iiz5qeu.htm](pathfinder-bestiary-2-items/Xb5fg9Wf3Iiz5qeu.htm)|Wing|Aile|libre|
|[xBrIiF2AXw27mMmW.htm](pathfinder-bestiary-2-items/xBrIiF2AXw27mMmW.htm)|Darkvision|Vision dans le noir|libre|
|[Xc15ip4V31NZTNdq.htm](pathfinder-bestiary-2-items/Xc15ip4V31NZTNdq.htm)|Frightful Presence|Présence terrifiante|libre|
|[XCa5wvlPu21MNSQl.htm](pathfinder-bestiary-2-items/XCa5wvlPu21MNSQl.htm)|Horn|Corne|libre|
|[XcF1XgnCOdQvrvbz.htm](pathfinder-bestiary-2-items/XcF1XgnCOdQvrvbz.htm)|Talon|Ergot|libre|
|[xCffJ5CYclgmYzmC.htm](pathfinder-bestiary-2-items/xCffJ5CYclgmYzmC.htm)|Sticky Filament|Filament collant|officielle|
|[XCFOQ29kYuqzushV.htm](pathfinder-bestiary-2-items/XCFOQ29kYuqzushV.htm)|Claw|Griffe|libre|
|[XcpCwjEjd7OKbKnx.htm](pathfinder-bestiary-2-items/XcpCwjEjd7OKbKnx.htm)|Low-Light Vision|Vision nocturne|libre|
|[XErVBE2Mmnzz099t.htm](pathfinder-bestiary-2-items/XErVBE2Mmnzz099t.htm)|Darkvision|Vision dans le noir|libre|
|[xgEzGbuJSMIIkD79.htm](pathfinder-bestiary-2-items/xgEzGbuJSMIIkD79.htm)|Frightful Presence|Présence terrifiante|libre|
|[XGTj8sfNa1YvUTdZ.htm](pathfinder-bestiary-2-items/XGTj8sfNa1YvUTdZ.htm)|Captivating Lure|Leurre captivant|officielle|
|[xgZ1NJFG41NW7CFG.htm](pathfinder-bestiary-2-items/xgZ1NJFG41NW7CFG.htm)|Telepathy|Télépathie|libre|
|[XHAPOvUbe2eKOW7E.htm](pathfinder-bestiary-2-items/XHAPOvUbe2eKOW7E.htm)|Whirlwind Throw|Projection tourbillonnante|officielle|
|[XHjNDrJBXaCEsi2i.htm](pathfinder-bestiary-2-items/XHjNDrJBXaCEsi2i.htm)|Earth Glide|Glissement de terrain|officielle|
|[XHSGBobf5LZDw5R5.htm](pathfinder-bestiary-2-items/XHSGBobf5LZDw5R5.htm)|Jaws|Mâchoires|libre|
|[XhyjVYavWAJIwq7b.htm](pathfinder-bestiary-2-items/XhyjVYavWAJIwq7b.htm)|Jaws|Mâchoires|libre|
|[XIeSqagI3L9jCuKJ.htm](pathfinder-bestiary-2-items/XIeSqagI3L9jCuKJ.htm)|Crafting|Artisanat|libre|
|[xijGvHanHygeHGCM.htm](pathfinder-bestiary-2-items/xijGvHanHygeHGCM.htm)|Hurl Blade|Lancer de lame|officielle|
|[xiOTeaVvejTPBrhk.htm](pathfinder-bestiary-2-items/xiOTeaVvejTPBrhk.htm)|At-Will Spells|Sorts à volonté|libre|
|[XJ7A5MPGGNFZpxXP.htm](pathfinder-bestiary-2-items/XJ7A5MPGGNFZpxXP.htm)|Negative Healing|Guérison négative|libre|
|[xJOBW00PxizZ4M8X.htm](pathfinder-bestiary-2-items/xJOBW00PxizZ4M8X.htm)|Draconic Momentum|Élan draconique|libre|
|[XjvtH7IilCnzJpIN.htm](pathfinder-bestiary-2-items/XjvtH7IilCnzJpIN.htm)|Darkvision|Vision dans le noir|libre|
|[xKGmyTmllHIjiYO0.htm](pathfinder-bestiary-2-items/xKGmyTmllHIjiYO0.htm)|Earth Shaker|Secousse terrestre|officielle|
|[XMbOWL8A6tcWyW18.htm](pathfinder-bestiary-2-items/XMbOWL8A6tcWyW18.htm)|Radiant Wings|Ailes resplendissantes|officielle|
|[xmBUGoRIvL3UEoqs.htm](pathfinder-bestiary-2-items/xmBUGoRIvL3UEoqs.htm)|Breath Weapon|Arme de souffle|officielle|
|[XmKUjz23s0dNFOYW.htm](pathfinder-bestiary-2-items/XmKUjz23s0dNFOYW.htm)|Bog Rot|Lèpre des marais|officielle|
|[Xmq08HzwUTnliDuF.htm](pathfinder-bestiary-2-items/Xmq08HzwUTnliDuF.htm)|Fist|Poing|libre|
|[XMTEsGgReo4d8QMB.htm](pathfinder-bestiary-2-items/XMTEsGgReo4d8QMB.htm)|Katana|Katana|officielle|
|[xmYe9vSGZokROsE8.htm](pathfinder-bestiary-2-items/xmYe9vSGZokROsE8.htm)|Blinding Soul|Âme aveuglante|officielle|
|[xnaTB4eWK8MokhN6.htm](pathfinder-bestiary-2-items/xnaTB4eWK8MokhN6.htm)|Claw|Griffe|libre|
|[xNfSi1bsS4hwfcXU.htm](pathfinder-bestiary-2-items/xNfSi1bsS4hwfcXU.htm)|Carnivorous Blob Acid|Acide de la vase carnivore|officielle|
|[Xnn4xjhGBTSE9KtK.htm](pathfinder-bestiary-2-items/Xnn4xjhGBTSE9KtK.htm)|Tail|Queue|libre|
|[xOfT1xQ4weGRsYHu.htm](pathfinder-bestiary-2-items/xOfT1xQ4weGRsYHu.htm)|Radiant Blast|Déflagration radiante|officielle|
|[xomzu6MJJ9pOCVdY.htm](pathfinder-bestiary-2-items/xomzu6MJJ9pOCVdY.htm)|Darkvision|Vision dans le noir|libre|
|[xpfZ2YkQuu4I8kW0.htm](pathfinder-bestiary-2-items/xpfZ2YkQuu4I8kW0.htm)|At-Will Spells|Sorts à volonté|libre|
|[xPid5CwljZFADBGF.htm](pathfinder-bestiary-2-items/xPid5CwljZFADBGF.htm)|Trample|Piétinement|libre|
|[XplLVQ3DKSYJCuYL.htm](pathfinder-bestiary-2-items/XplLVQ3DKSYJCuYL.htm)|At-Will Spells|Sorts à volonté|libre|
|[xPTfLI21EUbrSp4Z.htm](pathfinder-bestiary-2-items/xPTfLI21EUbrSp4Z.htm)|Low-Light Vision|Vision nocturne|libre|
|[XpTtuP0So5dN4MVl.htm](pathfinder-bestiary-2-items/XpTtuP0So5dN4MVl.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[xq3V1P1OXiEGcqZs.htm](pathfinder-bestiary-2-items/xq3V1P1OXiEGcqZs.htm)|Angled Entry|Entrée anguleuse|officielle|
|[xqB9FvxJUsDUDs15.htm](pathfinder-bestiary-2-items/xqB9FvxJUsDUDs15.htm)|Thorny Vine|Liane épineuse|libre|
|[XqpyTWP6g2DVmNqf.htm](pathfinder-bestiary-2-items/XqpyTWP6g2DVmNqf.htm)|Jaws|Mâchoires|libre|
|[xRcOZkgwBwJLmsbr.htm](pathfinder-bestiary-2-items/xRcOZkgwBwJLmsbr.htm)|Constrict|Constriction|libre|
|[xrD7IZz445jQXJ8L.htm](pathfinder-bestiary-2-items/xrD7IZz445jQXJ8L.htm)|Athletics|Athlétisme|libre|
|[xRPyNMWHvevzaNHI.htm](pathfinder-bestiary-2-items/xRPyNMWHvevzaNHI.htm)|Darkvision|Vision dans le noir|libre|
|[xS1M3nE7TiguYe63.htm](pathfinder-bestiary-2-items/xS1M3nE7TiguYe63.htm)|Arm|Bras|libre|
|[XSN5GyOi1NAbLqve.htm](pathfinder-bestiary-2-items/XSN5GyOi1NAbLqve.htm)|Holy Armaments|Armement saint|officielle|
|[XSrKkFryn35jFMnz.htm](pathfinder-bestiary-2-items/XSrKkFryn35jFMnz.htm)|Darkvision|Vision dans le noir|libre|
|[XucZtbvC1QhYV0HW.htm](pathfinder-bestiary-2-items/XucZtbvC1QhYV0HW.htm)|Ferocity|Férocité|libre|
|[xumH4auLNLHoCwRg.htm](pathfinder-bestiary-2-items/xumH4auLNLHoCwRg.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[xURZboPgZokTtHAT.htm](pathfinder-bestiary-2-items/xURZboPgZokTtHAT.htm)|Grab|Empoignade/Agrippement|libre|
|[XuX1YKa36ctJmZJH.htm](pathfinder-bestiary-2-items/XuX1YKa36ctJmZJH.htm)|Explosive Rebirth|Renaissance explosive|officielle|
|[XWdqUKdwgQ9Cn7vC.htm](pathfinder-bestiary-2-items/XWdqUKdwgQ9Cn7vC.htm)|Club|Gourdin|libre|
|[xwvyEIcVrXBhC3JM.htm](pathfinder-bestiary-2-items/xwvyEIcVrXBhC3JM.htm)|Low-Light Vision|Vision nocturne|libre|
|[xxEXe18G17Aqb8EC.htm](pathfinder-bestiary-2-items/xxEXe18G17Aqb8EC.htm)|Tail|Queue|libre|
|[xXv6eUeKYXjO80rc.htm](pathfinder-bestiary-2-items/xXv6eUeKYXjO80rc.htm)|Swoop|Piqué|officielle|
|[XyAMzK7AwDLSUd1X.htm](pathfinder-bestiary-2-items/XyAMzK7AwDLSUd1X.htm)|Water Blast|Déflagration aquatique|libre|
|[Y24jpiGNojJwV4RQ.htm](pathfinder-bestiary-2-items/Y24jpiGNojJwV4RQ.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[Y2i3TiK6GDsS9UIv.htm](pathfinder-bestiary-2-items/Y2i3TiK6GDsS9UIv.htm)|Darkvision|Vision dans le noir|libre|
|[Y3O7AOvH1wkpSV2Z.htm](pathfinder-bestiary-2-items/Y3O7AOvH1wkpSV2Z.htm)|Bite|Morsure|officielle|
|[y5Va61hQuxXJ9dhb.htm](pathfinder-bestiary-2-items/y5Va61hQuxXJ9dhb.htm)|At-Will Spells|Sorts à volonté|libre|
|[Y6Rqq2ahFM07WKRm.htm](pathfinder-bestiary-2-items/Y6Rqq2ahFM07WKRm.htm)|Swarming Stance|Posture de nuée|officielle|
|[y7LC1NdSY22WtjWg.htm](pathfinder-bestiary-2-items/y7LC1NdSY22WtjWg.htm)|Claw|Griffe|libre|
|[Y7ZMEhAtrNgNLJAW.htm](pathfinder-bestiary-2-items/Y7ZMEhAtrNgNLJAW.htm)|Claw|Griffe|libre|
|[Y810sLO1OzrK8e4z.htm](pathfinder-bestiary-2-items/Y810sLO1OzrK8e4z.htm)|Claw|Griffe|libre|
|[y9b4KGpxjtDbtBFO.htm](pathfinder-bestiary-2-items/y9b4KGpxjtDbtBFO.htm)|Grab|Empoignade/Agrippement|libre|
|[Y9QK094iVscecdmW.htm](pathfinder-bestiary-2-items/Y9QK094iVscecdmW.htm)|Animate Weapon|Animation de l'arme|officielle|
|[yAPG78TDrcEUnBIP.htm](pathfinder-bestiary-2-items/yAPG78TDrcEUnBIP.htm)|Protean Anatomy 8|Anatomie protéenne 8|officielle|
|[yb7yQZeKyk9xJFnV.htm](pathfinder-bestiary-2-items/yb7yQZeKyk9xJFnV.htm)|Lifesense 30 feet|Perception de la vie 9 m|libre|
|[YBhZi6V0xuvRTBkT.htm](pathfinder-bestiary-2-items/YBhZi6V0xuvRTBkT.htm)|Maul|Morsure|libre|
|[YBRIxpMXUrxJfeV1.htm](pathfinder-bestiary-2-items/YBRIxpMXUrxJfeV1.htm)|Grab|Empoignade/Agrippement|libre|
|[YBrJf0B8ODS54eSM.htm](pathfinder-bestiary-2-items/YBrJf0B8ODS54eSM.htm)|Constant Spells|Sorts constants|libre|
|[Ybx41yIdQO59hKjK.htm](pathfinder-bestiary-2-items/Ybx41yIdQO59hKjK.htm)|+1 Status to All Saves vs Magic|Bonus de statut de +1 à tous les jets de sauvegarde contre magie|libre|
|[YcPEa5pOVPtksEtZ.htm](pathfinder-bestiary-2-items/YcPEa5pOVPtksEtZ.htm)|Frightful Presence|Présence terrifiante|libre|
|[YdcYSwBDfJFctOhL.htm](pathfinder-bestiary-2-items/YdcYSwBDfJFctOhL.htm)|Pounce|Bond|officielle|
|[YEQydLBRNZJVJBAN.htm](pathfinder-bestiary-2-items/YEQydLBRNZJVJBAN.htm)|Dagger|Dague|libre|
|[YerPyajhgUANJwRq.htm](pathfinder-bestiary-2-items/YerPyajhgUANJwRq.htm)|Darkvision|Vision dans le noir|libre|
|[yf4k8ikFfqMlrB2E.htm](pathfinder-bestiary-2-items/yf4k8ikFfqMlrB2E.htm)|Scent|Odorat|libre|
|[YfFUNMAKaJ0wspVA.htm](pathfinder-bestiary-2-items/YfFUNMAKaJ0wspVA.htm)|Darkvision|Vision dans le noir|libre|
|[yFIIVeKjDVqU6qlw.htm](pathfinder-bestiary-2-items/yFIIVeKjDVqU6qlw.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[YHLI3DUcgA33ovkw.htm](pathfinder-bestiary-2-items/YHLI3DUcgA33ovkw.htm)|Jaws|Mâchoires|libre|
|[YHmyiWDMvEgujfl1.htm](pathfinder-bestiary-2-items/YHmyiWDMvEgujfl1.htm)|Bully's Bludgeon|Matraquage au morgenstern|officielle|
|[YhTp7RIQLVLYYNUn.htm](pathfinder-bestiary-2-items/YhTp7RIQLVLYYNUn.htm)|Sticky Feet|Pattes collantes|officielle|
|[yiiBFzSFMWspwb05.htm](pathfinder-bestiary-2-items/yiiBFzSFMWspwb05.htm)|Talon|Ergot|libre|
|[yJ6tSdgHei2VkcFn.htm](pathfinder-bestiary-2-items/yJ6tSdgHei2VkcFn.htm)|Swift Claw|Griffe rapide|officielle|
|[yjgVEFL2BwjC3Ylc.htm](pathfinder-bestiary-2-items/yjgVEFL2BwjC3Ylc.htm)|At-Will Spells|Sorts à volonté|libre|
|[yKOp4n7Sk96x65EX.htm](pathfinder-bestiary-2-items/yKOp4n7Sk96x65EX.htm)|Jaws|Mâchoires|libre|
|[ykVZX3vwjXXl9WoS.htm](pathfinder-bestiary-2-items/ykVZX3vwjXXl9WoS.htm)|At-Will Spells|Sorts à volonté|libre|
|[yl8CvIWxZx0c6fcn.htm](pathfinder-bestiary-2-items/yl8CvIWxZx0c6fcn.htm)|Tail|Queue|libre|
|[YlO5up5kkSrroKf6.htm](pathfinder-bestiary-2-items/YlO5up5kkSrroKf6.htm)|Tongue Grab|Agrippement avec la langue|officielle|
|[Ym0WhQCJHDGT3v4z.htm](pathfinder-bestiary-2-items/Ym0WhQCJHDGT3v4z.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[YMLwRoRwaGIKGsjv.htm](pathfinder-bestiary-2-items/YMLwRoRwaGIKGsjv.htm)|Foot|Pied|libre|
|[ymt7FhkmnZp7ABEM.htm](pathfinder-bestiary-2-items/ymt7FhkmnZp7ABEM.htm)|Scent|Odorat|libre|
|[YnfFdQs1dxCIBHYJ.htm](pathfinder-bestiary-2-items/YnfFdQs1dxCIBHYJ.htm)|Darkvision|Vision dans le noir|libre|
|[yNsJmPBe7WVLrPiX.htm](pathfinder-bestiary-2-items/yNsJmPBe7WVLrPiX.htm)|Magic-Warping Aura|Aura de distorsion de la magie|officielle|
|[yoc8sNwSb16TykKF.htm](pathfinder-bestiary-2-items/yoc8sNwSb16TykKF.htm)|Claw|Griffe|libre|
|[yokGmVR1dAkRTCmE.htm](pathfinder-bestiary-2-items/yokGmVR1dAkRTCmE.htm)|Trample|Piétinement|officielle|
|[YOoRUvvkgwbAOJ7M.htm](pathfinder-bestiary-2-items/YOoRUvvkgwbAOJ7M.htm)|Scent|Odorat|libre|
|[yqx6BirfDar1gC35.htm](pathfinder-bestiary-2-items/yqx6BirfDar1gC35.htm)|Tremorsense|Perception des vibrations|libre|
|[YRhZxIyDuKIuiHky.htm](pathfinder-bestiary-2-items/YRhZxIyDuKIuiHky.htm)|Sneak Attack|Attaque sournoise|officielle|
|[YrMHTjkFMDgVtW5i.htm](pathfinder-bestiary-2-items/YrMHTjkFMDgVtW5i.htm)|Grab|Empoignade/Agrippement|libre|
|[yRTlpaC9IkrTE5xd.htm](pathfinder-bestiary-2-items/yRTlpaC9IkrTE5xd.htm)|Grab|Empoignade/Agrippement|libre|
|[yRuXhvVShtatFwyU.htm](pathfinder-bestiary-2-items/yRuXhvVShtatFwyU.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[YRXnBeKMp7iUfdDz.htm](pathfinder-bestiary-2-items/YRXnBeKMp7iUfdDz.htm)|Shell Defense|Défense de l'enveloppe|officielle|
|[YsR1W3Gw985Xzr24.htm](pathfinder-bestiary-2-items/YsR1W3Gw985Xzr24.htm)|Grab|Empoignade/Agrippement|libre|
|[ysswRv1XkPgse3iC.htm](pathfinder-bestiary-2-items/ysswRv1XkPgse3iC.htm)|Petrifying Gaze|Regard pétrifiant|officielle|
|[ysSzKfSqDhv0OTY7.htm](pathfinder-bestiary-2-items/ysSzKfSqDhv0OTY7.htm)|Fist|Poing|libre|
|[YSVs60IJmBC8CFXN.htm](pathfinder-bestiary-2-items/YSVs60IJmBC8CFXN.htm)|Pack Attack|Attaque en meute|officielle|
|[ySwL52AguRVZG0uO.htm](pathfinder-bestiary-2-items/ySwL52AguRVZG0uO.htm)|Claw|Griffe|libre|
|[ysZ3MQQymY3E22BQ.htm](pathfinder-bestiary-2-items/ysZ3MQQymY3E22BQ.htm)|Survival|Survie|libre|
|[yTHhCN6VWoGZKUqM.htm](pathfinder-bestiary-2-items/yTHhCN6VWoGZKUqM.htm)|Darkvision|Vision dans le noir|libre|
|[yu67ZekTRXDanQoC.htm](pathfinder-bestiary-2-items/yu67ZekTRXDanQoC.htm)|Boiled by Light|Bouillie par la lumière|officielle|
|[YUHuvUIG5Z4hJRM4.htm](pathfinder-bestiary-2-items/YUHuvUIG5Z4hJRM4.htm)|Scent|Odorat|libre|
|[YuIDIcSKbqQHoLea.htm](pathfinder-bestiary-2-items/YuIDIcSKbqQHoLea.htm)|Low-Light Vision|Vision nocturne|libre|
|[YV7bdMpIOclaEmMg.htm](pathfinder-bestiary-2-items/YV7bdMpIOclaEmMg.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[YvhSv5FSqLRo98YJ.htm](pathfinder-bestiary-2-items/YvhSv5FSqLRo98YJ.htm)|Horn|Corne|libre|
|[YvIUXJiNq5FoRcoe.htm](pathfinder-bestiary-2-items/YvIUXJiNq5FoRcoe.htm)|Rend|Éventration|libre|
|[YwNfN5E1fuqbDqG5.htm](pathfinder-bestiary-2-items/YwNfN5E1fuqbDqG5.htm)|Mindwarping Tide|Vague de distorsion mentale|officielle|
|[YXm2W59CMTMhb25j.htm](pathfinder-bestiary-2-items/YXm2W59CMTMhb25j.htm)|Jaws|Mâchoires|libre|
|[yxVTa3OmhUhlgNqp.htm](pathfinder-bestiary-2-items/yxVTa3OmhUhlgNqp.htm)|Darkvision|Vision dans le noir|libre|
|[YyBAAbT6VQ7kpDW4.htm](pathfinder-bestiary-2-items/YyBAAbT6VQ7kpDW4.htm)|Jaws|Mâchoires|libre|
|[yZgxFF5bgFKbTkUO.htm](pathfinder-bestiary-2-items/yZgxFF5bgFKbTkUO.htm)|Swallow Whole|Gober rapidement|libre|
|[Z08HAZUvnJBGvImG.htm](pathfinder-bestiary-2-items/Z08HAZUvnJBGvImG.htm)|Swarm Mind|Esprit de nuée|libre|
|[Z1OJoOqn3eSaMnv3.htm](pathfinder-bestiary-2-items/Z1OJoOqn3eSaMnv3.htm)|At-Will Spells|Sorts à volonté|libre|
|[Z1uxEMSqDnZV9SCY.htm](pathfinder-bestiary-2-items/Z1uxEMSqDnZV9SCY.htm)|Glow|Lueur|officielle|
|[z3wK3ehfyd0WrBic.htm](pathfinder-bestiary-2-items/z3wK3ehfyd0WrBic.htm)|Scent|Odorat|libre|
|[Z5SqMzsLOhD9YS8w.htm](pathfinder-bestiary-2-items/Z5SqMzsLOhD9YS8w.htm)|Jaws|Mâchoires|libre|
|[z8DPFDp5ksGcM6bT.htm](pathfinder-bestiary-2-items/z8DPFDp5ksGcM6bT.htm)|Constrict|Constriction|libre|
|[Z8OBbNUnb8R13SCr.htm](pathfinder-bestiary-2-items/Z8OBbNUnb8R13SCr.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[zA4dkHLp5wB4ZlmE.htm](pathfinder-bestiary-2-items/zA4dkHLp5wB4ZlmE.htm)|Jaws|Mâchoires|libre|
|[ZAKbxZPkQhVGL09z.htm](pathfinder-bestiary-2-items/ZAKbxZPkQhVGL09z.htm)|Telepathy|Télépathie|libre|
|[zaLf6csWyorRHycA.htm](pathfinder-bestiary-2-items/zaLf6csWyorRHycA.htm)|Petrifying Glance|Coup d’oeil pétrifiant|officielle|
|[zbGgj0YXNPQnaWxc.htm](pathfinder-bestiary-2-items/zbGgj0YXNPQnaWxc.htm)|Foot|Pied|libre|
|[zbxStPzCzfkdF5cZ.htm](pathfinder-bestiary-2-items/zbxStPzCzfkdF5cZ.htm)|Claw|Griffe|libre|
|[zCBg1ZpignZBuhdE.htm](pathfinder-bestiary-2-items/zCBg1ZpignZBuhdE.htm)|Telepathy|Télépathie|libre|
|[zCljGoGN9zoZkBsw.htm](pathfinder-bestiary-2-items/zCljGoGN9zoZkBsw.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[zcMAmOWmYshvH5lq.htm](pathfinder-bestiary-2-items/zcMAmOWmYshvH5lq.htm)|Grab|Empoignade/Agrippement|libre|
|[ZcS1mFdodf2L8MND.htm](pathfinder-bestiary-2-items/ZcS1mFdodf2L8MND.htm)|Painsight|Vision de la douleur|officielle|
|[zcW44MHFdHDFSbQ5.htm](pathfinder-bestiary-2-items/zcW44MHFdHDFSbQ5.htm)|Jaws|Mâchoire|libre|
|[zczeWPJAvYdnHaPh.htm](pathfinder-bestiary-2-items/zczeWPJAvYdnHaPh.htm)|Darkvision|Vision dans le noir|libre|
|[ZDDAw2k12iKwWmyi.htm](pathfinder-bestiary-2-items/ZDDAw2k12iKwWmyi.htm)|Darkvision|Vision dans le noir|libre|
|[zDdJpAQ7FmnjLelY.htm](pathfinder-bestiary-2-items/zDdJpAQ7FmnjLelY.htm)|Jaws|Mâchoires|libre|
|[ZdHLSjspChUCaebu.htm](pathfinder-bestiary-2-items/ZdHLSjspChUCaebu.htm)|Rhoka Sword|Épée rhoka|officielle|
|[ZDqmS7tceuWsp4M1.htm](pathfinder-bestiary-2-items/ZDqmS7tceuWsp4M1.htm)|Claw|Griffe|libre|
|[zdW63lmShrSpZWkm.htm](pathfinder-bestiary-2-items/zdW63lmShrSpZWkm.htm)|Otherworldly Vision|Vision d'un autre monde|officielle|
|[ZDynxQcPOYUqgupw.htm](pathfinder-bestiary-2-items/ZDynxQcPOYUqgupw.htm)|Darkvision|Vision dans le noir|libre|
|[zE1qMdAfyFxOdrrK.htm](pathfinder-bestiary-2-items/zE1qMdAfyFxOdrrK.htm)|Swarming Bites|Nuée de morsures|officielle|
|[zEg1F3xL2SCNwVj7.htm](pathfinder-bestiary-2-items/zEg1F3xL2SCNwVj7.htm)|Weak Acid|Acide faible|officielle|
|[zeyB4l2CYjcqiDrH.htm](pathfinder-bestiary-2-items/zeyB4l2CYjcqiDrH.htm)|Claw|Griffe|libre|
|[ZfwjAt3nb27vE9CP.htm](pathfinder-bestiary-2-items/ZfwjAt3nb27vE9CP.htm)|Tentacle|Tentacule|libre|
|[ZFxl4Vr0gR7v2Vms.htm](pathfinder-bestiary-2-items/ZFxl4Vr0gR7v2Vms.htm)|Darkvision|Vision dans le noir|libre|
|[zgKk3c4d3SCNJutG.htm](pathfinder-bestiary-2-items/zgKk3c4d3SCNJutG.htm)|Spiked Chain|Chaîne cloutée|libre|
|[Zh1eW0zEMgFfvRik.htm](pathfinder-bestiary-2-items/Zh1eW0zEMgFfvRik.htm)|Grab|Empoignade/Agrippement|libre|
|[zhw2nm585TNwCDjL.htm](pathfinder-bestiary-2-items/zhw2nm585TNwCDjL.htm)|Telepathy|Télépathie|libre|
|[zHWFkxssQtSSZH42.htm](pathfinder-bestiary-2-items/zHWFkxssQtSSZH42.htm)|Claw|Griffe|libre|
|[Zi1IXK4SmJsyEThu.htm](pathfinder-bestiary-2-items/Zi1IXK4SmJsyEThu.htm)|Athletics|Athlétisme|libre|
|[ZiB2QG6oo1w3v4H2.htm](pathfinder-bestiary-2-items/ZiB2QG6oo1w3v4H2.htm)|Aura of Protection|Aura de protection|officielle|
|[zIgTjSPfr6rcIZod.htm](pathfinder-bestiary-2-items/zIgTjSPfr6rcIZod.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[zJ48k7LkzzGzYZXF.htm](pathfinder-bestiary-2-items/zJ48k7LkzzGzYZXF.htm)|Paralysis|Paralysie|officielle|
|[ZJGndV534YfOPIyl.htm](pathfinder-bestiary-2-items/ZJGndV534YfOPIyl.htm)|Low-Light Vision|Vision nocturne|libre|
|[zjVbARImBBAx6EKv.htm](pathfinder-bestiary-2-items/zjVbARImBBAx6EKv.htm)|Vile Touch|Vil contact|libre|
|[zJyMYGQVmegoEJJR.htm](pathfinder-bestiary-2-items/zJyMYGQVmegoEJJR.htm)|Horn Sweep|Balayage de cornes|libre|
|[ZKbT5kDUoOwBrcCA.htm](pathfinder-bestiary-2-items/ZKbT5kDUoOwBrcCA.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[zLE5uQwBNSB8wfkR.htm](pathfinder-bestiary-2-items/zLE5uQwBNSB8wfkR.htm)|Petrifying Glance|Coup d'oeil pétrifiant|officielle|
|[zMgcdf4D6sTVSdON.htm](pathfinder-bestiary-2-items/zMgcdf4D6sTVSdON.htm)|Scent|Odorat|libre|
|[ZMxrbMShYOtO3gEG.htm](pathfinder-bestiary-2-items/ZMxrbMShYOtO3gEG.htm)|Infiltration Tools|Outils d’infiltration.|officielle|
|[znnYZfV4tw03wze8.htm](pathfinder-bestiary-2-items/znnYZfV4tw03wze8.htm)|Wing|Aile|libre|
|[Znvln0bJmtXz7d7o.htm](pathfinder-bestiary-2-items/Znvln0bJmtXz7d7o.htm)|Low-Light Vision|Vision nocturne|libre|
|[ZnwcqtkzrHYsNa9W.htm](pathfinder-bestiary-2-items/ZnwcqtkzrHYsNa9W.htm)|Jaws|Mâchoires|libre|
|[zoC1ICbBEHFarVuk.htm](pathfinder-bestiary-2-items/zoC1ICbBEHFarVuk.htm)|Telepathy|Télépathie|libre|
|[ZoL2vMSlQqYIUJAX.htm](pathfinder-bestiary-2-items/ZoL2vMSlQqYIUJAX.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[ZPQhdXF3s5ZiNwMc.htm](pathfinder-bestiary-2-items/ZPQhdXF3s5ZiNwMc.htm)|Cinder|Braise|libre|
|[zsRr07XFkA5e8PQ3.htm](pathfinder-bestiary-2-items/zsRr07XFkA5e8PQ3.htm)|Stealth|Discrétion|libre|
|[Zt3p0OLPOAG0F5xm.htm](pathfinder-bestiary-2-items/Zt3p0OLPOAG0F5xm.htm)|Grab|Empoignade/Agrippement|libre|
|[zTHlNe6zIRCEQ1Gh.htm](pathfinder-bestiary-2-items/zTHlNe6zIRCEQ1Gh.htm)|Club|Gourdin|libre|
|[ZUHGleM4wU1ST5zV.htm](pathfinder-bestiary-2-items/ZUHGleM4wU1ST5zV.htm)|Trident|Trident|libre|
|[zuum0VinuCKcAh3D.htm](pathfinder-bestiary-2-items/zuum0VinuCKcAh3D.htm)|Light Hammer|Marteau léger|officielle|
|[zuY7RRh4sY8cLE7O.htm](pathfinder-bestiary-2-items/zuY7RRh4sY8cLE7O.htm)|Scent|Odorat|libre|
|[zUZAQv67fQN1itGn.htm](pathfinder-bestiary-2-items/zUZAQv67fQN1itGn.htm)|Negative Healing|Guérison négative|libre|
|[zVAwB8xvMUpj9BcX.htm](pathfinder-bestiary-2-items/zVAwB8xvMUpj9BcX.htm)|Jaws|Mâchoires|libre|
|[Zvjunjzq6eA7veaV.htm](pathfinder-bestiary-2-items/Zvjunjzq6eA7veaV.htm)|Jaws|Mâchoires|libre|
|[zvpqR9uOambi8VIJ.htm](pathfinder-bestiary-2-items/zvpqR9uOambi8VIJ.htm)|Jaws|Mâchoires|libre|
|[ZvqnxVWvm9QdJZVi.htm](pathfinder-bestiary-2-items/ZvqnxVWvm9QdJZVi.htm)|Blood Scent|Perception du sang|libre|
|[ZVXPGw1qzr4AJYst.htm](pathfinder-bestiary-2-items/ZVXPGw1qzr4AJYst.htm)|Low-Light Vision|Vision nocturne|libre|
|[ZwbFx8gGrqv10yY5.htm](pathfinder-bestiary-2-items/ZwbFx8gGrqv10yY5.htm)|Foot|Pattes|libre|
|[zWcMhsfvgxLXuw4D.htm](pathfinder-bestiary-2-items/zWcMhsfvgxLXuw4D.htm)|Constrict|Constriction|libre|
|[ZWe69IC3j34b3dqx.htm](pathfinder-bestiary-2-items/ZWe69IC3j34b3dqx.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[ZwOinMaTzmRM28rJ.htm](pathfinder-bestiary-2-items/ZwOinMaTzmRM28rJ.htm)|Shadow Scream|Hurlement de l’ombre|officielle|
|[zwROdG6waBZIlf4v.htm](pathfinder-bestiary-2-items/zwROdG6waBZIlf4v.htm)|Darkvision|Vision dans le noir|libre|
|[ZZAHwe62kuHct1sT.htm](pathfinder-bestiary-2-items/ZZAHwe62kuHct1sT.htm)|Low-Light Vision|Vision nocturne|libre|
|[zZd6BogevnvvWpQx.htm](pathfinder-bestiary-2-items/zZd6BogevnvvWpQx.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[zZs4I9L8EkJWK9UP.htm](pathfinder-bestiary-2-items/zZs4I9L8EkJWK9UP.htm)|Swift Tracker|Pistage accéléré|officielle|
|[zzv3YiVwAfvq91jl.htm](pathfinder-bestiary-2-items/zzv3YiVwAfvq91jl.htm)|Fist|Poing|libre|
|[ZzXV8so5F6pu1j3r.htm](pathfinder-bestiary-2-items/ZzXV8so5F6pu1j3r.htm)|Low-Light Vision|Vision nocturne|libre|
