# État de la traduction (ac-eidolons)

 * **libre**: 11


Dernière mise à jour: 2022-07-05 07:26 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1YJoaMhZPvLGvj1J.htm](ac-eidolons/1YJoaMhZPvLGvj1J.htm)|Devotion Phantom Eidolon|Eidolon fantôme dévoué|libre|
|[5IvhL9PsniwiVhEZ.htm](ac-eidolons/5IvhL9PsniwiVhEZ.htm)|Beast Eidolon|Eidolon bestial|libre|
|[7a1ei105k24XkJG0.htm](ac-eidolons/7a1ei105k24XkJG0.htm)|Plant Eidolon|Eidolon végétal|libre|
|[dnQUp0Vhg3XdZFtP.htm](ac-eidolons/dnQUp0Vhg3XdZFtP.htm)|Construct Eidolon|Eidolon créature artificielle|libre|
|[fvGz8fdP3iW8IBda.htm](ac-eidolons/fvGz8fdP3iW8IBda.htm)|Psychopomp Eidolon|Eidolon psychopompe|libre|
|[mKWiBb4xmnwVhmh7.htm](ac-eidolons/mKWiBb4xmnwVhmh7.htm)|Angel Eidolon|Eidolon ange|libre|
|[o3wSjAaxYUmdbDvB.htm](ac-eidolons/o3wSjAaxYUmdbDvB.htm)|Anger Phantom Eidolon|Eidolon fantome colérique|libre|
|[P5GSucPtackfg6Qt.htm](ac-eidolons/P5GSucPtackfg6Qt.htm)|Demon Eidolon|Eidolon démon|libre|
|[UjTUvPAdzj3Ybg8s.htm](ac-eidolons/UjTUvPAdzj3Ybg8s.htm)|Undead Eidolon|Eidolon mort-vivant|libre|
|[XDxYBQ6TRCJzoKbf.htm](ac-eidolons/XDxYBQ6TRCJzoKbf.htm)|Dragon Eidolon|Eidolon dragon|libre|
|[zjIVbS6nGX6BS2UD.htm](ac-eidolons/zjIVbS6nGX6BS2UD.htm)|Fey Eidolon|Eidolon fée|libre|
